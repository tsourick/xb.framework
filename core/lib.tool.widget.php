<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* WIDGET TOOLS library
*
* @version 1.0
* @package core
* @subpackage tools
*/


function _calendar_dayinfo($month, $day, $year, $day_selected)
{
	$ts = mktime(0, 0, 0, $month, $day, $year);
	$info = getdate($ts);
	
	unset($info['seconds']);
	unset($info['minutes']);
	unset($info['hours']);
	unset($info[0]);
	
	if ($day == $day_selected) $info['selected'] = true;
	else $info['selected'] = false;
	
	return $info;
}


/**
* Returns an associative array containing days of the month divided into weeks.
* For each day there is a date and a day number. Also, the array contains selected year and month.
* Current (or selected) day is marked with a flag.
*
* If $month, $day and $year are 0, current local date is used.
*
* @param int $month       month of date
* @param int $day         day of date
* @param int $year        year of date
* @param int $week_start  starting day of week (0 - Sunday, 1 - Monday)
* @return array $calendar associative array with calendar data
*/

function calendar($month = 0, $day = 0, $year = 0, $week_start = 0)
{
	// Initializing
	
	// Resulting array
	$calendar = array();
	
	// Checking input
	
	$month = intval($month);
	$day = intval($day);
	$year = intval($year);
	$week_start = intval($week_start);

	// Our (internal) days of week indexes sequence
	if ($week_start == 1) $week_days = array(1, 2, 3, 4, 5, 6, 0);
	else $week_days = array(0, 1, 2, 3, 4, 5, 6);
	
	// If no date specified, use current local date for timestamp
	if (empty($month) && empty($day) && empty($year))
	{
		// $ts = mktime();
		$ts = time();
		
		list($month, $day, $year) = explode(' ', date('n j Y', $ts));
	}
	else $ts = mktime(0, 0, 0, $month, $day, $year);

	// Processing
	
	// Number of days in the given month
	$days_count = date('t', $ts);

	// Starting day of week
	$weekday_start = date('w', mktime(0, 0, 0, $month, 1, $year));

	// Position of a day to be written to array within a month
	$days_pos = 1;
	
	// Flag of state (0 - before days; 1 - within days; 2 - after days)
	$f_state = 0;
	
	// Building up a week until days are over (state of 2)
	
	while ($f_state != 2)
	{
		// Initialize array for a week data
		$calendar_week = array();
		
		// Building up a week of days
		for ($i = 0; $i < 7; $i++)
		{
			// No day at position by default
			$item = NULL;
			
			// We are before actual days
			if ($f_state == 0)
			{
				// If we are at the weekday of the first day in the month;
				// Days position checking is excessive but stands for a wider functionality
				if ($week_days[$i] == $weekday_start && $days_pos <= $days_count)
				{
					// Add a day info
					$item = _calendar_dayinfo($month, $days_pos, $year, $day);
					
					// Advance position of a day
					$days_pos++;
					
					// Proceed to the next state
					$f_state = 1;
				}
			}
			
			// We are within actual days
			elseif ($f_state == 1)
			{
				
				// Checking if we are at the last day
				if ($days_pos == $days_count)
				{
					// Add a day info
					$item = _calendar_dayinfo($month, $days_pos, $year, $day);

					// Proceed to the next state
					$f_state = 2;
				}
				else
				{
					// Add a day info
					$item = _calendar_dayinfo($month, $days_pos, $year, $day);
					
					// Advance position of a day
					$days_pos++;
				}
			}
			
			$calendar_week[] = $item;
		}
		
		$calendar[] = $calendar_week;
	}
	
	return $calendar;
}


/**
* Returns an associative array containing pager data.
* Resulting array has three elements: 'items', 'first'.
* 'last'. 'first'/'last' is set to true when current page is first/last and set to false otherwise.
* Every element of 'items' has 'type' element which can be set to "page" or "gap". When the item is of page type, it
* also has 'page' and 'active' elements. 'page' is the page number, 'active' is set to true when the page is current
* and set to false otherwise.
*
* @param int $size size of pager, i.e. number of pages
* @param int $page current page number; page numbering begins with 1
* @param int $start_cnt number of pages to show at the beginning of pager control
* @param int $frame_cnt number of pages to show around current page
* @param int $end_cnt number of pages to show at the end of pager control
* @return
*/

function pager($size, $page, $start_cnt = 3, $frame_cnt = 1, $end_cnt = 3)
{
	// strong restrictions
	assert($size > 0);
	assert($page > 0 && $page <= $size);

	
	$intervals = array();

	// start interval

	// correction		
	if ($start_cnt < 0) $start_cnt = 0;
	if ($start_cnt > $size) $start_cnt = $size;
	
	// setting up interval
	if ($start_cnt > 0) $intervals[] = array(1, $start_cnt);


	// end interval
	
	// correction		
	if ($end_cnt < 0) $end_cnt = 0;
	if ($end_cnt > $size) $end_cnt = $size;
	
	// setting up interval
	if ($end_cnt > 0) $intervals[] = array($size - $end_cnt + 1, $size);


	// middle interval

	// correction		
	if ($frame_cnt < 0) $frame_cnt = 0;
	if ($page - $frame_cnt < 1) $l_boundary = 1; else $l_boundary = $page - $frame_cnt;
	if ($page + $frame_cnt > $size) $r_boundary = $size; else $r_boundary = $page + $frame_cnt;
	
	// setting up interval
	$intervals[] = array($l_boundary, $r_boundary);

	
	// skip duplicate values
	$intervals = array_unique_multi($intervals);
	

	$pager = array();
	$last_item = NULL;
	
	// merge intervals
	
	for ($i = 1; $i <= $size; $i++)
	{
		$item = array();
		$in = false;
		$append = true;
		
		// find out whether a page is within any of intervals
		$cnt = count($intervals);
		for ($j = 0; $j < $cnt; $j++)
		{
			if ($i >= $intervals[$j][0] && $i <= $intervals[$j][1])
			{
				$in = true;
				break;
			}
		}
		
		if ($in)
		{
			$item['type'] = 'page';
			$item['page'] = $i;
			if ($page == $i) $item['active'] = true; else $item['active'] = false;
		}
		else
		{
			if ($last_item['type'] == 'gap') $append = false;
			else $item['type'] = 'gap';
		}
		
		if ($append)
		{
			$pager['items'][] = $item;
			$last_item = $item;
		}
	}
	
	
	// Set up outermost flags
	
	if ($page == 1) $pager['first'] = true; else $pager['first'] = false;
	if ($page == $size) $pager['last'] = true; else $pager['last'] = false;
	
	
	return $pager;
}
?>
