<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* CLI library
*
* @version 1.0
* @package core
* @subpackage cli
*/

/*
class CancelException extends Exception
{
}
*/

/*
function cEcho($true)
{
	if (strpos(strtolower(PHP_OS), 'win') !== false)
	{
		if ($true)
		{
			cRun('echo on');
		}
		else
		{
			cRun('echo off');
		}
	}
}
*/

/**
* Sets word wrap width when writting to console
*
* Wrap width is used as second parameter for wordwrap().
*
* @param int $wrap wrap width
*/

function cSetWrap($wrap)
{
	global $RUNTIME;
	
	$RUNTIME['LIB']['CLI']['wrap'] = $wrap;
}

/**
* Pauses execution until user presses a key
*
* @param string $message optional text to write before pause
*/

function cPause($message = '')
{
	if (strpos(strtolower(PHP_OS), 'win') !== false)
	{
		if (! empty($message))
		{
			echo $message;
			
			fgetc(STDIN);
		}
		else cRun('pause');
	}
	else
	{
		if (empty($message)) $message = 'Press ENTER to continue...' . "\n"; 
		
		
		echo $message;
		
		fgetc(STDIN);
	}
}

/**
* Reads a line from user input
*
* @param string $message optional text to write before read
*
* @return string trimmed line
*/

function cReadLn($message = '')
{
	echo $message;
	
	return trim(fgets(STDIN));
}

/**
* Write to output
*
* @param string $message optional text to write, word wrap is applied if set
*/

function cWrite($message = '')
{
	global $RUNTIME;
	
	if (isset($RUNTIME['LIB']['CLI']['wrap']))
	{
		echo wordwrap($message, $RUNTIME['LIB']['CLI']['wrap']);
	}
	else
	{
		echo $message;
	}
}

/**
* Write a line to output
*
* @param string $message optional text to write, word wrap is applied if set
*/

function cWriteLn($message = '')
{
	cWrite($message . "\n");
}

/**
* Write splash line to output
*
* @param string $message optional text to write
* @param int|NULL $wrap custom word wrap, if NULL - RUNTIME wrap is applied, if not set - wrap is set to 80
* @param string $pad char to pad string with, * by default
*/

function cWriteSplash($message = '', $wrap = NULL, $pad = '*')
{
	global $RUNTIME;
	
	if (is_null($wrap))
	{
		if (isset($RUNTIME['LIB']['CLI']['wrap']))
		{
			$wrap = $RUNTIME['LIB']['CLI']['wrap'];
		}
		else
		{
			$wrap = 80;
		}
	}

	$message_length = strlen($message);
	if ($message_length > 0)
	{
		$message = ' ' . $message . ' ';
		$message_length += 2;
	}
	$ss = str_repeat('*', floor(($wrap - $message_length) / 2));
	$sss = (strlen($ss) * 2 + $message_length) < $wrap ? '*' : '';
	cWriteLn($ss . $message . $ss . $sss);
}

/**
* Clear console screen
*
* Does nothing yet.
*
* @access private
*/

function cCLS()
{
	// system('cls');
}

/**
* Write line to error output
*
* @param string $message optional text to write
*/

function cErrWriteLn($message = '')
{
	fwrite(STDERR, $message . "\n");
}

/**
* Request a confirmation from user
*
* Writes text if given, reads from user and returns true if user hit 'Y' or Enter, false otherwise
*
* @param string $message optional text to write
*
* @return bool
*/

function cConfirm($message = '')
{
	$r = cReadLn($message . ' [Y or Enter/N]: ');
	$r = strtolower(trim($r));
	
	return ($r == 'y' || empty($r)) ? true : false;
}


function _xbf_cli_parse_args()
{
	global $argv, $argc;
	global $RUNTIME;
	

	$args = array();

	
	if (isset($RUNTIME['cache']['cli']['-'])) $args = $RUNTIME['cache']['cli']['-']; // Check cache for given variable
	else
	{
		if (isset($argv) && isset($argc) && $argc > 1)
		{
			array_shift($argv); // skip arg 0 (the script name itself)

			foreach ($argv as $arg)
			{
				$arg = ltrim($arg, '-');
				$arg = explode('=', $arg);
				
				if (isset($arg[0]))
				{
					$name = $arg[0];
					$value = isset($arg[1]) ? $arg[1] : NULL;
					
					$args[$name] = $value;
				}
			}
		}
		
		$RUNTIME['cache']['cli']['-'] = $args;
	}

	return $args;
}

function xbf_cli_get_value($name, $type = 's', $default = NULL)
{
	global $argv, $argc;
	global $RUNTIME;


	$value = NULL;
	

	if (isset($RUNTIME['cache']['cli'][$name])) $value = $RUNTIME['cache']['cli'][$name];			// Check cache for given variable
	else
	{
		$args = _xbf_cli_parse_args();

		if (isset($args[$name]))
		{
			$arg = $args[$name];
			
			switch (substr($type, 0, 1))
			{
				case 'l': // list of values separated by comma
					$value = explode(',', $arg);
				break;
				
				case 'i':
					$value = intval(trim(substr($arg, 0, 1024)));
				break;
				
				case 'b':
					$value = trim(substr($arg, 0, 1024));
					settype($value, 'boolean');
				break;
				
				case 's': // default
				default:
					$value = $args[$name];
				break;
			}
		}
	
		$RUNTIME['cache']['cli'][$name] = $value;
	}		
		
	if ($value === NULL && $default !== NULL) $value = $default;

	
	return $value;
}

/**
* Execute shell command
*
* @param string $cmd command to execute
*
* @throws Exception
*/

function cRun($cmd)
{
	passthru($cmd, $status);
	if ($status != 0) throw new Exception("Command '{$cmd}' failed. (status: {$status})");
}

?>
