<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* Logging library
*
* @version 1.0
* @package core
* @subpackage tools
*/


if (! defined('LOGGER_ENABLED')) define('LOGGER_ENABLED', true);
if (! defined('LOGGER_FILENAME')) define('LOGGER_FILENAME', 'log.log');


/**
* Creates or truncates log file
*/

function logger_init()
{
	if (LOGGER_ENABLED)
	{
		if ($f = fopen(LOGGER_FILENAME, 'r+'))
		{
			$locked = false;
			
			$i = 5;
			while ($i > 0 && ! $locked = flock($f, LOCK_EX))
			{
				sleep(1);
				$i--;
			}
			
			if ($locked)
			{
				ftruncate($f, 0);
				
				flock($f, LOCK_UN);
			}
			
			fclose($f);
		}
	}
}

/**
* Dumps value with timestamp into file
*
* @param mixed $s optional value to output
* @param string $block_name optional block name is written at the beginning, if set 
*/

function logger_log($s = '', $block_name = '')
{
	if (LOGGER_ENABLED)
	{
		if (! empty($block_name)) $block_name = '[' . $block_name . '] :: '; 

		$now = date('Y-m-d H:i:s');
		
		if ($f = fopen(LOGGER_FILENAME, 'ab'))
		{
			$locked = false;
			
			$i = 5;
			while ($i > 0 && ! $locked = flock($f, LOCK_EX))
			{
				sleep(1);
				$i--;
			}
			
			if ($locked)
			{
				$ss = '> (' . $now . ') ' . $block_name;
				
				if (is_array($s)) $ss .= PHP_EOL . print_r($s, true); else $ss .= $s;
				
				fputs($f, $ss . PHP_EOL);
				
				flock($f, LOCK_UN);
			}
			
			fclose($f);
		}
	}
}


if (LOGGER_ENABLED)
{
	ini_set('log_errors', 1);
	ini_set('error_log', LOGGER_FILENAME);
}	
?>