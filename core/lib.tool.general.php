<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* GENERAL TOOLS library
*
* @version 1.1
* @category mycat
* @package core
* @subpackage tools
*/

/**
* Joins two or more strings with a glue
* Returns a new string containing joined strings
*
* @api
*
* @param string $sep a glue to join strings with
* [@param string, ...] strings to join
* @return string s new string containing joined strings
*/

function str_join($sep)
{
	$s = '';
	$args = func_get_args();
	
	for ($i = 1; $i < count($args); $i++)
	{
		$s .= func_get_arg($i);
		if ($i < count($args) - 1) $s .= $sep;
	}
	
	return $s;
}

/**
* Explode with limit and optionally walk resulting array with callback
*
* @param string $s string
* @param string $d divider to explode by
* @param string $l explode limit, maximum number of elements
* @param callback used in array_walk() after expode
*/

function str_explode($s, $d, $l, $h)
{
	if (! is_null($l))
	{
		$a = explode($d, $s, $l);
	}
	else
	{
		$a = explode($d, $s);
	}

	array_walk($a, $h);

	return $a;
}

/**
* Explodes string at specified position optionally excluding character at that position.
*/
function str_explode_at($s, $pos, $skip_pos = false)
{
	$first = substr($s, 0, $pos);
	$last = substr($s, $pos);
	
	if ($skip_pos) $last = substr($last, 1); // shift off first char
	
	return array($first, $last);
}

/**
* MySQL's SUBSTRING_INDEX() analog
*/
function substr_index($str, $sep, $count)
{
	if ($count > 0)
	{
		$str = implode($sep, array_slice(explode($sep, $str), 0, $count));
	}
	else
	{
		$str = implode($sep, array_slice(explode($sep, $str), $count));
	}
    
	return $str;
}


/**
* Explodes parameter string to array by given separators or ';' and '=' by default
*
* @param string $params strin of key/value pairs
* @param string |array $separators 0-level (pair) and 1-level (key/value) separator chars
* @param bool $preserve_empty_pairs if set to true empty pairs (due to leading and/or trailing separator) are skipped; default is false
*
* @return array array of parameters
*/

function explode_params($params, $separators = ';=', $preserve_empty_pairs = false)
{
	$result = array();
	
	if ($params != '')
	{
		if (! is_array($separators)) $separators = str_split($separators);
		
		$pairs = explode($separators[0], $params);
		foreach ($pairs as $pair)
		{
			if ($pair == '' && ! $preserve_empty_pairs) continue;
			
			list($n, $v) = explode($separators[1], $pair);
			$result[$n] = $v;
		}
	}	
	
	return $result;
}

/**
* Implodes parameter array to string with given separators or ';' and '=' by default
*
* @param array $params array of key/value pairs
* @param string |array $separators 0-level (pair) and 1-level (key/value) separator chars
*
* @return string string of parameters
*/

function implode_params(Array $params, $separators = ';=')
{
	$result = '';
	
	
	if (! is_array($separators)) $separators = str_split($separators);

	
	// PHP 5.3.0 style
	// $f = function($k, $v) use ($separators) {return $k . $separators[1] . $v;}; 
	//
	// $result = implode($separators[0], array_map($f, array_keys($params), $params));

	
	// older PHP style; for Zend Guard
	
	$f = create_function('$k, $v, $separator' , 'return $k . $separator . $v;'); 
	
	$result = implode($separators[0], array_map($f, array_keys($params), $params, array_fill(0, count($params), $separators[1])));
	
	
	return $result;
}


/**
* Clears CGI value from request
*
* {@internal
*   Clears from autoglobals returned by {@link _get_request()} and from runtime cache as well
* }}
*
* @param string $name name of variable
*/

function clear_cgi_value($name)
{
	global $RUNTIME;

	$REQUEST =& _get_request();
	
	if (isset($REQUEST[$name]))
	{
		unset($REQUEST[$name]);
		if (isset($RUNTIME['cache']['cgi'][$name])) unset($RUNTIME['cache']['cgi'][$name]);
	}
}


/**
* Reinject CGI value into library request data structures
*/

function set_cgi_value($name, $value)
{
	clear_cgi_value($name); // flush from request and cache if any
	
	$REQUEST =& _get_request();
	
	$REQUEST[$name] = $value;
}

/**
* @param array $values key/value pairs to inject with {@link set_cgi_values()}
*
* @see set_cgi_value
*/
function set_cgi_values($values)
{
	foreach ($values as $k => $v)
	{
		set_cgi_value($k, $v);
	}
}


/**
* Safely returns value set by CGI method GET, POST or sent within cookie.
* The value is checked for appropriate ranges for specified type and
* gets converted explicitly to that type.
*
* Values are cached in a global array $RUNTIME in element $RUNTIME['cache']['cgi'].
* Only actual CGI values are cached, default values passed to the function are not.
*
* Available types:
*   s - string (default)
*   i - integer
*   f - float
*   b - boolean
*
*   a - array
*       (For array type it is possible to specify additional flag to indicate the type of array elements.
*       These flags are s,i and b. This works up to second dimension of an array, i.e. dimensions more than 2 are not
*       processed. If additional flag is omitted, no additional processing occures and array is returned as is.)
*   as - see 'a'; default for 'a'
*   ai - see 'a'
*   af - see 'a'
*   ab - see 'a'

*   sa - string containing array
*       (A string of comma-separated values. Further processing if the same as for array. Addtional flags can be specified - s/i/f/b) 
*   sas - see 'sa'; default for 'sa'
*   sai - see 'sa' 
*   saf - see 'sa'
*   sab - see 'sa'
*
* @param string $name     name of variable
* @param string $type     expected type of variable .. 
* @param string $default  default value to return when original one is not set or empty
*
* @return mixed           value from CGI
*/

function _get_cgi_value__process_array(& $val, $type)
{
	switch ($type)
	{
		// array of integers
		case 'i':
			reset($val);
			while (list($k, $v) = each($val))
			{
				$val[$k] = intval(trim(substr($v, 0, 1024)));
			}
		break;

		// array of floats
		case 'f':
			reset($val);
			while (list($k, $v) = each($val))
			{
				$val[$k] = floatval(trim(substr($v, 0, 1024)));
			}
		break;

		// array of booleans
		case 'b':
			reset($val);
			while (list($k, $v) = each($val))
			{
				$v = trim(substr($v, 0, 1024));
				settype($v, 'boolean');
				
				$val[$k] = $v;
			}
		break;

		// array of strings (default)
		case 's':
			reset($val);
			while (list($k, $v) = each($val))
			{
				settype($v, 'string');
				
				$val[$k] = $v;
			}
		break;
	}
}

function get_cgi_value($name, $type = 's', $default = NULL)
{
	global $RUNTIME;
	
	$val = NULL;
	
	if (isset($RUNTIME['cache']['cgi'][$name])) $val = $RUNTIME['cache']['cgi'][$name];			// Check cache for given variable
	else
	{
		$REQUEST =& _get_request();
		
		if (isset($REQUEST[$name]))
		{
			// Strip magic quotes (removed rom PHP as of version 5.4)
			
			if (version_compare(PHP_VERSION, '5.4') < 0 && get_magic_quotes_gpc())
			{
				if (is_array($REQUEST[$name]))
				{
					$stack = array();
					
					reset($REQUEST[$name]);
					
					$stack[] = & $REQUEST[$name];

					while (! empty($stack))
					{
						$array = & $stack[count($stack) - 1];
						unset($stack[count($stack) - 1]);

						while (list($k, $v) = each($array))
						{
							if (is_array($v))
							{
								$stack[count($stack)] = & $array;
								
								reset($array[$k]);
								$array = & $array[$k];
								
								continue;									
							}
							else
							{
								$array[$k] = stripslashes($v);
							}
						}
					}
				}
				else $REQUEST[$name] = stripslashes($REQUEST[$name]);
			}

			switch (substr($type, 0, 1))
			{
				// string
				case 's':
					switch (substr($type, 1, 1))
					{
						// string containing array (comma-separated)
						case 'a':
							$val = explode(',', $REQUEST[$name]);
							
							_get_cgi_value__process_array($val, substr($type, 2, 1));
						break;
						
						// regular string
						default:
							$val = $REQUEST[$name];
							settype($val, 'string');
						break;
					}
				break;
					
				// integer
				case 'i':
					$val = intval(trim(substr($REQUEST[$name], 0, 1024)));
				break;
					
				// float
				case 'f':
					$val = floatval(trim(substr($REQUEST[$name], 0, 1024)));
					
					/*
					$lc = localeconv();
					$search = array
					(
						$lc['decimal_point'],
						$lc['mon_decimal_point'],
						$lc['thousands_sep'],
						$lc['mon_thousands_sep'],
						$lc['currency_symbol'],
						$lc['int_curr_symbol']
					);
					$replace = array('.', '.', '', '', '', '');
				
					$val = str_replace($search, $replace, $val);*/
				break;
					
				// boolean
				case 'b':
					//if (! empty($REQUEST[$name]))
					//{
						$val = trim(substr($REQUEST[$name], 0, 1024));
						settype($val, 'boolean');
					//}
				break;
	
				// array 
				case 'a':
					if (! empty($REQUEST[$name]))
					{
						$val = $REQUEST[$name];
						if (! is_array($val)) settype($val, 'array');
						
						/*
						switch (substr($type, 1, 1))
						{
							case 's':
								reset($val);
								while (list($k, $v) = each($val))
								{
									settype($v, 'string');
									
									$val[$k] = $v;
								}
							break;

							case 'i':
								reset($val);
								while (list($k, $v) = each($val))
								{
									$val[$k] = intval(trim(substr($v, 0, 1024)));
								}
							break;

							case 'f':
								reset($val);
								while (list($k, $v) = each($val))
								{
									$val[$k] = floatval(trim(substr($v, 0, 1024)));
								}
							break;

							case 'b':
								reset($val);
								while (list($k, $v) = each($val))
								{
									$v = trim(substr($v, 0, 1024));
									settype($v, 'boolean');
									
									$val[$k] = $v;
								}
							break;
						}
						*/
						
						_get_cgi_value__process_array($val, substr($type, 1, 1));
					}
				break;
				
				default:
					throw new Exception("Wrong type {$type}");
					assert(false);
			}
		}
		
		$RUNTIME['cache']['cgi'][$name] = $val;
	}
	
	if ($val === NULL && $default !== NULL) $val = $default;
	
	return $val;
}


/**
* Safely returns uploaded file contents or path.
* The file is checked for size, error and with is_uploaded_file()
*
* Value is cached in a global array $RUNTIME in element $RUNTIME['cache']['cgi'].
* Only actual value is cached, default value passed to the function is not.
*
* @param string $name name of file variable
* @param string $return 'content' - to return file contents, 'path' (default) - to return path to temporary file
* @param string $default default value to return if no legal file was uploaded
*
* @return string file contents or path to uploaded file
*/

function get_cgi_file($name, $return = '', $default = NULL)
{
	global $RUNTIME;

	$val = NULL;
	
	
	if (isset($RUNTIME['cache']['cgi'][$name])) $val = $RUNTIME['cache']['cgi'][$name];			// Check cache for given variable
	else
	{
		if (isset($_FILES[$name]))
		{
			$file =& $_FILES[$name];
			
			if ($file['size'] > 0 && $file['error'] == UPLOAD_ERR_OK && is_uploaded_file($file['tmp_name']))
			{
				switch ($return)
				{
					case 'content':
						$val = file_get_contents($file['tmp_name']);
					break;

					case 'path':
					default:
						$val = $file['tmp_name'];
				}
			}
		}
		
		$RUNTIME['cache']['cgi'][$name] = $val;
	}

	if ($val === NULL && $default !== NULL) $val = $default;

	return $val;
}

function get_cgi_files($name, $return = '', $default = NULL)
{
	global $RUNTIME;

	$val = NULL;
	
	
	if (isset($RUNTIME['cache']['cgi'][$name])) $val = $RUNTIME['cache']['cgi'][$name];			// Check cache for given variable
	else
	{
		if (isset($_FILES[$name]))
		{
			$_files =& $_FILES[$name];
			
			$files = array();
			foreach (array_keys($_files['name']) as $index)
			{
				$file = array
				(
					// $_files['name'][$index] unused
					// $_files['type'][$index] unused
					'tmp_name' => $_files['tmp_name'][$index],
					'error' => $_files['error'][$index],
					'size' => $_files['size'][$index]
				);
				
				if ($file['size'] > 0 && $file['error'] == UPLOAD_ERR_OK && is_uploaded_file($file['tmp_name']))
				{
					switch ($return)
					{
						case 'content':
							$files[$index] = file_get_contents($file['tmp_name']);
						break;
	
						case 'path':
						default:
							$files[$index] = $file['tmp_name'];
					}
				}
			}
			
			
			if (! empty($files))
			{
				$val = $files;
			}
		}
		
		$RUNTIME['cache']['cgi'][$name] = $val;
	}

	if ($val === NULL && $default !== NULL) $val = $default;

	return $val;
}


/**
* Appends custom include path to the default one already specified in php configurations.
* 
* @param string $include_path1 a path to append to include_path
*/

function append_include_path($include_path1)
{
	$include_dirs = array();
	
	if (!empty($include_path1))
		$include_dirs = array_merge($include_dirs, explode(PATH_SEPARATOR, $include_path1));
		
	$include_path2 = ini_get('include_path');
	if (!empty($include_path2))
		$include_dirs = array_merge($include_dirs, explode(PATH_SEPARATOR, $include_path2));
	
	if (!empty($include_dirs))
	{
		$include_dirs = array_unique($include_dirs);
		ksort($include_dirs);
	
		ini_set('include_path', implode(PATH_SEPARATOR, $include_dirs));
	}
}


/**
* Returns request array merged from $_GET and $_POST autoglobals in order specified by 'variables_order' php option
*
* @return array runtime request array by reference
*/

function &_get_request()
{
	global $RUNTIME;
	
	if (! isset($RUNTIME['request']))
	{
		$REQUEST = array();
		
		$order = strtolower(ini_get('variables_order'));
		
		$G_position = strpos($order, 'g');
		$P_position = strpos($order, 'p');
	
		if ($G_position === false && $P_position !== false)
		{
			$REQUEST = $_POST;
		}
		elseif ($G_position !== false && $P_position === false)
		{
			$REQUEST = $_GET;
		}
		elseif ($G_position !== false && $P_position !== false)
		{
			if ($G_position < $P_position) $REQUEST = array_merge($_GET, $_POST); else $REQUEST = array_merge($_POST, $_GET);
		}

		$RUNTIME['request'] =& $REQUEST;
	}


	return $RUNTIME['request'];
}

/**
* Returns all CGI values except names given in parameters
*
* All parameters are returned unchanged. This is primarily used to silently pass insignificant parameters (such as, page number,
* filter criteria etc.) between requests
*
* @param array $names array of names to skip
*
* @return array array of values
*/

function get_cgi_values_except($names)
{
	$values = array();
	
	$REQUEST =& _get_request();

	reset($REQUEST);
	while (list($name, $value) = each($REQUEST))
	{
		if (! in_array($name, $names)) $values[$name] = $value;
	}

	return $values;
}


/**
* Explodes URL query string into array
*
* @param string URL query string
*
* @return array array of query parameters
*/

function cgi_query_to_array($query, $separator = '&amp;')
{
	$array = array();
	
	$pairs = explode($separator, $query);
	
	reset($pairs);
	while (list(, $pair) = each($pairs))
	{
		list($name, $value) = explode('=', $pair);
		$array[$name] = $value;
	}
	
	return $array;
}

/**
* Recursively adds array values or simply adds plain parameters
*
* Names and values are encoded with urlencode().
* 
* @access private
*
* @param string $name parameter name
* @param mixed $value parameter value
* @param mixed & $pairs resulting array
*/

function _cgi_array_to_query__value($name, $value, &$pairs)
{
	if (is_array($value))
	{
		reset($value);
		while (list($sub_name, $sub_value) = each($value))
		{
			// $name .= '[' . $sub_name . ']';
			_cgi_array_to_query__value($name . '[' . $sub_name . ']', $sub_value, $pairs);
			
			
			// $pairs[] = urlencode($name . '[' . $sub_name . ']') . '=' . urlencode($sub_value);
		}
	}
	else $pairs[] = (($name !== 0) ? (urlencode($name) . '=') : '') . urlencode($value); // skip '0 index' name, allows to add <build> number to query
}

/**
* Implodes array of URL query parameters into string
*
* Names and values are encoded with urlencode().
* Recursively adds array values if any. 
*
* NOTE: accepts build number for query, pass the number at zero number index
*
* @param array URL query parameters
*
* @return string URL query string
*/

function cgi_array_to_query($array, $separator = '&amp;')
{
	$query = '';
	
	$pairs = array();
	
	reset($array);
	while (list($name, $value) = each($array))
	{
		// wdebug($name);
		_cgi_array_to_query__value($name, $value, $pairs);
		// wdebug($pairs);
		/*
		if (is_array($value))
		{
			reset($value);
			while (list($sub_name, $sub_value) = each($value))
			{
				$pairs[] = urlencode($name . '[' . $sub_name . ']') . '=' . urlencode($sub_value);
			}
		}
		else $pairs[] = urlencode($name) . '=' . urlencode($value);
		*/
	}

	$query = implode($separator, $pairs);
	
	return $query;
}

/**
* Implodes array of URL query parameters into HTML fragment of hidden input fields
*
* Names and values are encoded with htmlspecialchars().
*
* @param array URL query parameters
*
* @return string HTML fragment of hidden input fields
*/

function cgi_array_to_form($array)
{
	$form = '';
	
	$inputs = array();
	
	reset($array);
	while (list($name, $value) = each($array))
	{
		$inputs[] = '<input type="hidden" name="' . htmlspecialchars($name) . '" value="' . htmlspecialchars($value) . '">';
	}
	
	$form = implode("\n", $inputs);
	
	return $form;
}

/**
* Checks whether a piece of path exists under specified directory
*
* Dividers are slashes (not backslashes). Trailing slash for $root is required (ex. root='./files/').
* $root should be the real verified absolute path otherwise the functions has no sense.
*
* @param string $root valid existing absolute directory path with trailing slash 
* @param string $path subpath
*
* @return bool
*/

function is_path_inside($root, $path)
{
	$result = true;
	
	if (DIRECTORY_SEPARATOR != '/') list($root, $path) = str_replace('/', DIRECTORY_SEPARATOR, array($root, $path));

	$path = realpath($root . $path);
	if (! $path || strpos($path, realpath($root)) !== 0) $result = false;
	
	return $result;
}

/**
* Checks for valid relative path
*
* Valid path can not:
*   - begin with a slash
*   - end with a slash
*   - first filename cannot consist of dots and spaces
*   - last filename cannot consist of dots and spaces
*   - any other filename cannot consist of dots and spaces
*   - single filename cannot consist of dots and spaces
*   - two adjacent slashes are forbidden
*
* Valid path may consist of:	
*	  - A-Z
*   - a-z
*   - 0-9
*   - '_' (underline)
*   - '.' (dot)
*   - '/' (slash)
*   - '-' (hyphen)
*   - ' ' (space)
*
* @param string $vpath
*
* @return bool
*/

function is_path_safe($vpath)
{
	$result = true;
	
	// vpath cannot...
	$bad_re_s = array
	(
		'|^/|',								// ...begin with a slash
		'|/$|',								// ...end with a slash
		'/^(?:\\.| )+\\//',		// first filename cannot consist of dots and spaces
		'/\\/(?:\\.| )+$/',		// last filename cannot consist of dots and spaces
		'/\\/(?:\\.| )+\\//',	// any other filename cannot consist of dots and spaces
		'/^(?:\\.| )+$/',			// single filename cannot consist of dots and spaces
		'|//|'								// two adjacent slashes are forbidden
	);
	
	$good_re = '|[A-Za-z_0-9./\\- ]|';

	
	if (!preg_match($good_re, $vpath)) $result = false;
	else
	{
		foreach ($bad_re_s as $re)
		{
			if (preg_match($re, $vpath))
			{
				$result = false;
				break;
			}		
		}
	}

	return $result;
}


/**
* Aligns path and adds trailing slash if it is absent
*
* At first, align_path() is called for the given path. Then the resulting path is tested for trailing slash
* and modified if one is absent.
*
* @param string $path string containing any part of a path
* @param string $sep string used as directory separator (default is DIRECTORY_SEPARATOR constant)
*
* @return string path with slashes aligned and trailing slash guaranteed
*/

function dirpath($path, $sep = DIRECTORY_SEPARATOR)
{
	$path = align_path($path, $sep);
	
	if (strcmp($path, $sep) != 0)
	{
		if (substr($path, -1, 1) != $sep) $path .= $sep;
	}
	
	return $path;
}

/**
* Get aligned path to dir of given file path (cut off trailing file name)
*/
function filedirpath($filepath, $sep = DIRECTORY_SEPARATOR)
{
	return dirpath(dirname($filepath), $sep);
}


/**
* Replaces both slashes with given directory separator
*
* By default, the local filesystem directory separator is used (DIRECTORY_SEPARATOR constant).
*
* @param string $path string containing any part of a path
* @param string $sep string used as directory separator (default is DIRECTORY_SEPARATOR constant)
*
* @return string path with slashes aligned
*/

function align_path($path, $sep = DIRECTORY_SEPARATOR)
{
	$path = str_replace(array('/', '\\'), $sep, $path);
	
	return $path;
}

/**
* Safely exits script
*
* Calls session_write_close() before exit if session is started.
*/

function safe_exit()
{
	if (isset($_SESSION)) session_write_close();

	exit();
}

/**
* Generates random alphanumeric string of given length
*
* @param int $length count of chars
*
* @return string random string
*/

function generate_password($length)
{
	return str_rand($length);
}

/**
* Generates random string of given length from given set of chars
*
* @param int $length count of chars
* @param string $chars set of chars to use, if empty - alphanumeric set is used
*
* @return string random string
*/

function str_rand($length = 8, $chars = '')
{
	$pass = '' ;
	
	if (empty($chars)) $chars = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	$chars_len = strlen($chars);

	for ($i = 0; $i < $length; $i++)
	{
		$num = mt_rand() % $chars_len;
		$tmp = substr($chars, $num, 1);  
		$pass = $pass . $tmp; 
	}
	
	return $pass;
}

/**
* Checks date (including time) for validity.
*
* @param int $year year
* @param int $month month
* @param int $day day
* @param int $hour hour
* @param int $minute minute
* @param int $second second
*
* @return bool returns TRUE if date/time is valid, FALSE otherwise
*/

function is_datetime_valid($year, $month, $day, $hour, $minute, $second)
{
	$return = true;

	if (! checkdate($month, $day, $year)) $return = false;
	else
	{
		if (! ($hour >= 0 && $hour <= 23 && $minute >= 0 && $minute <= 59 && $second >= 0 && $second <= 59)) $return = false;
	}
	
	return $return;
}


/**
* Converts an arbitrary length one-dimensional array to matrix which can be used to display data in a table
*
* @param array $data 1-dimensional array
* @param int $col_num column count
*
* @return array 2-dimensional array of rows and columns
*/

function list_to_grid($data, $col_num = 2)
{
	$my_colNum = $col_num;
	$my_dataGrid = array();
	$my_dataGridRow = array();
	
	$my_i = 0;
	foreach ($data as $my_key => $my_field)
	{
		$my_dataGridRow[$my_key] = $my_field;
		$my_i++;
		
		if ($my_i == $my_colNum)
		{
			$my_dataGrid[] = $my_dataGridRow;
			
			$my_dataGridRow = array();
			$my_i = 0;
		}					
	}
	
	while ($my_i % $my_colNum != 0)
	{
		$my_dataGridRow[] = NULL;
		$my_i++;

		if ($my_i == $my_colNum)
		{
			$my_dataGrid[] = $my_dataGridRow;
		}
	}
	
	return $my_dataGrid;
}


/**
* Stops executing of script with error message.
* Function is intended to be called from abstract methods to stop executing of script in case
* when the method is absent in extending class.
*/

function abstract_class($class_name, $method_name)
{
	trigger_error('Method ' . $class_name . ' :: ' . $method_name . '() should be defined in extending class!', E_USER_ERROR);
}

function abstract_method($class_name, $method_name)
{
	trigger_error('Method ' . $class_name . ' :: ' . $method_name . '() should be defined in extending class!', E_USER_ERROR);
}

/**
* Stub for unimplemented function
*
* Triggers E_USER_ERROR with message 'Not implemented!'.
*/

function not_implemented()
{
	trigger_error('Not implemented!', E_USER_ERROR);
}

/**
* Stub for unimplemented function
*
* Triggers E_USER_ERROR with message title 'Deprecated' and default or custom message.
*
* @param string $custom_message custom message for trigger_error()
*/

function deprecated($custom_message = 'Some call or action is deprecated and intended to be invoked in another suitable way.')
{
	trigger_error('<b>Deprecated</b>: ' . $custom_message, E_USER_ERROR);
}

/**
*
*
* @access private
*
* @param int $n hex char index
*
* @return string hex char
*/

function _bcdechex_tochar($n)
{
	$alpha = "0123456789ABCDEF";
	
	return $alpha[$n];
}

/**
* Converts arbitrary precision number from dec to hex
*
* @param string $d arbitrary precision decimal number
*
* @return string arbitrary precision hexadecimal number
*/

function bcdechex($d)
{
	$r = bcmod($d, 16);
	
	if (bccomp(bcsub($d, $r),0) == 0) $result = _bcdechex_tochar($r);
	else $result = bcdechex(bcdiv(bcsub($d, $r), 16)) . _bcdechex_tochar($r);
	
	return $result;
}

/**
*
*
* @param
* @param
*
* @return
*/

function _bchexdec_tonum($c)
{
	$numbers = array
	(
		'0' => '0',
		'1' => '1',
		'2' => '2',
		'3' => '3',
		'4' => '4',
		'5' => '5',
		'6' => '6',
		'7' => '7',
		'8' => '8',
		'9' => '9',
		'A' => '10',
		'B' => '11',
		'C' => '12',
		'D' => '13',
		'E' => '14',
		'F' => '15'
	);
	
	return $numbers[$c];
}

/**
*
*
* @param
* @param
*
* @return
*/

function bchexdec($d)
{
	$result = '';
	
	$d = strrev($d);
	
	for ($i = 0; $i < strlen($d); $i++)
	{
		$char_num = _bchexdec_tonum($d[$i]);
		$pos_mul = bcpow('16', $i);
		$result = bcadd($result, bcmul($char_num, $pos_mul));
	}

	return $result;
}

/**
*
*
* @param
* @param
*
* @return
*/

function bcaddmulti()
{
	$result = '';
	
	$nums = func_get_args();
	
	if (! empty($nums))
	{
		do
		{
			$num = array_shift($nums);
			$result = bcadd($result, $num);
		} while (count($nums) > 0);
	}
	
	return $result;
}

/**
*
*
* @param
* @param
*
* @return
*/

function define_section()
{
	$GLOBALS['_define_section_value_counter'] = 0;
}

/**
*
*
* @param
* @param
*
* @return
*/

function _define($constant_name)
{
	define($constant_name, $GLOBALS['_define_section_value_counter']++);
}

/**
* Checks whether all given values are empty
*
* @param mixed $value,... values to check
*
* @return bool
*/

function empty_all()
{
	$empty_all = true;
	
	$vars = func_get_args();
	
	foreach ($vars as $var)
	{
		if (! empty($var))
		{
			$empty_all = false;
			break;
		}
	}
	
	return $empty_all;
}

/**
* empty_all() clone
*/
function all_empty()
{
	$empty_all = true;
	
	$vars = func_get_args();
	
	foreach ($vars as $var)
	{
		if (! empty($var))
		{
			$empty_all = false;
			break;
		}
	}
	
	return $empty_all;
}

function all_not_empty()
{
	$all_not_empty = true;
	
	$vars = func_get_args();
	
	foreach ($vars as $var)
	{
		if (empty($var))
		{
			$all_not_empty = false;
			break;
		}
	}
	
	return $all_not_empty;
}

/**
* Calls htmlspecialchars with ENT_QUOTES quote style
*
* @param string $s string to convert
*
* @return string converted string
*/

function html_special_chars($s)
{
	return htmlspecialchars($s, ENT_QUOTES);
}

/**
* Escapes javascript string
*
* @param string $s string to escape
*
* @return string escaped string
*/

function escape_javascript_string($s)
{
	return str_replace
	(
		array('\\',		'\'',		'"',		"\r\n",	"\n",		"\r"),
		array('\\\\',	'\\\'',	'\\"',	"\\n", "\\n", "\\n"),
		$s
	);
}


/**
* Mathces string for email
*
* @param string $s string to match
*
* @return bool
*/

function is_email($s)
{
	$ru = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя';

	$re = '/^[a-z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-z0-9'.$ru.'](?:[a-z0-9'.$ru.'-]*[a-z0-9'.$ru.'])?\\.)+(?:[a-z]{2}|рф|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum)$/i';
	
	return preg_match($re, $s);
}


/**
* Sets LC_ALL with appropriate value depending of OS
*
* Supports only NIX and WIN, russian and english languages, United States and Russia, utf-8 and cp1251.
*
* @param en|ru $language locale language
* @param US|RU $country locale country
* @param UTF-8|CP1251 $charset locale charset
*/

function setlocaleall($language, $country, $charset)
{
	$languages = array
	(
		'en' => array('WIN' => 'English', 'NIX' => 'en'),
		'ru' => array('WIN' => 'Russian', 'NIX' => 'ru')
	);

	$countries = array
	(
		'US' => array('WIN' => 'United States', 'NIX' => 'US'),
		'RU' => array('WIN' => 'Russia', 'NIX' => 'RU'),
	);

	$charsets = array
	(
		'UTF-8' => array('WIN' => '65001', 'NIX' => 'UTF-8'),
		'CP1251' => array('WIN' => '1251', 'NIX' => 'CP1251')
	);

	$php_os = (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') ? 'WIN' : 'NIX'; // assume NIX if not WIN
	/*
		Possible values are:
    * CYGWIN_NT-5.1
    * Darwin
    * FreeBSD
    * HP-UX
    * IRIX64
    * Linux
    * NetBSD
    * OpenBSD
    * SunOS
    * Unix
    * WIN32
    * WINNT
    * Windows
	
	*/
	$locale = $languages[$language][$php_os] . '_' . $countries[$country][$php_os] . '.' . $charsets[$charset][$php_os];

	setlocale(LC_ALL, $locale);
}


/**
* Generates a set of random numbers from min to max
*
* @param int $min min value of set
* @param int $max max value of set
* @param int $count coount of numbers
*
* @return array array of random numbers
*/

function rand_set($min, $max, $count = 1)
{
	$set = array();
	
	
	if ($count > 0)
	{
		$numbers = range($min, $max);
		
		$keys = array_rand($numbers, $count);

		
		foreach ($keys as $key)
		{
			$set[] = $numbers[$key];
		}
	}
	
	
	return $set;
}

function get_ua_info()
{
    $u_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'Unknown';
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
   
    // Next get the name of the useragent yes seperately and for good reason
    
    $ub = 'undefined';
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$u_agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }
    elseif(preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Opera';
        $ub = "Opera";
    }
    elseif(preg_match('/Netscape/i',$u_agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }
   
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
   
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
        	// tsourick: does not always work. fix it
          if (isset($matches['version'][0])) $version = $matches['version'][0];
        }
        else {
        	// tsourick: does not always work. fix it
          if (isset($matches['version'][1])) $version = $matches['version'][1];
        }
    }
    else {
    	// tsourick: does not always work. fix it
      if (isset($matches['version'][0])) $version = $matches['version'][0];
    }
   
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
   
    return array(
    		'method' => 'php_custom',
        'user_agent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
}


function parseTemplate($template, $values)
{
	$result = '';

	
	$vars = array();
	foreach ($values as $n => $v)
	{
		if (is_array($v))
		{
			foreach ($v as $n2 => $v2)
			{
				if (is_array($v2))
				{
					foreach ($v2 as $n3 => $v3)
					{
						$vars['{$' . $n . '.' . $n2 . '.' . $n3 . '}'] = $v3;
					}
				}
				else
				{
					$vars['{$' . $n . '.' . $n2 . '}'] = $v2;
				}
			}
		}
		else
		{
			$vars['{$' . $n . '}'] = $v;
		}
	}

	$result = str_replace(array_keys($vars), array_values($vars), $template);
	
	
	return $result;
}


/**
* Get value from a set of defined values
*
* Returns value or NULL if the value not found in the set.
*/

function get_set_value($value, $array)
{
	return (in_array($value, $array) ? $value : NULL);
}


function get_upload_max_file_size()
{
	$max_upload = (int)(ini_get('upload_max_filesize'));
	$max_post = (int)(ini_get('post_max_size'));
	$memory_limit = (int)(ini_get('memory_limit'));
	$upload_mb = min($max_upload, $max_post, $memory_limit);
	
	return $upload_mb;
}

function is_cli()
{
	/*
	global $argc;
	
	return $argc > 0;
	*/
	return (php_sapi_name() === 'cli');
}


/**
* Make a string of text a single line, replace "newline"s with $sep, trim before transformation if $trim is true.
*/
function text_to_line($text, $sep, $trim)
{
	if ($trim) $text = trim($text);
	$text = str_replace(array("\r\n", "\r", "\n"), $sep, $text);
	return $text;
}


function xbf_mb_ucfirst($string, $encoding = NULL)
{
	if (! is_null($encoding))
	{
		$_encoding = mb_internal_encoding(); // save
		mb_internal_encoding($encoding); // modify
	}
	
	$char = mb_substr($string, 0, 1);
	$rest = mb_substr($string, 1);
	$string = mb_strtoupper($char) . $rest;
	
	if (! is_null($encoding))
	{
		mb_internal_encoding($_encoding); // restore
	}
	
	return $string;
}



/**
* Checks whether the value is of a given class (or derived from, or of interface)
*
* Assumed to be used for passed parameter inside a function. 
*
* NOTE: Check against interface is only available when $any_subclass is true
* 
* @param object $param                 the passed object
* @param string $class_name            name of the class to check against
* @param bool   $any_subclass          when false the param must be of a given class, otherwise it can be also derived from (default)
* @param string $exception_class_name  exception class to raise when the check fails (default is Exception)
*/

function xbf_ensure_param_class($param, $class_name, $any_subclass = true, $exception_class_name = NULL)
{
	$_passed   = get_class($param);
	$_expected = $class_name;
	$_e = is_null($exception_class_name) ? 'Exception' : $exception_class_name;
	
	$match = $any_subclass ? $param instanceof $class_name : $_passed == $_expected;
	
	if (! $match) throw new $_e("Object of wrong class '{$_passed}' passed, expected '{$_expected}'");
}


/**
* Duplicate variable's value many times
*/
function xbf_replicate(& $v, $c = 5)
{
	if (is_array($v)) // as array
	{
		$a = array();
		for ($i = 0; $i < $c; $i++)
		{
			$a = array_merge($a, $v);
		}
		$v = $a;
	}
	else // as string
	{
		$v = str_repeat($v, $c);
	}
}


function xbf_wordtruncate($text, $max_char_count = 75, $encoding = 'UTF-8')
{
	$text = $text . ' ';
	$text = mb_substr($text, 0, $max_char_count, $encoding);
	$text = mb_substr($text, 0, mb_strrpos($text, ' ', 0, $encoding), $encoding);
	
	return $text;
}

function xbf_is_number_natural($v)
{
	return ctype_digit(strval($v));
}

?>