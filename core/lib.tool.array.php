<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* ARRAY TOOLS library
*
* @version 1.0
* @package core
* @subpackage tools
*/


/**
* Enqoutes every value of array with specified quotes
*
* Assume backticks as quotes if not
* specified explicitly. Operates on original array, not on copy.
*
* @param array & $array target array
* @param string $quote quote char
*/

function array_enquote(&$array, $quote = '`')
{
	// for ($i = 0; $i < count($array); $i++) $array[$i] = $quote . $array[$i] . $quote;
	foreach (array_keys($array) as $k)
	{
		$array[$k] = $quote . $array[$k] . $quote;
	}
}


/**
* Rotates a numeric array values forward or backward skipping key/value bindings.
*
* @param array $a source array
* @param int $shift shift value (may be negative) 
*
* @return array resulting array
*/

function array_rotate_values($a, $shift)
{
	// Skipping array indexes
	$a = array_values($a);
	
	if ($shift != 0)
	{
		// Normalizing shift to avoid useless rotations; It can become 0 after that
		if (abs($shift) >= count($a))
		{
			$sign = $shift > 0 ? 1 : -1;
			$shift = (abs($shift) - count($a) * (floor(abs($shift) / count($a)))) * $sign;
		}

		// Processing forward rotation (to the right)
		if ($shift > 0)
		{
			// Storing leftmost part of array
			$r1 = array_slice($a, 0, count($a) - $shift);
	
			// Storing rightmost part of array
			$r2 = array_slice($a, count($a) - $shift, $shift);
		}
		elseif ($shift < 0)
		{
			// Storing leftmost part of array
			$r1 = array_slice($a, 0, abs($shift));
	
			// Storing rightmost part of array
			$r2 = array_slice($a, abs($shift), count($a) - abs($shift));
		}

		
		// Exchange stored parts of array
		$a = array_merge($r2, $r1);
	}		
	
	return $a;
}

/**
* Rotates an array forward or backward preserving key/value bindings.
*
* @param array $a source array
* @param int $shift shift value (may be negative) 
*
* @return array resulting array
*/

function array_rotate($a, $shift)
{
	if ($shift != 0)
	{
		// Normalizing shift to avoid useless rotations; It can become 0 after that
		if (abs($shift) >= count($a))
		{
			$sign = $shift > 0 ? 1 : -1;
			$shift = (abs($shift) - count($a) * (floor(abs($shift) / count($a)))) * $sign;
		}

		// Preparation for backward rotation (to the left)
		if ($shift < 0)
		{
			$a = array_reverse($a, true);
		}
		
		// Processing forward rotation steps (to the right)
		for ($i = 0; $i < abs($shift); $i++)
		{
			// Initialise resulting array (for the case when $shift is greater than 1)
			$r = array();
			
			// Storing last element

			$v = end($a); // moving pointer to the last element and storing its value
			$k = key($a); // storing its key

			// Last element becomes the first
			$r[$k] = $v; 
	
			// Adding initial array to the resulting array including the last element;
			// The last element is already in the array, but it is copied one more time at the first
			// physical position because it is referred by its key.
			
			reset($a);
			while (list($k, $v) = each($a)) $r[$k] = $v;
			
			// Storing one step processed array
			
			$a = $r;
		}
			
		// Finalization of backward rotation (to the left)
		if ($shift < 0)
		{
			$a = array_reverse($a, true);
		}
	}
	
	return $a;
}


/**
* Unsets first matched value with its key
*
* Returns false if nothing found
* 
* @param mixed $needle value to search
* @param array & $haystack array to search in
*
* @return bool true if value removed, false otherwise
*/

function array_remove($needle, &$haystack)
{
	$result = true;
	
	if (false !== ($k = array_search($needle, $haystack)))
	{
		unset($haystack[$k]);
	}
	else $result = false;
	
	return $result;
}


function array_remove_null_values(&$a)
{
	foreach ($a as $k => $v)
	{
		if (is_null($v))
		{
			unset($a[$k]);
		}
	}
}


function array_remove_empty_strings(&$a)
{
	foreach ($a as $k => $v)
	{
		if (is_string($v) && $v == '')
		{
			unset($a[$k]);
		}
	}
}


/**
* Removes values which are empty (in terms of PHP's empty() function), optionally reindexing the array from zero.
*/
function array_remove_empty_values(&$a, $reindex = false)
{
	foreach ($a as $k => $v)
	{
		if (empty($v))
		{
			unset($a[$k]);
		}
	}
	
	if ($reindex) $a = array_values($a);
}


/**
* Removes arbitrary number of elements from specified array by names of its keys
*
* @param array & $array target array
* @param string,... arbitrary number of elements' keys
*/

function array_unset(&$array)
{
	for ($i = 1; $i < func_num_args(); $i++)
	{
		$key = func_get_arg($i);
		
		if (array_key_exists($key, $array)) unset($array[$key]);
	}
}


/**
* Removes duplicate arrays from a two-dimensional array
*
* @param array $array two-dimensional array of arrays
*
* @return array array without duplicates
*/

function array_unique_multi($array)
{
	$array_s = array();
	$array_r = array();
	
	reset($array);
	while (list($k, $v) = each($array))
	{
		$array_s[$k] = serialize($v);
	}
	
	$array_s = array_unique($array_s);
	
	reset($array_s);
	while (list($k, $v) = each($array_s))
	{
		$array_r[$k] = unserialize($v);
	}
	
	return $array_r;
}

/**
* Makes associative array of given strings, where keys are the same strings
*
* @param string $string,... 

* @return array resulting array
*/

function array_compact_strings()
{
	$a = array();
	
	for ($i = 0; $i < func_num_args(); $i++)
	{
		$string = func_get_arg($i);
		
		$a[$string] = $string;
	}

	return $a;
}


/**
* Creates element with to_key key for element with from_key key and removes element this element
*
* @param array $array source array
* @param mixed $from_key old key
* @param mixed $to_key new key
*
* @return array resulting array
*/

function array_replace_key($array, $from_key, $to_key)
{
	if (! array_key_exists($from_key, $array)) return false;

	
	$array[$to_key] = $array[$from_key];
	
	unset($array[$from_key]);
	
	
	return $array;
}


/*
 already implemented in PHP core, but PHP version returns keys instead of values with keys
 
function array_rand($src, $count = 1)
{
	$dst = array();
	
	
	if (count($src) > 0 && $count > 0)
	{
		$keys = array_keys($src);
		
		while ($count > 0)
		{
			$key_index = mt_rand(0, count($keys));
			$key = $keys[$key_index];

			$dst[$key] = $src[$key];
			
			unset($keys[$key_index]); // unset used key index - gap occurs
			$keys = array_values($keys); // avoid gap

			$count--;
		}
	}
	
	
	return $dst;
}
*/


/**
* Sorts two-dimensional array by key names in specified orders
*
* Key name/order pairs are unlimited. At least one should be given.
*
* If $key_name is array its values are used as sorting arguments, arguments ignored then.
*
* @param $a array array to sort
* @param $key_name string name of key to sort by or array of arguments
* @param $order (SORT_ASC|SORT_DESC) sorting order for specified key; default is SORT_ASC
*
* @return array sorted array
*  
* @author jimpoz@jimpoz.com
* @author Roman V Tsourick (tsourick@gmail.com)
*/

function array_sort_multi($a, $key_name, $order = SORT_ASC)
{
	if (is_array($key_name))
	{
		$args =& $key_name;
	}
	else
	{
		$args = func_get_args();
		array_shift($args); // skip $a
	}
		
	foreach ($args as $i => $arg)
	{
		if (is_string($arg)) // catch field name
		{
			$tmp = array();
			foreach ($a as $j => $row)
			{
				$tmp[$j] = $row[$arg];
			}
			
			$args[$i] = $tmp;
		}
		else
		{
			$args[$i] = $arg;
		}
	}
	
	$args[] =& $a;

	call_user_func_array('array_multisort', $args);
	
	return array_pop($args);
}



/**
* Return value by key if any and cut it off from the array
*
* @param array &$array reference to the array
* @param mixed $key key to check for
* @param mixed $default default value to return when no specified key is set, default is NULL
*
* @return mixed value by key or default one
*/

function array_cut_value(&$array, $key, $default = NULL)
{
	$value = NULL;
	
	
	if (isset($array[$key]))
	{
		$value = $array[$key];
		unset($array[$key]);
	}
	else
	{
		$value = $default;
	}
	
	
	return $value;
}

/**
* Creates necessary keys with NULL value if they not set.
*
* Both key and value of passed arg will be used as array key except NULL values.
*
* @param array $array reference to the array
* @param mixed|array key name or array of key names of any structure (either as key or value except NULL value)
* [@param ... ]
*/

function array_ensure_keys(&$array)
{
	$args = func_get_args();
	array_shift($args); // skip $array
	
	foreach ($args as $arg)
	{
		if (is_null($arg)) continue;
		
		
		if (is_array($arg))
		{
			foreach ($arg as $k => $v)
			{
				if (! isset($array[$k])) $array[$k] = NULL;
				
				array_ensure_keys($array[$k], $v);
			}
		}
		else
		{
			if (! isset($array[$arg])) $array[$arg] = NULL;
		}
	}
}

function array_all_not_empty(&$array)
{
	$all_not_empty = true;
	
	$keys = func_get_args();
	array_shift($keys);
	
	foreach ($keys as $key)
	{
		if (! isset($array[$key]) || empty($array[$key]))
		{
			$all_not_empty = false;
			break;
		}
	}
	
	return $all_not_empty;
}

function array_not_empty(&$array, $key)
{
	return array_all_not_empty($array, $key);
}

function array_all_set(&$array)
{
	$all_set = true;
	
	$keys = func_get_args();
	array_shift($keys);
	
	foreach ($keys as $key)
	{
		if (! isset($array[$key]))
		{
			$all_set = false;
			break;
		}
	}
	
	return $all_set;
}

function array_get_value(&$a, $name, $default = NULL)
{
	$value = NULL;
	
	if (is_array($a) && array_key_exists($name, $a))
	{
		$value = $a[$name];
	}
	else
	{
		$value = $default;
	}
	
	return $value;
}


/**
* Extract a slice of associative array, optionally replacing keys with aliases in corresponding
* integer key positions. Size of $names should match size of $aliases.
*/

function array_slice_values(&$a, $names, $aliases = array(), $non_empty_only = false)
{
	$values = array();

	if (! is_array($names))
	{
		$names = str_explode($names, ',', NULL, create_function('&$v,$k', 'return $v = trim($v);'));
	}
	
	if (! is_array($aliases))
	{
		$aliases = str_explode($aliases, ',', NULL, create_function('&$v,$k', 'return $v = trim($v);'));
	}

	foreach ($names as $i => $name)
	{
		if (array_key_exists($name, $a))
		{
			if ($non_empty_only && empty($a[$name])) continue; // skip empty if option given
			
				
			if (isset($aliases[$i]) && ! empty($aliases[$i]))
			{
				$alias = $aliases[$i];
			}
			else
			{
				$alias  = $name;
			}

			$values[$alias] = $a[$name];
		}
	}	
	
	return $values;
}


function array_trim(&$a)
{
	$a = array_map('trim', $a);
}

function xbf_array_collect_values(&$rows, $key_name)
{
	$values = array();
	
	if (! empty($rows))
	{
		foreach ($rows as $row)
		{
			foreach ($row as $k => $v)
			{
				if ($k == $key_name) $values[] = $v;
			}
		}
	}
	
	return $values;
}

/**
* Explodes number-indexed array into arrays by specified counts in each chunk.
* Counts can be passed directly as variable length parameter list or as array.
* If $counts length is less than resulting chunk count, the last chunk will contain the rest of array.
*
* @param array array to explode
* @param (array|int) $counts array of counts or the count itself
* [@param int $count1 ...] next count ...
*/
function & xbf_array_explode_by_counts($a, $counts)
{
	$as = array();
	
	if (func_num_args() > 2)
	{
		$counts = func_get_args();
		array_shift($args); // skip $a
	}
	
	if (! is_array($counts)) // single count passed as a number 
	{
		$counts = array($counts);
	}
	/*
	// Cut off from the end of array
	$indexes = array_reverse($indexes);
	foreach ($indexes as $index)
	{
		if (isset($a[$index]))
		{
			$as[] = array_splice($a, $index);
		}
	}
	//$as = array_reverse($as);
	*/
	
	while ((! empty($a)) && ($count = array_shift($counts))) // shift off count from the beginning
	{
		$as[] = array_splice($a, 0, $count); // cut from beginning to index
	}
	if (! empty($a)) $as[] = $a;
	
	
	return $as;
}

// const XBF_ARRAY_SEARCH_LIST_SINGLE = 0x01; // n/a; default; return first match
define('XBF_ARRAY_SEARCH_LIST_SINGLE', 0x01);

// const XBF_ARRAY_SEARCH_LIST_MULTI = 0x02; // return all matches
define('XBF_ARRAY_SEARCH_LIST_MULTI', 0x02);

// const XBF_ARRAY_SEARCH_LIST_RETURN_KEY = 0x04; // n/a; default; return key for each match
define('XBF_ARRAY_SEARCH_LIST_RETURN_KEY', 0x04);

// const XBF_ARRAY_SEARCH_LIST_RETURN_ROW = 0x08; // return array (row) for each match
define('XBF_ARRAY_SEARCH_LIST_RETURN_ROW', 0x08);

function xbf_array_search_list(&$rows, $name, $value, $options = XBF_ARRAY_SEARCH_LIST_SINGLE | XBF_ARRAY_SEARCH_LIST_RETURN_KEY)
{
	$result = array();

	$option_multi = $options & XBF_ARRAY_SEARCH_LIST_MULTI;
	$option_return_row = $options & XBF_ARRAY_SEARCH_LIST_RETURN_ROW;

	foreach ($rows as $k => $row)
	{
		if (isset($row[$name]) && $row[$name] == $value)
		{
			// found
			
			if ($option_return_row)
			{
				$result[] = $row;
			}
			else
			{
				$result[] = $k;
			}
			
			if (! $option_multi) break; // stop at first match
		}
	}
	
	if ((! $option_multi) && ! empty($result))
	{
		$result = $result[0]; // single result, first match only
	}
	
	return $result;
}



function xbf_array_dim($a)
{
	$dim = 0;
	
	if (is_array($a))
	{
		$dim += xbf_array_dim($a[0]);
	}
	
	return $dim;
}


function xbf_array_group_list_by_ref(& $list, $by)
{
	$r = array();
	
	foreach ($list as & $row)
	{
		$group_value = is_callable($by) ? $by($row) : $row[$by];

		if (! isset($r[$group_value]))
		{
			$r[$group_value] = [];
		}
		
		$r[$group_value][] = & $row;
	}


	return $r;
}

