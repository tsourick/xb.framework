<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* FILE SYSTEM TOOLS library
*
* @version 1.0
* @package core
* @subpackage tools
*/


/**
* Deletes recursively file or directory.
* Deletion ends up and function returns immediately, if an error occurs somewhere in file tree.
*
* @param string $path relative or absolute physical path to file or directory
* @return bool TRUE on success, FALSE on failure
*/

function fs_delete($path)
{
	clearstatcache();

	$result = _fs_delete($path);
	
	return $result;
}

/**
*
*
* @param
* @param
*
* @return
*/

function _fs_delete($path)
{
	$result = true;
	
	if (is_dir($path))
	{
		if ($d = opendir($path))
		{
			while (false !== ($entry = readdir($d)))
			{
				if ($entry != '.' && $entry != '..')
				{
					if (!_fs_delete($path . DIRECTORY_SEPARATOR . $entry))
					{
						$result = false;
						break;
					}
				}
			}
			
			closedir($d);
			
			if (!rmdir($path)) $result = false;
		}
		else $result = false;
	}
	elseif (file_exists($path))
	{
		if (!unlink($path)) $result = false;
	}
	else $result = false;	
	
	return $result;
}


/**
* Copies recursively file or directory.
* Copy ends up and function returns immediately, if an error occurs somewhere in file tree.
*
* @param string $path relative or absolute physical path to file or directory
* @return bool TRUE on success, FALSE on failure
*/

function fs_copy($src_path, $dst_path)
{
	$result = true;
	
	
	clearstatcache();

	if (is_file($dst_path))
	{
		// File to file copy
		
		if (! is_file($src_path)) trigger_error('Multiple to one file copy not supported!', E_USER_ERROR);
		
		$result = copy($src_path, $dst_path);
	}
	else
	{
		$target_dir = dirpath($dst_path);
		
		
		if (in_array(basename($src_path), array('*', '*.*')))
		{
			// Copy dir contents

			$src_path = dirname($src_path);
			
			
			if (is_dir($src_path))
			{
				$src_path = dirpath($src_path);
		
				
				if ($d = opendir($src_path))
				{
					while (false !== ($entry = readdir($d)))
					{
						if ($entry != '.' && $entry != '..')
						{
							if (! _fs_copy($src_path . DIRECTORY_SEPARATOR . $entry, $target_dir))
							{
								$result = false;
								break;
							}
						}
					}
						
					closedir($d);
				}
				else $result = false;
			}
			
		}
		else
		{
			// Copy dir itself or file
			
			$result = _fs_copy($src_path, $target_dir);
		}
	}
	
	
	return $result;
}

/**
*
*
* @param
* @param
*
* @return
*/

function _fs_copy($src_path, $target_dir)
{
	$result = true;
	
	
	if (is_dir($src_path))
	{
		$src_path = dirpath($src_path);


		$dst_dir = dirpath($target_dir . basename($src_path));
		
		
		if ($d = opendir($src_path))
		{
			if (! mkdir($dst_dir)) $result = false;
			else
			{
				while (false !== ($entry = readdir($d)))
				{
					if ($entry != '.' && $entry != '..')
					{
						if (! _fs_copy($src_path . DIRECTORY_SEPARATOR . $entry, $dst_dir))
						{
							$result = false;
							break;
						}
					}
				}
				
				closedir($d);
			}
		}
		else $result = false;
	}
	elseif (file_exists($src_path))
	{
		if (! copy($src_path, $target_dir . basename($src_path))) $result = false;
	}
	else $result = false;
	
	
	return $result;
}


/**
* Get file (path base name) extension
*
* @param
* @param
*
* @return
*/

function baseext($path)
{
	$ext = '';
	
	$data = array_reverse(explode('.', basename($path)));
	
	if (count($data) > 1) $ext = $data[0]; // else - file has no extension
	
	return $ext;
}

global $FRAMEWORK;

$FRAMEWORK = array
(
	'lib.tool.filesystem' => array
	(
		'mime_types_by_ext' => array
		(
			'asc' => 'text/plain',
			'au' => 'audio/basic',
			'avi' => 'video/x-msvideo',
			'bin' => 'application/octet-stream',
			'bmp' => 'image/bmp',
			'class' => 'application/octet-stream',
			'css' => 'text/css',
			'dll' => 'application/octet-stream',
			'doc' => 'application/msword',
			'dvi' => 'application/x-dvi',
			'exe' => 'application/octet-stream',
			'gif' => 'image/gif',
			'gtar' => 'application/x-gtar',
			'gzip' => 'application/x-gzip',
			'htm' => 'text/html',
			'html' => 'text/html',
			'ico' => 'image/vnd.microsoft.icon',
			'jpe' => 'image/jpeg',
			'jpeg' => 'image/jpeg',
			'jpg' => 'image/jpeg',
			'js' => 'application/x-javascript',
			'm3u' => 'audio/x-mpegurl',
			'mid' => 'audio/midi',
			'midi' => 'audio/midi',
			'mov' => 'video/quicktime',
			'mp2' => 'audio/mpeg',
			'mp3' => 'audio/mpeg',
			'mpe' => 'video/mpeg',
			'mpeg' => 'video/mpeg',
			'mpg' => 'video/mpeg',
			'pdf' => 'application/pdf',
			'png' => 'image/png',
			'ppt' => 'application/vnd.ms-powerpoint',
			'qt' => 'video/quicktime',
			'ra' => 'audio/x-realaudio',
			'ram' => 'audio/x-pn-realaudio',
			'rm' => 'audio/x-pn-realaudio',
			'rpm' => 'audio/x-pn-realaudio-plugin',
			'rtf' => 'text/rtf',
			'snd' => 'audio/basic',
			'spl' => 'application/x-futuresplash',
			'swf' => 'application/x-shockwave-flash',
			'tar' => 'application/x-tar',
			'tif' => 'image/tiff',
			'tiff' => 'image/tiff',
			'txt' => 'text/plain',
			'wav' => 'audio/x-wav',
			'wbxml' => 'application/vnd.wap.wbxml',
			'wml' => 'text/vnd.wap.wml',
			'wmlc' => 'application/vnd.wap.wmlc',
			'wmls' => 'text/vnd.wap.wmlscript',
			'wmlsc' => 'application/vnd.wap.wmlscriptc',
			'wmv' => 'video/x-ms-wmv',
			'xhtml' => 'application/xhtml+xml',
			'xls' => 'application/vnd.ms-excel',
			'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			'xml' => 'text/xml',
			'xsl' => 'text/xml',
			'zip' => 'application/zip'
		)
	)
);

/**
*
*
* @param
* @param
*
* @return
*/

function fs_get_mime_type_by_ext($path)
{
	global $FRAMEWORK;
	
	$mtbe =& $FRAMEWORK['lib.tool.filesystem']['mime_types_by_ext'];
	

	// if (empty($path)) return false;
	if ($path == '') return false;

	$ext = baseext($path);

	// if (empty($ext)) return false;
	if ($ext == '') return false;


	if (isset($mtbe[$ext])) return $mtbe[$ext]; else return false;
}


function fs_is_absolute_path($path)
{
	return preg_match('/^([\/\\\\]|[a-zA-Z]:[\/\\\\])/', $path);
}


/**
* Creates new file if not exists and returns true. Otherwise returns false.
*/

function fs_create_new_file($file_path)
{
	$r = false;
	
	
	if ($h = @fopen($file_path, 'x'))
	{
		fclose($h);
		$r = true;
	}
	

	return $r;
}

/**
* Creates directory full path if not exists.
*
* Returns false if no dirs were created (including when target directory already exists).
*/

function fs_create_directory($dir_path)
{
	$r = false;
	

	if ($dir_path != '')
	{
		$dir_path = dirpath($dir_path);
	
		
		// Cycle up through the path till existing directory OR empty root (e.g. fully non-existent relativepath)
		
		$name_stack = array();
		$root_path = rtrim($dir_path, DIRECTORY_SEPARATOR); // skip trailing spearator

		while (! file_exists($root_path) && $root_path != '')
		{
			$last_pos = strrpos($root_path, DIRECTORY_SEPARATOR);

			if (false !== $last_pos)
			{
				list($root_path, $name_stack[]) = str_explode_at($root_path, $last_pos, true);
				
				if ($root_path == '') // possible only if absolute path was specified starting from '/' (root) and now we reached it
				{
					$root_path = '/'; // this should stop 'while' cycle as the root always exists
				}
			}
			else
			{
				$name_stack[] = $root_path;
				$root_path = '';
			}
		}
		

		// Create non-existing parts of the path if any strting from existing dir down to given path

		if (! empty($name_stack))
		{
			$name = array_pop($name_stack);
			while (! is_null($name))
			{
				$root_path .= (($root_path == '/' || $root_path == '') ? $name : DIRECTORY_SEPARATOR . $name); // always perpend with separator except filesystem root OR empty root (e.g. fully non-existent relativepath)
				$r = mkdir($root_path);

				$name = array_pop($name_stack);
			}
		}
		/*
		if (! file_exists($dir_path))
		{
			$parts = explode(DIRECTORY_SEPARATOR, $dir_path);
			array_remove_empty_values($parts);
			
			if (! empty($parts))
			{
				$path = '';
				foreach ($parts as $part)
				{
					$path .= $part;
					if (! file_exists($path))
					{
						mkdir($path);
					}
				}
				
				$r = true;
			}
		}
		*/
	}
	
	
	return $r;
}

/**
* Human readable file size
*
* @param mixed $size file size as integer number or file path to actual file
* @param int|null $presision auto if null (default) 
* @param int|null $unit auto if null (default) 
*/

function _xbf_fs_hsize($size, $precision = NULL, $unit = NULL)
{	
	static $precisions = [ 1,   1,   1,   2,   2,   3,   3,   4,   4];
	static $units      = ['B','KB','MB','GB','TB','PB','EB','ZB','YB'];
	
	$t = ! is_null($unit) ? array_search($unit, $units) : floor(log($size, 1024)); // thousand group count
	
	
	$precision = ! is_null($precision) ? $precision : $precisions[$t];

	return round($size / pow(1024, $t), $precision) . $units[$t];
}

function xbf_fs_hsize($size, $precision = NULL, $unit = NULL)
{
	$hsize = false;
	
	if (! is_numeric($size))
	{
		if (is_string($size) && file_exists($size)) $size = filesize($size);
	}
	
	if ($size)
	{
		$hsize = _xbf_fs_hsize($size, $precision, $unit);
	}
	
	return $hsize;
}

?>
