<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* DEBUGGING library
*
* When DEBUG is set to true before including this file (or framework) these options are switched on:
* - display_errors
* - display_startup_errors
* - ASSERT_ACTIVE
* - ASSERT_WARNING
* - ASSERT_BAIL
*
* and error_reporting set to E_ALL
* 
* When also DEBUG_LOGGING set to true these options are switched on as well:
* - log_errors
*
* and error_log set to DEBUG_DUMP_FILENAME
*
* When also DEBUG_NOCACHE set to true browser caching is prevented
*
* @version 1.1
* @package core
* @subpackage debug
*/


if (! defined('DEBUG')) define('DEBUG', false);
// if (! defined('DEBUG_DUMP_FILENAME')) define('DEBUG_DUMP_FILENAME', 'debug.log');
// if (! defined('DEBUG_LOGGING')) define('DEBUG_LOGGING', false);
if (! defined('DEBUG_NOCACHE')) define('DEBUG_NOCACHE', true);


// global $watchdog;
// $watchdog = 0;
global $trace_index;
$trace_index = 0;


/**
* Echoes or returns text (web echo)
*
* htmlspecialchars() is applied to text
*
* @param string $s optional text to echo
* @param bool $return if true, the text is returned, otherwise (default) - echoed
*
* @return string
*
* @access private
*/

function _dbg_wecho($s = '', $return = false)
{
	settype($s, 'string');
	$s = htmlspecialchars($s);
	
	if ($return) return $s;
	else echo $s;
}

/**
* Echoes or returns line of text (web echo)
*
* htmlspecialchars() is applied to text
*
* @param string $s optional text to echo
* @param bool $return if true, the text is returned, otherwise (default) - echoed
*
* @return string
*
* @access private
*/

function _dbg_wechos($s = '', $return = false)
{
	$s = _dbg_wecho($s, true) . '<br>' . "\n";
	
	if ($return) return $s;
	else echo $s;
}

/**
* Echoes or returns preformatted text (web pre)
*
* htmlspecialchars() is applied to text
*
* @param string $s optional text to echo
* @param bool $return if true, the text is returned, otherwise (default) - echoed
*
* @return string
*
* @access private
*/

function _dbg_wpre($s = '', $return = false)
{
	$s = '<pre>' . "\n" . _dbg_wecho($s, true) . "\n" . '</pre>' . "\n";

	if ($return) return $s;
	else echo $s;
}

/**
* Echoes or returns preformatted text (web printr)
*
* htmlspecialchars() is applied to text
*
* @param string $a array to echo
* @param bool $return if true, the text is returned, otherwise (default) - echoed
*
* @return string
*
* @access private
*/

function _dbg_wprintr($a, $return = false)
{
	settype($a, 'array');

	$s = _dbg_wpre(print_r($a, true), true);

	if ($return) return $s;
	else echo $s;
}

	
/**
* Prevent browser caching
*
* @access private
*/

function _dbg_xbf_http_nocache()
{
	// Date in the past
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	
	// always modified
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	
	// HTTP/1.1
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	
	// HTTP/1.0
	header("Pragma: no-cache");
}

	
/**
* Web echo auto incrementing index
*
* Has no effect when DEBUG is false
*/

function trace()
{
	global $trace_index;
	
	if (DEBUG)
	{
		_dbg_wechos($trace_index++);
	}
}

/**
* Outputs value with timestamp for debugging (web debug)
*
* @param mixed $s optional value to output
* @param string $block_name optional block name is written at the beginning, if set 
*/

function wdebug($s = '', $block_name = '')
{
	if (DEBUG)
	{
		$block_name = '';
		if (! empty($block_name)) $block_name = '[' . $block_name . '] :: '; 
	
		$now = date('H:i:s');
		
		
		_dbg_wechos('> (' . $now . ') ' . $block_name);
		
		if (is_array($s)) _dbg_wprintr($s); else _dbg_wechos($s);
	}
}

/**
* Creates or truncates dump log file for debugging
*/

function init_dump()
{
	if (DEBUG)
	{/*
		if ($f = fopen(DEBUG_DUMP_FILENAME, 'r+'))
		{
			$locked = false;
			
			$i = 5;
			while ($i > 0 && ! $locked = flock($f, LOCK_EX))
			{
				sleep(1);
				$i--;
			}
			
			if ($locked)
			{
				ftruncate($f, 0);
				
				flock($f, LOCK_UN);
			}
			
			fclose($f);
		}
		*/
		logger_init();
	}
}

/**
* Dumps value with timestamp into file for debugging
*
* @param mixed $s optional value to output
* @param string $block_name optional block name is written at the beginning, if set 
*/

function dump($s = '', $block_name = '')
{
	if (DEBUG)
	{
		logger_log($s, $block_name);
	}
}

function dump_backtrace($block_name = '', $limit = 0, $o = DEBUG_BACKTRACE_IGNORE_ARGS)
{
	$ts = debug_backtrace($o, $limit);
	array_shift($ts);
	
	// dump($ts, $block_name);
	
	$r = ['BACKTRACE'];
	foreach ($ts as $index => $t)
	{
		$func  = $t['function'] . '()';
		$class = isset($t['class']) ? $t['class'] : null;
		$type  = isset($t['type'])  ? $t['type']  : null;
		$file  = isset($t['file'])  ? basename($t['file']) : null;
		$line  = isset($t['line'])  ? $t['line'] : null;
		$path  = isset($t['file'])  ? $t['file'] : null;
		
		$_loc = ($file && $line) ? "@ $file($line) ": '';
		$_cls = $class && $type ? "{$class} {$type} " : '';
		
		$index = str_pad($index, 2);
		// $call = "#{$index} {$class}{$type}{$func}{$file}{$line}($path)";
		$call = "#{$index} {$_cls}{$func}";
		$call = str_pad($call, 60);
		$call .= $_loc;
		
		$r[] = $call;
	}
	$r = implode(PHP_EOL, $r);
	
	dump($r, $block_name);
}

function dump_var($v = null, $block_name = '')
{
	dump(var_export($v, true), $block_name);
}

if (DEBUG)
{
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	/*
	if (DEBUG_LOGGING)
	{
		ini_set('log_errors', 1);
		ini_set('error_log', DEBUG_DUMP_FILENAME);
	}	
	*/
	assert_options(ASSERT_ACTIVE, 1);
	assert_options(ASSERT_WARNING, 1);
	assert_options(ASSERT_BAIL, 1);

	if (DEBUG_NOCACHE) _dbg_xbf_http_nocache();
}
?>