<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* HTTP TOOLS library
*
* @version 1.0
* @package core
* @subpackage tools
*/

require_once 'lib.tool.general.php';

/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_nocache()
{
	// Date in the past
	xbf_http_set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	
	// always modified
	xbf_http_set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	
	// HTTP/1.1
	xbf_http_set_header("Cache-Control: no-store, no-cache, must-revalidate");
	xbf_http_set_header("Cache-Control: post-check=0, pre-check=0", false);
	
	// HTTP/1.0
	xbf_http_set_header("Pragma: no-cache");
}

/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_send_file($file_name, $mime_type, $content, $size = NULL, $disposition = 'attachment', $exit = true)
{
	xbf_http_set_header('Content-Description: File Transfer');

	xbf_http_set_header('Content-Type: ' . $mime_type);
	xbf_http_set_header('Content-Disposition: ' . $disposition . '; filename="' . $file_name . '"');
	
	if (is_null($size)) $size = strlen($content);
	
	xbf_http_set_header('Content-Length: ' . $size);

	
	xbf_http_apply_headers();
	
	echo $content;
	@ob_flush(); 
	flush();
	
	
	if ($exit) safe_exit();
}

/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_send_response_new($content, $content_type = 'text/plain', $charset = '', $response_code = 200, $response_message = 'OK')
{
	xbf_http_set_header("HTTP/1.0 $response_code $response_message");
	
	xbf_http_send_response($content, $content_type, $charset);
}

/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_send_response($content, $mode = 'plaintext', $charset = '', $disable_caching = false, $attachment = false)
{
	if (empty($charset))
	{
		if (isset($_SERVER['HTTP_ACCEPT_CHARSET']))
		{
			list($charset) = explode(';', $_SERVER['HTTP_ACCEPT_CHARSET']);
		}
		
		if (empty($charset)) $charset = 'utf-8';
	}
	
	switch ($mode)
	{
		case 'html':
			xbf_http_set_header('Content-Type: text/html; charset=' . $charset);
		break;

		case 'xml':
			if (stristr($_SERVER['HTTP_ACCEPT'], 'application/xhtml+xml'))
			{
				xbf_http_set_header('Content-type: application/xhtml+xml; charset=' . $charset);
			}
			else
			{
				xbf_http_set_header('Content-type: text/xml; charset=' . $charset);
			}
		break;
		
		case 'plaintext':
		case 'js':
			xbf_http_set_header('Content-Type: text/plain; charset=' . $charset);
		break;
		
		case 'json':
			xbf_http_set_header('Content-Type: application/json; charset=' . $charset);
		break;
		
		case 'doc':
			xbf_http_set_header("Content-type: application/msword");
		break;
		
		default:
			xbf_http_set_header('Content-Type: ' . $mode . '; charset=' . $charset);
	}
	
	if ($disable_caching)
	{
		xbf_http_nocache();
	}
	
	if ($attachment)
	{
		xbf_http_set_header('Content-Description: File Transfer');
		xbf_http_set_header('Content-length: ' . strlen($content));
		xbf_http_set_header('Content-Disposition: attachment; filename="' . $file_name . '"');
	}
	
	
	xbf_http_apply_headers();
	
	echo $content;
	
	
	safe_exit();
}

/**
* Makes browser redirect to specified location with "Location" HTTP parameter.
*
* @param string $target URL, URI or filename to redirect to
* @param array $params array of CGI params where key is parameter name and value is parameter value;
* every parameter value is safely urlencoded
* @param bool $redirect if set to TRUE, function sends location header and terminates current script;
* otherwise, redirection URI is returned. Defaults to TRUE.
* @return string $location location HTTP field
*
* If $target and $params are empty '?' is used as target URL which effectively redirects to the same
* URI.
*/

function xbf_http_redirect($target = '', $params = array(), $redirect = true)
{
	$url = '';
	
	
	// if (empty($target) && empty($params)) $url = $_SERVER['PHP_SELF'];
	if (empty($target) && empty($params)) $url = '?';
	else
	{
		$uri = '';
		$query = '';
		
		$parts = explode('?', $target, 2);
		
		$uri = $parts[0];
		if (isset($parts[1])) $query = $parts[1];
 
		
		if (! empty($params))
		{
			$params_encoded = array();
			foreach ($params as $k => $v) $params_encoded[] = $k . '=' . urlencode($v);
			
			if (! empty($query)) $query .= '&';
			
			$query .= implode('&', $params_encoded);
		}
		
		$url = $uri;
		if (! empty($query)) $url .= '?' . $query;
	}

	
	if ($redirect)
	{
		xbf_http_set_header('Location: ' . $url);
		
		xbf_http_apply_headers();
		
		safe_exit();
	}
	
	
	return $url;
}

/**
*
*
* @param
* @param
*
* @return
*/
/*
function redirect($target = '', $params = array(), $redirect = true)
{
	return xbf_http_redirect($target, $params, $redirect);
}
*/
/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_not_found($content = 'not found', $exit = true)
{
	// xbf_http_set_header("HTTP/1.1 404 Not Found");
	
	xbf_http_set_response_code(404);
	
	$message = xbf_http_get_response_message(404);
	xbf_http_set_header("Status: 404 ". $message);
	
	xbf_http_apply_headers();

	echo $content;
	echo str_repeat('&nbsp;', 100); // for IE

	if ($exit)
	{
		safe_exit();
	}
}

/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_content_length($length)
{
	xbf_http_set_header('Content-Length: ' . $length);
}

/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_last_modified($utimestamp)
{
	// Force browser if-modified-since check on every request
//	header('Cache-Control: no-cache');

	
	// Switch to GMT timezone for appropriate date() processing
	
	$old_tz = date_default_timezone_get(); // save current zone
	date_default_timezone_set('GMT'); // set new zone

	xbf_http_set_header('Last-Modified: ' . date('D, d M Y H:i:s T', $utimestamp));
	
	// Switch back to timezone
	date_default_timezone_set($old_tz);
}

/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_last_modified_date_info($s)
{
	$info = false;
	
	
	$MM = array(NULL, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
	
	list($D, $d, $M, $Y, $H, $i, $s, $T) = sscanf($s, '%03s, %02d %03s %04d %02d:%02d:%02d %03s');
	/*
	if (! $MMM = array_search($M, $MM)) trigger_error("xbf_http_last_modified_date_info(): Invalid date string '$s' provided.", E_USER_ERROR);
	
	if (! checkdate($MMM, $d, $Y)) trigger_error("xbf_http_last_modified_date_info(): Invalid date string '$s' provided.", E_USER_ERROR);
	*/
	if (($MMM = array_search($M, $MM)) && checkdate($MMM, $d, $Y) && checktime($H, $i, $s) && $T == 'GMT')
	{
		$info = array('hour' => $H, 'minute' => $i, 'second' => $s, 'month' => $MMM, 'day' => $d, 'year' => $Y, 'tz_abbr' => $T);
	}

	
	return $info;
}

/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_if_modified_since()
{
	$utimestamp = NULL;
	
	
	if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
	{
		// $if_modified_since_info = date_parse_from_format('D, d M Y H:i:s T', $_SERVER['HTTP_IF_MODIFIED_SINCE']);
		$if_modified_since_info = xbf_http_last_modified_date_info($_SERVER['HTTP_IF_MODIFIED_SINCE']);

		// if ($if_modified_since_info['error_count'] == 0 && $if_modified_since_info['warning_count'] == 0)
		if (! empty($if_modified_since_info))
		{
			// Switch to if-modified-since timezone for appropriate mktime() processing (should be GMT)
			
			$old_tz = date_default_timezone_get(); // save current zone
			date_default_timezone_set($if_modified_since_info['tz_abbr']); // set new zone (should be GMT)

			// Get Unix timestamp for if-modified-since date
			$utimestamp = mktime
			(
				$if_modified_since_info['hour'],
				$if_modified_since_info['minute'],
				$if_modified_since_info['second'],
				$if_modified_since_info['month'],
				$if_modified_since_info['day'],
				$if_modified_since_info['year']
			);
			
			// Switch back to timezone
			date_default_timezone_set($old_tz);
		}
	}
	
	
	return $utimestamp;
}
	
/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_not_modified()
{
	xbf_http_set_header("HTTP/1.1 304 Not Modified");
	xbf_http_set_header('Content-Length: 0');
	
	xbf_http_apply_headers();
	
	safe_exit();
}


/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_set_header($header, $replace = true)
{
	global $RUNTIME;
	

	settype($header, 'string');

	if (strpos($header, 'HTTP/') === 0)
	{
		$name = 'HTTP/';
		$value = substr($header, 4);
		$replace = true;
	}
	else
	{
		@list($name, $value) = explode(':', $header, 2);
	}
	
	if (is_null($value)) trigger_error("xbf_http_set_header(): Invalid header '$header' passed.", E_USER_ERROR);


	if (! isset($RUNTIME['http']['headers'])) $RUNTIME['http']['headers'] = array();
	
	
	if (! isset($RUNTIME['http']['headers'][$name]))
	{
		$RUNTIME['http']['headers'][$name] = array($value);
	}
	else
	{
		if ($replace) $RUNTIME['http']['headers'][$name] = array($value);
		else $RUNTIME['http']['headers'][$name][] = $value;
	}
}

// with colon - remove the whole matched header string, without colon - treat $header as name and remove all those values
/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_remove_header($header)
{
	global $RUNTIME;

	
	@list($name, $value) = explode(':', $header);
	
	if (is_null($value))
	{
		// header name (or HTTP/ header) given - remove all values with the name
		
		$name = (strpos($header, 'HTTP/') === 0) ? 'HTTP/' : $header;

		if (isset($RUNTIME['http']['headers'][$name])) unset($RUNTIME['http']['headers'][$name]);
	}
	else
	{
		// the whole header string given - remove single header
		if (isset($RUNTIME['http']['headers'][$name]))
		{
			while (false !== ($index = array_search($value, $RUNTIME['http']['headers'][$name])))
			{
				unset($RUNTIME['http']['headers'][$name][$index]);
			}
		}
	}
}

/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_set_headers($headers)
{
	global $RUNTIME;

	
	$RUNTIME['http']['headers'] = $headers;
}

function & xbf_http_get_headers($headers)
{
	global $RUNTIME;

	
	if (! isset($RUNTIME['http']['headers'])) $RUNTIME['http']['headers'] = array();
	
	
	return $RUNTIME['http']['headers'];
}

/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_header_list()
{
	global $RUNTIME;

	$list = array();
	if (isset($RUNTIME['http']['headers']) && ! empty($RUNTIME['http']['headers']))
	{
		foreach ($RUNTIME['http']['headers'] as $name => $values)
		{
			if ($name == 'HTTP/')
			{
				$list[] = $name.$values[0];
			}
			else
			{
				foreach ($values as $value)
				{
					$list[] = $name.':'.$value;
				}
			}
		}
	}
	
	return $list;
}

/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_apply_headers()
{
	$list = xbf_http_header_list();

	if (! empty($list))
	{
		foreach ($list as $header)
		{
			header($header);
		}
		
		xbf_http_set_headers(NULL); // prevent from re-apply on consequent calls
	}
}


/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_get_response_message($code)
{
	$r = null;
	
	$messages = array
	(
		300 => 'Multiple Choices',
		301 => 'Moved Permanently',
		302 => 'Found',
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		307 => 'Temporary Redirect',

		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Request Entity Too Large',
		413 => 'Request Entity Too Large',
		414 => 'Request-URI Too Long',
		415 => 'Unsupported Media Type',
		416 => 'Requested Range Not Satisfiable',
		417 => 'Expectation Failed'
	);
	
	if (array_key_exists($code, $messages))
	{
		$r = $messages[$code];
	}
	
	return $r;
}


/**
*
*
* @param
* @param
*
* @return
*/

function xbf_http_set_response_code($code, $custom_message = NULL, $proto_version = '1.1')
{
	if (! is_null($custom_message))
	{
		$message = $custom_message;
	}
	else
	{
		$message = xbf_http_get_response_message($code);
	}
	
	xbf_http_set_header('HTTP/' . $proto_version . ' ' . $code . ' ' . $message);
}

/*
function getRealIpAddr()
{
	$ip = NULL;
	
	if (! empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
	{
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	}
	elseif (! empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	{
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else
	{
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	
	return $ip;
}
*/

function xbf_http_get_ip()
{
	$ip = NULL;
	
	
	$header_checks = array
	(
		'HTTP_CLIENT_IP',
		'HTTP_PRAGMA',
		'HTTP_XONNECTION',
		'HTTP_CACHE_INFO',
		'HTTP_XPROXY',
		'HTTP_PROXY',
		'HTTP_PROXY_CONNECTION',
		'HTTP_VIA',
		'HTTP_X_COMING_FROM',
		'HTTP_COMING_FROM',
		'HTTP_X_FORWARDED_FOR',
		'HTTP_X_FORWARDED',
		'HTTP_X_CLUSTER_CLIENT_IP',
		'HTTP_FORWARDED_FOR',
		'HTTP_FORWARDED',
		'ZHTTP_CACHE_CONTROL',
		'REMOTE_ADDR'
	);
	
	foreach ($header_checks as $key)
	{
		if (array_key_exists($key, $_SERVER) === true)
		{
			foreach (explode(',', $_SERVER[$key]) as $ip)
			{
				$ip = trim($ip);
				if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) break;
			}
		}
	}

	
	return $ip;
}

function xbf_http_get_url($include_proto = true)
{
	$url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	
	if ($include_proto) $url = (! empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $url;
	
	return $url;
}
?>
