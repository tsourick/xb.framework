<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* WEB OUTPUT TOOLS library
*
* @version 1.0
* @package core
* @subpackage tools
*/


global $RUNTIME;

$RUNTIME['weboutput']['autoflush'] = false;

/**
*
*
* @param
* @param
*
* @return
*/

function wautoflush($bool)
{
	global $RUNTIME;
	
	$RUNTIME['weboutput']['autoflush'] = $bool;
}

function _wprint($s)
{
	global $RUNTIME;
	
	echo $s;
	
	if ($RUNTIME['weboutput']['autoflush']) wflush();
}

/**
*
*
* @param
* @param
*
* @return
*/

function wecho($s = '', $return = false, $convertspecials = true)
{
	global $RUNTIME;

	settype($s, 'string');
	if ($convertspecials) $s = htmlspecialchars($s);
	
	if ($return) return $s;
	else _wprint($s);
}

/**
*
*
* @param
* @param
*
* @return
*/

function wechos($s = '', $return = false, $convertspecials = true)
{
	$s = wecho($s, true, $convertspecials) . '<br>' . "\n";
	
	if ($return) return $s;
	else _wprint($s);
}


/**
*
*
* @param
* @param
*
* @return
*/

function wpre($s = '', $return = false)
{
	$s = '<pre>' . "\n" . wecho($s, true) . "\n" . '</pre>' . "\n";

	if ($return) return $s;
	else _wprint($s);
}

/**
*
*
* @param
* @param
*
* @return
*/

function wprintr($a, $return = false)
{
	$s = wpre(print_r($a, true), true);

	if ($return) return $s;
	else _wprint($s);
}

/**
*
*
* @param
* @param
*
* @return
*/

function wflush()
{
	echo str_repeat(' ', 4096);
	@ob_flush(); 
	flush();
}
?>
