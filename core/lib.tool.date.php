<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* DATE TOOLS library
*
* @version 1.0
* @package core
* @subpackage tools
*/

require_once 'lib.tool.general.php';


/**
* Safely returns date value of three variables set by CGI method GET, POST or sent within cookie.
* The value is checked for appropriate ranges and with checkdate().

* Value is cached in a global array $RUNTIME in element $RUNTIME['cache']['cgi'] as a whole value.
* Only actual CGI values are cached, default values passed to the function are not.
*
* @param string $name name of variable
* @param string $default default value to return when original one is not set or empty
* @param array $postfixes 0-based array of postfixes for three variable names in the order [year, month, day]
* @return string date formatted as 'YYYY-MM-DD'
*/

function get_cgi_date($name, $default = NULL, $postfixes = array('_year', '_month', '_day'))
{
	global $RUNTIME;
	
	$val = NULL;
	
	if (isset($RUNTIME['cache']['cgi'][$name])) $val = $RUNTIME['cache']['cgi'][$name];			// Check cache for given name
	else
	{
		$REQUEST =& _get_request();

		$year_name = $name . $postfixes[0]; 
		$month_name = $name . $postfixes[1]; 
		$day_name = $name . $postfixes[2];
		
		if (isset($REQUEST[$year_name], $REQUEST[$month_name], $REQUEST[$day_name]))
		{
			$y = get_cgi_value($year_name, 'i');
			$m = get_cgi_value($month_name, 'i');
			$d = get_cgi_value($day_name, 'i');
			
			if (checkdate($m, $d, $y))
			{
				$val = sprintf('%04d-%02d-%02d', $y, $m, $d);
			}
		}
		
		$RUNTIME['cache']['cgi'][$name] = $val;
	}
	
	if ($val === NULL && $default !== NULL) $val = $default;
	
	return $val;
}


/**
*
*
* @param
* @param
*
* @return
*/

function get_cgi_date_string($name, $default = NULL)
{
	$date = get_cgi_value($name, 's', $default);
	
	if (! is_null($date))
	{
		// Check date
		
		list($y, $m, $d) = sscanf($date, "%04d-%02d-%02d");
	
		if (! checkdate($m, $d, $y))
		{
			$date = NULL;
		}
	}

	
	return $date;
}


/**
*
*
* @param
* @param
*
* @return
*/

function checktime($hour, $minute, $second)
{
	// TODO: implement time checking in a way like checkdate() does
	
	$r = true;
	
	if ($hour < 0 || $hour > 23) $r = false;
	if ($minute < 0 || $minute > 59) $r = false;
	if ($second < 0 || $second > 59) $r = false;
	
	return $r;
}

/**
* Safely returns datetime value of six variables set by CGI method GET, POST or sent within cookie.
* The value is checked for appropriate ranges and with {@link PHP_MANUAL#checkdate() checkdate()} and {@link checktime checktime()}.

* Value is cached in a global array $RUNTIME in element $RUNTIME['cache']['cgi'] as a whole value.
* Only actual CGI values are cached, default values passed to the function are not.
*
* @param string $name name of variable
* @param string $default default value to return when original one is not set or empty
* @param array $postfixes 0-based array of postfixes for six variable names in the order [year, month, day, hour, minute, second]
* @return string date formatted as 'YYYY-MM-DD hh:mm:ss'
*/

function get_cgi_datetime($name, $default = NULL, $postfixes = array('_year', '_month', '_day', '_hour', '_minute', '_second'))
{
	global $RUNTIME;

	$val = NULL;
	
	if (isset($RUNTIME['cache']['cgi'][$name])) $val = $RUNTIME['cache']['cgi'][$name];			// Check cache for given name
	else
	{
		$REQUEST =& _get_request();

		$year_name = $name . $postfixes[0]; 
		$month_name = $name . $postfixes[1]; 
		$day_name = $name . $postfixes[2];
		$hour_name = $name . $postfixes[3]; 
		$minute_name = $name . $postfixes[4]; 
		$second_name = $name . $postfixes[5];

		if (isset($REQUEST[$year_name], $REQUEST[$month_name], $REQUEST[$day_name], $REQUEST[$hour_name], $REQUEST[$minute_name], $REQUEST[$second_name]))
		{
			$y = get_cgi_value($year_name, 'i');
			$mon = get_cgi_value($month_name, 'i');
			$d = get_cgi_value($day_name, 'i');
			
			$h = get_cgi_value($hour_name, 'i');
			$min = get_cgi_value($minute_name, 'i');
			$s = get_cgi_value($second_name, 'i');

			if (checkdate($mon, $d, $y) && checktime($h, $min, $s))
			{
				$val = sprintf('%04d-%02d-%02d %02d:%02d:%02d', $y, $mon, $d, $h, $min, $s);
			}
		}
		
		$RUNTIME['cache']['cgi'][$name] = $val;
	}
	
	if ($val === NULL && $default !== NULL) $val = $default;
	
	return $val;
}


/**
*
*
* @param
* @param
*
* @return
*/

function get_cgi_datetime_string($name, $default = NULL)
{
	$value = get_cgi_value($name, 's', $default);
	
	if (! is_null($value))
	{
		// Check date
		
		list($y, $m, $d, $h, $i, $s) = sscanf($value, "%04d-%02d-%02d %02d:%02d:%02d");
	
		if (! checkdate($m, $d, $y) || ! checktime($h, $i, $s))
		{
			$value = NULL;
		}
	}

	
	return $value;
}


/**
* Safely returns time value of three variables set by CGI method GET, POST or sent within cookie.
* The value is checked for appropriate ranges and with checktime().

* Value is cached in a global array $RUNTIME in element $RUNTIME['cache']['cgi'] as a whole value.
* Only actual CGI values are cached, default values passed to the function are not.
*
* @param string $name name of variable
* @param string $default default value to return when original one is not set or empty
* @param array $postfixes 0-based array of postfixes for three variable names in the order [hour, minute, second]
* @return string date formatted as 'hh:mm:ss'
*/

function get_cgi_time($name, $default = NULL, $postfixes = array('_hour', '_minute', '_second'))
{
	global $RUNTIME;
	
	$val = NULL;
	
	if (isset($RUNTIME['cache']['cgi'][$name])) $val = $RUNTIME['cache']['cgi'][$name];			// Check cache for given name
	else
	{
		$REQUEST =& _get_request();

		$hour_name = $name . $postfixes[0]; 
		$minute_name = $name . $postfixes[1]; 
		$second_name = $name . $postfixes[2];
		
		if (isset($REQUEST[$hour_name], $REQUEST[$minute_name], $REQUEST[$second_name]))
		{
			$h = get_cgi_value($hour_name, 'i');
			$m = get_cgi_value($minute_name, 'i');
			$s = get_cgi_value($second_name, 'i');

			if (checktime($h, $m, $s))
			{
				$val = sprintf('%02d:%02d:%02d', $h, $m, $s);
			}
		}
		
		$RUNTIME['cache']['cgi'][$name] = $val;
	}
	
	if ($val === NULL && $default !== NULL) $val = $default;
	
	return $val;
}



/**
*
*
* @param
* @param
*
* @return
*/

function get_cgi_time_string($name, $default = NULL)
{
	$value = get_cgi_value($name, 's', $default);
	
	if (! is_null($value))
	{
		// Check date
		
		list($h, $i, $s) = sscanf($value, "%02d:%02d:%02d");
	
		if (! checktime($h, $i, $s))
		{
			$value = NULL;
		}
	}

	
	return $value;
}


/*

function bugFormatDate($ts, $lang = 'ru')
{
	$r = '';
	

	sscanf($ts, '%04d-%02d-%02d', $y, $m, $d);
	
	if ($lang == 'ru')
	{
		$ms = array(1 => 'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Август', 'Сентябрь', 'Октября', 'Ноября', 'Декабря');
		
		$r = "{$d} {$ms[$m]} {$y}";
	}
	else
	{
		$r = date("d F Y", mktime(0, 0, 0, $m, $d, $y));
	}
	
	return $r;
}
*/

/**
* strftime() extension 
* Introduces %Q format - properly translates russian month names
*/
function strftime2($format, $timestamp = NULL)
{
	if (is_null($timestamp)) $timestamp = time();
	

	$russian_month_names_in_use = false;
	$fix_the_bug = false; // means convert from CP1251 to UTF-8
	$target_encoding = 'utf-8';

	if (strpos($format, '%Q') !== false)
	{
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
		{
			// This is a server using Windows

			$current_locale = setlocale(LC_ALL, 0);

			if (preg_match('/LC_CTYPE=([^.]+)\\.(\\d+)/', $current_locale, $matches))
			{
				$language_country = $matches[1];
				$target_encoding = $matches[2];

				if (strpos(strtolower($language_country), 'rus') === 0)
				{
					$russian_month_names_in_use = true;
					
					if ($target_encoding == '1251')
					{
						// TRICK: Due to Windows bug LC_CTYPE is not affected when locale is set to UTF-8 (65001) and retain cp1251 (1251)
						// Try to guess if this is the case:
						if
						(
							strpos($current_locale, 'LC_TIME=Russian_Russia.65001') !== false &&
							strpos($current_locale, 'LC_COLLATE=Russian_Russia.65001') !== false &&
							strpos($current_locale, 'LC_MONETARY=Russian_Russia.65001') !== false &&
							strpos($current_locale, 'LC_NUMERIC=Russian_Russia.65001') !== false
						)
						{
							// Assume this is the case - fix the bug
							$target_encoding = '65001';
						}
					}
				}
			}
			elseif (preg_match('/^([^.]+)\\.(\\d+)$/', $current_locale, $matches))
			{
				$language_country = $matches[1];
				$target_encoding = $matches[2];

				if (strpos(strtolower($language_country), 'rus') === 0)
				{
					$russian_month_names_in_use = true;
				}
			}
			
			
			if ($russian_month_names_in_use)
			{
				// Map windows encoding notation to unix one
				switch ($target_encoding)
				{
					case '1251': $target_encoding = 'cp1251'; break;
					case '65001': $target_encoding = 'utf-8'; break;
					default:
						trigger_error('strftime2() is not intended for use under Windows with locale encoding other than utf-8 or cp1251', E_USER_WARNING);
					break;
				}
			}
		}
		else
		{
			// This is a server not using Windows

			$current_locale = setlocale(LC_ALL, 0);
			
			if (preg_match('/^([^.]+)\\..*$/', $current_locale, $matches))
			{
				$language_country = $matches[1];
				$target_encoding = $matches[2];
				
				if ($language_country == 'ru_RU')
				{
					$russian_month_names_in_use = true;
				}
			}
		}
		

		if ($russian_month_names_in_use)
		{
			$russian_month_names = array(1 => 'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря');

			$month_index = date('n', $timestamp);
			
			$month_name = $russian_month_names[$month_index];
			
			if ($target_encoding != 'utf-8')
			{
				$month_name = iconv('utf-8', $target_encoding, $month_name);
			}
		}
	}


	if ($russian_month_names_in_use)
	{
		$format = str_replace('%Q', ' sathskehgkhgruk ', $format);
	}
	else
	{
		$format = str_replace('%Q', '%B', $format);
	}

	$result = strftime($format, $timestamp);

	if ($russian_month_names_in_use)
	{
		$result = str_replace(' sathskehgkhgruk ', $month_name, $result);
	}
	
	
	return $result;
}


/**
*
*
* @param
* @param
*
* @return
*/

function date_from_sql($format, $sql_date)
{
	list($Y, $M, $D, $h, $m, $s) = sscanf($sql_date, '%04d-%02d-%02d %02d:%02d:%02d');
	$timestamp = mktime($h, $m, $s, $M, $D, $Y);
	return date($format, $timestamp);
}
?>
