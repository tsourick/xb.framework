<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage ini
*/

/**
* undocumented class
*/

class IniConfigFileParserException extends Exception
{
}

class IniConfigFileParser
{
	private static $data;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function parse($path, $skip_cache = false)
	{
		if (! is_file($path) || ! is_readable($path)) throw new IniConfigFileParserException("'{$path}' is not a readable regular file");

		
		$cpath = $path . '.cached';

		
		if (! $skip_cache && is_file($cpath) && is_readable($cpath) && (filectime($path) < filectime($cpath)) && (filemtime($path) < filemtime($cpath)))
		{
			// Use cached file
			self::$data = parse_ini_file($cpath, true);
		}
		else
		{
			// Use original file
			
			self::$data = parse_ini_file($path, true);
			
			
			// Parse inline references to values
	
			// self::$rawData = $data;
			
			foreach (self::$data as $section_name => $section_vars)
			{
				foreach ($section_vars as $var_name => $var_value)
				{
					/*
					array_walk($section_vars, array('ConfigFileParser', 'parseValue'));
					self::$data[$section_name] = $section_vars;
					*/
					$var_value = self::parseValue($var_value);
					self::$data[$section_name][$var_name] = $var_value;
				}
			}
			
			
			// Try to cache parsed file
			
			$d = '';
			foreach (self::$data as $section_name => $section)
			{
				$d .= '[' . $section_name . ']' . "\n";
				
				foreach ($section as $name => $value)
				{
					$d .= $name . '="' . $value . '"' . "\n";
				}
			}
			
			@file_put_contents($cpath, $d);
		}
		
		
		return self::$data;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	private static function parseValue($value)
	{
		return preg_replace_callback('|\{([^}]*)\}|', array('self', 'replaceValue'), $value);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	private static function replaceValue($matches)
	{
		$value = $matches[1];
		
		if (! empty($value))
		{
			if (strpos($value, '/') !== false)
			{
				list($section_name, $option_name) = explode('/', $value);
				
				if (! array_key_exists($section_name, self::$data)) throw new IniConfigFileParserException("Section not found when parsing inline reference '{$value}'");

				if (! array_key_exists($option_name, self::$data[$section_name])) throw new IniConfigFileParserException("Option not found when parsing inline reference '{$value}'");
				
				$value = self::$data[$section_name][$option_name];
			}
			else
			{
				throw new IniConfigFileParserException("Syntax error in inline reference '{$value}'");
			}
		}
		
		return $value;
	}
}

?>
