<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

abstract class DMLink extends DMLinkBase
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function createFromConfig(DMDataModel $dm, $config, $__class__ = __CLASS__)
	{
		$custom_field_map = isset($config['custom_field_map']) ? $config['custom_field_map'] : NULL;
		
		$from_entity = $dm->entity($config['from']);
		$to_entity = $dm->entity($config['to']);
		
		
		$dmLink = new $__class__($dm, $config['name'], $from_entity, $config['from_required'], $to_entity, $config['to_required'], $custom_field_map);
		
		
		return $dmLink;
	}
}

?>
