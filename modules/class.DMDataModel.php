<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

// require_once('class.DMPlainEntity.php');
// require_once('class.DMPlainTimestampedEntity.php');
// require_once('class.DMCelkoEntity.php');
Framework::useClass('DMPlainEntity');
Framework::useClass('DMPlainTimestampedEntity');
Framework::useClass('DMCelkoEntity');

// require_once('class.DMOneToManyLink.php');
// require_once('class.DMManyToManyLink.php');
// require_once('class.DMOneToOneNaturalLink.php');
Framework::useClass('DMOneToManyLink');
Framework::useClass('DMManyToManyLink');
Framework::useClass('DMOneToOneNaturalLink');

class DMDataModelException extends ORMException
{
}


class DMDataModel
{
	private $dbc = NULL;
	
	private $name = ''; // Model name

	public $options = array(); // Options

	private $ormDatabase = NULL;
	                                                                   
	private $dmEntities = array(); // Datamodel entities
	private $dmLinks = array(); // Datamodel links
	
	private $makeORMTableNameHandler = NULL;
	private $makeLinkORMFieldNameHandler = NULL;
	
	
	static public $fieldTypeConfigs = array
	(
		'pri_id'           => array('ormTypeConfig' => array('name' => 'id', 'type' => array('name' => 'INT',      'length' => 10,                  'unsigned' => true), 'auto_increment' => true),  'input_type' => 'i',         'input_default' => NULL),
                                                                                                                                                                                                
		'ext_id'           => array('ormTypeConfig' => array(                'type' => array('name' => 'INT',      'length' => 10,                  'unsigned' => true), 'default' => 0          ),  'input_type' => 'i',         'input_default' => NULL),
		'strong_ext_id'    => array('ormTypeConfig' => array(                'type' => array('name' => 'INT',      'length' => 10,                  'unsigned' => true)                          ),  'input_type' => 'i',         'input_default' => NULL),
		                                                                                                                                                                                                                          
		'string'           => array('ormTypeConfig' => array(                'type' => array('name' => 'VARCHAR',  'length' => 255                                    )                          ),  'input_type' => 's',         'input_default' => NULL),
		'text'             => array('ormTypeConfig' => array(                'type' => array('name' => 'TEXT'                                                         )                          ),  'input_type' => 's',         'input_default' => NULL),
                                                                                                                                                                                                                              
		'flag'             => array('ormTypeConfig' => array(                'type' => array('name' => 'TINYINT',  'length' => 1,                   'unsigned' => true), 'default' => 0          ),  'input_type' => 'b',         'input_default' => 0   ),
		'flag_off'         => array('ormTypeConfig' => array(                'type' => array('name' => 'TINYINT',  'length' => 1,                   'unsigned' => true), 'default' => 0          ),  'input_type' => 'b',         'input_default' => 0   ),
		'flag_on'          => array('ormTypeConfig' => array(                'type' => array('name' => 'TINYINT',  'length' => 1,                   'unsigned' => true), 'default' => 1          ),  'input_type' => 'b',         'input_default' => 1   ),
                                                                         
		'date'             => array('ormTypeConfig' => array(                'type' => array('name' => 'DATE'                                                         ), 'null' => true          )                                                       ),
		'date_strong'      => array('ormTypeConfig' => array(                'type' => array('name' => 'DATE'                                                         )                          )                                                       ),
		'time'             => array('ormTypeConfig' => array(                'type' => array('name' => 'TIME'                                                         )                          ),  'input_type' => 'time',      'input_default' => NULL),
		'datetime'         => array('ormTypeConfig' => array(                'type' => array('name' => 'DATETIME'                                                     ), 'null' => true          ),  'input_type' => 'datetime',  'input_default' => NULL),
		'datetime_strong'  => array('ormTypeConfig' => array(                'type' => array('name' => 'DATETIME'                                                     )                          ),  'input_type' => 'datetime',  'input_default' => NULL),
                                                                                                                                                                                                                              
		'created'          => array('ormTypeConfig' => array(                'type' => array('name' => 'DATETIME'                                                     )                          ),  'input_type' => 'datetime',  'input_default' => NULL),
		'modified'         => array('ormTypeConfig' => array(                'type' => array('name' => 'DATETIME'                                                     ), 'null' => true          ),  'input_type' => 'datetime',  'input_default' => NULL),
		'deleted'          => array('ormTypeConfig' => array(                'type' => array('name' => 'DATETIME'                                                     ), 'null' => true          ),  'input_type' => 'datetime',  'input_default' => NULL),
		                                                                                                                                                                                                                          
		'signed'           => array('ormTypeConfig' => array(                'type' => array('name' => 'INT',      'length' => 10                                     ), 'default' => 0          ),  'input_type' => 'i',         'input_default' => NULL),
		'unsigned'         => array('ormTypeConfig' => array(                'type' => array('name' => 'INT',      'length' => 10,                  'unsigned' => true), 'default' => 0          ),  'input_type' => 'i',         'input_default' => NULL),
		                                                                                                                                                                                                                          
		'float_signed'     => array('ormTypeConfig' => array(                'type' => array('name' => 'FLOAT'                                                        ), 'default' => 0          ),  'input_type' => 'f',         'input_default' => NULL),
		'float_unsigned'   => array('ormTypeConfig' => array(                'type' => array('name' => 'FLOAT',                                     'unsigned' => true), 'default' => 0          ),  'input_type' => 'f',         'input_default' => NULL),
		                                                                                                                                                                                                                          
		'double_signed'    => array('ormTypeConfig' => array(                'type' => array('name' => 'DOUBLE'                                                       ), 'default' => 0          ),  'input_type' => 'f',         'input_default' => NULL),
		'double_unsigned'  => array('ormTypeConfig' => array(                'type' => array('name' => 'DOUBLE',                                    'unsigned' => true), 'default' => 0          ),  'input_type' => 'f',         'input_default' => NULL),
                                                                                                                                                                                                                              
		'money'            => array('ormTypeConfig' => array(                'type' => array('name' => 'DECIMAL',  'length' => 10, 'decimals' => 2, 'unsigned' => true), 'default' => 0          ),  'input_type' => 'f',         'input_default' => NULL)
	);
	
	
	/**
	*
	*                                                                                        
	* @param
	* @param
	*
	* @return
	*/
                                                                                                   
	public function __construct($dbc, $name, $options = array())
	{
		$this->dbc = $dbc;
		
		$this->name = $name;


		// Process options

		if (isset($options['makeORMTableNameHandler']) && is_callable($options['makeORMTableNameHandler']))
		{
			$this->makeORMTableNameHandler = $options['makeORMTableNameHandler'];
			
			unset($options['makeORMTableNameHandler']);
		}
		else
		{
			$this->makeORMTableNameHandler = array($this, 'makeORMTableNameDefaultHandler');
		}
			
		if (isset($options['makeLinkORMFieldNameHandler']) && is_callable($options['makeLinkORMFieldNameHandler']))
		{
			$this->makeLinkORMFieldNameHandler = $options['makeLinkORMFieldNameHandler'];
			
			unset($options['makeLinkORMFieldNameHandler']);
		}
		else
		{
			$this->makeLinkORMFieldNameHandler = array($this, 'makeLinkORMFieldNameDefaultHandler');
		}

		
		$this->options = $options; // save the rest of options
		
		
		$this->ormDatabase = new ORMDatabase($dbc, $name);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getDBConnection()
	{
		return $this->dbc;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getName()
	{
		return $this->name;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function database()
	{
		return $this->ORMDatabase();
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function ORMDatabase()
	{
		return $this->ormDatabase;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	private function makeORMTableNameDefaultHandler($entity_name)
	{
		$p = isset($this->options['table_name_prefix']) ? $this->options['table_name_prefix'] : '';
		
		return $p . $entity_name;		
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function makeORMTableName($entity_name)
	{
		return call_user_func($this->makeORMTableNameHandler, $entity_name);
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	private function makeLinkORMFieldNameDefaultHandler($entity_name, $field_name)
	{
		return $entity_name . '_' . $field_name;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function makeLinkORMFieldName($entity_name, $field_name)
	{
		return call_user_func($this->makeLinkORMFieldNameHandler, $entity_name, $field_name);
	}
	
	
	/**
	* $config structure:

	array
	(
		'name' => '<data model/ database name>',
		'entities' => array
		(
			array
			(
				'type' => '(plain|celko)',

				'name' => '<entity name>',
				'fields' => array
				(
					array('name' => '<field name>', 'type' => '<field type>')
				)
			),
			
			...
		),
		'links' => array
		(
			array
			(
				'type' => '(1:1 natural|1:n|n:n)',
				'from' => '<entity name>',
				'from_required' => (true|false),
				'to' => '<entity name>',
				'to_required' => (true|false)
			)
		)
	);
	*/
	
	static public function createFromConfig($dbc, $config)
	{
		$dmDataModel = new self($dbc, $config['name'], $config['options']);
		

		foreach ($config['entities'] as $entity_config)
		{
			$dmDataModel->createEntity($entity_config);
		}
		
		
		if (isset($config['links']))
		{
			foreach ($config['links'] as $link_config)
			{
				$dmDataModel->createLink($link_config);
			}
		}

		
		return $dmDataModel;
	}
	
	/**
	* Config structure:
	* @see DMEntity->__construct()
	*/
	private function createEntity($config)
	{
		switch ($config['type'])
		{
			case 'plain':
				// $dmEntity = new DMPlainEntity($this, $config);
				$dmEntity = DMPlainEntity::createFromConfig($this, $config);
			break;

			case 'celko':
				// $dmEntity = new DMCelkoEntity($this, $config);
				$dmEntity = DMCelkoEntity::createFromConfig($this, $config);
			break;
		}
		// $this->dmEntities[$dmEntity->getName()] = $dmEntity;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function deleteEntity($name)
	{
		if (! isset($this->dmEntities[$name])) throw new DMDataModelException("Could not delete entity '" . $name . "' from DMModel '" . $this->getName() . "'. Entity not found in the model.");

		
		$dmEntity = $this->dmEntities[$name];
		
		// $amEntity = $this->removeEntity($this->amEntities[$name]);
		$dmEntity->destruct(); // this->removeEntity() is called from within entity
		
		
		unset($dmEntity);
	}

	
	/**
	* Config structure:
	* @see DMLink->__construct()
	*/
	private function createLink($config)
	{
		switch ($config['type'])
		{
			case '1:1 natural':
				$dmLink = DMOneToOneNaturalLink::createFromConfig($this, $config);
			break;

			case '1:n':
				$dmLink = DMOneToManyLink::createFromConfig($this, $config);
			break;

			case 'n:n':
				$dmLink = DMManyToManyLink::createFromConfig($this, $config);
			break;
		}
		
		// $this->dmLinks[$dmLink->getName()] = $dmLink;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function deleteLink($name)
	{
		if (! isset($this->dmLinks[$name])) throw new DMDataModelException("Could not delete link '" . $name . "' from model '" . $this->getName() . "'. Link not found in the model.");

		
		$dmLink = $this->dmLinks[$name];
		
		
		$dmLink->destruct(); // this->removeLink() is called from within link
		
		
		unset($dmLink);
	}


	// called from entity
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addEntity(DMEntity $dmEntity)
	{
		if ($dmEntity->dataModel() !== $this) throw new DMDataModelException("Could not add entity '" . $dmEntity->getName() . "' to model '" . $this->getName() . "' because entity belongs to another model.");
		
		
		$this->dmEntities[$dmEntity->getName()] = $dmEntity;
	}
	
	// called from entity
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function removeEntity(DMEntity $dmEntity)
	{
		if ($dmEntity->dataModel() !== $this) throw new DMDataModelException("Could not remove entity '" . $dmEntity->getName() . "' from model '" . $this->getName() . "' because entity belongs to another model.");
		
		
		$name = $dmEntity->getName();
		
		
		if (! isset($this->dmEntities[$name])) throw new DMDataModelException("Could not remove entity '" . $dmEntity->getName() . "' from model '" . $this->getName() . "'. Entity belongs to this model but was not added.");

			
		unset($this->dmEntities[$name]);
	}

	// called from link
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addLink(DMLink $dmLink)
	{
		if ($dmLink->dataModel() !== $this) throw new DMDataModelException("Could not add link '" . $dmLink->getName() . "' to model '" . $this->getName() . "' because link belongs to another model.");
		
		
		$this->dmLinks[$dmLink->getName()] = $dmLink;
	}

	// called from link
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function removeLink(DMLink $dmLink)
	{
		if ($dmLink->dataModel() !== $this) throw new DMDataModelException("Could not remove link '" . $dmLink->getName() . "' from model '" . $this->getName() . "' because link belongs to another model.");
		
		
		$name = $dmLink->getName();
		
		
		if (! isset($this->dmLinks[$name])) throw new DMDataModelException("Could not remove link '" . $dmLink->getName() . "' from model '" . $this->getName() . "'. Link belongs to this model but was not added.");

			
		unset($this->dmLinks[$name]);
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function entity($name)
	{
		if (! isset($this->dmEntities[$name])) throw new DMDataModelException("'$name' entity not exists in data model.");
			
		return $this->dmEntities[$name];
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function link($name)
	{
		if (! isset($this->dmLinks[$name])) throw new DMDataModelException("'$name' link not exists in data model.");
		
		return $this->dmLinks[$name];
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function remapEntity(DMEntity $dmEntity, $old_name)
	{
		if ($dmEntity->dataModel() !== $this) throw new DMDataModelException("Could not remap entity '" . $dmEntity->getName()  . "' from name '" . $old_name . "' because entity belongs to another model.");

		if (! array_key_exists($old_name, $this->dmEntities)) throw new DMDataModelException("Could not remap entity '" . $dmEntity->getName()  . "' from name '" . $old_name . "' because old name was not found within model.");

			
		array_replace_key($this->dmEntities, $old_name, $dmEntity->getName());
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function beginTransaction()
	{
		return $this->ormDatabase->beginTransaction();
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function commit()
	{
		return $this->ormDatabase->commit();
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function rollBack()
	{
		return $this->ormDatabase->rollBack();
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function inTransaction()
	{
		return $this->ormDatabase->inTransaction();
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function query($qset)
	{
		return $this->ormDatabase->query($qset);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function exec($qset)
	{
		return $this->ormDatabase->exec($qset);
	}
	
	/*
	// DBConnection methods
	
	public function query($qset)
	{
		return $this->dbc->query($qset);
	}

	public function exec($qset)
	{
		return $this->dbc->exec($qset);
	}


	public function select($fields, $storage = '', $condition = NULL, $group = '', $order = '', $limits = NULL)
	{
		return $this->dbc->select($fields, $storage, $condition, $group, $order, $limits);
	}

	public function getRow($fields, $storage = '', $condition = NULL)
	{
		return $this->dbc->getRow($fields, $storage, $condition);
	}

	public function getRowAll($storage, $condition = NULL)
	{
		return $this->dbc->getRowAll($storage, $condition);
	}

	public function getRowCustom($select, $storage = '', $condition = NULL)
	{
		return $this->dbc->getRowCustom($select, $storage, $condition);
	}

	public function getRowValue($field_name, $storage = '', $condition = NULL)
	{
		return $this->dbc->getRowValue($field_name, $storage, $condition);
	}

	public function getRowCount($table, $condition = NULL, $group = '')
	{
		return $this->dbc->getRowCount($table, $condition, $group);
	}

	public function getRows($fields, $storage = '', $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dbc->getRows($fields, $storage, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	public function getRowsAll($storage, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dbc->getRowsAll($storage, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	public function getRowsCustom($select, $storage = '', $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dbc->getRowsCustom($select, $storage, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	public function delete($storage, $using = NULL, $condition = NULL, $order = '', $limit = '')
	{
		return $this->dbc->delete($storage, $using, $condition, $order, $limit);
	}

	public function update($storage, $set, $condition = NULL, $order = '', $limit = '')
	{
		return $this->dbc->update($storage, $set, $condition, $order, $limit);
	}

	public function ins($table, $values)
	{
		return $this->dbc->ins($table, $values);
	}

	public function insert($table, $values, $fields = array())
	{
		return $this->dbc->insert($table, $values, $fields);
	}

	public function replace($table, $values, $fields = array())
	{
		return $this->dbc->replace($table, $values, $fields);
	}

	public function queryGetRows($qset, $row_key_field = '', $row_value_field = '')
	{
		return $this->dbc->queryGetRows($qset, $row_key_field, $row_value_field);
	}

	public function beginTransaction()
	{
		return $this->dbc->beginTransaction();
	}

	public function commit()
	{
		return $this->dbc->commit();
	}

	public function rollBack()
	{
		return $this->dbc->rollBack();
	}

	public function inTransaction()
	{
		return $this->dbc->inTransaction();
	}
	*/
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getDDL($options = array())
	{
		$touchDatabase = isset($options['touchDatabase']) ? $options['touchDatabase'] : true;
		
		
		if ($touchDatabase)
		{
			$qset = $this->ormDatabase->SQLDDLGetCreateQuerySet();
		}
		else
		{
			$qset = $this->ormDatabase->SQLDDLGetFillQuerySet();
		}
		
		return $qset;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function init($options = array())
	{
		$touchDatabase = isset($options['touchDatabase']) ? $options['touchDatabase'] : true;
		
		
		if ($touchDatabase)
		{
			$qset = $this->ormDatabase->SQLDDLGetCreateQuerySet();
		}
		else
		{
			$qset = $this->ormDatabase->SQLDDLGetFillQuerySet();
		}
		
		echo implode(';<br>', $qset);
		
		// $this->dbc->exec($qset);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function deinit($options = array())
	{
		$touchDatabase = isset($options['touchDatabase']) ? $options['touchDatabase'] : true;
		
		
		if ($touchDatabase)
		{
			$qset = $this->ormDatabase->SQLDDLGetDropSQLDump();
		}
		else
		{
			$qset = $this->ormDatabase->SQLDDLGetEmptySQLDump();
		}
		
		
		echo implode(';<br>', $qset);
		
		// $this->dbc->exec($qset);
	}
}

?>