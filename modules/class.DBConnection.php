<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage db
*/

// require_once('class.SQL.php');
Framework::useClass('SQL');

/**
* Database access library
*/

/**
* DBConnection class exception
*/

class DBConnectionException extends PDOException
{
	public function __construct($message)
	{
		parent::__construct($message);
	}
}

/**
* PDO-based database connection
*/

class DBConnection
{
	const FETCH_BOTH = PDO::FETCH_BOTH;
	const FETCH_ASSOC = PDO::FETCH_ASSOC;
	const FETCH_NUM = PDO::FETCH_NUM;
	
	/**
	* @var PDO
	* PDO object reference
	*/
	private $pdo;
	
	/**
	* @var dbName
	* Database name from DSN, if available, NULL otherwise
	*/
	private $dbName = NULL;

	/**
	* @var bool
	* if true then object is in idle mode when no query is really run
	*/
	private $idle = false;
	/**
	* @var string
	* holds latest query
	*/
	private $lastQuery = '';

	/**
	* @var bool
	* if true all queries are logged with logger_log()
	*/
	private $log = false;
	
	/**
	* @var bool
	* if true stat is collected
	*/
	private $stat = false;

	/**
	* @var string
	* holds unix timestamp of now given by underlaying DBMS
	* @see nowTimestamp(), nowDateTime()
	*/
	private $now = NULL;
	
	/**
	* @var integer
	* holds PDO's fetch mode used when fetching rows
	* @see {PHP_MANUAL#PDOStatement::fetch()}
	*/
	private $fetchMode = PDO::FETCH_BOTH;
	
	/**
	* @var boolean
	* indicates whether a transaction is active
	* @see inTransaction()
	*/
	private $inTransaction = false;
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($dsn, $username = '', $password = '', array $driver_options = NULL)
	{
		try
		{
			$dsn_parts = explode_params($dsn, ';=');
			
			if (isset($dsn_parts['dbname'])) $this->dbName = $dsn_parts['dbname'];
			
			
			$pdo = new PDO($dsn, $username, $password, $driver_options);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
			
			$this->pdo = $pdo;
		}
		catch (PDOException $e)
		{
			$message = $e->getMessage(); 

			throw new DBConnectionException($message);
		}
	}
	
	
	private $statTotalTime = 0;
	private $statLastTime = 0;
	
	private $statStartTime = 0;
	private function statStart()
	{
		$this->statStartTime = microtime(true);
	}
	private function statStop()
	{
		$this->statLastTime = microtime(true) - $this->statStartTime;
		$this->statTotalTime += $this->statLastTime;
	}
	public function statLog()
	{
		logger_log('Last query time: ' . $this->statTotalTime);
		logger_log('Total time: ' . $this->statTotalTime);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getDBName()
	{
		return $this->dbName;
	}

	/**
	* Switches between regular/idle modes or retrieves current mode.
	* In idle mode all the actions are done and query is prepared to run but not run.
	* Function is intended for debugging when there is a need to inspect latest query but
	* query itself should not be run.
	*
	* @param bool $idle if true, mode is idle; if false, mode is regular; if NULL, current mode returned
	* @return bool current mode when $idle provided; true otherwise
	*/
	public function idle($idle = NULL)
	{
		if ($idle !== NULL)
		{
			settype($idle, 'boolean');
			
			$this->idle = $idle;
			
			return true;
		}
		else return $this->idle;
	}
	
	/**
	* Returns latest query which should have been executed.
	* The query is not executed in idle mode.
	*/
	public function getLastQuery()
	{
		return $this->lastQuery;
	}
	
	public function logLastQuery()
	{
		logger_log($this->lastQuery);
	}

	public function dumpLastQuery()
	{
		dump($this->lastQuery);
	}

	/**
	* Enables/disables query logging
	*/
	public function log($log = NULL)
	{
		if ($log !== NULL)
		{
			settype($log, 'boolean');
			
			$this->log = $log;
			
			return true;
		}
		else return $this->log;
	}

	/**
	* Enables/disables stat collecting
	*/
	public function stat($b = NULL)
	{
		if ($b !== NULL)
		{
			settype($b, 'boolean');
			
			$this->stat = $b;
			
			return true;
		}
		else return $this->stat;
	}

	/**
	* Magic PHP method which hooks all the calls to non-existant methods of this class.
	* This magic method translates calls to underlying PDO object.
	*/
	public function __call($m, $p)
	{
		if (method_exists($this->pdo, $m))
		{
			return call_user_func_array(array($this->pdo, $m), $p);
		}
		else
		{
			trigger_error('Call to undefined method ' . __CLASS__ . '::' . $m . '()', E_USER_ERROR);
		}
	}

	/**
	* Returns unix timestamp returned by nowTimestamp formatted as 'Y-m-d H:i:s'
	*
	* @param bool $reset re-retrieve timestamp from DMBS
	* @return string SQL date and time
	*
	* @see nowTimestamp
	*/
	public function nowDateTime($reset = false)
	{
		$now = $this->nowTimestamp($reset);
		
		return date('Y-m-d H:i:s', $now);
	}
	
	/**
	* Returns unix timestamp returned by nowTimestamp formatted as 'Y-m-d'
	*
	* @param bool $reset re-retrieve timestamp from DMBS
	* @return string SQL date
	*
	* @see nowTimestamp
	*/
	public function nowDate($reset = false)
	{
		$now = $this->nowTimestamp($reset);
		
		return date('Y-m-d', $now);
	}

	/**
	* Returns unix timestamp returned by nowTimestamp formatted as 'H:i:s'
	*
	* @param bool $reset re-retrieve timestamp from DMBS
	* @return string SQL time
	*
	* @see nowTimestamp
	*/
	public function nowTime($reset = false)
	{
		$now = $this->nowTimestamp($reset);
		
		return date('H:i:s', $now);
	}

	/**
	* Returns unix timestamp of now retrieved from underlaying DBMS
	* The value is retrieved once from DBMS and stored in $now. Consequent calls
	* retrieve stored value until $reset set to true
	*
	* @param bool $reset re-retrieve timestamp from DMBS
	* @return string unix timestamp
	*/
	public function nowTimestamp($reset = false)
	{
		if ($this->now === NULL || $reset)
		{
			$pdo = $this->pdo;
			
			$this->lastQuery = 'SELECT UNIX_TIMESTAMP() now';
			
			$s = $pdo->query($this->lastQuery);
			$row = $s->fetch();
	
			$this->now = $row['now'];
		}
		
		return $this->now;
	}

	/**
	* Create IN-condition
	* Creates condition with MySQL IN comparision operator from given array of values.
	*
	* @param string $name the expression to compare against the values; usually the name of a field
	* @param array $values an array of values to campare name against
	* @param bool $negate if true then NOT IN will be used instead of IN; default if false
	* @return string IN-condition which is ready to be inserted into plain SQL query
	* @static
	*/
/*
	public function in($name, $values, $negate = false)
	{
		$this->_name($name);
		array_walk($values, array($this, '_value'));
		$not = $negate ? 'NOT ' : '';
		return $name . ' ' . $not . 'IN (' . implode(',', $values) . ')';
	}
*/

	/**
	* Make SQL identifier be SQL-ready
	* Quotes SQL identifier with backticks. The function recognizes and quotes field, field AS alias,
	* table.field and table.field AS alias expressions.
	*
	* @param string $name the identifier to prepare
	* @return string SQL-ready string
	*/
/*
	private function prepare_name($name)
	{
		$this -> _name($name);
		
		
		return $name;
	}
*/
	
	/**
	* Make SQL identifier be SQL-ready
	* Quotes SQL identifier with backticks. The function recognizes and quotes field, field AS alias,
	* table.field and table.field AS alias expressions.
	* Nothing is returned but original value is modified.
	*
	* @param string $name reference to the value to quote
	* @internal
	* @static
	*/
/*
	public function _name(&$name)
	{
		$pieces = array();
		//wechos('Field: ' . $field);
		preg_match('/^([a-zA-Z_][\w-]*)(?:\.([a-zA-Z_][\w]*))?(?:\s+(?:(?:as)\s+)?(.+))?$/i', trim($name), $pieces);
		//printr($pieces);
		assert(isset($pieces[1]) && ! empty($pieces[1]));
		
		$name = '`' . $pieces[1] . '`';
		if (isset($pieces[2]) && ! empty($pieces[2])) $name .= '.`' . $pieces[2] . '`';
		if (isset($pieces[3]) && ! empty($pieces[3])) $name .= ' `' . $pieces[3] . '`';
	}
*/

	/**
	* Make SQL value be SQL-ready
	* Escapes and quotes SQL identifier with single quotes.
	*
	* @param string $value the value to prepare
	* @return string SQL-ready string
	*/
/*
	public function prepare_value($value)
	{
		$this -> _value($value);
		
		
		return $value;
	}
*/

	/**
	* Make SQL value be SQL-ready
	* Escapes and quotes SQL identifier with single quotes.
	*
	* @param string $value reference to the value to escape and quote
	* @return string SQL-ready string
	* @internal
	* @static
	*/
/*
	public function _value(&$value)
	{
		$value = '\'' . addslashes($value) . '\'';
	}
*/

	/**
	* Sets fetch mode used when fetching rows
	*/
	public function setFetchMode($mode)
	{
		$this->fetchMode = $mode;
	}
	

	/**
	* Converts $fields to SQL-string ready for query
	*/
	
	static private function _process_fields($fields)
	{
		$prepare_fields = true;
		
		if (! is_array($fields))
		{
			if (substr($fields, 0, 1) != '!')
			{
				$fields = explode(',', $fields);
			}
			else
			{
				$fields = substr($fields, 1);
				$prepare_fields = false;
			}
		}

		$_fields = '';
		if ($prepare_fields)
		{
			array_walk($fields, array('SQL', '_name'));
			$_fields = implode(',', $fields);
		}
		else $_fields = $fields;
		
		return $_fields;
	}
	
	/**
	* Wrapper for mysql_query('SELECT ...')
	* Runs SELECT query. Query is not run in idle mode (i.e. when $this -> idle is set to true) and true is returned.
	*
	* @param mixed $fields "select" part of SQL query
	* If $fields is an array, then it is treated as a set of field names to retrieve. Every field is properly escaped.
	* If $fields is a string, then it is firstly split into an array by comma and then treated as it was passed
	* in an array (mentioned before)
	* @param string $storage table to select from. This parameter IS NOT escaped/quoted!
	* @param mixed $condition "where" part of SQL query
	* If $condition is a string, then it is placed as is into a query.
	* If $condition is an array, then it is treated as a set of conditions united with AND logic
	* where key is a field name and value is a field value.
	* @param string $group "group by" part of SQL query. This parameter IS NOT escaped/quoted!
	* @param string $order "order by" part of SQL query. This parameter IS NOT escaped/quoted!
	* @param array $limits an array of one or two integers representing number of rows to retrieve or
	* an offset and a number of rows to retrieve
	* @return resource|true|errors resource on success, true in idle mode or errors instance on error
	*/

	public function select($fields, $storage = '', $condition = NULL, $group = '', $order = '', $limits = NULL)
	{
		assert(! empty($fields));
		
		$_select = 'SELECT ';
		$_from = '';
		$_where = '';
		$_group_by = '';
		$_order_by = '';
		$_limit = '';

		
		$_select .= self::_process_fields($fields);

		
		if (! empty($storage)) $_from = ' FROM ' . $storage;
		
		if (! empty($condition)) $_where = ' WHERE ' . self::_process_condition($condition);
		
		if (! empty($group)) $_group_by = ' GROUP BY ' . $group;
		
		if (! empty($order)) $_order_by = ' ORDER BY ' . $order;
		
		if (! empty($limits))
		{
			$_limit = ' LIMIT ' . implode(',', $limits);
		}

		
		$q = $_select . $_from . $_where . $_group_by . $_order_by . $_limit;

		$s = $this->_query($q);
		
		return $s;
	}
	
	/**
	* Get row from a table
	* This method calls select() and fetches one row then. 
	*
	* @param mixed $fields the same as select() accepts
	* @param string $storage the same as select() accepts
	* @param mixed $condition the same as select() accepts
	* @return array|false array of values on success, false when there are no rows or errors instance on error
	*
	* @see select()
	*/
	private function _getRow($fields, $storage = '', $condition = NULL)
	{
		$s = $this->select($fields, $storage, $condition);
		
		$row = $s->fetch($this->fetchMode);

		return $row;
	}
	
	/**
	* Get row from a table upon SQL query
	*
	* @param mixed $fields the same as select() accepts
	* @param string $storage the same as select() accepts
	* @param mixed $condition the same as select() accepts
	* @return array|false array of values on success, false when there are no rows or errors instance on error
	*
	* @see select()
	*/
	public function getRow($fields, $storage = '', $condition = NULL)
	{
		return $this->_getRow($fields, $storage, $condition);
	}
	
	/**
	* Get all the field values of row from a table
	*
	* @see getRow()
	*/
	public function getRowAll($storage, $condition = NULL)
	{
		return $this->_getRow('!*', $storage, $condition);
	}
	
	/**
	* Makes query with customized SELECT part
	*
	* @see getRow()
	*/
	public function getRowCustom($select, $storage = '', $condition = NULL)
	{
		return $this->_getRow('!' . $select, $storage, $condition);
	}
	
	/**
	* Get value of particular field in a row from a table
	*
	* @param string $field_name name of table field
	* @return mixed value of the field
	*
	* @see getRow()
	*/
	public function getRowValue($field_name, $storage = '', $condition = NULL)
	{
		$value = NULL;
		
		$row = $this->_getRow($field_name, $storage, $condition);
		
		if (! empty($row)) $value = $row[$field_name];
		
		return $value;
	}
	
	/**
	* Get count of rows in a table matching given condition
	* Runs SELECT COUNT(*) ... query and fetches the result.
	* Query is not run in idle mode (i.e. when $this -> idle is set to true) and true is returned.
	*
	* @param string $table a table to search in. This parameter IS NOT escaped/quoted!
	* @param mixed $condition "where" part of SQL query
	* If $condition is a string, then it is placed as is into a query.
	* If $condition is an array, then it is treated as a set of conditions united with AND logic where key is a field name and value
	* is a field value.
	* @param string $group "group" part of SQL query. This parameter IS NOT escaped/quoted!
	* @return integer|true matching rows count on success, true in idle mode or errors instance on error
	*/
	
	public function getRowCount($table, $condition = NULL, $group = '')
	{
		// DEBUG: [[
		/*
		assert(! empty($table));
		assert($r = mysql_query($q = 'SHOW TABLES FROM `' . $this -> dbname . '`'));

		$tables = array();
		
		while (list($v) = mysql_fetch_row($r)) $tables[] = $v;

		assert(in_array($table, $tables));
		*/
		// ]]

		
		$_select = 'SELECT COUNT(*) ';
		$_from = ' FROM ' . $table;
		$_where = '';
		$_group_by = '';
		
		if (! empty($condition)) $_where = ' WHERE ' . self::_process_condition($condition);
		
		if (! empty($group)) $_group_by = ' GROUP BY ' . $group;

		
		$q = $_select . $_from . $_where . $_group_by;

		$s = $this->_query($q);
		
		list($count) = $s->fetch(PDO::FETCH_NUM);
		
		return $count;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	private function _fetchRows($s, $row_key_field = '', $row_value_field = '')
	{
		$rows = array();
		
		if ($s)
		{
			if (empty($row_key_field) && empty($row_value_field))
			{
				while ($row = $s->fetch($this->fetchMode)) $rows[] = $row;
			}
			elseif (! empty($row_key_field) && empty($row_value_field))
			{
				while ($row = $s->fetch($this->fetchMode))
				{
					assert(isset($row[$row_key_field]));
					
					
					$rows[$row[$row_key_field]] = $row;
				}
			}
			elseif (empty($row_key_field) && ! empty($row_value_field))
			{
				while ($row = $s->fetch($this->fetchMode))
				{
					assert(isset($row[$row_value_field]));
					

					$rows[] = $row[$row_value_field];
				}
			}
			elseif (! empty($row_key_field) && ! empty($row_value_field))
			{
				while ($row = $s->fetch($this->fetchMode))
				{
					assert(isset($row[$row_key_field], $row[$row_value_field]));
					
					
					$rows[$row[$row_key_field]] = $row[$row_value_field];
				}
			}
		}

		return $rows;
	}
	
	/**
	* Get rows from a table
	*
	* @see _getRow()
	*/

	/**
	* Get rows from a table
	* This method calls select() and fetches rows then.
	* If $row_key_field and $row_value_field are empty, the resulting array is array of all retrieved rows with numeric 0-based keys 
	* If $row_key_field is given, keys of the resulting array are values of specified field
	* If $row_key_value is given, keys of the resulting array are 0-based indexes and values are values of specified field
	* If $row_key_field and $row_value_field are given, keys and values of the resulting array are keys and values of specified fields
	*
	* @param mixed $fields the same as select() accepts
	* @param string $storage the same as select() accepts
	* @param mixed $condition the same as select() accepts
	* @param string $group the same as select() accepts
	* @param string $order the same as select() accepts
	* @param mixed $limits the same as select() accepts
	* @param string $row_key_field
	* @param string $row_value_field
	* @return array array of values on success
	*
	* @see select()
	*/
	private function _getRows($fields, $storage = '', $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		$s = $this->select($fields, $storage, $condition, $group, $order, $limits);
		
		return $this->_fetchRows($s, $row_key_field, $row_value_field);
		/*
		$rows = array();
		
		if ($s)
		{
			if (empty($row_key_field) && empty($row_value_field))
			{
				while ($row = $s->fetch($this->fetchMode)) $rows[] = $row;
			}
			elseif (! empty($row_key_field) && empty($row_value_field))
			{
				while ($row = $s->fetch($this->fetchMode))
				{
					assert(isset($row[$row_key_field]));
					
					
					$rows[$row[$row_key_field]] = $row;
				}
			}
			elseif (empty($row_key_field) && ! empty($row_value_field))
			{
				while ($row = $s->fetch($this->fetchMode))
				{
					assert(isset($row[$row_value_field]));
					
					
					$rows[] = $row[$row_value_field];
				}
			}
			elseif (! empty($row_key_field) && ! empty($row_value_field))
			{
				while ($row = $s->fetch($this->fetchMode))
				{
					assert(isset($row[$row_key_field], $row[$row_value_field]));
					
					
					$rows[$row[$row_key_field]] = $row[$row_value_field];
				}
			}
		}
		
		return $rows;*/
	}
	
	/**
	* Get rows from a table
	*
	* @see _getRows()
	*/
	public function getRows($fields, $storage = '', $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->_getRows($fields, $storage, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	* Get all the field values of rows from a table
	*
	* @see _getRows()
	*/
	public function getRowsAll($storage, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->_getRows('!*', $storage, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	* Makes query with customized SELECT part
	*
	* @see _getRows()
	*/
	public function getRowsCustom($select, $storage = '', $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->_getRows('!' . $select, $storage, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
	
	/**
	*
	*
	* $resource
	*/
/*
	function fetch($resource)
	{
		return mysql_fetch_assoc($resource);
	}
*/

	
	/**
	* Implements SQL DELETE query
	*
	* @param mixed $storage table or table set to delete from
	* @param mixed $using table or table set to use when searching for rows to delete
	* @param mixed $condition "where" part of SQL query
	* If $condition is a string, then it is placed as is into a query.
	* If $condition is an array, then it is treated as a set of conditions united with AND logic
	* where key is a field name and value is a field value.
	* @param string $order "order by" part of SQL query, allowed only in single table mode in conjunction with $limit
	* @param string $limit "limit" part of SQL query, maximum rows number to delete, allowed only in single table mode
	*/
	
	public function delete($storage, $using = NULL, $condition = NULL, $order = '', $limit = '')
	{
		// DEBUG: [[
		assert(! empty($storage));
		
		// 1 - order and limit can be used only in a single table mode
		// 2 - order without limits is a silly
		if (! empty($using))
		{
			if (! empty($order) || ! empty($limit)) assert(false);
		}
		else
		{
			if (! empty($order) && empty($limit)) assert(false);
		}
		// ]]
		
		$_delete = 'DELETE ';
		$_from = ' FROM ';
		$_using = '';
		$_where = '';
		$_order_by = '';
		$_limit = '';
		

		$prepare_storage = true;
		
		if (! is_array($storage))
		{
			if (substr($storage, 0, 1) != '!')
			{
				$storage = explode(',', $storage);
			}
			else
			{
				$storage = substr($storage, 1);
				$prepare_storage = false;
			}
		}

		if ($prepare_storage)
		{
			array_walk($storage, array('SQL', '_name'));
			$_from .= implode(',', $storage);
		}
		else $_from .= $storage;

		if (! empty($using))
		{
			if (is_array($using))
			{
				array_walk($using, array('SQL', '_name'));
				$_using =  ' USING ' . implode(', ', $using);
			}
			else $_using = ' USING ' . $using;
		}

		if (! empty($condition)) $_where = ' WHERE ' . self::_process_condition($condition);
		
		if (! empty($order)) $_order_by = ' ORDER BY ' . $order;
		
		if (! empty($limit))
		{
			$_limit = ' LIMIT ' . $limit;
		}

		
		$q = $_delete . $_from . $_using . $_where . $_order_by . $_limit;
		
		$r = $this->_exec($q);
		
		return $r;
	}
	

	/**	
	* Implements SQL TRUNCATE query
	*/
	public function truncate($table_name)
	{
		$_truncate = 'TRUNCATE ';
		
	
		$table_name = SQL::prepare_name($table_name);
		

		$q = $_truncate . $table_name;
		
		$r = $this->_exec($q);
		
		return $r;
	}

	/**
	* Implements SQL UPDATE query
	*
	* @param mixed $storage table or table set to use when updating
	* @param mixed $set array, containing updatable fields and values to update with
	* The key stays for a storage container and the value stays for a value assigned.
	* @param mixed $condition "where" part of SQL query
	* If $condition is a string, then it is placed as is into a query.
	* If $condition is an array, then it is treated as a set of conditions united with AND logic
	* where key is a field name and value is a field value.
	* @param string $order "order by" part of SQL query, allowed only in single table mode in conjunction with $limit
	* @param string $limit "limit" part of SQL query, maximum rows number to delete, allowed only in single table mode
	*
	* NB: Multi-table mode is triggered when $storage is array and has more than 1 elements. Otherwise, the mode is treated
	* as single-table, so, neither ORDER and LIMIT usage checking is done nor $storage contents (i.e. UPDATE clause contents)
	* are escaped.
	*/
	
	public function update($storage, $set, $condition = NULL, $order = '', $limit = '')
	{
		// DEBUG: [[
		assert(! empty($storage));
		assert(! empty($set));
		
		// 1 - order and limit can be used only in a single table mode
		// 2 - order without limits is a silly
		if (is_array($storage) && count($storage) > 1)
		{
			if (! empty($order) || ! empty($limit)) assert(false);
		}
		else
		{
			// if (! empty($order) && empty($limit)) assert(false);
		}
		// ]]
		
		$_update = 'UPDATE ';
		$_set = ' SET ';
		$_where = '';
		$_order_by = '';
		$_limit = '';
		
		if (is_array($storage))
		{
			array_walk($storage, array('SQL', '_name'));
			$_update .= implode(', ', $storage);
		}
		else $_update .= $storage;
		
		if (is_array($set))
		{
			$pairs = array();
			
			reset($set);
			while (list($k, $v) = each($set))
			{
				$pairs[] = SQL::prepare_name($k) . '=' . SQL::prepare_value($v);
			}

			$_set .= implode(', ', $pairs);
		}
		else $_set .= $set;

		if (! empty($condition)) $_where = ' WHERE ' . self::_process_condition($condition);
		
		if (! empty($order)) $_order_by = ' ORDER BY ' . $order;
		
		if (! empty($limit))
		{
			$_limit = ' LIMIT ' . $limit;
		}

		
		$q = $_update . $_set . $_where . $_order_by . $_limit;
		
		$r = $this->_exec($q);

		return $r;
	}

	/**
	* Short form of insert()
	*
	* @param string $table name of a table to insert to
	* @param array $values an associative array of the form <field name>=<value>
	* @return result of mysql_query(), true (when idle mode is set) or error
	*/
	
	public function ins($table, $values)
	{
		return $this->insert($table, array_values($values), array_keys($values));
	}
	
	/**
	* Implements SQL INSERT query
	*
	* @param string $table name of a table to insert to
	* @param mixed $values string, one-dimensional or multi-dimensional array
	* If given as string it is passed as is and neither escaping nor quoting is done.
	* If given as one-dimensional array it is walked with escaping/quoting function.
	* If given as multi-dimensional array it is walked with escaping/quoting function
	* and the multi-insert is done so that every element of given array is treated as
	* array of the row data. 
	* @param array $fields an array of field names in the same order which $values are given in
	* @return result of mysql_query(), true (when idle mode is set) or error
	*/

	public function insert($table, $values, $fields = array())
	{
		return $this->_insert($table, $values, $fields);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function replace($table, $values, $fields = array())
	{
		return $this->_insert($table, $values, $fields, true);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	private function _insert($table, $values, $fields = array(), $replace = false)
	{
		// DEBUG: [[
		assert(! empty($table));
		assert(! empty($values));
		// ]]
		
		
		$_insert = $replace ? 'REPLACE INTO ' : 'INSERT INTO ';
		$_fields = '';
		$_values = ' VALUES ';
		
		
		$_insert .= SQL::prepare_name($table);
		
		if (! empty($fields)) 
		{
			array_walk($fields, array('SQL', '_name'));
			$_fields = ' (' . implode(',', $fields) . ')';
		}

		if (is_array($values))
		{
			if (is_array($values[0]))
			{
				$rows = array();
				
				reset($values);
				while (list($k, $v) = each($values))
				{
					array_walk($v, array('SQL', '_value'));
					$rows[] = '(' . implode(',', $v) . ')';
				}
				
				$_values .= implode(',', $rows);
			}			
			else
			{
				array_walk($values, array('SQL', '_value'));
				$_values .= '(' . implode(',', $values) . ')';
			}
		}
		else $_values .= '(' . $values . ')';
		

		$q = $_insert . $_fields . $_values;
		
		$r = $this->_exec($q);

		return $r;
	}
	
	/**
	* Wrapper for PDO::query()
	* Respects lastQuery, idle and DBConnectionException of this class.
	*
	* @param string $q SQL query
	* @return PDOStatement all the PDO::query() returns
	*
	* @see {PHP_MANUAL#PDO::query()}
	*/
	private function _query($q)
	{
		$this->lastQuery = $q;
		
		if ($this->log)
		{
			logger_log($this->lastQuery);
		}
	
		if ($this->idle) $s = new PDOStatement;
		else
		{
			try
			{
				if ($this->stat)
				{
					$this->statStart();
				}


				$s = $this->pdo->query($this->lastQuery);

				
				if ($this->stat)
				{
					$this->statStop();
					
					if ($this->log)
					{
						$this->statLog();
					}
				}
				
			}
			catch (PDOException $e)
			{
				$message = $e->getMessage(); 
				// $code = $e->getCode();
				// $file = $e->getFile();
				// $line = $e->getLine();    

				throw new DBConnectionException($message . ' [' . $this -> lastQuery . ']');
			}
		}
		
		return $s;
	}
	
	/**
	* Wrapper for PDO::exec()
	* Respects lastQuery, idle and DBConnectionException of this class.
	*
	* @param string $q SQL query
	* @return int|false all the PDO::exec() returns
	*
	* @see {PHP_MANUAL#PDO::exec()}
	*/
	private function _exec($q)
	{
		$this->lastQuery = $q;
		
		if ($this->log)
		{
			logger_log($this->lastQuery);
		}
		
		if ($this->idle) $r = false;
		else
		{
			try
			{
				if ($this->stat)
				{
					$this->statStart();
				}
				

				$r = $this->pdo->exec($this->lastQuery);

				
				if ($this->stat)
				{
					$this->statStop();
					
					if ($this->log)
					{
						$this->statLog();
					}
				}
			}
			catch (PDOException $e)
			{
				$message = $e->getMessage(); 
				// $code = $e->getCode();
				// $file = $e->getFile();
				// $line = $e->getLine();    

				throw new DBConnectionException($message . ' [' . $this -> lastQuery . ']');
			}
		}
		
		return $r;
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function queryGetRows($qset, $row_key_field = '', $row_value_field = '')
	{
		$s = $this->query($qset);
		
		return $this->_fetchRows($s, $row_key_field, $row_value_field);
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function queryGetRow($qset)
	{
		$s = $this->query($qset);
		
		$row = $s->fetch($this->fetchMode);
	
		return $row;
	}


	/**
	* Overloads PDO's query.
	* If number of passed arguments more than 1 then PDO::query() call invoked with all the arguments passed.
	* If number of passed arguments is 1 then overloaded algorithm used.
	
	* In this case multiple query run is enabled.
	* If the argument is array then every element is treated as a string of SQL query. All the queries are run
	* continuously one by one and the result (i.e. PDOStatement object) of the last query run is returned.
	* If the argument is a string then it is treated as a single query.
	*/

	public function query($qset)
	{
		$args_num = func_num_args();
		
		if ($args_num == 1)
		{
			if (! is_array($qset)) $qset = array($qset);
	
	
			while (list(, $q) = each($qset))
			{
				$s = $this->_query($q);
			}
		}
		else
		{
			$this->lastQuery = func_get_arg(0);
		
			if ($this->idle) $s = new PDOStatement;
			else
			{
				try
				{
					$args = func_get_args();
					$s = call_user_func_array(array($this->pdo, 'query'), $args);
				}
				catch (PDOException $e)
				{
					$message = $e->getMessage(); 
					// $code = $e->getCode();
					// $file = $e->getFile();
					// $line = $e->getLine();    
	
					throw new DBConnectionException($message . ' [' . $this->lastQuery . ']');
				}
			}
		}


		return $s;
	}

	/**
	* Executes one or more queries with _exec() 
	*
	* @param array|string $qset one or several queries to execute
	* @return mixed result of last _exec() call
	*
	* @see _exec()
	*/
	public function exec($qset)
	{
		if (! is_array($qset)) $qset = array($qset);


		while (list(, $q) = each($qset))
		{
			$r = $this->_exec($q);
		}


		return $r;
	}

	/**
	* Retrieves latest DB error info in printable form
	*
	* @deprecated
	* @see getPrintableErrorInfo()
	*/

	public function get_error_info()
	{
		return $this->getPrintableErrorInfo();
	}

	/**
	* Retrieves latest DB error info in printable form
	*/

	public function getPrintableErrorInfo()
	{
		$error_info = $this->pdo->errorInfo();
		
		return 'DB error (' . $error_info[0] . ', ' . $error_info[1] . '): ' .$error_info[2];
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function beginTransaction()
	{
		$this->pdo->beginTransaction();
		
		$this->inTransaction = true;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function commit()
	{
		$this->pdo->commit();

		$this->inTransaction = false;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function rollBack()
	{
		$this->pdo->rollBack();

		$this->inTransaction = false;
	}

	/**
	* Returns true if a transaction is active, false otherwise.
	* ATTENTION: This method does not return proper value if a transaction was autocommitted.
	* This is only valid when transaction is finished (committed or rolled back) manually with
	* commit() or rollBack().
	*/
	
	public function inTransaction()
	{
		return $this->inTransaction;
	}
/*
	function close()
	{
		$R = NULL;
		
		
		if ($R = mysql_close($this -> link)) $this -> link = NULL;
		else
		{
			$E = new errors;
	 		$E -> add(ER_DB_CLOSE);
			$R = $E;
		}
		
		
		return $R;
	}
*/
	

	/**
	* Executes MYSQL GET_LOCK()
	*/
	
	public function getLock($lock_name, $timeout)
	{
		$row = $this->getRowCustom('GET_LOCK("'.$lock_name.'", ' . $timeout . ') AS result');
		
		if (intval($row['result']) != 1) throw new DBConnectionException('GET_LOCK() did not return 1 [' . $this->lastQuery . ']');
	}

	/**
	* Executes MYSQL RELEASE_LOCK()
	*/
	
	public function releaseLock($lock_name)
	{
		$row = $this->getRowCustom('RELEASE_LOCK("'.$lock_name.'") AS result');
	}
	
		
	/**
	* Performs simple locking with LOCK TABLES for a set of tables.
	* An argument of array type should be passed for every table to be locked. The array should have three elements with numeric
	* indices 0, 1 and 2 wich define table name, aliases and locking mode accordingly. Table name is defined in a string.
	* Aliases are defined in a comma-separated string. Mode can be either 'w' or 'r' which initiates write lock or read lock
	* accordingly.
	*/
	
	public function begin_query()
	{
		// DEBUG: [[
		assert(func_num_args() > 0);
		// ]]
		
		
		$locks = array();
		
		$args = func_get_args();

		reset($args);
		while (list(, $lock_definition) = each($args))
		{
			list($table, $aliases, $mode) = $lock_definition;

			// DEBUG: [[
			assert(in_array($mode, array('w', 'r'))); 
			// ]]
			
			$mode = $mode == 'w' ? 'WRITE' : 'READ';
			
			$locks[] = '`' . $table . '` ' . $mode;
			
			if (! empty($aliases))
			{
				$aliases = explode(',', $aliases);

				reset($aliases);
				while (list(, $alias) = each($aliases))
				{
					$locks[] = '`' . $table . '` AS ' . $alias . ' ' . $mode;
				}
			}
		}
		
		$q = 'LOCK TABLES ' . implode(', ', $locks);
		
		return $this->exec($q);
	}
	
	
	/**
	* Releases locks set with begin_query().
	*/
	
	public function end_query()
	{
		return $this->exec('UNLOCK TABLES');
	}

	/**
	* Prepares condition before it becames a part of SQL query.
	* When $condition is array, every key/value pair is interpreted as field name/field value,
	* and every pair is joint with AND operator. Names and values are properly quoted with backticks and quotes
	* accordingly. <table name>.<field name> form of a field is recognized.
	* When $condition is a string, it is passed through without any modifications.
	*
	* @param $condition
	* @return string prepared condition
	*/

	static private function _process_condition($condition)
	{
		$R = '';
		
		
		if (is_array($condition))
		{
			$pairs = array();
			
			foreach ($condition as $k => $v)
			{
				if (! is_int($k))
				{
					if (is_null($v))
					{
						$pairs[] = SQL::prepare_name($k) . ' IS ' . SQL::prepare_value($v);
					}
					else
					{
						$pairs[] = SQL::prepare_name($k) . '=' . SQL::prepare_value($v);
					}
				}
				else $pairs[] = $v;
			}

			$R = implode(' AND ', $pairs);
		}
		else $R = $condition;
		
		
		return $R;
	}
	
	
	/**
	* Condition helper function. Adds condition
	*/
	
	static public function conditionAnd($target_condition, $condition = NULL)
	{
		if (! is_null($condition) && $condition !== '')
		{
			if (is_null($target_condition))
			{
				$target_condition = $condition;
			}
			elseif (is_string($target_condition))
			{
				if (is_array($condition))
				{
					$condition = self::_process_condition($condition);
				}

				$target_condition .= ' AND ' . $condition;
			}
			elseif (is_array($target_condition))
			{
				if (is_array($condition))
				{
					$target_condition = array_merge($target_condition, $condition);
				}
				else
				{
					$target_condition[] = $condition;
				}
			}
			else
			{
				assert(true);
			}
		}
		
		
		return $target_condition;
	}
	
	
	/**
	* Saves row in a table.
	* Inserts new or updates existant row. Checks row existance by value in the field given in
	* $primary_key_field. Field values are passed in data in a form "field name => field value".
	*
	* @param string $table_name name of a table
	* @param array $data row field values
	* @param string $primary_key_name name of a field treated as primary key
	* @return int|bool all the PDO::exec() returns
	*/
	public function saveRow($table_name, $data, $primary_key_name = 'id')
	{
		$r = true;
		

		$mode = 'insert';
		
		if (array_key_exists($primary_key_name, $data))
		{
			$count = $this->getRowCount($table_name, array($primary_key_name => $data[$primary_key_name]));

			if ($count > 0) $mode = 'update';
		}

		switch ($mode)
		{
			case 'insert':
				$field_names = array_keys($data);
				$field_values = array_values($data);
				
				$r = $this->ins($table_name, $data);
			break;
			
			case 'update':
				$primary_key_value = $data[$primary_key_name];
				unset($data[$primary_key_name]);
				
				$r = $this->update($table_name, $data, array($primary_key_name => $primary_key_value));
			break;
		}

		
		return $r;
	}
	
	/**
	* Creates SQL condition upon request data filtered according to $fields parameter
	* 
	* Fields format:
	* $tables = array
  * {
  *   <table_name> => array
  *   {
  *     <field_name>[ => <field_options>][,]
  *   }
  * }
  *
	*	
	* FIELD OPTIONS
	*	-------------
	*	<string>
	*	<array key>
	*	`name`
	*	`type`
	*	`conditions` - for `custom` type
	*	`search_type`
	*	`skip_empty` - when set to true empty value of a field is skipped, otherwise empty value is used in condition
	*	`trim` - whether to trim incoming value
	*	`encoding` - source encoding (target encoding is cp1251)
	*	
	*	
	*	defaults: type = 'scalar', search_type = 'exact', 'skip_empty' => true, 'trim' => true, 'legacy_mode' => false
	*
	*
	*	types
	*	-----
	*	scalar - single cgi value, single field (single value from cgi is checked for equality with = or LIKE depending on `search_type`)
	*	set - set of cgi values, single field (array from cgi is checked with IN)
	*	date - three cgi values, single DATE field (three values from cgi are joined as sql date and checked for equality with =)
	*	datetime - three or five cgi values, single DATETIME field (three or five values from cgi are joined and padded up to DATETIME type
	*	           and checked with = when time specified and with a pair of conditions otherwise, i.e. from 00:00:00 till 23:59:59 of given day)
	*	           date_range - 
	*	custom - single cgi value, no field required (single value from cgi is compared with keys of `conditions` and appropriate condition is used)
	*	
	*	
	*	search types
	*	------------
	*	like
	*	like_right
	*	like_left
	*	exact
	*
	*
	*	Usage sample:
	*	
	*	$tables = array
	*	(
	*		'tbl_ipoteka_base_c_cards' => array
	*		(
	*			'field1', // default config used
	*			'field2', // default config used
	*			'scalar_like_left_skip_empty' => array('type' => 'scalar', 'search_type' => 'like_left', 'skip_empty' => true, 'trim' => true), 
	*			'scalar_like_right_skip_empty' => array('type' => 'scalar', 'search_type' => 'like_right', 'skip_empty' => true),
	*			'scalar_like_skip_empty' => array('type' => 'scalar', 'search_type' => 'like', 'skip_empty' => true),
	*			array('name' => 'duplicate_field_name', 'type' => 'date_range', 'skip_empty' => false, 'legacy_mode' => true),
	*			array('name' => 'duplicate_field_name', 'type' => 'raw', 'skip_empty' => false),
	*			array('name' => 'cc_zil_cdocs_date_15', 'type' => 'custom', 'conditions' => array(1 => 'HOUR(tbl_ipoteka_base_c_cards.cc_zil_cdocs_date) <= 15', 2 => 'HOUR(tbl_ipoteka_base_c_cards.cc_zil_cdocs_date) > 15'))
	*		),
	*	
	*		'table2' => array
	*		(
	*			'date_range_exact_skip_empty' => array('type' => 'date_range', 'search_type' => 'exact', 'skip_empty' => true),
	*			'date_exact' => array('type' => 'date', 'search_type' => 'exact', 'skip_empty' => false),
	*			'set_exact' => array('type' => 'set', 'search_type' => 'exact', 'skip_empty' => false),
	*		)
	*	);
	*
	*	$sql_condition = create_condition_from_cgi($tables);
	*
	* @param array $fields field configuration
	* @param array $options options are 'default_encoding', 'date_postfixes', 'time_postfixes', 'datetime_postfixes' 
	*/
	
	public function create_condition_from_cgi($fields, $options = array())
	{
		// main::debug($_REQUEST);
		// var_dump($db);
		
		if (empty($options['default_encoding'])) $options['default_encoding'] = '';
		if (empty($options['date_postfixes'])) $options['date_postfixes'] = array('_year', '_month', '_day');
		if (empty($options['time_postfixes'])) $options['time_postfixes'] = array('_hour', '_minute', '_second');
		if (empty($options['datetime_postfixes'])) $options['datetime_postfixes'] = array('_year', '_month', '_day', '_hour', '_minute', '_second');

		
		$conditions = array();
		
		foreach ($fields as $field_name => $field)
		{
			// Convert filed config from short or duplicate-oriented value to complex value
			
			if (! is_array($field))
			{
				$field_name = $field;
				$field = array();
			}
			elseif (isset($field['name']))
			{
				$field_name = $field['name'];
				unset($field['name']);
			}
			
			
			// Fix PHP behaviour concerned dot in the user input
			
			$cgi_field_name = str_replace('.', '_', $field_name);
			
			
			// Implement defauts
			
			if (! isset($field['type'])) $field['type'] = 'scalar';
			if (! isset($field['search_type'])) $field['search_type'] = 'exact';
			if (! isset($field['skip_empty'])) $field['skip_empty'] = true;
			if (! isset($field['trim'])) $field['trim'] = true;
			
			
			switch ($field['type'])
			{
				case 'set':
					$cgi_value = get_cgi_value($cgi_field_name, 'a');

					if (! empty($cgi_value))
					{
						$conditions[] = SQL::in($cgi_value, $field_name);
					}
				break;

				case 'date':
					$cgi_date = get_cgi_date($cgi_field_name, NULL, $options['date_postfixes']);
					
					if ($cgi_date !== NULL)
					{
						switch ($field['search_type'])
						{
							/*
							case 'like':
								$sql_date = sprintf("%04d-%02d-%02d", $cgi_year, $cgi_month, $cgi_day);
							break;
							*/
							
							default:
								$conditions[] = $field_name . '="' . $cgi_date . '"';
						}
					}
				break;

				case 'time':
					$cgi_time = get_cgi_time($cgi_field_name, NULL, $options['time_postfixes']);
					
					if ($cgi_time !== NULL)
					{
						switch ($field['search_type'])
						{
							/*
							case 'like':
								$sql_date = sprintf("%04d-%02d-%02d", $cgi_year, $cgi_month, $cgi_day);
							break;
							*/
							
							default:
								$conditions[] = $field_name . '="' . $cgi_time . '"';
						}
					}
				break;

				case 'datetime':
					$cgi_datetime = get_cgi_datetime($cgi_field_name, NULL, $options['datetime_postfixes']);
					
					if ($cgi_datetime !== NULL)
					{
						switch ($field['search_type'])
						{
							/*
							case 'like':
								$sql_date = sprintf("%04d-%02d-%02d", $cgi_year, $cgi_month, $cgi_day);
							break;
							*/

							default: // exact
								$conditions[] = $field_name . '="' . $cgi_datetime . '"';
						}
					}
				break;

				case 'date_range':
					$cgi_date_from = get_cgi_date($cgi_field_name . '_from', NULL, $options['date_postfixes']);
					$cgi_date_till = get_cgi_date($cgi_field_name . '_till', NULL, $options['date_postfixes']);


					if ($cgi_date_from !== NULL || $cgi_date_till !== NULL)
					{
						/*
						$sql_from_date = '';
						$sql_till_date = '';
						
						switch ($field['search_type'])
						{
							
							//case 'like':
							//	$sql_from_date = sprintf("%04d-%02d-%02d", $cgi_from_year, $cgi_from_month, $cgi_from_day);
							//	$sql_till_date = sprintf("%04d-%02d-%02d", $cgi_till_year, $cgi_till_month, $cgi_till_day);
							// break;
							
							default: // exact
								//if (checkdate($cgi_from_month, $cgi_from_day, $cgi_from_year) && checkdate($cgi_till_month, $cgi_till_day, $cgi_till_year))
								//{
									$sql_from_date = sprintf("%04d-%02d-%02d", $cgi_from_year, $cgi_from_month, $cgi_from_day);
									$sql_till_date = sprintf("%04d-%02d-%02d", $cgi_till_year, $cgi_till_month, $cgi_till_day);
								//}
						}
						*/
						if (! empty($cgi_date_from))
						{
							$conditions[] = '"' . $cgi_date_from . '" <= ' . $field_name;
						}
						
						if (! empty($cgi_date_till))
						{
							$conditions[] = $field_name . ' <= "' . $cgi_date_till . '"';
						}
					}
				break;
				
				case 'datetime_range':
					$cgi_datetime_from = get_cgi_datetime($cgi_field_name . '_from', NULL, $options['datetime_postfixes']);
					$cgi_datetime_till = get_cgi_datetime($cgi_field_name . '_till', NULL, $options['datetime_postfixes']);
					// wdebug($options);
					if ($cgi_datetime_from !== NULL || $cgi_datetime_till !== NULL)
					{
						if (! empty($cgi_datetime_from))
						{
							$conditions[] = '"' . $cgi_datetime_from . '" <= ' . $field_name;
						}
						
						if (! empty($cgi_datetime_till))
						{
							$conditions[] = $field_name . ' <= "' . $cgi_datetime_till . '"';
						}
					}							
				break;
				
				case 'custom':
					$cgi_value = get_cgi_value($cgi_field_name);

					if ($cgi_value !== NULL)
					{
						if (in_array($cgi_value, array_keys($field['conditions'])))
						{
							$conditions[] = $field['conditions'][$cgi_value];
						}
					}
				break;
				/*
				case 'raw':
					$cgi_value = get_cgi_value($field_name, 's', NULL, array('utf-8', 'cp1251'));
				break;
				*/
				default: // scalar
					$conv_options = array();

					if (! empty($field['encoding']) && $field['encoding'] != 'cp1251') $conv_options = array($field['encoding'], 'cp1251');
					elseif (! empty($options['default_encoding']) && $options['default_encoding'] != 'cp1251') $conv_options = array($options['default_encoding'], 'cp1251');

					$cgi_value = get_cgi_value($cgi_field_name, 's', NULL, $conv_options); // Try <field_name> only

					if ($cgi_value !== NULL)
					{
						if ($field['trim']) $cgi_value = trim($cgi_value);
						
						$use_value = true;
						
						// if ($field['skip_empty'] && empty($cgi_value)) $use_value = false;
						if ($field['skip_empty'] && strlen($cgi_value) == 0) $use_value = false;
						
						if ($use_value)
						{
							if (preg_match('|^SQL_CONDITION[^(]*\((.*?)\)$|', $cgi_value, $matches))
							{
								$conditions[] = $matches[1];
							}
							else
							{
								switch ($field['search_type'])
								{
									case 'like':
										$conditions[] = $field_name . ' LIKE "%' . SQL::escape($cgi_value) . '%"';
									break;
									
									case 'like_left':
										$conditions[] = $field_name . ' LIKE "' . SQL::escape($cgi_value) . '%"';
									break;
									
									case 'like_right':
										$conditions[] = $field_name . ' LIKE "%' . SQL::escape($cgi_value) . '"';
									break;
									
									default:
										$conditions[] = $field_name . '="' . SQL::escape($cgi_value) . '"';
								}
							}
						}
					}
			}
		}

		return implode(' AND ', $conditions);
	}
	
	/**
	* Escapes string with addslashes()
	*
	* @param string $string string to escape
	*/
/*	
	public function escape($string)
	{
		return addslashes($string);
	}
*/	
	/**
	* Implements SQL DROP query
	*
	* @param mixed $tables table or table set to drop
	* @todo Reimplement for PDO
	*/
	public function drop_tables($tables)
	{
		// DEBUG: [[
		assert(! empty($tables));
		// ]]
		
		
		$_drop = 'DROP TABLES ';
		

		if (! is_array($tables)) $tables = explode(',', $tables);

		array_walk($tables, array('SQL', '_name'));
		$_drop .= implode(', ', $tables);

		
		$q = $_drop;
		
		$r = $this->_exec($q);
		
		return $r;
	}
	
}



class DBClassicTreeTable
{
	private $dbConnection = NULL;
	
	public function __construct(DBConnection $dbConnection)
	{
		$this->dbConnection = $dbConnection;
	}
	
	public function insertHive($hive, $table, $pri_key_name, $ext_key_name, $parent_node_id)
	{
		$this->dbConnection->beginTransaction();
		
		
		if ($parent_node_id != 0)
		{
			$c = $this->dbConnection->getRowCount($table, array($pri_key_name => $parent_node_id));
		
			if ($c == 0) throw new Exception("Parent node ($parent_node_id) not found in table '$table'. Hive was not inserted.");
		}
		
		$this->_ = array();
		$this->_['table'] = $table;
		$this->_['ext_key_name'] = $ext_key_name;
		
		$this->_insertHive($hive, $parent_node_id);


		$this->dbConnection->commit();
	}
	
	public function _insertHive($hive, $parent_node_id)
	{
		if (! empty($hive))
		{
			foreach ($hive as $node)
			{
				$node[$this->_['ext_key_name']] = $parent_node_id;
				
				$node_data = $node;
				if (isset($node_data['-'])) unset($node_data['-']);
				
				$this->dbConnection->ins($this->_['table'], $node_data);
				$new_node_id = $this->dbConnection->lastInsertId();
				
				if (isset($node['-'])) $this->_insertHive($node['-'], $new_node_id);
			}
		}
	}
	
	public function deleteChildren($table, $pri_key_name, $ext_key_name, $parent_node_id)
	{
		$this->_ = array();
		$this->_['children_ids'] = array();
		$this->_['table'] = $table;
		$this->_['pri_key_name'] = $pri_key_name;
		$this->_['ext_key_name'] = $ext_key_name;


		$this->dbConnection->beginTransaction();
		

		$this->_enumChildren($parent_node_id);
		
		if (! empty($this->_['children_ids']))
		{
			$this->dbConnection->delete($table, NULL, $this->dbConnection->in($pri_key_name, $this->_['children_ids']));
		}

		
		$this->dbConnection->commit();
		
		
		unset($this->_);
	}
	
	private function _enumChildren($parent_node_id)
	{
		$rows = $this->dbConnection->getRows($this->_['pri_key_name'], $this->_['table'], array($this->_['ext_key_name'] => $parent_node_id));
		
		foreach($rows as $row)
		{
			$this->_enumChildren($row[$this->_['pri_key_name']]);
			
			$this->_['children_ids'][] = $row[$this->_['pri_key_name']];
		}
	}
	
	/**
	* Creates a multidimensional array with tree structure which the specified table contains (downward)
	* 
	* Works on a table containing a tree structured data.
	* Data collecting occurs downward from node which's id is specified in $root_id and captures all the
	* leaves in a subtree up to the $max_depth. Resulting data does not include the root node data itself.
	* 
	* @param array &$tree multidimensional array containing resulting hive
	* @param string $table name of a table to operate on
	* @param string	$pri_key_name name of a field which is a primary key in the table
	* @param string	$ext_key_name name of a field which is an external key in the table
	* @param string	$order part of SQL query after ORDER BY to sort resulting data
	* @param array	$display_fields set of fields of the table to retrieve data from
	* @param integer $root_id id of a node which is a root of the processed subtree
	* @param integer $max_depth maximum depth of a resulting hive
	*/
	
	public function getHive($table, $pri_key_name, $ext_key_name, $order, $display_fields, $root_id, $max_depth)
	{
		$result = array();
		
		if ($max_depth > 0)
		{
			$select_fields = $display_fields;
			if (! in_array($pri_key_name, $select_fields)) array_unshift($select_fields, $pri_key_name);
			
			$this->_ = array();
			$this->_['tree'] = array();
			$this->_['displayable_fields'] = $display_fields;
			$this->_['selectable_fields'] = $select_fields;
			$this->_['table'] = $table;
			$this->_['pri_key_name'] = $pri_key_name;
			$this->_['ext_key_name'] = $ext_key_name;
			$this->_['order'] = $order;
			$this->_['max_depth'] = $max_depth;
			
			
			$this->dbConnection->beginTransaction();
			
			$this->_getHive($this->_['tree'], $root_id);
			
			$this->dbConnection->commit();

			
			$result = $this->_['tree'];

			unset($this->_);
		}
		
		return $result;
	}
	
	/**
	*
	*
	* &$tree, $pri_key_name, $display_fields, $root_id, $max_depth, &$_q1, &$_q2, $_depth = -1
	*/

	private function _getHive(&$tree, $node_id, $_depth = -1)
	{
		$_depth++;
	
		if ($_depth < $this->_['max_depth'])
		{
			$rows = $this->dbConnection->getRows($this->_['selectable_fields'], $this->_['table'], array($this->_['ext_key_name'] => $node_id), '', $this->_['order']);
			
			if (! empty($rows))
			{
				foreach($rows as $i => $row)
				{
					$row1 = array();
					
					$row1['_' . $this->_['pri_key_name']] = $row[$this->_['pri_key_name']];
					
					foreach($this->_['displayable_fields'] as $field_name) $row1['data'][$field_name] = $row[$field_name];
		
					$tree[] = $row1;
					$i = count($tree) - 1;
			
					$this->_getHive($tree[$i]['-'], $row[$this->_['pri_key_name']], $_depth);
					
					if (empty($tree[$i]['-'])) unset($tree[$i]['-']);
				}
			}
		}
	}
	
	
	/**
	* Creates a multidimensional array with tree structure which the specified table contains (upward)
	* 
	* Works on a table containing a tree structured data.
	* Data collecting occurs upward from node which's id is specified in $rec_id and captures all the
	* leaves in a subtree up to the node which's id is specified in $root_id.
	* Resulting data does not include the root node data itself.
	* 
	* @param array &$tree multidimensional array containing resulting hive
	* @param string $table name of a table to operate on
	* @param string	$pri_key_name name of a field which is a primary key in the table
	* @param string	$ext_key_name name of a field which is an external key in the table
	* @param string	$order part of SQL query after ORDER BY to sort resulting data
	* @param array	$display_fields set of fields of the table to retrieve data from
	* @param integer $root_id id of a node which is a root of the processed subtree
	* @param integer $rec_id id of a node which the retrieving process starts from
	*/
	
	public function getHiveRev($table, $pri_key_name, $ext_key_name, $order, $display_fields, $root_id, $rec_id)
	{
		$result = array();
		
		if ($rec_id != $root_id)
		{
			$select_fields = $display_fields;
			if (! in_array($pri_key_name, $select_fields)) array_unshift($select_fields, $pri_key_name);
	
			$this->_ = array();
			$this->_['tree'] = array();
			$this->_['displayable_fields'] = $display_fields;
			$this->_['selectable_fields'] = $select_fields;
			$this->_['table'] = $table;
			$this->_['pri_key_name'] = $pri_key_name;
			$this->_['ext_key_name'] = $ext_key_name;
			$this->_['order'] = $order;
			$this->_['root_id'] = $root_id;
			
			
			$this->dbConnection->beginTransaction();
			
			$this->_getHiveRev($this->_['tree'], $rec_id);
			
			$this->dbConnection->commit();

			
			$result = $this->_['tree'];

			unset($this->_);
		}
		
		return $result;
	}
	
	/**
	*
	*
	* &$tree, $pri_key_name, $ext_key_name, $display_fields, $root_id, $rec_id, &$_q1, &$_q2, &$_q3
	* @todo Reimplement for PDO
	*/

	private function _getHiveRev(&$tree, $rec_id)
	{
		if ($rec_id != $this->_['root_id'])
		{
			$row = $this->dbConnection->getRow($this->_['ext_key_name'], $this->_['table'], array($this->_['pri_key_name'] => $rec_id), '', $this->_['order']);
			
			if (! empty($row))
			{
				$parent_id = $row[$this->_['ext_key_name']];
				
				$rows = $this->dbConnection->getRows($this->_['selectable_fields'], $this->_['table'], array($this->_['ext_key_name'] => $parent_id), '', $this->_['order']);
				
				if (! empty($rows))
				{
					$_rows = array();
					$bond_row_index = -1;
					
					foreach($rows as $row)
					{
						$_rows[] = array();
						$i = count($_rows) - 1;
						
						$_rows[$i]['_' . $this->_['pri_key_name']] = $row[$this->_['pri_key_name']];
						
						if ($row[$this->_['pri_key_name']] == $rec_id) $bond_row_index = $i;
		
						foreach ($this->_['displayable_fields'] as $field_name)
						{
							$_rows[$i]['data'][$field_name] = $row[$field_name];
						}
					}
					
					if (! empty($tree)) $_rows[$bond_row_index]['-'] = $tree;
			
					$tree = $_rows;
				
					$this->_getHiveRev($tree, $parent_id);
				}
			}
		}
	}


	/**
	* Creates an array with trunk of a tree structure which the specified table contains (upward)
	* 
	* Works on a table containing a tree structured data.
	* Data collecting occurs upward from node which's id is specified in $rec_id and captures parent
	* nodes in a subtree up to the node which's id is specified in $root_id. Process stops if there is no data
	* any more. Resulting data does not include the root node data itself.
	* 
	* @param array &$trunk array containing resulting trunk
	* @param string $table name of a table to operate on
	* @param string	$pri_key_name name of a field which is a primary key in the table
	* @param string	$ext_key_name name of a field which is an external key in the table
	* @param array	$display_fields set of fields of the table to retrieve data from
	* @param integer $root_id id of a node which is a root of the processed subtree
	* @param integer $rec_id id of a node which the retrieving process starts from
	*
	* @todo Reimplement for PDO
	*/
	
	public function getTrunkRev($table, $pri_key_name, $ext_key_name, $display_fields, $root_id, $rec_id)
	{
		$result = array();
		
		if ($rec_id != $root_id)
		{
			$select_fields = $display_fields;
			if (! in_array($pri_key_name, $select_fields)) array_unshift($select_fields, $pri_key_name);
			if (! in_array($ext_key_name, $select_fields)) array_unshift($select_fields, $ext_key_name);

			$this->_ = array();
			$this->_['trunk'] = array();
			$this->_['displayable_fields'] = $display_fields;
			$this->_['selectable_fields'] = $select_fields;
			$this->_['table'] = $table;
			$this->_['pri_key_name'] = $pri_key_name;
			$this->_['ext_key_name'] = $ext_key_name;
			$this->_['root_id'] = $root_id;
			

			$this->dbConnection->beginTransaction();
			
			$this->_getTrunkRev($rec_id);

			$this->dbConnection->commit();
			
			
			$result = $this->_['trunk'];

			unset($this->_);
		}
		
		return $result;
	}
	
	/**
	*
	*
	* &$trunk, $ext_key_name, $display_fields, $root_id, $rec_id, &$_q1
	* @todo Reimplement for PDO
	*/

	private function _getTrunkRev($rec_id)
	{
		if ($rec_id != $this->_['root_id'])
		{
			$row = $this->dbConnection->getRow($this->_['selectable_fields'], $this->_['table'], array($this->_['pri_key_name'] => $rec_id));
			
			if (! empty($row))
			{
				$parent_id = $row[$this->_['ext_key_name']];
				
				$data = array();
				
				$data['_' . $this->_['pri_key_name']] = $row[$this->_['pri_key_name']];
				
				foreach ($this->_['displayable_fields'] as $field_name)
				{
					$data[$field_name] = $row[$field_name];
				}

				array_unshift($this->_['trunk'], $data);
			
				$this->_getTrunkRev($parent_id);
			}
		}
	}

	
	/**
	*
	*
	* &$list, &$tree, $pri_key_name, $rec_id
	*/

	public function hiveToList(&$hive, $pri_key_name, $rec_id)
	{
		$result = array();

		if (! empty($hive))
		{
			$this->_ = array();
			$this->_['list'] = array();
			$this->_['id_name'] = '_' . $pri_key_name;
			$this->_['rec_id'] = $rec_id;
	
			$this->_hiveToList($hive);
			
			$result = $this->_['list'];
	
			unset($this->_);
		}
		
		return $result;
	}

	/**
	*
	*
	* &$list, &$tree, $id_name, $rec_id, $_depth = 0
	*/

	private function _hiveToList(&$hive, $_depth = 0)
	{
		for ($i = 0; $i < count($hive); $i++)
		{
			$v = & $hive[$i]['data'];
			
			$v['_active'] = ($hive[$i][$this->_['id_name']] == $this->_['rec_id']) ? true : false;
			$v['_depth'] = $_depth;
			
			$this->_['list'][] = $v;

			if (isset($hive[$i]['-'])) $this->_hiveToList($hive[$i]['-'], $_depth + 1);
		}
	}
}


/**
* Provides methods to manipulate "celko" tree tables 
*/

class DBCelkoTreeTable
{
	private $dbConnection = NULL;
	private $tableName, $priKeyName, $leftIndexName, $rightIndexName, $levelName;
	private $preCondition = '';
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DBConnection $db_connection, $table_name, $pri_key_name, $left_index_name, $right_index_name, $level_name)
	{
		$this->dbConnection = $db_connection;
		
		$this->tableName = $table_name;
		$this->priKeyName = $pri_key_name;
		$this->leftIndexName = $left_index_name;
		$this->rightIndexName = $right_index_name;
		$this->levelName = $level_name;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setPreCondition($condition)
	{
		$this->preCondition = $condition;
	}
	
	/**
	* Inserts node at the end of layer under specified parent node
	* Inserts root node if $parent_node_id == 0
	*
	* @param array $node_data field/value pairs to insert into table
	* @param int $parent_node_id ID of the parent node for a new node
	* 
	* @return int ID of new inserted node 
	*/
	public function insertNode($node_data, $parent_node_id)
	{
		$id = NULL;

		
		if ($this->dbConnection->inTransaction())
		{
			$id = $this->_insertNode($node_data, $parent_node_id);
		}
		else
		{
			$this->dbConnection->beginTransaction();
	
			try
			{
				$id = $this->_insertNode($node_data, $parent_node_id);
				
				$this->dbConnection->commit();
			}
			catch (Exception $e)
			{
				$this->dbConnection->rollBack();
				
				throw $e;
			}
		}

		
		return $id;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	private function _insertNode($node_data, $parent_node_id)
	{
		$id = NULL;
		
		
		$t = $this->tableName;
		$_t = SQL::prepare_name($t);
		$pk = $this->priKeyName;
		$_pk = SQL::prepare_name($pk);
		$li = $this->leftIndexName;
		$_li = SQL::prepare_name($li);
		$ri = $this->rightIndexName;
		$_ri = SQL::prepare_name($ri);
		$ll = $this->levelName;
		$_ll = SQL::prepare_name($ll);

		
		if (! empty($parent_node_id))
		{
			$parent_node = $this->dbConnection->getRowAll($_t, array($pk => $parent_node_id));

			if (empty($parent_node)) throw new Exception("Parent node ($parent_node_id) not found in table '{$t}'. Node was not inserted.");
		
		
			$q = "
				UPDATE IGNORE {$_t} AS t
				SET
					t.{$_li} = IF(t.{$_li} > {$parent_node[$ri]}, t.{$_li} + 2, t.{$_li}),
					t.{$_ri} = IF(t.{$_ri} >= {$parent_node[$ri]}, t.{$_ri} + 2, t.{$_ri})
				WHERE t.{$_ri} >= {$parent_node[$ri]}
			";
				
			$_prec = $this->preCondition;
			if (! empty($_prec)) $q .= " AND ($_prec)";
			
			$this->dbConnection->exec($q);
	
			$node_data[$li] = $parent_node[$ri];
			$node_data[$ri] = $parent_node[$ri] + 1;
			$node_data[$ll] = $parent_node[$ll] + 1;

			$this->dbConnection->ins($t, $node_data);

			$id = $this->dbConnection->lastInsertId();
		}
		else
		{
			// Assume root insertion
			
			$node_data[$li] = 1;
			$node_data[$ri] = 2;
			$node_data[$ll] = 1;
			
			$this->dbConnection->ins($t, $node_data);
			
			$id = $this->dbConnection->lastInsertId();
		}

		
		return $id;
	}
	
	/**
	* Makes a copy of a hive and inserts it after another "sibling" node
	* Inserts new hive on the same level as sibling or as a child of the sibling node.
	*
	* @param int $node_id ID the copied hive root node
	* @param int $sibling_node_id ID of a node to insert after
	* @param bool $as_child if TRUE, new hive is inserted as a child; otherwise, as a sibling
	*
	* @return int ID of the new copied hive root node
	*/
	public function copyHiveAfter($node_id, $sibling_node_id, $as_child)
	{
		$new_node_id = NULL;

		
		if ($this->dbConnection->inTransaction())
		{
			$new_node_id = $this->_copyHiveAfter($node_id, $sibling_node_id, $as_child);
		}
		else
		{
			$this->dbConnection->beginTransaction();
	
			try
			{
				$new_node_id = $this->_copyHiveAfter($node_id, $sibling_node_id, $as_child);
				
				$this->dbConnection->commit();
			}
			catch (Exception $e)
			{
				$this->dbConnection->rollBack();
				
				throw $e;
			}
		}

		
		return $new_node_id;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	private function _copyHiveAfter($node_id, $sibling_node_id, $as_child)
	{
		$new_node_id = false;


		$t = $this->tableName;
		$_t = SQL::prepare_name($t);
		$pk = $this->priKeyName;
		$_pk = SQL::prepare_name($pk);
		$li = $this->leftIndexName;
		$_li = SQL::prepare_name($li);
		$ri = $this->rightIndexName;
		$_ri = SQL::prepare_name($ri);
		$ll = $this->levelName;
		$_ll = SQL::prepare_name($ll);

		$_prec = $this->preCondition;


		$node = $this->dbConnection->getRowAll($_t, array($pk => $node_id));
		
		if (empty($node)) throw new Exception("Node ($node_id) not found in table '$t'. Hive was not copied.");
		
		
		$sibling_node = $this->dbConnection->getRowAll($_t, array($pk => $sibling_node_id));
		
		if (empty($sibling_node)) throw new Exception("Sibling node ($sibling_node_id) not found in table '$t'. Hive was not copied.");
		
		
		if ($as_child)
		{
			$parent_node = $sibling_node;
		}
		else
		{
			$parent_node = $this->getParent($sibling_node_id);
		}

		
		if ($node[$li] <= $parent_node[$li] && $parent_node[$ri] <= $node[$ri]) throw new Exception("Can not copy hive to itself");


		$gap_shift = $node[$ri] - $node[$li] + 1;
		
		if ($as_child)
		{
			$level_shift = $sibling_node[$ll] - $node[$ll] + 1;

			$gap_start_threshold = $sibling_node[$li];
			
			$node_shift = $sibling_node[$li] - $node[$li] + 1;
		}
		else
		{
			$level_shift = $sibling_node[$ll] - $node[$ll];
			
			$gap_start_threshold = $sibling_node[$ri];

			$node_shift = $sibling_node[$ri] - $node[$li] + 1;
		}
		

		// Get max id
		
		$row = $this->dbConnection->getRowCustom("MAX($_pk) as max_pk", $_t);
		$max_pk_value = $row['max_pk'];

		$this->dbConnection->exec("SET @sub_hive_new_id_counter := $max_pk_value");


		// Make a copy of hive into the same table

		$this->dbConnection->setFetchMode(DBConnection::FETCH_ASSOC);
		
		$cond = "t.{$_li} >= {$node[$li]} AND t.{$_ri} <= {$node[$ri]}";
		if (! empty($_prec)) $cond .= " AND ($_prec)";

		$rows = $this->dbConnection->getRowsAll("{$_t} AS t", $cond);

		
		$next_pk_value = $max_pk_value + 1;
		foreach ($rows as $i => $row)
		{
			$rows[$i][$pk] = $next_pk_value++;
		}

		$this->dbConnection->insert($_t, $rows, array_keys($rows[0]));


		// Create gap and adjust copied hive
		
		$q = "
			UPDATE {$_t} AS t
			SET
				t.{$_ll} = IF
				(
					t.{$_pk} > {$max_pk_value}, t.{$_ll} + {$level_shift}, t.{$_ll}
				),
				
				t.{$_li} = IF
				(
					t.{$_pk} > {$max_pk_value}, t.{$_li} + {$node_shift},
					IF
					(
						t.{$_li} > {$gap_start_threshold}, t.{$_li} + {$gap_shift}, t.{$_li}
					)
				),
				t.{$_ri} = IF
				(
					t.{$_pk} > {$max_pk_value}, t.{$_ri} + {$node_shift},
					IF
					(
						t.{$_ri} > {$gap_start_threshold}, t.{$_ri} + {$gap_shift}, t.{$_ri}
					)
				)
		";
		if (! empty($_prec)) $q .= " WHERE $_prec";
		
		$this->dbConnection->exec($q);
		

		// Get new node id
		
		$s = $this->dbConnection->query("SELECT {$_pk} FROM {$_t} WHERE {$_pk} > {$max_pk_value} ORDER BY {$_ll} LIMIT 1");
		list($new_node_id) = $s->fetch(DBConnection::FETCH_NUM);
		
		
		return $new_node_id;
	}

	/**
	* Moves a hive after another "sibling" node
	* Inserts hive on the same level as sibling or as a child of the sibling node.
	*
	* @param int $node_id ID the moved hive root node
	* @param int $sibling_node_id ID of a node to insert after
	* @param bool $as_child if TRUE, hive is inserted as a child; otherwise, as a sibling
	*/
	public function moveHiveAfter($node_id, $sibling_node_id, $as_child)
	{
		if ($this->dbConnection->inTransaction())
		{
			$this->_moveHiveAfter($node_id, $sibling_node_id, $as_child);
		}
		else
		{
			$this->dbConnection->beginTransaction();
	
			try
			{
				$this->_moveHiveAfter($node_id, $sibling_node_id, $as_child);
				
				$this->dbConnection->commit();
			}
			catch (Exception $e)
			{
				$this->dbConnection->rollBack();
				
				throw $e;
			}
		}
		
		
		return true;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	private function _moveHiveAfter($node_id, $sibling_node_id, $as_child)
	{
		$t = $this->tableName;
		$_t = SQL::prepare_name($t);
		$pk = $this->priKeyName;
		$_pk = SQL::prepare_name($pk);
		$li = $this->leftIndexName;
		$_li = SQL::prepare_name($li);
		$ri = $this->rightIndexName;
		$_ri = SQL::prepare_name($ri);
		$ll = $this->levelName;
		$_ll = SQL::prepare_name($ll);
		
		$_prec = $this->preCondition;

		
		$node = $this->dbConnection->getRowAll($_t, array($pk => $node_id));

		if (empty($node)) throw new Exception("Node ($node_id) not found in table '{$t}'. Hive was not moved.");
		
		
		$sibling_node = $this->dbConnection->getRowAll($_t, array($pk => $sibling_node_id));
		
		if (empty($sibling_node)) throw new Exception("Sibling node ($sibling_node_id) not found in table '{$t}'. Hive was not moved.");
		
		
		if ($as_child)
		{
			$parent_node = $sibling_node;
		}
		else
		{
			$parent_node = $this->getParent($sibling_node_id);
		}

		
		if ($node[$li] <= $parent_node[$li] && $parent_node[$ri] <= $node[$ri]) throw new Exception("Can not move hive to itself");


		$node_interval = $node[$li] . ' AND ' . $node[$ri];
		
		if ($as_child)
		{
			$level_shift = $sibling_node[$ll] - $node[$ll] + 1;
			
			if ($sibling_node[$li] > $node[$li])
			{
				$parent_node_interval = ($node[$ri] + 1) . ' AND ' . ($sibling_node[$li]);
				$parent_node_shift = - ($node[$ri] - $node[$li] + 1);
				
				$node_shift = ($sibling_node[$li] - $node[$li] + 1) + $parent_node_shift;
				// order -> DESC ????
			}
			else
			{
				$node_shift = - ($node[$li] - $sibling_node[$li] - 1);
				
				$parent_node_interval = ($sibling_node[$li] + 1) . ' AND ' . ($node[$li] - 1);
				$parent_node_shift = ($node[$ri] - $node[$li] + 1);
				
				// order -> ASC ???
			}
		}
		else
		{
			$level_shift = $sibling_node[$ll] - $node[$ll];
			
			if ($node[$li] < $sibling_node[$ri]) // node is on the left side or inside sibling (using _ri instead of _li)
			{
				$parent_node_interval = ($node[$ri] + 1) . ' AND ' . ($sibling_node[$ri]);
				$parent_node_shift = - ($node[$ri] - $node[$li] + 1);
				
				$node_shift = ($sibling_node[$ri] - $node[$li] + 1) + $parent_node_shift;
				// order -> DESC ????
			}
			else
			{
				$node_shift = - ($node[$li] - $sibling_node[$ri] - 1);
				
				$parent_node_interval = ($sibling_node[$ri] + 1) . ' AND ' . ($node[$li] - 1);
				$parent_node_shift = ($node[$ri] - $node[$li] + 1);
				
				// order -> ASC ???
			}
		}
		
		$q = "
			UPDATE {$_t} AS t
			SET
			t.{$_ll} =
				CASE
					WHEN t.{$_li} BETWEEN {$node_interval} THEN t.{$_ll} + ({$level_shift}) ELSE t.{$_ll}
				END,
			t.{$_li} =
				CASE
					WHEN t.{$_li} BETWEEN {$node_interval} THEN t.{$_li} + ({$node_shift})
					WHEN t.{$_li} BETWEEN {$parent_node_interval} THEN t.{$_li} + ({$parent_node_shift})
					ELSE t.{$_li}
				END,
			t.{$_ri} =
				CASE 
					WHEN t.{$_ri} BETWEEN {$node_interval} THEN t.{$_ri} + ({$node_shift})
					WHEN t.{$_ri} BETWEEN {$parent_node_interval} THEN t.{$_ri} + ({$parent_node_shift})
					ELSE t.{$_ri}
				END
		";
		if (! empty($_prec)) $q .= " WHERE $_prec";
		
		$this->dbConnection->exec($q);
	}
	
	/**
	* Moves a hive from one parent node to another
	*
	* @param int $node_id ID the moved hive root node
	* @param int $parent_node_id ID of a new parent node
	*/
  function moveHive($node_id, $parent_node_id)
	{
		$t = $this->tableName;
		$_t = SQL::prepare_name($t);
		$pk = $this->priKeyName;
		$_pk = SQL::prepare_name($pk);
		$li = $this->leftIndexName;
		$_li = SQL::prepare_name($li);
		$ri = $this->rightIndexName;
		$_ri = SQL::prepare_name($ri);
		$ll = $this->levelName;
		$_ll = SQL::prepare_name($ll);

		$_prec = $this->preCondition;

		
		$this->dbConnection->beginTransaction();

		try
		{
			$node = $this->dbConnection->getRowAll($_t, array($pk => $node_id));

			if (empty($node)) throw new Exception("Node ($node_id) not found in table '{$t}'. Hive was not moved.");
			
			$parent_node = $this->dbConnection->getRowAll($_t, array($pk => $parent_node_id));
			
			if (empty($parent_node)) throw new Exception("Parent node ($parent_node_id) not found in table '{$t}'. Hive was not moved.");
			
			
			if ($node[$li] <= $parent_node[$li] && $parent_node[$ri] <= $node[$ri])
				throw new Exception("Can not move hive to itself");
			
			$level_shift = $parent_node[$ll] - $node[$ll] + 1;
			
			$node_interval = $node[$li] . ' AND ' . $node[$ri];
			
			if ($parent_node[$ri] > $node[$ri])
			{
				$node_shift = ($parent_node[$ri] - $node[$ri] - 1);
				
				$parent_node_interval = ($node[$ri] + 1) . ' AND ' . ($parent_node[$ri] - 1);
				$parent_node_shift = - ($node[$ri] - $node[$li] + 1);
			}
			else
			{
				$node_shift = - ($node[$li] - ($parent_node[$ri]));
				
				$parent_node_interval = $parent_node[$ri] . ' AND ' . ($node[$li] - 1);
				$parent_node_shift = ($node[$ri] - $node[$li] + 1);
			}
			
			$q = "
				UPDATE {$_t} AS t
				SET
				t.{$_ll} =
					CASE
						WHEN t.{$_li} BETWEEN {$node_interval} THEN t.{$_ll} + ({$level_shift}) ELSE t.{$_ll}
					END,
				t.{$_li} =
					CASE
						WHEN t.{$_li} BETWEEN {$node_interval} THEN t.{$_li} + ({$node_shift})
						WHEN t.{$_li} BETWEEN {$parent_node_interval} THEN t.{$_li} + ({$parent_node_shift})
						ELSE t.{$_li}
					END,
				t.{$_ri} =
					CASE 
						WHEN t.{$_ri} BETWEEN {$node_interval} THEN t.{$_ri} + ({$node_shift})
						WHEN t.{$_ri} BETWEEN {$parent_node_interval} THEN t.{$_ri} + ({$parent_node_shift})
						ELSE t.{$_ri}
					END
			";
			if (! empty($_prec)) $q .= " WHERE $_prec";
			
			$this->dbConnection->exec($q);
			
			$this->dbConnection->commit();
		}
		catch (Exception $e)
		{
			$this->dbConnection->rollBack();
			
			throw $e;
		}
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getChildren($display_fields, $node_id, $max_depth = NULL, $order = '')
	{
		$result = array();


		if ($this->dbConnection->inTransaction())
		{
			$result = $this->_getChildren($display_fields, $node_id, $max_depth, $order);
		}
		else
		{
			$this->dbConnection->beginTransaction();
	
			try
			{
				$result = $this->_getChildren($display_fields, $node_id, $max_depth, $order);
				
				$this->dbConnection->commit();
			}
			catch (Exception $e)
			{
				$this->dbConnection->rollBack();
				
				throw $e;
			}
		}
		
		
		return $result;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	private function _getChildren($display_fields, $node_id, $max_depth = NULL, $order = '')
	{
		$result = array();
		
		
		$t = $this->tableName;
		$_t = SQL::prepare_name($t);
		$pk = $this->priKeyName;
		$_pk = SQL::prepare_name($pk);
		$li = $this->leftIndexName;
		$_li = SQL::prepare_name($li);
		$ri = $this->rightIndexName;
		$_ri = SQL::prepare_name($ri);
		$ll = $this->levelName;
		$_ll = SQL::prepare_name($ll);

		$_prec = $this->preCondition;

		
		$c = $this->dbConnection->getRowCount($_t, array($pk => $node_id));
		
		if ($c == 0) throw new Exception("Node ($node_id) was not found in '{$t}' table.");
		

		$select_fields = $display_fields;
		if (! in_array($pk, $select_fields)) array_unshift($select_fields, $pk);
		if (! in_array($li, $select_fields)) array_unshift($select_fields, $li);
		if (! in_array($ri, $select_fields)) array_unshift($select_fields, $ri);
		if (! in_array($ll, $select_fields)) array_unshift($select_fields, $ll);
		
		foreach ($select_fields as $i => $field_name)
		{
			$select_fields[$i] = 't.' . $field_name;
		}
		
		$condition = "_t.{$_pk} = {$node_id} AND _t.{$_li} < t.{$_li} AND t.{$_ri} < _t.{$_ri}";
		if (! is_null($max_depth)) $condition .= " AND _t.{$_ll} >= CAST(t.{$_ll} AS SIGNED) - {$max_depth}";
		if (! empty($_prec)) $condition .= " AND ($_prec)";
		
		$result = $this->dbConnection->getRows
		(
			$select_fields,
			"$_t AS _t, $_t AS t",
			$condition,
			'',
			$order
		);


		return $result;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getParent($node_id, $level = NULL)
	{
		$result = array();
		
		$t = $this->tableName;
		$_t = SQL::prepare_name($t);
		$pk = $this->priKeyName;
		$_pk = SQL::prepare_name($pk);
		$li = $this->leftIndexName;
		$_li = SQL::prepare_name($li);
		$ri = $this->rightIndexName;
		$_ri = SQL::prepare_name($ri);
		$ll = $this->levelName;
		$_ll = SQL::prepare_name($ll);


		$condition = "_t.{$_pk} = {$node_id} AND _t.{$_li} BETWEEN t.{$_li} AND t.{$_ri}";

		$_prec = $this->preCondition;
		if (! empty($_prec)) $condition .= " AND ($_prec)";
		
		if ($level) $condition .= " AND t.{$_ll} = {$level}";
		else $condition .= " AND t.{$_ll}=_t.{$_ll} - 1";

/*		
		$result = $this->dbConnection->getRowsCustom
		(
			"t.*",
			"$_t AS _t, $_t AS t",
			$condition,
			'',
			$_pk
		);
*/		
		$result = $this->dbConnection->getRowCustom
		(
			"t.*",
			"$_t AS _t, $_t AS t",
			$condition
		);

		return $result;		
	}
	
	// !!! including the given node
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getParentTrunk($node_id, $min_level = 1)
	{
		$result = array();
		
		$t = $this->tableName;
		$_t = SQL::prepare_name($t);
		$pk = $this->priKeyName;
		$_pk = SQL::prepare_name($pk);
		$li = $this->leftIndexName;
		$_li = SQL::prepare_name($li);
		$ri = $this->rightIndexName;
		$_ri = SQL::prepare_name($ri);
		$ll = $this->levelName;
		$_ll = SQL::prepare_name($ll);
		
		
		$condition = "_t.{$_pk} = {$node_id} AND _t.{$_li} BETWEEN t.{$_li} AND t.{$_ri}";

		$_prec = $this->preCondition;
		if (! empty($_prec)) $condition .= " AND ($_prec)";
		
		if ($min_level > 1) $condition .= " AND (t.{$_ll} >= {$min_level})";
		

		$result = $this->dbConnection->getRowsCustom
		(
			"t.*",
			"$_t AS _t, $_t AS t",
			$condition,
			'',
			// $_pk
			$_li
		);

		return $result;		
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function deleteNode($node_id)
	{
		if ($this->dbConnection->inTransaction())
		{
			$this->_deleteNode($node_id);
		}
		else
		{
			$this->dbConnection->beginTransaction();
	
			try
			{
				$this->_deleteNode($node_id);
				
				$this->dbConnection->commit();
			}
			catch (Exception $e)
			{
				$this->dbConnection->rollBack();
				
				throw $e;
			}
		}
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function _deleteNode($node_id)
	{
		$t = $this->tableName;
		$_t = SQL::prepare_name($t);
		$pk = $this->priKeyName;
		$_pk = SQL::prepare_name($pk);
		$li = $this->leftIndexName;
		$_li = SQL::prepare_name($li);
		$ri = $this->rightIndexName;
		$_ri = SQL::prepare_name($ri);
		$ll = $this->levelName;
		$_ll = SQL::prepare_name($ll);

		$_prec = $this->preCondition;

		
		$node = $this->dbConnection->getRowAll($_t, array($pk => $node_id));

		if (empty($node)) throw new Exception("Node ($node_id) not found in table '{$t}'. Node was not deleted.");
		
		
		$q = "DELETE FROM t USING {$_t} AS t WHERE t.{$_li} >= {$node[$li]} AND t.{$_ri} <= {$node[$ri]}";
		if (! empty($_prec)) $q .= " AND ($_prec)";

		$this->dbConnection->exec($q);
		

		$k = $node[$ri] - $node[$li] + 1;

		$q = "UPDATE {$_t} AS t SET t.{$_li} = IF(t.{$_li} > {$node[$li]}, t.{$_li} - {$k}, t.{$_li}), t.{$_ri} = IF(t.{$_ri} > {$node[$li]}, t.{$_ri} - {$k}, t.{$_ri}) WHERE t.{$_ri} > {$node[$ri]}";
		if (! empty($_prec)) $q .= " AND ($_prec)";

		$this->dbConnection->exec($q);
	}
}
?>
