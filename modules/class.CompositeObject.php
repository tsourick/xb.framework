<?php

class CompositeObject
{
	public $items;
	
	public function __construct($items = [])
	{
		if (! is_array($items)) throw new Exception("CompositeObject argument must be an array");
		
		$this->items = $items;
	}
	
	public function add($item)
	{
		$this->items[] = $item;
	}
	
	private function call($name, $arguments)
	{
		foreach ($this->items as $item)
		{
			call_user_func_array([$item, $name], $arguments);
		}
	}
	
	public function __call($name, $arguments)
	{
		$this->call($name, $arguments);
	}
}
