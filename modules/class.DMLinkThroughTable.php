<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

require_once('class.DMLink.php');


abstract class DMLinkThroughTable extends DMLink
{
	protected $ormTable = NULL; // Binding ORMTable instance
	
	protected $fromFieldPairs = array(); // (0: (<from DMField>), 1: (<to link ORMField>)), ...
	protected $toFieldPairs = array(); // (0: (<from DMField>), 1: (<to link ORMField>)), ...

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	protected function createLinkTable($from_entity, $to_entity)
	{
		$this->ormTable = new ORMTable($this->dm->database(), $this->dm->makeORMTableName($this->name));
		
		
		// Add link fields to table, store link fields
		
		$from_fields = $from_entity->getPrimaryFields();
		foreach ($from_fields as $from_field)
		{
			// Create new link field (clone from config)
			
			$field_config = $from_field->ORMField()->getConfig();
			// $field_config['name'] = $from_entity->getName() . '_' . $from_field->getName();
			$field_config['name'] = $this->dm->makeLinkORMFieldName($from_entity->getName(), $from_field->getName());
			$field_config['null'] = false;
			$field_config['auto_increment'] = false;
			
			$ormField = ORMField::createFromConfig($this->ormTable, $field_config);
			
			// Save field
			$this->fromFieldPairs[] = array($from_field, $ormField);
		}
		
		$to_fields = $to_entity->getPrimaryFields();
		foreach ($to_fields as $to_field)
		{
			// Create new link field (clone from config)
			
			$field_config = $to_field->ORMField()->getConfig();
			// $field_config['name'] = $to_entity->getName() . '_' . $to_field->getName();
			$field_config['name'] = $this->dm->makeLinkORMFieldName($to_entity->getName(), $to_field->getName());
			$field_config['null'] = false;
			$field_config['auto_increment'] = false;
			
			$ormField = ORMField::createFromConfig($this->ormTable, $field_config);
			
			// Save field
			$this->toFieldPairs[] = array($to_field, $ormField);
		}
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function ORMTable()
	{
		return $this->ormTable;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPreparedName()
	{
		return $this->ormTable->getPreparedName();
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function fromFieldPairs()
	{
		return $this->fromFieldPairs;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function toFieldPairs()
	{
		return $this->toFieldPairs;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function createItemLink($from_pk, $to_pk)
	{
		$from_entity_name = $this->fromEntity->getName();
		$to_entity_name = $this->toEntity->getName();
		
		
		if ($this->fromRequired || $this->toRequired) throw new DMLinkException("createItemLink() not implemented when either entity is required");


		// Check entity items
		
		$c = $this->fromEntity->getItemCount($from_pk);

		if ($c == 0) throw new DMLinkException("'$from_entity_name' entity item with key values (" . implode_params($from_pk, array(', ', ': ')) . ") and referred to as parent was not found on link between entity '$from_entity_name' and entity '$to_entity_name'.");
		
		$c = $this->toEntity->getItemCount($to_pk);

		if ($c == 0) throw new DMLinkException("'$to_entity_name' entity item with key values (" . implode_params($to_pk, array(', ', ': ')) . ") and referred to as child was not found on link between entity '$from_entity_name' and entity '$to_entity_name'.");

		
		$link_data = array();
		foreach ($this->fromFieldPairs as $index => $from_field_pair)
		{
			$dmFromField = $from_field_pair[0];
			$ormFromField = $from_field_pair[1];
			
			$to_field_pair = $this->toFieldPairs[$index];
			
			$dmToField = $to_field_pair[0];
			$ormToField = $to_field_pair[1];
			
			
			$link_data[$ormFromField->getName()] = $from_pk[$dmFromField->getName()];
			$link_data[$ormToField->getName()] = $to_pk[$dmToField->getName()];
		}
		
		
		$c = $this->ormTable->getRowCount($link_data);
		
		if ($c == 0) // if not already linked
		{
			$this->ormTable->ins($link_data);
		}
	}
	
	
	/**
	* Create several links at once
	*
	* @param mixed $from_pk_values array of PKs or single PK
	* @param mixed $to_pk_values array of PKs or single PK
	*/
	
	public function createItemLinks($from_pks, $to_pks)
	{
		if (empty($from_pks) || empty($to_pks)) throw new DMLinkException("createItemLinks(): both primary keys must be given");
		
		if (! is_array($from_pks)) throw new DMLinkException("createItemLinks(): wrong left PK value given");
		if (! is_array($to_pks)) throw new DMLinkException("createItemLinks(): wrong right PK value given");
		

		// Convert to arrays of PKs

		$keys = array_keys($from_pks);
		if (is_string($keys[0]))
		{
			$from_pks = array($from_pks);
		}

		$keys = array_keys($to_pks);
		if (is_string($keys[0]))
		{
			$to_pks = array($to_pks);
		}

		
		// Create links
		foreach ($from_pks as $from_pk)
		{
			foreach ($to_pks as $to_pk)
			{
				$this->createItemLink($from_pk, $to_pk);
			}
		}
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function dropItemLink($from_pk, $to_pk)
	{
		$from_entity_name = $this->fromEntity->getName();
		$to_entity_name = $this->toEntity->getName();
		
		
		if ($this->fromRequired || $this->toRequired) throw new DMLinkException("linkItems() not implemented when either entity is required");


		// Check entity items
		
		$c = $this->fromEntity->getItemCount($from_pk);

		if ($c == 0) throw new DMLinkException("'$from_entity_name' entity item with key values (" . implode_params($from_pk, array(', ', ': ')) . ") and referred to as parent was not found on drop link between entity '$from_entity_name' and entity '$to_entity_name'.");
		
		$c = $this->toEntity->getItemCount($to_pk);

		if ($c == 0) throw new DMLinkException("'$to_entity_name' entity item with key values (" . implode_params($to_pk, array(', ', ': ')) . ") and referred to as child was not found on drop link between entity '$from_entity_name' and entity '$to_entity_name'.");

		
		$unlink_condition = array();
		foreach ($this->fromFieldPairs as $index => $from_field_pair)
		{
			$dmFromField = $from_field_pair[0];
			$ormFromField = $from_field_pair[1];
			
			$to_field_pair = $this->toFieldPairs[$index];
			
			$dmToField = $to_field_pair[0];
			$ormToField = $to_field_pair[1];
			
			
			$unlink_condition[$ormFromField->getName()] = $from_pk[$dmFromField->getName()];
			$unlink_condition[$ormToField->getName()] = $to_pk[$dmToField->getName()];
		}

		
		$this->ormTable->delete($unlink_condition);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function dropAllToItemLinks($from_pk)
	{
		$from_entity_name = $this->fromEntity->getName();
		$to_entity_name = $this->toEntity->getName();
		
		
		if ($this->fromRequired || $this->toRequired) throw new DMLinkException("linkItems() not implemented when either entity is required");


		// Check entity items
		
		$c = $this->fromEntity->getItemCount($from_pk);

		if ($c == 0) throw new DMLinkException("'$from_entity_name' entity item with key values (" . implode_params($from_pk, array(', ', ': ')) . ") and referred to as left was not found on drop link between entity '$from_entity_name' and entity '$to_entity_name'.");

		
		$unlink_condition = array();
		foreach ($this->fromFieldPairs as $index => $from_field_pair)
		{
			$dmFromField = $from_field_pair[0];
			$ormFromField = $from_field_pair[1];
			
			
			$unlink_condition[$ormFromField->getName()] = $from_pk[$dmFromField->getName()];
		}

		
		$this->ormTable->delete($unlink_condition);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function dropAllFromItemLinks($to_pk)
	{
		$from_entity_name = $this->fromEntity->getName();
		$to_entity_name = $this->toEntity->getName();
		
		
		if ($this->fromRequired || $this->toRequired) throw new DMLinkException("linkItems() not implemented when either entity is required");


		// Check entity items
		
		$c = $this->toEntity->getItemCount($to_pk);

		if ($c == 0) throw new DMLinkException("'$to_entity_name' entity item with key values (" . implode_params($to_pk, array(', ', ': ')) . ") and referred to as right was not found on drop link between entity '$from_entity_name' and entity '$to_entity_name'.");

		
		$unlink_condition = array();
		foreach ($this->toFieldPairs as $index => $to_field_pair)
		{
			$dmToField = $to_field_pair[0];
			$ormToField = $to_field_pair[1];
			
			
			$unlink_condition[$ormToField->getName()] = $to_pk[$dmToField->getName()];
		}

		
		$this->ormTable->delete($unlink_condition);
	}
}

?>
