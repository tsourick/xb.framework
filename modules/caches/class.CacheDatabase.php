<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2013
*
* @package modules
* @subpackage cache
*/

require_once('class.DBConnection.php');
require_once('class.SQL.php');


/**
* CACHE Database class
*
* @version 1.0
*/

/**
* Class exception
*/

class CacheDatabaseException extends Exception
{
}

class CacheDatabase implements CacheInterface
{
	private $application = NULL;
	
	private $dbc = NULL;	
	private $tableName = '';

	protected $debug_log_disabled = true;
	protected $debug_log_blocks = array('info', 'info::init');
	// protected $debug_log_blocks = array('info');
	
	
	protected function debug_log($message, $block)
	{
		if (! $this->debug_log_disabled)
		{
			if (in_array($block, $this->debug_log_blocks))
			{
				dump('framework.modules.' . get_class($this) . ': ' . $message, $block);
			}
		}
	}
	

	public function __construct()
	{
		$this->debug_log("Constructing cache instance of '" . __CLASS__ . "'", 'info::init');
	}
	
	public function connect()
	{
	}
	
	public function useConnection(DBConnection $dbc, $table_name)
	{
		$this->dbc = $dbc;
		$this->tableName = $table_name;
	}
	
	public function set($key, $value, $expires = 0)
	{
		$dbc = $this->dbc;
		$table_name = $this->tableName;


		$_set = '';

		
		$fields = array('key', 'value', 'expires');

		$_key = SQL::prepare_value($key);
		$_value = SQL::prepare_value(serialize($value));
		$values = "{$_key}, {$_value}";
		
		if ($expires > 0)
		{
			$values .= ", DATE_ADD(NOW(), INTERVAL {$expires} SECOND)";
		}
		else
		{
			$values .= ', NULL';
		}
		
		$dbc->replace($table_name, $values, $fields);
	}
	
	public function get($key)
	{
		$value = $this->dbc->getRowValue('value', $this->tableName, array('key' => $key, '(expires IS NULL OR expires > NOW())'));
		
		if (! is_null($value)) $value = unserialize($value);
		
		return $value;
	}
	
	public function clear($key)
	{
		$this->dbc->delete($this->tableName, NULL, compact('key'));
	}
}

?>