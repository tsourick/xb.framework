<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

class DMPlainTimestampedEntityBase extends DMPlainEntity
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DMDataModel $dm, $name, $dmFields = array())
	{
		parent::__construct($dm, $name, $dmFields);

		
		// Predefined fields
		
		$dmField = $this->createFieldFromConfig(array('name' => 'created', 'type' => 'created'));
		$this->addField($dmField);
		
		$dmField = $this->createFieldFromConfig(array('name' => 'modified', 'type' => 'modified'));
		$this->addField($dmField);
	}
}

?>
