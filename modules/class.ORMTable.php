<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage orm
*/

require_once('class.ORMField.php');
require_once('class.ORMIndex.php');
require_once('class.ORMField.php');
require_once('class.SQL.php');


class ORMTableException extends ORMException
{
}

class ORMTable
{
	private $ormDatabase = NULL; // ORMDatabase
	
	private $dbc = NULL; // DB Connection
	
	private $name; // Table name strign
	
	private $options; // Array of table options
	
	private $ormFields; // Array of table fields
	private $ormIndexes; // Array of table indexes
	// private $ormReferences; // Array of table references
	
	// private $pkFields = array();
	
	private $primaryIndex = NULL;
	
	
	static private $log = false;


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(ORMDatabase $ormDatabase, $name, $options = array(), $ormFields = array(), $ormIndexes = array()/* , $ormReferences = array()*/)
	{
		$this->ormDatabase = $ormDatabase;

		
		$this->dbc = $ormDatabase->getDBConnection();
		
		
		$this->name = strtolower($name);

		
		$this->options = $options; // TODO: options processing
		
		
		$this->ormFields = $ormFields;
		$this->ormIndexes = $ormIndexes;
		// $this->ormReferences = $ormReferences;
		
		
		$ormDatabase->addTable($this);
	}
	
	// THIS IS NOT A LANGUAGE DESTRUCTOR
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function destruct()
	{
		$this->ormDatabase->removeTable($this);
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getName()
	{
		return $this->name;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPreparedName()
	{
		return SQL::prepare_name($this->name);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getFullName()
	{
		return $this->ormDatabase->getName() . '.' . $this->name;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPreparedFullName()
	{
		return $this->ormDatabase->getPreparedName() . '.' . $this->getPreparedName();
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setName($name)
	{
		if (strcmp($this->name, $name) != 0)
		{
			$old_name = $this->name;

			$this->name = $name;
			
			
			$this->ormDatabase->remapTable($this, $old_name);
		}
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function remapField(ORMField $ormField, $old_name)
	{
		if ($ormField->table() !== $this) throw new ORMTableException("Could not remap field '" . $ormField->getName()  . "' from name '" . $old_name . "' because field belongs to another table.");

		if (! array_key_exists($old_name, $this->ormFields)) throw new ORMTableException("Could not remap field '" . $ormField->getName()  . "' from name '" . $old_name . "' because old name was not found within table.");

		
		array_replace_key($this->ormFields, $old_name, $ormField->getName());
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function log($log = NULL)
	{
		if (is_null($log))
		{
			return self::$log;
		}
		else
		{
			self::$log = $log;
		}
	}
	
	
	/**
	* $config structure:

	array
	(
		'name' => '<table name>',
		'fields' => array
		(
			array
			(
				'name' => '<field name>',
				'type' => array
				{
					'name' => '(BIT |TINYINT |SMALLINT |MEDIUMINT |INT |INTEGER |BIGINT |REAL |DOUBLE |FLOAT |DECIMAL |NUMERIC| ...)', 					
					'length' => (<type length>|NULL), // if applicable
					'decimals' => (<decimal count>|NULL), // if applicable
					'unsigned' => (true|false|NULL), // if applicable
				),
				'null' => (true|false),
				'default' => (<default value>|NULL),
				'auto_increment' => (true|false),
				'comment' => '<comment on field>'
			),
			... 			
		),
		'indexes' => array
		(
		@see ORMIndex::createFromConfig()
		)
	)
	*/
	static public function createFromConfig(ORMDatabase $ormDatabase, $config)
	{
		$options = isset($config['options']) ? $config['options'] : array();
		
		$ormTable = new self($ormDatabase, $config['name'], $options);
		
		
		foreach ($config['fields'] as $field_config)
		{
			$ormTable->createFieldFromConfig($field_config);
			
			// $ormTable->addField($ormField);
		}

		foreach ($config['indexes'] as $index_config)
		{
			$ormTable->createIndexFromConfig($index_config);
			
			// $ormTable->addIndex($ormIndex);
		}
/*
		foreach ($config['references'] as $reference_config)
		{
			$ormReference = $ormTable->createReferenceFromConfig($reference_config);
			
			$ormTable->addReference($ormReference);
		}
*/
		return $ormTable;
	}
	
	/**
	* Config structure:
	* @see ORMField->createFromConfig()
	*/
	public function createFieldFromConfig($config)
	{
		$ormField = ORMField::createFromConfig($this, $config);
		
		return $ormField;
	}
	
	/**
	* Config structure:
	* @see ORMIndex->createFromConfig()
	*/
	public function createIndexFromConfig($config)
	{
		$ormIndex = ORMIndex::createFromConfig($this, $config);
		
		return $ormIndex;
	}

	/**
	* Config structure:
	* @see ORMReference->createFromConfig()
	* /
	public function createReferenceFromConfig($config)
	{
		$ormReference = ORMReference::createFromConfig($config);
		
		return $ormReference;
	}
*/
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addField(ORMField $ormField)
	{
		if ($ormField->table() !== $this) throw new ORMTableException("Could not add field '" . $ormField->getName() . "' to table '" . $this->getName() . "' because field belongs to another table.");
		
		
		$this->ormFields[$ormField->getName()] = $ormField;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addIndex(ORMIndex $ormIndex)
	{
		if ($ormIndex->table() !== $this) throw new ORMTableException("Could not add index '" . $ormIndex->getName() . "' to table '" . $this->getName() . "' because index belongs to another table.");
		
		
		if ($ormIndex->getType() == 'PRIMARY')
		{/*
			$index_fields = $ormIndex->fields();
			

			$pkFields = array();
			foreach ($index_fields as $index_field)
			{
				$field_name = $index_field->getName();
				
				$pkFields[$field_name] = $this->field($field_name);
			}

			
			$this->pkFields = $pkFields;
			*/
			
			$this->primaryIndex = $ormIndex;
		}
		
		
		$this->ormIndexes[$ormIndex->getName()] = $ormIndex;
	}
/*
	public function addReference(ORMReference $ormReference)
	{
		$this->ormReferences[$ormReference->getName()] = $ormReference;
	}
*/	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function database()
	{
		return $this->ormDatabase;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function field($name)
	{
		return $this->ormFields[$name];
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function fields()
	{
		return $this->ormFields;
	}
	
	/*
	public function pkFields()
	{
		return $this->pkFields;
	}
*/
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function primaryIndex()
	{
		return $this->primaryIndex;
	}
	
	/*
	public function createDDLSource()
	{
		$ddl = 'CREATE TABLE ' . SQL::prepare_name($this->name);
		

		// Create fields' DDL
		
		$fieldsDDL = array();
		foreach ($this->ormFields as $ormField)
		{
			$fieldsDDL[] = $ormField->_createDDLSource();
		}
		
		$ddl .= "\n(\n\t" . implode(",\n\t", $fieldsDDL) . "\n);\n";


		// Create indexes' DDL
		
		$indexesDDL = array();
		foreach ($this->ormIndexes as $ormIndex)
		{
			$indexesDDL[] = $ormIndex->createDDLSource();
		}
		
		$ddl .= implode("\n", $indexesDDL);


		// Create auto_increment field's DDL
		
		$aiFieldDDL = array();
		foreach ($this->ormFields as $ormField)
		{
			if ($ormField->isAutoincrement())
			{
				$ddl .= "\n" . $ormField->createAutoincrementDDLSource();
			}
		}


		return $ddl;
	}
	*/
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetCreateQuerySet()
	{
		$qset = array();
		
		
		// Create table's DDL
		
		$q = 'CREATE TABLE ' . SQL::prepare_name($this->name);

		// Fields' types
		
		$fieldTypes = array();
		foreach ($this->ormFields as $ormField)
		{
			$fieldTypes[] = $ormField->_SQLDDLGetDefinition();
		}
		
		$q .= "\n(\n\t" . implode(",\n\t", $fieldTypes) . "\n)";
		
		$qset[] = $q;


		// Create indexes' DDL
		
		foreach ($this->ormIndexes as $ormIndex)
		{
			$qset = array_merge($qset, $ormIndex->SQLDDLGetCreateQuerySet());
		}


		// Create auto_increment field's DDL
		
		$aiFieldDDL = array();
		foreach ($this->ormFields as $ormField)
		{
			if ($ormField->isAutoincrement())
			{
				$qset = array_merge($qset, $ormField->SQLDDLGetCreateAutoincrementQuerySet());
			}
		}


		return $qset;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetRenameQuerySet($old_name)
	{
		$qset = array();
		

		$q = 'ALTER TABLE ' . SQL::prepare_name($old_name) . ' RENAME TO ' . $this->getPreparedName();

		$qset[] = $q;

		
		return $qset;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetDropQuerySet()
	{
		$qset = array();
		
		$qset[] = 'DROP TABLE ' . SQL::prepare_name($this->name);
		
		return $qset;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowValue($field_name, $condition = NULL)
	{
		$result = $this->dbc->getRowValue($field_name, $this->getPreparedFullName(), $condition);
		
		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}
		
		return $result;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowCount($condition = NULL, $group = '')
	{
		$result =  $this->dbc->getRowCount($this->getPreparedFullName(), $condition, $group);

		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}

		return $result;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowData($fields, $condition = NULL)
	{
		$result = $this->dbc->getRow($fields, $this->getPreparedFullName(), $condition);

		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}

		return $result;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowDataAll($condition = NULL)
	{
		$result = $this->dbc->getRowAll($this->getPreparedFullName(), $condition);

		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}

		return $result;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowDataCustom($select, $condition = NULL)
	{
		$result = $this->dbc->getRowCustom($select, $this->getPreparedFullName(), $condition);

		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}

		return $result;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowDataList($fields, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		$result = $this->dbc->getRows($fields, $this->getPreparedFullName(), $condition, $group, $order, $limits, $row_key_field, $row_value_field);;

		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}

		return $result;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowDataListAll($condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		$result =  $this->dbc->getRowsAll($this->getPreparedFullName(), $condition, $group, $order, $limits, $row_key_field, $row_value_field);;

		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}

		return $result;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowDataListCustom($select, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		$result =  $this->dbc->getRowsCustom($select, $this->getPreparedFullName(), $condition, $group, $order, $limits, $row_key_field, $row_value_field);;

		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}

		return $result;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowPKDataList($condition = NULL, $group = '', $order = '', $limits = NULL)
	{
		$fieldNames = array();
		foreach ($this->primaryIndex()->fields() as $ormIndexField)
		{
			$ormField = $ormIndexField->getORMField();
			$fieldNames[] = $ormField->getName();
		}
		
		$result = $this->dbc->getRows($fieldNames, $this->getPreparedFullName(), $condition, $group, $order, $limits);
		
		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}
		
		return $result;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function insert($values, $fields = array())
	{
		$result =  $this->dbc->insert($this->getFullName(), $values, $fields);

		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}

		return $result;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function ins($values)
	{
		$result = $this->dbc->ins($this->getFullName(), $values);

		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}

		return $result;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function select($fields, $condition = NULL, $group = '', $order = '', $limits = NULL)
	{
		$result =  $this->dbc->select($fields, $this->getPreparedFullName(), $condition, $group, $order, $limits);;

		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}

		return $result;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function update($set, $condition = NULL, $order = '', $limit = '')
	{
		$result =  $this->dbc->update($this->getPreparedFullName(), $set, $condition, $order, $limit);

		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}

		return $result;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function replace($values, $fields = array())
	{
		$result =  $this->dbc->replace($this->getFullName(), $values, $fields);;

		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}

		return $result;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function delete($condition = NULL, $order = '', $limit = '')
	{
		$result =  $this->dbc->delete($this->getFullName(), NULL, $condition, $order, $limit);;

		if (self::$log)
		{
			$q = $this->dbc->getLastQuery();
			wechos($q);
		}

		return $result;
	}
}
?>
