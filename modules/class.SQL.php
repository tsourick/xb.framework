<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage db
*/

/**
* undocumented class
*/

class SQL
{
	/**
	* Create IN-condition
	* Creates condition with MySQL IN comparision operator from given array of values.
	*
	* @param string $name the expression to compare against the values; usually the name of a field
	* @param array $values an array of values to campare name against
	* @param bool $negate if true then NOT IN will be used instead of IN; default if false
	* @return string IN-condition which is ready to be inserted into plain SQL query
	* @static
	*/

	public static function in($name, $values, $negate = false)
	{
		SQL::_name($name);
		array_walk($values, array('SQL', '_value'));
		$not = $negate ? 'NOT ' : '';
		return $name . ' ' . $not . 'IN (' . implode(',', $values) . ')';
	}

	/**
	* Arbitrary PK not yet supported. Single name only.
	*/
	public static function pkin($values, $name = 'id', $negate = false)
	{
	 return SQL::in($name, $values, $negate);
	}

	
	/**
	* Make SQL identifier be SQL-ready
	* Quotes SQL identifier with backticks. The function recognizes and quotes field, field AS alias,
	* table.field and table.field AS alias expressions.
	*
	* @param string $name the identifier to prepare
	* @return string SQL-ready string
	*/

	public static function prepare_name($name)
	{
		SQL::_name($name);
		
		
		return $name;
	}
	
	
	/**
	* Make SQL identifier be SQL-ready
	* Quotes SQL identifier with backticks. The function recognizes and quotes field, field AS alias,
	* table.field and table.field AS alias expressions.
	* Nothing is returned but original value is modified.
	*
	* @param string $name reference to the value to quote
	* @internal
	* @static
	*/

	public static function _name(&$name)
	{
		$pieces = array();
		// wechos('Field: ' . $name);
		// preg_match('/^([a-zA-Z_][\w-]*)(?:\.([a-zA-Z_][\w-]*))?(?:\s+(?:(?:as)\s+)?(.+))?$/i', trim($name), $pieces);
		preg_match('/^([\w-()]+)(?:\.([\w-()]+))?(?:\s+(?:(?:as)\s+)?(.+))?$/i', trim($name), $pieces);
		//printr($pieces);
		// assert(isset($pieces[1]) && ! empty($pieces[1]));
		 if (! (isset($pieces[1]) && ! empty($pieces[1])))
		 {
		 	 throw new Exception("SQL::_name: string '$name' did not pass regexp.");
		 }
		
		$name = '`' . $pieces[1] . '`';
		if (isset($pieces[2]) && ! empty($pieces[2])) $name .= '.`' . $pieces[2] . '`';
		if (isset($pieces[3]) && ! empty($pieces[3])) $name .= ' `' . $pieces[3] . '`';
	}


	/**
	* Make SQL value be SQL-ready
	* Escapes and quotes SQL identifier with single quotes.
	*
	* @param string $value the value to prepare
	* @return string SQL-ready string
	*/

	public static function prepare_value($value)
	{
		SQL::_value($value);
		
		
		return $value;
	}


	/**
	* Make SQL value be SQL-ready
	* Escapes and quotes SQL identifier with single quotes.
	*
	* @param string $value reference to the value to escape and quote
	* @return string SQL-ready string
	* @internal
	* @static
	*/

	public static function _value(&$value)
	{
		if (is_null($value))
		{
			$value = 'NULL';
		}
		elseif (is_bool($value))
		{
			$value = $value ? 'TRUE' : 'FALSE';
		}
		elseif (is_float($value))
		{
			$value = sprintf("%F", $value);
		}
		else
		{
			$value = '\'' . addslashes($value) . '\'';
		}
	}
	
	/**
	* Escapes string with addslashes()
	*
	* @param string $string string to escape
	*/
	
	public static function escape($string)
	{
		return addslashes($string);
	}
	
	
	public static function pk($value, $name = 'id')
	{
		return array($name => $value);
	}
	
	
	public static function casewhenthen($name, $whenthen, $else = null)
	{
		$sql = "CASE " . (! is_null($name) ? $name : '') . "\n";
		
		foreach ($whenthen as $when => $then)
		{
			$sql .= "\tWHEN {$when} THEN {$then}\n";
		}
		
		$sql .= (! is_null($else) ? "ELSE {$else}" : '') . "\nEND";
		
		return $sql;
	}
}
?>
