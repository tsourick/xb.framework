<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
*/

/**
* undocumented class
*/

class SimpleMailException extends Exception
{
}

class SimpleMail
{
	public $email_from;
	public $email_to;
	public $subject;
	public $body;
	public $content_type;
	
	public $data_charset;
	public $send_charset;

	public $name_from;
	public $name_to;
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($email_from, $email_to, $subject = '', $body = '', $content_type = 'text/plain', $data_charset = 'utf-8', $send_charset = 'utf-8', $name_from = '', $name_to = '')
	{
		$email_from = trim($email_from);
		$email_to = trim($email_to);
		
		if (empty($email_from)) throw new SimpleMailException("Missing source email address");
		if (! is_email($email_from)) throw new SimpleMailException("Wrong source email address");
		if (empty($email_to)) throw new SimpleMailException("Missing destination email address");
		if (! is_email($email_to)) throw new SimpleMailException("Wrong destination email address");
		
		$this->email_from = $email_from;
		$this->email_to = $email_to;
		$this->subject = $subject;
		$this->body = $body;
		$this->content_type = $content_type;

		$this->data_charset = $data_charset;
		$this->send_charset = $send_charset;

		$this->name_from = $name_from;
		$this->name_to = $name_to;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function send()
	{
		$to = '';
		if (! empty($this->name_to))
		{
			$to = $this->mime_header_encode($this->name_to, $this->data_charset, $this->send_charset) . ' <' . $this->email_to . '>';
		}
		else
		{
			$to = $this->email_to;
		}
		
		$subject = $this->mime_header_encode($this->subject, $this->data_charset, $this->send_charset);

		$from = '';
		if (! empty($this->name_from))
		{
			$from = $this->mime_header_encode($this->name_from, $this->data_charset, $this->send_charset) . ' <' . $this->email_from . '>';
		}
		else
		{
			$from = $this->email_from;
		}
		
		if ($this->data_charset != $this->send_charset)
		{
			$body = iconv($this->data_charset, $this->send_charset, $this->body);
		}
		else
		{
			$body = $this->body;
		}
		
		$headers = 'From: ' . $from . "\r\n";
		$headers .= 'Content-type: ' . $this->content_type . '; charset=' . $this->send_charset . "\r\n";
	
		return @mail($to, $subject, $body, $headers);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function mime_header_encode($str, $data_charset, $send_charset)
	{
		if ($data_charset != $send_charset)
		{
			$str = iconv($data_charset, $send_charset, $str);
		}
		
		return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
	}
}
?>
