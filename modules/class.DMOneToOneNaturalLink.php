<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

require_once('class.DMLinkThroughTable.php');


require_once('class.DMOneToOneNaturalLinkBase.php');
if (PHP_VERSION >= '5.3')
{
	include('class.DMOneToOneNaturalLink.php_5_3.php');
}
else
{
	include('class.DMOneToOneNaturalLink.php_5_2.php');
}

?>
