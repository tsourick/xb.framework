<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

class DMCelkoEntityBase extends DMEntity
{
	const pri_field_name = 'id';
	const left_index_field_name = 'node_left_index';
	const right_index_field_name = 'node_right_index';
	const level_field_name = 'node_level';
	
	private $celkoTreeTable = NULL;
	private $preCondition = '';
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DMDataModel $dm, $name, $dmFields = array())
	{
		parent::__construct($dm, $name, $dmFields);
		
		
		// $this->ormTable = new ORMTable($dm->database(), $dm->options['table_name_prefix'] . $this->name);

		
		// Predefined fields
		
		// Preset PRIMARY key field

		$dmField = $this->createFieldFromConfig(array('name' => self::pri_field_name, 'type' => 'pri_id'));
		/*
		$fieldConfig = DMDataModel::$fieldTypeConfigs['pri_id'];
		$fieldConfig['name'] = self::pri_field_name;
		$fieldConfig['type'] = 'pri_id';
		
		$dmField = DMField::createFromConfig($this, $fieldConfig);
		*/

		$this->dmPrimaryFields[] = $dmField;
		
		
		// Preset left index field

		$this->createFieldFromConfig(array('name' => self::left_index_field_name, 'type' => 'strong_ext_id'));
		/*
		$fieldConfig = DMDataModel::$fieldTypeConfigs['strong_ext_id'];
		$fieldConfig['name'] = self::left_index_field_name;
		$fieldConfig['type'] = 'strong_ext_id';
		
		DMField::createFromConfig($this, $fieldConfig);
		*/

		// Preset right index field

		$this->createFieldFromConfig(array('name' => self::right_index_field_name, 'type' => 'strong_ext_id'));
		/*
		$fieldConfig = DMDataModel::$fieldTypeConfigs['strong_ext_id'];
		$fieldConfig['name'] = self::right_index_field_name;
		$fieldConfig['type'] = 'strong_ext_id';
		
		DMField::createFromConfig($this, $fieldConfig);
		*/

		// Preset level field

		$this->createFieldFromConfig(array('name' => self::level_field_name, 'type' => 'strong_ext_id'));
		/*
		$fieldConfig = DMDataModel::$fieldTypeConfigs['strong_ext_id'];
		$fieldConfig['name'] = self::level_field_name;
		$fieldConfig['type'] = 'strong_ext_id';
		
		DMField::createFromConfig($this, $fieldConfig);
		*/

		$this->ormTable->addIndex
		(
			ORMIndex::createFromConfig
			(
				$this->ormTable,
				array
				(
					'name' => 'PRIMARY',
					'type' => 'PRIMARY',
					'fields' => array
					(
						array('name' => 'id')
					)
				)
			)
		);

		
		// $dm->getORMDatabase()->addTable($this->ormTable);
		
		$this->celkoTreeTable = new DBCelkoTreeTable
		(
			$dm->getDBConnection(), $this->ormTable->getFullName(),
			self::pri_field_name,
			self::left_index_field_name,
			self::right_index_field_name,
			self::level_field_name
		);
	}
	
		
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addLinkEndPoint(DMLinkEndPoint $dmLinkEndPoint)
	{
		// Prevent unsupported relations

		$dmLink = $dmLinkEndPoint->link();
		
		// Celko entity can not be required
		if (($dmLinkEndPoint->isFrom() && $dmLink->fromRequired) || ($dmLinkEndPoint->isTo() && $dmLink->toRequired))
			throw new DMEntityException("Unsupported relation: celko entity can not be required.");
		
		
		$this->dmLinkEndPoints[] = $dmLinkEndPoint;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setPreCondition($pre_condition)
	{
		$this->preCondition = $pre_condition;
		
		$this->celkoTreeTable->setPreCondition($pre_condition);
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getFromCGIParentItemDataAll($level = NULL)
	{
		$primaryField = $this->dmPrimaryFields[0]; // In Celko entity there should be only one primary field
		
		$node_id = get_cgi_value($primaryField->getName(), $primaryField->getInputType(), $primaryField->getInputDefault());


		return $this->celkoTreeTable->getParent($node_id, $level);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getFromCGIParentItemDataListAll($min_level = 1)
	{
		$primaryField = $this->dmPrimaryFields[0]; // In Celko entity there should be only one primary field
		
		$node_id = get_cgi_value($primaryField->getName(), $primaryField->getInputType(), $primaryField->getInputDefault());


		return $this->celkoTreeTable->getParentTrunk($node_id, $min_level);
	}

	/**
	* Enity table should be referenced through 't' alias (i.e. 't.<field name>') within precondition.
	*/
	public function getParentItemDataListAll($node_pk, $min_level = 1, $pre_condition = NULL)
	{
		$result = NULL;


		// Set temporary precondition
		if (! is_null($pre_condition))
		{
			$this->celkoTreeTable->setPreCondition($pre_condition);
		}

		$result = $this->celkoTreeTable->getParentTrunk($node_pk['id'], $min_level);

		// Restore permanent precondition
		if (! is_null($pre_condition))
		{
			$this->celkoTreeTable->setPreCondition($this->preCondition);
		}
		
		
		return $result;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getParentItemDataAll($node_pk, $level = NULL, $pre_condition = NULL)
	{
		$result = NULL;


		// Set temporary precondition
		if (! is_null($pre_condition))
		{
			$this->celkoTreeTable->setPreCondition($pre_condition);
		}

		$result = $this->celkoTreeTable->getParent($node_pk['id'], $level);

		// Restore permanent precondition
		if (! is_null($pre_condition))
		{
			$this->celkoTreeTable->setPreCondition($this->preCondition);
		}
		
		
		return $result;
	}
	

	/**
	* Enity table should be referenced through 't' alias (i.e. 't.<field name>') within precondition.
	*/
	public function getChildItemDataListAll($node_pk, $max_depth = 1, $pre_condition = NULL) // $node_pk holds id only for now
	{
		$result = NULL;


		$display_fields = array();
		foreach ($this->dmFields as $dmField)
		{
			$display_fields[] = $dmField->getName();
		}


		// Set temporary precondition
		if (! is_null($pre_condition))
		{
			$this->celkoTreeTable->setPreCondition($pre_condition);
		}

		$result = $this->celkoTreeTable->getChildren($display_fields, $node_pk['id'], $max_depth, self::left_index_field_name);
		
		// Restore permanent precondition
		if (! is_null($pre_condition))
		{
			$this->celkoTreeTable->setPreCondition($this->preCondition);
		}
		
		
		return $result;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getFromCGIByPKChildItemDataListAll($max_depth = 1)
	{
		$primaryField = $this->dmPrimaryFields[0]; // In Celko entity there should be only one primary field
		
		$node_id = get_cgi_value($primaryField->getName(), $primaryField->getInputType(), $primaryField->getInputDefault());
		
		return $this->getChildItemDataListAll(array('id' => $node_id), $max_depth);
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function _insertItemData($data)
	{
		$item_pk = array();
		
		
		// Check parent node id (insert new root node if id=0)

		if
		(
			! isset($data[$this->name]['depends']) ||
			! isset($data[$this->name]['depends'][$this->name]) ||
			! isset($data[$this->name]['depends'][$this->name][self::pri_field_name])
		) throw new DMEntityException("No parent node id provided through 'depends' section for entity {$this->name} on insert.");
		
		
		$parent_node_id = $data[$this->name]['depends'][$this->name][self::pri_field_name];
		
		if ($parent_node_id != 0)
		{
			$c = $this->ormTable->getRowCount(array(self::pri_field_name => $parent_node_id));
			
			if ($c == 0) throw new DMEntityException("Could not find parent node (id: $parent_node_id) on insert into entity {$this->name}.");
		}
		
		
		unset($data[$this->name]['depends'][$this->name]); // Exclude parent node dependency loop from further checking
		

		// Check/set primary fields' values
		
		$values =& $data[$this->name]['values']; // create ref
		
		foreach ($this->dmPrimaryFields as $dmField)
		{
			$field_name = $dmField->getName();
			
			if (! isset($values[$field_name]))
			{
				if (! $dmField->ORMField()->isAutoincrement()) throw new DMEntityException("No primary values provided for insert entity '{$this->name}' data.");
				
				$next_value = $dmField->ORMField()->getNextValue(); // throws ORMFieldException
				$values[$field_name] = $next_value;
			}
			
			
			$item_pk[$field_name] = $values[$field_name]; // store inserted item PK value
		}
		
		unset($values); // remove ref
		

		// Check/set special fields' values
		
		$values =& $data[$this->name]['values']; // create ref

		foreach ($this->dmFields as $dmField)
		{
			$field_name = $dmField->getName();
			$field_type = $dmField->getType();
			
			switch ($field_type)
			{
				case 'created':
					$values[$field_name] = $this->dbc->nowDateTime();
				break;
			}
		}
		
		unset($values); // remove ref


		// Scan link end points for dependencies (required and non-required)

		$dependencies = array();
		
		foreach ($this->dmLinkEndPoints as $dmLinkEndPoint)
		{
			$dmLink = $dmLinkEndPoint->link();
			
			
			// Skip uncheckable cases
			if ($dmLink->getType() == '1:n' && $dmLinkEndPoint->isFrom() && ! $dmLink->fromRequired) continue;
			
			
			// Skip links in use
			if (in_array($dmLink->getName(), self::$_insertItemData_LinkNamesInUse)) continue;
				

			self::$_insertItemData_LinkNamesInUse[] = $dmLink->getName(); // store link usage
			
			
			if ($dmLinkEndPoint->isFrom())
			{
				$dependencies[] = array('link' => $dmLink, 'actOn' => 'from');
			}

			if ($dmLinkEndPoint->isTo())
			{
				$dependencies[] = array('link' => $dmLink, 'actOn' => 'to');
			}
		}
		
		
		// Check dependencies and adjust values if necessary
		
		if (! empty($dependencies))
		{
			foreach ($dependencies as $dependency)
			{
				$data = $dependency['link']->checkAndAdjustForInsert($dependency['actOn'], $data); // throws DMLinkException
			}
		}


		// Insert entity item data
		$this->celkoTreeTable->insertNode($data[$this->name]['values'], $parent_node_id);		

		
		return $item_pk;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function _deleteItemData($condition = NULL)
	{
		// Collect deleted nodes' pk values
		
		$node_ids = $this->ormTable->getRowDataList(self::pri_field_name, $condition, '', '', NULL, '', self::pri_field_name);
		
		if (! empty($node_ids)) // there is something to delete
		{
			// Get all children nodes and the most parent nodes
			
			$parent_node_ids = $node_ids;
			$children_node_ids = array();
			foreach ($node_ids as $node_id)
			{
				if (in_array($node_id, $children_node_ids))
				{
					// node is already a child of another node and can not be the most parent
					array_remove($node_id, $parent_node_ids);
					
					continue; // children of the node already collected, so skip iteration
				}
					
	
				$rows = $this->celkoTreeTable->getChildren(array(self::pri_field_name), $node_id);
				
				foreach ($rows as $row)
				{
					$children_node_ids[] = $row[self::pri_field_name];
				}
				
	
				$overlapped_parent_node_ids = array_intersect($parent_node_ids, $children_node_ids);
				if (! empty($overlapped_parent_node_ids))
				{
					// exclude overlapped from parent
					$parent_node_ids = array_diff($parent_node_ids, $overlapped_parent_node_ids);
				}
			}
			
			$children_node_ids = array_unique($children_node_ids);
			
			
			$dependencies = array();
			
			foreach ($this->dmLinkEndPoints as $dmLinkEndPoint)
			{
				$dmLink = $dmLinkEndPoint->link();
				
	
				// Skip uncheckable cases
				if ($dmLink->getType() == '1:n' && $dmLinkEndPoint->isTo() && ! $dmLink->fromRequired) continue;
				
				
				// Skip links in use
				if (in_array($dmLink->getName(), self::$_deleteItemData_LinkNamesInUse)) continue;
					
	
				self::$_deleteItemData_LinkNamesInUse[] = $dmLink->getName(); // store link usage
	
	
				if ($dmLinkEndPoint->isFrom())
				{
					$dependencies[] = array('link' => $dmLink, 'actOn' => 'from');
				}
	
				if ($dmLinkEndPoint->isTo())
				{
					$dependencies[] = array('link' => $dmLink, 'actOn' => 'to');
				}
			}
			
			
			if (! empty($dependencies))
			{
				// Change condition to just refer all deleted nodes
				$all_node_ids = array_merge($parent_node_ids, $children_node_ids);
				$condition = SQL::in(self::pri_field_name, $all_node_ids);
				
	
				foreach ($dependencies as $dependency)
				{
					$dependency['link']->checkAndAdjustForDelete($dependency['actOn'], $condition);
				}
			}
			
	
			foreach ($parent_node_ids as $node_id)
			{
				$this->celkoTreeTable->deleteNode($node_id);
			}
		}
	}
/*	
	static public function createFromConfig(DMDataModel $dm, $config)
	{
		$dmEntity = new self($dm, $config['name']);
		

		foreach ($config['fields'] as $field_config)
		{
			$dmEntity->createFieldFromConfig($field_config);
		}
		
		
		return $dmEntity;
	}
*/

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function moveNodeAfter($node_id, $sibling_node_id, $as_child)
	{
		$this->celkoTreeTable->moveHiveAfter($node_id, $sibling_node_id, $as_child);
	}
}

?>
