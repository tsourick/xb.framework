<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage orm
*/

require_once('class.ORMTable.php');
require_once('class.SQL.php');


class ORMDatabaseException extends ORMException
{
}

class ORMDatabase
{
	private $dbc = NULL;
	
	private $name = ''; // Database name
	
	private $ormTables = array(); // Database tables
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($dbc, $name)
	{
		$this->dbc = $dbc;

		$this->name = $name;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getName()
	{
		return $this->name;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPreparedName()
	{
		return SQL::prepare_name($this->name);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getDBConnection()
	{
		return $this->dbc;
	}
	
	
	/**
	* $config structure:

	array
	(
		'name' => '<database name>',
		'tables' => array
		(
			<table config>,
			<table config>,
			...
		)
	);
	*/
	
	static public function createFromConfig($dbc, $config)
	{
		$ormDatabase = new self($dbc, $config['name']);
		
		foreach ($config['tables'] as $table_config)
		{
			$ormDatabase->createTableFromConfig($table_config);
			
			// $ormDatabase->addTable($ormTable);
		}
		
		return $ormDatabase;
	}
	
	/**
	* Config structure:
	* @see ORMTable->createFromConfig()
	*/
	public function createTableFromConfig($config)
	{
		$ormTable = ORMTable::createFromConfig($this, $config);
		
		return $ormTable;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function deleteTable($name)
	{
		if (! isset($this->ormTables[$name])) throw new ORMDatabaseException("Could not delete table '" . $name . "' from ORMDatabase '" . $this->getName() . "'. Table not found in the database.");

		
		$ormTable = $this->ormTables[$name];
		
		// $amEntity = $this->removeEntity($this->amEntities[$name]);
		$ormTable->destruct(); // this->removeEntity() is called from within entity
		
		
		unset($ormTable);
	}

	
	// called from table
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addTable(ORMTable $ormTable)
	{
		if ($ormTable->database() !== $this) throw new ORMDatabaseException("Could not add table '" . $ormTable->getName() . "' to database '" . $this->getName() . "' because table belongs to another database.");

		$this->ormTables[$ormTable->getName()] = $ormTable;
	}

	// called from table
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function removeTable(ORMTable $ormTable)
	{
		if ($ormTable->database() !== $this) throw new ORMDatabaseException("Could not remove table '" . $ormTable->getName() . "' from database '" . $this->getName() . "' because table belongs to another database.");
		
		
		$name = $ormTable->getName();
		
		
		if (! isset($this->ormTables[$name])) throw new ORMDatabaseException("Could not remove table '" . $ormTable->getName() . "' from database '" . $this->getName() . "'. Table belongs to this database but was not added.");

			
		unset($this->ormTables[$name]);
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetFillQuerySet()
	{
		$qset = array();
		
		
		$qset[] = 'USE ' . SQL::prepare_name($this->getName());
		
		
		foreach ($this->ormTables as $ormTable)
		{
			$qset = array_merge($qset, $ormTable->SQLDDLGetCreateQuerySet());
		}
		
		
		return $qset;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetCreateQuerySet()
	{
		$qset = array();
		
		
		$qset[] = 'CREATE DATABASE ' . SQL::prepare_name($this->getName());

		
		$qset = array_merge($qset, $this->SQLDDLGetFillQuerySet());

		
		return $qset;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetDropQuerySet()
	{
		$qset = array();
		

		$qset[] = 'DROP DATABASE ' . SQL::prepare_name($this->name);

		
		return $qset;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetEmptyQuerySet()
	{
		$qset = array();
		

		$qset[] = 'USE ' . SQL::prepare_name($this->getName());


		foreach ($this->ormTables as $ormTable)
		{
			$qset = array_merge($qset, $ormTable->SQLDDLGetDropQuerySet());
		}

		
		return $qset;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetCreateSQLDump()
	{
		$qset = $this->SQLDDLGetCreateQuerySet();
		
		return implode("\n\n", array_map(create_function('$q', 'return $q . \';\';'), $qset));
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetDropSQLDump()
	{
		$qset = $this->SQLDDLGetDropQuerySet();
		
		return implode("\n\n", array_map(create_function('$q', 'return $q . \';\';'), $qset));
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetEmptySQLDump()
	{
		$qset = $this->SQLDDLGetEmptyQuerySet();

		return implode("\n\n", array_map(create_function('$q', 'return $q . \';\';'), $qset));
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function table($name)
	{
		return $this->ormTables[$name];
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function remapTable(ORMTable $ormTable, $old_name)
	{
		if ($ormTable->database() !== $this) throw new ORMDatabaseException("Could not remap table '" . $ormTable->getName()  . "' from name '" . $old_name . "' because table belongs to another database.");

		if (! array_key_exists($old_name, $this->ormTables)) throw new ORMDatabaseException("Could not remap table '" . $ormTable->getName()  . "' from name '" . $old_name . "' because old name was not found within database.");

			
		array_replace_key($this->ormTables, $old_name, $ormTable->getName());
	}
	

	// DBConnection methods
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function query($qset)
	{
		return $this->dbc->query($qset);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function exec($qset)
	{
		return $this->dbc->exec($qset);
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function select($fields, $storage = '', $condition = NULL, $group = '', $order = '', $limits = NULL)
	{
		return $this->dbc->select($fields, $storage, $condition, $group, $order, $limits);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRow($fields, $storage = '', $condition = NULL)
	{
		return $this->dbc->getRow($fields, $storage, $condition);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowAll($storage, $condition = NULL)
	{
		return $this->dbc->getRowAll($storage, $condition);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowCustom($select, $storage = '', $condition = NULL)
	{
		return $this->dbc->getRowCustom($select, $storage, $condition);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowValue($field_name, $storage = '', $condition = NULL)
	{
		return $this->dbc->getRowValue($field_name, $storage, $condition);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowCount($table, $condition = NULL, $group = '')
	{
		return $this->dbc->getRowCount($table, $condition, $group);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRows($fields, $storage = '', $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dbc->getRows($fields, $storage, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowsAll($storage, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dbc->getRowsAll($storage, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRowsCustom($select, $storage = '', $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dbc->getRowsCustom($select, $storage, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function delete($storage, $using = NULL, $condition = NULL, $order = '', $limit = '')
	{
		return $this->dbc->delete($storage, $using, $condition, $order, $limit);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function update($storage, $set, $condition = NULL, $order = '', $limit = '')
	{
		return $this->dbc->update($storage, $set, $condition, $order, $limit);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function ins($table, $values)
	{
		return $this->dbc->ins($table, $values);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function insert($table, $values, $fields = array())
	{
		return $this->dbc->insert($table, $values, $fields);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function replace($table, $values, $fields = array())
	{
		return $this->dbc->replace($table, $values, $fields);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function queryGetRows($qset, $row_key_field = '', $row_value_field = '')
	{
		return $this->dbc->queryGetRows($qset, $row_key_field, $row_value_field);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getLastQuery()
	{
		return $this->dbc->getLastQuery();
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function beginTransaction()
	{
		return $this->dbc->beginTransaction();
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function commit()
	{
		return $this->dbc->commit();
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function rollBack()
	{
		return $this->dbc->rollBack();
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function inTransaction()
	{
		return $this->dbc->inTransaction();
	}
}

?>