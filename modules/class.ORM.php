<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage orm
*/

class ORMException extends Exception
{
}

require_once('class.ORMDatabase.php');

?>