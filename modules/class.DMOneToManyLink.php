<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

require_once('class.DMLink.php');
require_once('class.SQL.php');


require_once('class.DMOneToManyLinkBase.php');
if (PHP_VERSION >= '5.3')
{
	include('class.DMOneToManyLink.php_5_3.php');
}
else
{
	include('class.DMOneToManyLink.php_5_2.php');
}

?>
