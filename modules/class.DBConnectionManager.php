<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage db
*/

// require_once('class.DBConnection.php');
Framework::useClass('DBConnection');

class DBConnectionManagerException extends Exception
{
}

class DBConnectionManager
{
	private static $instance = NULL;

	// private static $systemConfig = NULL;
	
	private $connections = array();

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function getObject()
	{
		$object = NULL;
		
		if (! is_null(self::$instance))
		{
			$object = self::$instance;
		}
		
		return $object;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __clone()
	{
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct()
	{
		if (! is_null(self::$instance)) throw new DBConnectionManagerException(__CLASS__ . ' singleton already created');
		
		// if (is_null(self::$systemConfig)) throw new DBConnectionManagerException('System config should be set before initialization');
		

		$connection_list = explode(',', Framework::get('dbconnections/list'));
		
		foreach ($connection_list as $connection_name)
		{
			if(! $c = Framework::get($connection_name)) throw new DBConnectionManagerException("Connection '$connection_name' is not configured. Section not found.");
			
			$dsn = "{$c['type']}:host={$c['host']};port={$c['port']};dbname={$c['dbname']}";
			
			$this->connections[$connection_name] = array('dsn' => $dsn, 'username' => $c['username'], 'password' => $c['password'], 'object' => NULL); 
		}


		self::$instance = $this;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getConnection($name = '')
	{
		$connectionObject = NULL;

		if (empty($name)) list($name) = array_keys($this->connections);
		
		if (isset($this->connections[$name]))
		{
			$connection =& $this->connections[$name];
			
			if (is_null($connection['object']))
			{
				$connection['object'] = new DBConnection($connection['dsn'], $connection['username'], $connection['password']);
			}
			
			$connectionObject = $connection['object'];
		}
		
		return $connectionObject;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getDefaultConnection()
	{
		return $this->getConnection();
	}

	/**
	* Config may be set only once and should be set before object initialization
	*/
	/*
	public static function setSystemConfig($c)
	{
		$r = false;
		
		if (is_null(self::$instance))
		{
			self::$systemConfig = $c;
			$r = true;
		}
		
		return $r;
	}
	*/
}

?>
