<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

class DMEntity extends DMEntityBase
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function createFromConfig(DMDataModel $dm, $config)
	{
		$dmEntity = new static($dm, $config['name']);
		

		foreach ($config['fields'] as $field_config)
		{
			$dmEntity->createFieldFromConfig($field_config);
		}
		
		
		return $dmEntity;
	}
}

?>
