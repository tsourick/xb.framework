<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage ui
*/

/**
* WIZARD class
*
* @version 1.0
*/



require_once('class.SimpleForm.php');
	
class Wizard
{
	// public $db;
	// public $user;
	
	public $id = NULL;
	public $storage = NULL;
	
	public $fieldGroups;
	public $wizardLocation;
	public $activeForm;
	public $step;

	public $cgiTriggerName;
	public $cgiStepName;
	public $stepHandler;
	public $wizardHandler;
	public $redirectUrl;

/*
		$trigger_group = array
		(
			'fields' => array
			(
				$cgi_trigger_name => array('type' => 'hidden', 'value' => 1, 'required' => true)
				'wizard_id' => array('type' => 'hidden', 'value' => 1, 'required' => true)
				$cgi_step_name => array('type' => 'hidden', 'value' => 1, 'required' => true)
			)
		);
		
		array_unshift($field_groups, $trigger_group);
*/

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($storage, $field_groups, $wizard_location, $id = '', $wizard_handler = NULL, $redirect_url = NULL, $cgi_trigger_name = 'check_trigger', $cgi_step_name = 'step')
	{
		$this->fieldGroups = $field_groups;
		$this->wizardLocation = $wizard_location;

		$this->cgiTriggerName = $cgi_trigger_name;
		$this->cgiStepName = $cgi_step_name;
		$this->stepHandler = NULL;
		$this->wizardHandler = $wizard_handler;
		$this->redirectUrl = $redirect_url;
		
		$this->step = get_cgi_value($this->cgiStepName, 'i', 1);
		
		// $storage = Session :: singleton();
		$wd = $storage->get('wizard');
		
		if (! empty($id))
		{
			if (isset($wd[$id]))
			{
				$this->id = $id;
			}
			else throw new Exception('Wizard ID "' . $id . '" not found!');
		}
		else
		{
			$this->id = str_rand(8);
			$wd = array($this->id => array());
			$storage->set('wizard', $wd);
		}

		$this->storage = $storage;


		$field_groups = $this->fieldGroups[$this->step];
		
		$wizard_group = array
		(
			'fields' => array
			(
				'wizard_id' => array('type' => 'hidden', 'value' => $this->id, 'required' => true),
				$this->cgiStepName => array('type' => 'hidden', 'value' => $this->step, 'required' => true)
			)
		);
		
		array_unshift($field_groups, $wizard_group);

		$this->activeForm = new SimpleForm($field_groups, NULL, NULL, $this->cgiTriggerName);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setStepHandler($callback)
	{
		$result = false;
		
		if (is_callable($callback))
		{
			$this->stepHandler = $callback;
			
			$result = true;
		}
		
		return $result;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function handleStep()
	{
		$r = true;
		
		// handle step
		
		if ($this->stepHandler !== NULL)
		{
			$r = call_user_func($this->stepHandler, $this, $this->step, $this->getData($this->step));
		}
		
		
		// handle wizard
		
		if ($r)
		{
			if ($this->step == count($this->fieldGroups))
			{
				if ($this->wizardHandler !== NULL)
				{
					$r = call_user_func($this->wizardHandler, $this->getData());
				}
	
				if ($r && $this -> redirectUrl !== NULL)
				{
					$this->close();
					
					xbf_http_redirect($this->redirectUrl);
				}
			}
			else
			{
				$params = $this->wizardLocation[1];
				$params['wizard_id'] = $this->id;
				$params[$this->cgiStepName] = $this->step + 1;
				
				xbf_http_redirect($this->wizardLocation[0], $params);
			}
		}
		
		return $r;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getId()
	{
		return $this->id;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getStep()
	{
		return $this->step;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setActiveFormData($data)
	{
		$this->activeForm->setData($data);
	}
/*
	function get_form($field_groups, $step)
	{
		// Init form
		
		$form =& new form($this -> cms);

		$form -> set_fields($field_groups);

		
		// Load data from wizard step into the form if the data exist
		if (isset($_SESSION['wizard'][$this -> id][$step])) $form -> set_data($_SESSION['wizard'][$this -> id][$step]);


		// Return the form
		return $form -> get_form();
	}
*/		
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getActiveForm()
	{
		$wd = $this->storage->get('wizard');
		
		// Load data from wizard step into the form if the data exist
		if (isset($wd[$this -> id][$this->step])) $this->activeForm->setData($wd[$this -> id][$this->step]);


		// Return the form
		return $this->activeForm->getForm();
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function checkActiveForm()
	{
		$r = false;
		
		$wd = $this->storage->get('wizard');

		// Load data from wizard step into the form if the data exist
		if (isset($wd[$this -> id][$this->step])) $this->activeForm->setData($wd[$this -> id][$this->step]);


		// Check and save the form
		if ($r = $this->activeForm->check())
		{
			// Save captured form data
			$wd[$this->id][$this->step] = $this->activeForm->getData();
			$this->storage->set('wizard', $wd);
		}

		return $r;
	}
	/*
	function processForm($field_groups, $step, &$captured_form)
	{
		$R = NULL;
		$E =& new errors;
		
		
		// Init form
		
		$form =& new form($this -> cms);

		$form -> set_fields($field_groups);

		
		// Load data from wizard step into the form if the data exist
		if (isset($_SESSION['wizard'][$this -> id][$step])) $form -> set_data($_SESSION['wizard'][$this -> id][$step]);


		// Capture the form
		$r = $form -> capture();

		if (errors :: is_error($r)) $E -> import($r);
		
		
		if ($E -> is_empty())
		{
			// Save captured form data
			$_SESSION['wizard'][$this -> id][$step] = $form -> get_data();
		}
		else
		{
			$captured_form = $form -> get_form();
		}


		if (! $E -> is_empty()) $R = $E;
		
		
		return $R;
	}
*/
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRawData($step = '')
	{
		$r = NULL;

		
		$wd = $this->storage->get('wizard');
		
		if (! empty($step))
		{
			if (isset($wd[$this -> id][$step])) $r = $wd[$this -> id][$step];
		}
		else
		{
			$r = array();
			
			foreach ($wd[$this -> id] as $step_data)
			{
				$r = array_merge($r, $step_data);
			}
		}


		return $r;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getData($step = '')
	{
		$r = NULL;

		$r = $this->getRawData($step);
		
		if ($r !== NULL)
		{
			unset($r['wizard_id']);
			unset($r[$this->cgiStepName]);
		}
		
		
		return $r;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getValue($step, $name)
	{
		$r = NULL;
		
		$wd = $this->storage->get('wizard');
		
		if (isset($wd[$this -> id][$step]) && isset($wd[$this -> id][$step][$name]))
		{
			$r = $wd[$this -> id][$step][$name];
		}
		
		return $r;
	}
	
	/**
	* Should be rewritten
	*/
	public function setValue($step, $name, $value)
	{
		$r = false;
		
		$wd = $this->storage->get('wizard');
		
		if (isset($wd[$this -> id][$step]) && isset($wd[$this -> id][$step][$name]))
		{
			$wd[$this -> id][$step][$name] = $value;
			
			$this->storage->set('wizard', $wd);
			
			$r = true;
		}
		
		return $r;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function close()
	{
		$wd = $this->storage->get('wizard');
		if (isset($wd[$this -> id]))
		{
			unset($wd[$this -> id]);
			$this->storage->set('wizard', $wd);
		}
	}
}

?>
