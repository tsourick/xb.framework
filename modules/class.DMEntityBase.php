<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

class DMEntityBase
{
	protected $dm = NULL; // Data model

	protected $dbc = NULL; // DBConnection instance

	protected $name; // Entity name string
	
	protected $ormTable; // ORMTable instance

	protected $dmFields = array();
	
	protected $dmPrimaryFields = array();
	
	protected $dmLinkEndPoints = array();
	
	
	static protected $_insertItemData_LinkNamesInUse = array();
	static protected $_deleteItemData_LinkNamesInUse = array();
	
	/**
	*
		Config structure
		
		array
		(
			'name' => '<entity name>',
			'fields' => array
			(
				array('name' => '<field name>', 'type' => '<entity field type name>')
			)
		)
	*/
	public function __construct(DMDataModel $dm, $name, $dmFields = array())
	{
		$this->dm = $dm;
		
		$this->dbc = $dm->getDBConnection();

		// $this->name = $config['name'];
		$this->name = $name;
		
		$this->dmFields = $dmFields;

		
		$dm->addEntity($this);
		
		
		$this->ormTable = new ORMTable($dm->database(), $dm->makeORMTableName($this->name));
	}
	
	// THIS IS NOT A LANGUAGE DESTRUCTOR
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function destruct()
	{
		$this->ormTable->destruct();

		
		$this->dm->removeEntity($this);
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getName()
	{
		return $this->name;
	}
	
	
	/**
	*
	*
	* @return
	*/

	public function getPreparedName()
	{
		return $this->ormTable->getPreparedName();
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setName($name)
	{
		if (strcmp($this->name, $name) != 0)
		{
			$this->ormTable->setName($this->dm->makeORMTableName($name));
			
			
			$old_name = $this->name;

			$this->name = $name;
			
			
			$this->dm->remapEntity($this, $old_name);
			
			
			foreach ($this->dmLinkEndPoints as $dmLinkEndPoint)
			{
				$dmLinkEndPoint->link()->remapEntity($this, $old_name);
			}
		}
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function dataModel()
	{
		return $this->dm;
	}	


	/**
	*
	*
	* @return
	*/

	public function getLastQuery()
	{
		return $this->dbc->getLastQuery();
	}	

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function ORMTable()
	{
		return $this->ormTable;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPrimaryFields()
	{
		return $this->dmPrimaryFields;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function field($name)
	{
		return $this->dmFields[$name];
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function fields()
	{
		return $this->dmFields;
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function remapField(DMField $dmField, $old_name)
	{
		if ($dmField->entity() !== $this) throw new DMEntityException("Could not remap field '" . $dmField->getName()  . "' from name '" . $old_name . "' because field belongs to another entity.");

		if (! array_key_exists($old_name, $this->dmFields)) throw new DMEntityException("Could not remap field '" . $dmField->getName()  . "' from name '" . $old_name . "' because old name was not found within entity.");

		if (array_key_exists($old_name, $this->dmPrimaryFields)) throw new DMEntityException("Could not remap primary field '" . $dmField->getName()  . "' from name '" . $old_name . "' because primary field remapping is currently forbidden.");

			
		array_replace_key($this->dmFields, $old_name, $dmField->getName());
	}

/*
	static public function createFromConfig(DMDataModel $dm, $config)
	{
		$dmEntity = new self($dm, $config['name']);
		

		foreach ($config['fields'] as $field_config)
		{
			$dmEntity->createFieldFromConfig($field_config);
		}
		
		
		return $dmEntity;
	}
*/	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function createFieldFromConfig($config)
	{
		$dmConfig = DMDataModel::$fieldTypeConfigs[$config['type']];
		$dmConfig['name'] = $config['name'];
		$dmConfig['type'] = $config['type'];
		
		$dmField = DMField::createFromConfig($this, $dmConfig);
		
		
		return $dmField;
	}
	
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addField(DMField $dmField)
	{
		$this->dmFields[$dmField->getName()] = $dmField;
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addLinkEndPoint(DMLinkEndPoint $dmLinkEndPoint)
	{
		$this->dmLinkEndPoints[] = $dmLinkEndPoint;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function removeLinkEndPoint(DMLinkEndPoint $dmLinkEndPoint)
	{
		foreach ($this->dmLinkEndPoints as $k => $p)
		{
			if ($p == $dmLinkEndPoint) unset($this->dmLinkEndPoints[$k]);
		}
	}


	/*
	public function primaryIndex()
	{
		return $this->ormTable->primaryIndex();
	}
	*/
		
/*	
	public function getFromCGIByPKItemDataAll($condition = NULL)
	{
		if (is_null($condition))
		{
			$condition = array();
		}
		
		foreach ($this->dmPrimaryFields as $dmField)
		{
			$cgi_value = get_cgi_value($dmField->getName(), $dmField->getInputType(), $dmField->getInputDefault());
			
			$condition[$dmField->getName()] = $cgi_value;
		}
	
		return $this->ormTable->getRowDataAll($condition);
	}
*/	
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getItemCount($condition = NULL, $group = '')
	{
		return $this->ormTable->getRowCount($condition, $group);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getItemData($fields, $condition = NULL)
	{
		return $this->ormTable->getRowData($fields, $condition);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getItemDataAll($condition = NULL)
	{
		return $this->ormTable->getRowDataAll($condition);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getItemDataCustom($select, $condition = NULL)
	{
		return $this->ormTable->getRowDataCustom($select, $condition);
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getItemValue($field_name, $condition = NULL)
	{
		return $this->ormTable->getRowValue($field_name, $condition);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getItemDataList($fields, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->ormTable->getRowDataList($fields, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getItemDataListAll($condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->ormTable->getRowDataListAll($condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getItemDataListCustom($select, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->ormTable->getRowDataListCustom($select, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function updateItemData($set, $condition = NULL, $order = '', $limit = '')
	{
		// Check/set special fields' values
		
		foreach ($this->dmFields as $dmField)
		{
			$field_name = $dmField->getName();
			$field_type = $dmField->getType();
			
			switch ($field_type)
			{
				case 'modified':
					if (is_array($set))
					{
						$set[$field_name] = $this->dbc->nowDateTime();
					}
					else
					{
						$set .= ', ' . $dmField->ORMField()->getPreparedNameValue($this->dbc->nowDateTime());
					}					
				break;
			}
		}


		return $this->ormTable->update($set, $condition, $order, $limit);
	}

	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function _insertItemData($data)
	{
		$item_pk = array();
		
		
		// Check/set primary fields' values
		
		$values =& $data[$this->name]['values']; // create ref

		foreach ($this->dmPrimaryFields as $dmField)
		{
			$field_name = $dmField->getName();

			if (! isset($values[$field_name]))
			{
				if (! $dmField->ORMField()->isAutoincrement()) throw new DMEntityException("No primary values provided for insert entity '{$this->name}' data.");
				
				$next_value = $dmField->ORMField()->getNextValue(); // throws ORMFieldException
				$values[$field_name] = $next_value;
			}
			
			
			$item_pk[$field_name] = $values[$field_name]; // store inserted item PK value
		}
		
		unset($values); // remove ref


		// Check/set special fields' values
		
		$values =& $data[$this->name]['values']; // create ref

		foreach ($this->dmFields as $dmField)
		{
			$field_name = $dmField->getName();
			$field_type = $dmField->getType();
			
			switch ($field_type)
			{
				case 'created':
					$values[$field_name] = $this->dbc->nowDateTime();
				break;
			}
		}
		
		unset($values); // remove ref
		
		
		// Scan link end points for dependencies (required and non-required)

		$dependencies = array();
		
		foreach ($this->dmLinkEndPoints as $dmLinkEndPoint)
		{
			$dmLink = $dmLinkEndPoint->link();
			
			
			// Skip uncheckable cases
			if ($dmLink->getType() == '1:n' && $dmLinkEndPoint->isFrom() && ! $dmLink->fromRequired) continue;
			
			
			// Skip links in use
			if (in_array($dmLink->getName(), self::$_insertItemData_LinkNamesInUse)) continue;
				

			self::$_insertItemData_LinkNamesInUse[] = $dmLink->getName(); // store link usage
			
			
			if ($dmLinkEndPoint->isFrom())
			{
				$dependencies[] = array('link' => $dmLink, 'actOn' => 'from');
			}

			if ($dmLinkEndPoint->isTo())
			{
				$dependencies[] = array('link' => $dmLink, 'actOn' => 'to');
			}
		}
		
		
		// Check dependencies and adjust values if necessary

		if (! empty($dependencies))
		{
			foreach ($dependencies as $dependency)
			{
				$data = $dependency['link']->checkAndAdjustForInsert($dependency['actOn'], $data); // throws DMLinkException
			}
		}


		// Insert entity item data
		$this->ormTable->ins($data[$this->name]['values']);

		
		return $item_pk;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function insertItemData($data)
	{
		$item_pk = NULL;
		

		if (! $this->dbc->inTransaction())
		{
			$this->dbc->beginTransaction();
	
			try
			{
				$item_pk = $this->_insertItemData($data);
				
				// finally
				self::$_insertItemData_LinkNamesInUse = array(); // clear
	
				$this->dbc->commit();
			}
			catch (Exception $e)
			{
				// finally
				self::$_insertItemData_LinkNamesInUse = array(); // clear
	
				$this->dbc->rollBack();
				
				throw $e;
			}
		}
		else
		{
			$item_pk = $this->_insertItemData($data);
			
			// finally
			self::$_insertItemData_LinkNamesInUse = array(); // clear
		}
		
		
		return $item_pk;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function insertFromCGIItemData()
	{
		$values = array();


		// Collect field data from CGI
		foreach ($this->dmFields as $dmField)
		{
			if (in_array($dmField, $this->dmPrimaryFields)) continue;
			
			$values[$dmField->getName()] = $dmField->getCGIValue();
		}
		

		$this->insertItemData($values);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function _deleteItemData($condition = NULL)
	{
		$dependencies = array();
		
		foreach ($this->dmLinkEndPoints as $dmLinkEndPoint)
		{
			$dmLink = $dmLinkEndPoint->link();
			

			// Skip uncheckable cases
			if ($dmLink->getType() == '1:n' && $dmLinkEndPoint->isTo() && ! $dmLink->fromRequired) continue;
			
			
			// Skip links in use
			if (in_array($dmLink->getName(), self::$_deleteItemData_LinkNamesInUse)) continue;
				

			self::$_deleteItemData_LinkNamesInUse[] = $dmLink->getName(); // store link usage


			if ($dmLinkEndPoint->isFrom())
			{
				$dependencies[] = array('link' => $dmLink, 'actOn' => 'from');
			}

			if ($dmLinkEndPoint->isTo())
			{
				$dependencies[] = array('link' => $dmLink, 'actOn' => 'to');
			}
		}
		
		
		if (! empty($dependencies))
		{
			foreach ($dependencies as $dependency)
			{
				$dependency['link']->checkAndAdjustForDelete($dependency['actOn'], $condition);
			}
		}
		

		$this->ormTable->delete($condition);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function deleteItemData($condition = NULL)
	{
		if (! $this->dbc->inTransaction())
		{
			$this->dbc->beginTransaction();
			
			try
			{
				$this->_deleteItemData($condition);
				
				// finally
				self::$_deleteItemData_LinkNamesInUse = array(); // clear
	
				$this->dbc->commit();
			}
			catch (Exception $e)
			{
				// finally
				self::$_deleteItemData_LinkNamesInUse = array(); // clear
	
				$this->dbc->rollBack();
				
				throw $e;
			}
		}
		else
		{
			$this->_deleteItemData($condition);
			
			// finally
			self::$_deleteItemData_LinkNamesInUse = array(); // clear
		}
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function deleteFromCGIItemData()
	{
		$condition = array();
		foreach ($this->dmPrimaryFields as $dmField)
		{
			$value = $dmField->getCGIValue();
			
			if (is_null($value)) throw new DMException("'" . $dmField->getName() . "' CGI value not given for deleteFromCGIItemData()");
			
			$condition[] = $dmField->ORMField()->getPreparedName() . ' = ' . SQL::prepare_value($value);
		}
			
		
		$this->deleteItemData(implode(' AND ', $condition));
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function createItemLink($foreign_entity_name, $local_pk, $foreign_pk)
	{
		$dmTargetLink = NULL;
		foreach ($this->dmLinkEndPoints as $dmLinkEndPoint)
		{
			$dmLink = $dmLinkEndPoint->link();
			
			
			if ($dmLinkEndPoint->isFrom())
			{
				$linked_entity_name = $dmLink->toEntity->getName();
				$from_pk = $local_pk;
				$to_pk = $foreign_pk;
			}
			else // isTo()
			{
				$linked_entity_name = $dmLink->fromEntity->getName();
				$from_pk = $foreign_pk;
				$to_pk = $local_pk;
			}
			
			
			if ($linked_entity_name == $foreign_entity_name)
			{
				$dmTargetLink = $dmLink;
				break;
			}
		}
		
		
		if (is_null($dmTargetLink)) throw new DMEntityException("Could not create item link for entity '{$this->name}' to entity {$target_entity_name}, because entities have no links");
			
		
		$dmTargetLink->createItemLink($from_pk, $to_pk);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function dropItemLink($foreign_entity_name, $local_pk, $foreign_pk)
	{
		$dmTargetLink = NULL;
		foreach ($this->dmLinkEndPoints as $dmLinkEndPoint)
		{
			$dmLink = $dmLinkEndPoint->link();
			
			
			if ($dmLinkEndPoint->isFrom())
			{
				$linked_entity_name = $dmLink->toEntity->getName();
				$from_pk = $local_pk;
				$to_pk = $foreign_pk;
			}
			else // isTo()
			{
				$linked_entity_name = $dmLink->fromEntity->getName();
				$from_pk = $foreign_pk;
				$to_pk = $local_pk;
			}
			
			
			if ($linked_entity_name == $foreign_entity_name)
			{
				$dmTargetLink = $dmLink;
				break;
			}
		}
		
		
		if (is_null($dmTargetLink)) throw new DMEntityException("Could not drop item link for entity '{$this->name}' to entity {$foreign_entity_name}, because entities have no links");
			
		
		$dmTargetLink->dropItemLink($from_pk, $to_pk);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function dropAllItemLinks($foreign_entity_name, $local_pk)
	{
		$dmTargetLink = NULL;
		foreach ($this->dmLinkEndPoints as $dmLinkEndPoint)
		{
			$dmLink = $dmLinkEndPoint->link();
			
			
			if ($dmLinkEndPoint->isFrom())
			{
				$linked_entity_name = $dmLink->toEntity->getName();
				$drop_method_name = 'dropAllToItemLinks';
			}
			else // isTo()
			{
				$linked_entity_name = $dmLink->fromEntity->getName();
				$drop_method_name = 'dropAllFromItemLinks';
			}
			
			
			if ($linked_entity_name == $foreign_entity_name)
			{
				$dmTargetLink = $dmLink;
				break;
			}
		}
		
		
		if (is_null($dmTargetLink)) throw new DMEntityException("Could not drop all item links for entity '{$this->name}' to entity {$foreign_entity_name}, because entities have no links");
			
		
		$dmTargetLink->$drop_method_name($local_pk);
	}
}
?>
