<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage tparser
*/


Framework::useClass('ClassLoaderFactory');


/**
* TParserInterface class
*
* @version 1.0
*/

interface TParserInterface
{
	// public function __construct($application, $template_dir = '');
	public function __construct($template_dir = '');

	public function debug($debug = NULL);

	public function register_function($name, $callback);
	
	public function parseString($template_contents);
	public function parseFile($template_path);

	public function getDelimiters();
	public function setDelimiters(Array $delimiters);
	public function set($var, $value = NULL);
	public function clear($var);
	public function clearAll();
}

class TParserFactoryException extends Exception
{
}

class TParserFactory extends ClassLoaderFactory 
{
	protected static $loadedClasses = array();


	/**
	* Private constructor
	*/

	private function __construct()
	{
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function createTParser($engine_name, $template_dir, $theme = '', $default_theme = '')
	{
		$class_name = 'TParser' . ucfirst($engine_name);
		

		static::loadClass($class_name, 'tparsers');

		
		// Create parser
		$tparser = new $class_name($template_dir, $theme, $default_theme);
		
		
		return $tparser;
	}
}

?>
