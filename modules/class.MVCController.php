<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* MVCController
*
* @package modules
*/

/**
* Module interface which should be implemented by module called through this controller class
*/

interface MVCModuleInterface
{
	public function dispatchAction($name, $params = array(), $options = array());
	public function init();
}

interface MVCInterface
{
	public function invokeModuleAction($module_name, $action_name, $params = array(), $options = array());
	public function invokeModuleMethod($module_name, $method_name, $params = array());
}

/**
* Class exception
*/

class MVCControllerException extends Exception
{
}

/**
* Class
*/

class MVCController implements MVCInterface
{
	protected $tparser = NULL;
	protected $moduleDir;
	
	protected $loadedModules = array();
	
	protected $defaultModuleName = 'default';
	protected $defaultActionName = 'default';
	

	/**
	* Constructor
	*
	* Sets up template parser and module directory.
	*
	* @param object $tparser template parser object
	* @param string $module_dir module directory
	*/

	public function __construct($tparser, $module_dir)
	{
		$this->tparser = $tparser;
		$this->moduleDir = dirpath($module_dir);
	}
	
	
	/**
	* Returns path to module class file
	*
	* @param string $name
	*
	* @return string module class file path
	*/

	protected function makeModuleFilePath($name)
	{
		return $this->moduleDir . $name . '.php';
	}	
	
	/**
	* Returns module class normalized name
	*
	* May be overridden in derived class to change normalization method.
	* No conversion is made by default, name is returned as given.
	*
	* @param string $name module class name
	*
	* @return string module class normalized name
	*/

	protected function makeModuleClassName($name)
	{
		return $name;
	}	

	private $loadingModules = array(); // loading modules' stack; to check for infinite loop
	

	protected function _loadModule($name)
	{
		$module_path = $this->makeModuleFilePath($name);

		if (! is_readable($module_path)) throw new MVCControllerException("Could not read application module '$name' at '$module_path'");
		
		include_once($module_path);

		$class_name = $this->makeModuleClassName($name);

		if (! class_exists($class_name)) throw new MVCControllerException('Application module class "' . $class_name . '" not found');

		$module = new $class_name($this);
		
		
		return $module;
	}

	/**
	* Returns module object after loading if necessary 
	*
	* For newly requested module loads module class file, creates module object, saves module object and calls its init() method.
	*
	* @param string $name module name
	*
	* @return object module object
	*/

	public function loadModule($name)
	{
		$module = NULL;
		
		
		if (! isset($this->loadedModules[$name]))
		{
			// Check for infinite loop
			
			if (in_array($name, $this->loadingModules))
			{
				throw new MVCControllerException("Infinite loop detected. Tried to load module '$name' while it is already loading.");
			}
			
			
			array_push($this->loadingModules, $name);
			

			$module = $this->_loadModule($name);

			
			array_pop($this->loadingModules);
			
			
			$this->loadedModules[$name] = $module;
			

			$module->init();
		}
		else
		{
			$module = $this->loadedModules[$name];
		}
		
		
		return $module;
	}
	
	/**
	* Loads module and dispatches specified action
	*
	* @param string $module_name module name
	* @param string $action_name action name
	* @param array $params action parameters
	* @param array $options action options for arbitrary purposes
	*
	* @return mixed action result
	*/

	public function invokeModuleAction($module_name, $action_name, $params = array(), $options = array())
	{
		$result = false;
		
/*
		if (! isset($this->loadedModules[$module_name]))
		{
			$module =& $this->loadModule($module_name);
			
			$this->loadedModules[$module_name] =& $module;
		}
		else
		{
			$module =& $this->loadedModules[$module_name];			
		}
*/
		$module = $this->loadModule($module_name);
	
		
		$result = $module->dispatchAction($action_name, $params, $options);

		return $result;
	}

	/**
	* Loads module and directly calls specified method
	*
	* @param string $module_name module name
	* @param string $method_name method name
	* @param array $params method parameters
	*
	* @return mixed method result
	*/

	public function invokeModuleMethod($module_name, $method_name, $params = array())
	{
		$result = false;
		
/*		
		if (! isset($this->loadedModules[$module_name]))
		{
			$module =& $this->loadModule($module_name);
			
			$this->loadedModules[$module_name] =& $module;
		}
		else
		{
			$module =& $this->loadedModules[$module_name];			
		}
*/
		$module = $this->loadModule($module_name);
		
		
		$result = call_user_func_array(array($module, $method_name), $params);

		return $result;
	}

	/**
	* Executes controller
	*
	* Executes module action specified in CGI parameters if any.
	* If no module specified, default module is invoked.
	* If no action specified, default action is invoked.
	*/

	public function run()
	{
		$call = get_cgi_value('call', 's');
		
		if (! is_null($call))
		{
			// new technique
			
			@list($module, $action) = explode('.', $call);

			if (is_null($module)) $module = $this->defaultModuleName;
			if (is_null($action)) $action = $this->defaultActionName;
		}
		else
		{
			// old technique
			
			$module = get_cgi_value('module', 's', $this->defaultModuleName);
			$action = get_cgi_value('action', 's', $this->defaultActionName);
		}
		
		$this->invokeModuleAction($module, $action);
	}
}
?>
