<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

class DMOneToManyLinkBase extends DMLink
{
	private $dmFieldPairs = array(); // from-to link field pairs
	private $dmFromToFieldMap = array();
	private $dmToFromFieldMap = array();

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DMDataModel $dm, $name, DMEntity $from_entity, $from_required, DMEntity $to_entity, $to_required, $custom_field_map = NULL)
	// public function __construct(DMDataModel $dm, $config)
	{
		parent::__construct($dm, $name, $from_entity, $from_required, $to_entity, $to_required, $custom_field_map);
		
		
		$this->type = '1:n';
		
		
		// $from_entity = $dm->entity($config['from']);
		// $to_entity = $dm->entity($config['to']);
		
		
		// Scan "from" primary index, add corresponding fields to target entity table, store link fields
		
		$from_fields = $from_entity->getPrimaryFields();
		$to_fields = array();
		
		foreach ($from_fields as $from_field)
		{
			// Create new field
			
			$dm_type = $to_required ? 'strong_ext_id' : 'ext_id';
			
			$to_field_config = DMDataModel::$fieldTypeConfigs[$dm_type];
			
			if (! is_null($custom_field_map) && isset($custom_field_map['to']))
			{
				$to_field_config['name'] = $custom_field_map['to'][$from_field->getName()];
			}
			else
			{
				// $to_field_config['name'] = $from_entity->getName() . '_' . $from_field->getName();
				$to_field_config['name'] = $dm->makeLinkORMFieldName($from_entity->getName(), $from_field->getName());
			}
			
			$to_field_config['type'] = $dm_type;
			
			$to_field = DMField::createFromConfig($to_entity, $to_field_config);
			
			
			// Save field pair
			$this->dmFieldPairs[] = array($from_field, $to_field);
			$this->dmFromToFieldMap[$from_field->getName()] = $to_field;
			$this->dmToFromFieldMap[$to_field->getName()] = $from_field;
			
			
			$to_fields[$from_field->getName()] = $to_field;
		}
		
		
		// Add target index
		
		$from_index_config = $from_entity->ORMTable()->primaryIndex()->getConfig();
		
		$from_index_config['name'] = $this->name;
		$from_index_config['type'] = 'INDEX';
		
		foreach ($from_index_config['fields'] as $i => $field_config)
		{
			$from_index_config['fields'][$i]['name'] = $to_fields[$field_config['name']]->getName();
		}
		
		$to_table = $to_entity->ORMTable();
		$to_index = ORMIndex::createFromConfig($to_table, $from_index_config);

		
		// Register link in entities through endpoints

		$from_entity->addLinkEndPoint(new DMLinkEndPoint($this, true, false));
		$to_entity->addLinkEndPoint(new DMLinkEndPoint($this, false, true));
		
		
		// Finalize construction

		$this->fromEntity = $from_entity;
		$this->fromRequired = $from_required;
		
		$this->toEntity = $to_entity;
		$this->toRequired = $to_required;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function fieldPairs()
	{
		return $this->dmFieldPairs;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function fromFields()
	{
		$dmFields = array();
		
		
		foreach ($this->dmFieldPairs as $dmFieldPair)
		{
			$dmFields[] = $dmFieldPair[0];
		}
		
		
		return $dmFields;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function toFields()
	{
		$dmFields = array();
		
		
		foreach ($this->dmFieldPairs as $dmFieldPair)
		{
			$dmFields[] = $dmFieldPair[1];
		}
		
		
		return $dmFields;
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getFromCGIParentItemDataAll($condition = NULL)
	{
		if (is_null($condition))
		{
			$condition = array();
		}

		foreach ($this->dmFieldPairs as $field_pair)
		{
			$from_field = $field_pair[0];
			$to_field = $field_pair[1];

			$cgi_value = get_cgi_value($to_field->getName(), $to_field->getInputType(), $to_field->getInputDefault());
			
			$condition[$from_field->getName()] = $cgi_value;
		}			

		
		return $this->dbc->getRowAll($this->fromEntity->ORMTable()->getName(), $condition);
	}

	/*
	public function getChildItemDataListAll($condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->dbc->getRowsAll($this->toORMTable->getName(), $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
	*/

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getFromCGIChildItemDataListAll($condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		if (is_null($condition))
		{
			$condition = array();
		}

		foreach ($this->dmFieldPairs as $field_pair)
		{
			$from_field = $field_pair[0];
			$to_field = $field_pair[1];

			$cgi_value = get_cgi_value($from_field->getName(), $from_field->getInputType(), $from_field->getInputDefault());
			
			$condition[$to_field->getName()] = $cgi_value;
		}			


		return $this->dbc->getRowsAll($this->toEntity->ORMTable()->getName(), $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getChildItemDataList($parent_pk_values, $fields, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		if (is_null($condition))
		{
			$condition = array();
		}

		foreach ($this->dmFieldPairs as $field_pair)
		{
			$from_field = $field_pair[0];
			$to_field = $field_pair[1];

			// $cgi_value = get_cgi_value($from_field->getName(), $from_field->getInputType(), $from_field->getInputDefault());
			$value = $parent_pk_values[$from_field->getName()];
			
			$condition[$to_field->getName()] = $value;
		}			


		return $this->dbc->getRows($fields, $this->toEntity->ORMTable()->getName(), $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getChildItemDataListAll($parent_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		if (is_null($condition))
		{
			$condition = array();
		}

		foreach ($this->dmFieldPairs as $field_pair)
		{
			$from_field = $field_pair[0];
			$to_field = $field_pair[1];

			// $cgi_value = get_cgi_value($from_field->getName(), $from_field->getInputType(), $from_field->getInputDefault());
			$value = $parent_pk_values[$from_field->getName()];
			
			$condition[$to_field->getName()] = $value;
		}			


		return $this->dbc->getRowsAll($this->toEntity->ORMTable()->getName(), $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getParentItemChildItemCount($condition)
	{
		$fromFieldNames = array();
		foreach ($this->fromEntity->getPrimaryFields() as $dmFromField)
		{
			$fromFieldNames[] = $dmFromField->getName();
		}
		
		$parentItemValues = $this->fromEntity->ORMTable()->getRowData($fromFieldNames, $condition);
		
		
		return $this->getParentItemByPKChildItemCount($parentItemValues);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getParentItemByPKChildItemCount($fromPKFieldValues)
	{
		$condition = array();
		foreach ($this->fromEntity->getPrimaryFields() as $dmFromField)
		{
			$fromFieldName = $dmFromField->getName();
			$fromFieldValue = $fromPKFieldValues[$fromFieldName];
			
			$dmToField = $this->dmFromToFieldMap[$fromFieldName];
			$toFieldName = $dmToField->getName();
			
			$condition[$toFieldName] = $fromFieldValue;
		}
		
		
		return $this->toEntity->ORMTable()->getRowCount($condition);
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function checkAndAdjustForDelete($actOn, $condition = NULL)
	{
		if ($actOn == 'from')
		{
			// Deleting parent(s)
			
			
			$local_entity = $this->fromEntity;
			$foreign_entity = $this->toEntity;

			
			// Get deleted rows' link field (pk) values
			
			$local_entity_pk_values = array();
			
			$local_entity_pk_names = array();
			foreach ($this->dmFieldPairs as $field_pair)
			{
				$dmFromField = $field_pair[0]; // get 'from' field
				$local_entity_pk_names[] = $dmFromField->getName();
			}

			$local_entity_pk_values = $local_entity->ORMTable()->getRowDataList($local_entity_pk_names, $condition);
			

			if (! empty($local_entity_pk_values)) // there is something to delete
			{				
				// Create condition to get children rows (for further through-link processing)
				
				$foreign_condition = array();

				foreach ($local_entity_pk_values as $pk_data)
				{
					$and_condition = array();
					foreach ($pk_data as $field_name => $field_value)
					{
						$dmToField = $this->dmFromToFieldMap[$field_name];
						
						$and_condition[] = $dmToField->ORMField()->getPreparedName() . ' = ' . SQL::prepare_value($field_value);
					}
					
					assert(! empty($and_condition)); // prevent delete all the table data
					
					$foreign_condition[] = implode(' AND ', $and_condition);
				}
				
				assert(! empty($foreign_condition)); // prevent delete all the table data
				
				$foreign_condition = '(' . implode(') OR (', $foreign_condition) . ')';
				
	
				/*
				if ($this->fromRequired && $this->toRequired)
				{
					// delete children; same as "only to required"
				}
				elseif ($this->fromRequired) // only from required
				{
					// unbind children; same as "no required"
				}
				*/
				if ($this->toRequired)
				{
					// Delete bound children
					$foreign_entity->_deleteItemData($foreign_condition);
				}
				else
				{
					// Unbind bound children if any
	
					$set = array();
					foreach ($this->dmFieldPairs as $field_pair)
					{
						$dmToField = $field_pair[1]; // get 'to' field
						$ormToField = $dmToField->ORMField();
						
						$name = $ormToField->getName();
						$value = $ormToField->getDefaultValue();
						
						$set[$name] = $value;
					}
					
					$foreign_entity->ORMTable()->update($set, $foreign_condition);
				}
			}
			
		}
		else
		{
			// Deleting children
			
			
			/*
			if ($this->fromRequired && $this->toRequired)
			{
				// same as "only from required"
			}
			*/
			if ($this->fromRequired)
			{
				// Delete parent as well if empty

	
				$local_entity = $this->toEntity;
				$foreign_entity = $this->fromEntity;
	
				
				// Get deleted rows
				
				// $local_entity_ext_values = array();
				$local_entity_item_list = array();
				
				$local_entity_ext_names = array();
				foreach ($this->dmFieldPairs as $field_pair)
				{
					$dmToField = $field_pair[1]; // get 'from' field
					$local_entity_ext_names[] = $dmToField->getName();
				}
	
				$local_entity_pk_names = array();
				foreach ($local_entity->getPrimaryFields() as $dmField)
				{
					$local_entity_pk_names[] = $dmField->getName();
				}

				// $local_entity_ext_values = $local_entity->ORMTable()->getRowDataList($local_entity_ext_names, $condition);
				$local_entity_item_list = $local_entity->ORMTable()->getRowDataList(array_merge($local_entity_pk_names, $local_entity_ext_names), $condition);
				
				
				if (! empty($local_entity_item_list)) // there is something to delete
				{
				
					// Get foreign-to-local (parent-to-children) rows tree
					
					$foreign_to_local_item_data_tree = array();
	
					foreach ($local_entity_item_list as $item_data)
					{
						// Get foreign PK and local EK (same values, different names)
	
						$foreign_entity_pk_values = array();
						$local_entity_ext_values = array();
						
						foreach ($local_entity_ext_names as $field_name)
						{
							$value = $item_data[$field_name];
							
							
							$dmFromField = $this->dmToFromFieldMap[$field_name];
							
							$foreign_entity_pk_values[$dmFromField->getName()] = $value;
							$local_entity_ext_values[$field_name] = $value;
						}
						
						
						// Get local PK
						
						$local_entity_pk_values = array();
						
						foreach ($local_entity_pk_names as $field_name)
						{
							$value = $item_data[$field_name];
							
							$local_entity_pk_values[$field_name] = $value;
						}
						
						
						// Store to tree
						
						$hash_key = serialize($foreign_entity_pk_values);
						
						if (! isset($foreign_to_local_item_data_tree[$hash_key]))
						{
							$foreign_to_local_item_data_tree[$hash_key] = array
							(
								'pk_values' => $foreign_entity_pk_values, 'children_ext_values' => $local_entity_ext_values, 'children' => array()
							);
						}
						
						$foreign_to_local_item_data_tree[$hash_key]['children'][] = $local_entity_pk_values;
					}
	
	
					// Collect parent item pk values (cascade style) which would have no children after children are deleted.
					
					$foreign_entity_pk_values = array();
					
					foreach ($foreign_to_local_item_data_tree as $foreign_item_data)
					{
						if (! $this->toRequired) // check foreign count if NOT all items are required to have parent
						{
							// Filter out non-existent parents (if toRequired is false and there are unbound children)
							$foreign_item_count = $foreign_entity->ORMTable()->getRowCount($foreign_item_data['pk_values']);
						}
						
						if ($this->toRequired || $foreign_item_count > 0) // skip foreign count check if all items are required to have parent
						{
							// Create condition to match all children (local entity) of current parent (foreign entity)
							//  excluding deleted
							
							$condition = '';
							
							
							// Add matching condition
							
							$matching_condition = array();
							foreach ($foreign_item_data['children_ext_values'] as $field_name => $field_value)
							{
								$matching_condition[] = $local_entity->field($field_name)->ORMField()->getPreparedName() . ' = ' . SQL::prepare_value($field_value);
							}
							
							$condition .= implode(' AND ', $matching_condition);
							
							
							// Add excluding condition
							
							$excluding_condition = array();
							foreach ($foreign_item_data['children'] as $local_entity_pk_values)
							{
								$and_condition = array();
								foreach ($local_entity_pk_values as $field_name => $field_value)
								{
									$and_condition[] = $local_entity->field($field_name)->ORMField()->getPreparedName() . ' = ' . SQL::prepare_value($field_value);
								}
								
								$excluding_condition[] = 'NOT (' . implode(' AND ', $and_condition) . ')';
							}
							
							$condition .= ' AND ' . implode(' AND ', $excluding_condition);
							
							
							// Store for deletion if parent would have no children after children delete
							
							$rest_row_count = $local_entity->ORMTable()->getRowCount($condition);
							
							if ($rest_row_count == 0) $foreign_entity_pk_values[] = $foreign_item_data['pk_values'];
						}
					}
					
	
					if (! empty($foreign_entity_pk_values))
					{
						$or_condition = array();
						foreach ($foreign_entity_pk_values as $pk_data)
						{
							$and_condition = array();
							foreach ($pk_data as $field_name => $field_value)
							{
								$and_condition[] = $foreign_entity->field($field_name)->ORMField()->getPreparedName() . ' = ' . SQL::prepare_value($field_value);
							}
							
							assert(! empty($and_condition)); // prevent delete all the table data
							
							$or_condition[] = implode(' AND ', $and_condition);
						}
						
						assert(! empty($or_condition)); // prevent delete all the table data
						
						$foreign_entity->_deleteItemData('(' . implode(') OR (', $or_condition) . ')'); // delete from parent entity
					}
					
					/*
					$foreign_entity_pk_values = array();
					
					foreach ($local_entity_ext_values as $ext_data)
					{
						$pk_data = array();
						
						foreach ($ext_data as $field_name => $field_value)
						{
							$dmFromField = $this->dmToFromFieldMap[$field_name];
							
							$pk_data[$dmFromField->getName()] = $field_value;
						}
						
						$foreign_entity_pk_values[] = $pk_data;
					}
					
					$foreign_entity_pk_values = array_unique_multi($foreign_entity_pk_values);
					*/
	
	
	
					/*
					// Get deleted rows' link field values
					
					$toFieldNames = array();
					foreach ($this->dmFieldPairs as $dmFieldPair)
					{
						$dmToField = $dmFieldPair[1]; // get 'to' field
						$toFieldNames[] = $dmToField->ORMField()->getName();
					}
	
					$toFieldValuesList = $local_entity->ORMTable()->getRowDataList($toFieldNames, $condition);
					//wdebug($toFieldValuesList);
					
	
					$local_entity->ORMTable()->delete($condition);
					*/
	
	
					/*
					// Cascade - delete parent items which would have no children after children are deleted
					foreach ($orCondition as $k => $cond1)
					{
						$c = $this->getParentItemChildItemCount($cond1);
						
						if ($c > 0)
						{
							unset($orCondition[$k]);
						}
					}
	
					
					if (! empty($orCondition))
					{
						//wechos('deleting parent...');
						$foreign_entity->deleteItemData('(' . implode(') OR (', $orCondition) . ')'); // delete from parent entity
					}
					*/
				}
				
			}
			/*
			elseif ($this->toRequired)
			{
			}
			else
			{
			}
			*/
			
		}
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function checkAndAdjustForInsert($actOn, $data)
	{
		if ($actOn == 'from')
		{
			// Inserting parent
			

			// Check upon required

			/*
			if ($this->fromRequired && $this->toRequired)
			{
				// Double insert required
				// The same as $this->fromRequired only
			}
			*/
			if ($this->fromRequired) // link needs a child
			{
				// Double insert required (new child)
				
				$from_entity_name = $this->fromEntity->getName();
				$to_entity_name = $this->toEntity->getName();
				
				if (! isset($data[$to_entity_name]))
					throw new DMLinkException("Double insert required on insert into entity '$from_entity_name', but no data entry provided for entity '$to_entity_name'.");
				
				if (! isset($data[$to_entity_name]['values']))
					throw new DMLinkException("Double insert required on insert into entity '$from_entity_name', but no values provided for entity '$to_entity_name'.");

				
				// Inject key values for dependent entity
				
				$data2 = $data;
				foreach ($this->dmFieldPairs as $dmFieldPair)
				{
					$dmFromField = $dmFieldPair[0];
					$dmToField = $dmFieldPair[1];
					
					$data2[$to_entity_name]['values'][$dmToField->getName()] = $data2[$from_entity_name]['values'][$dmFromField->getName()];
				}
				
				
				// Insert data for dependent child
				$this->toEntity->_insertItemData($data2);
			}
			elseif ($this->toRequired) // link needs a parent
			{
				// Just insert values in DMEntity, nothing to do here
			}
			else // nothing required
			{
				// Just insert values in DMEntity, nothing to do here
			}
		}
		else // vice versa
		{
			// Inserting child
			
			
			$from_entity_name = $this->fromEntity->getName();
			$to_entity_name = $this->toEntity->getName();
			
			
			// Get parent key values
			
			$parent_key_values = array();
				
			if (isset($data[$to_entity_name]['depends']) && isset($data[$to_entity_name]['depends'][$from_entity_name]))
			{
				// Check parent row

				
				// Create condition
				foreach ($this->dmToFromFieldMap as $dmField)
				{
					$field_name = $dmField->getName();
					
					if (! isset($data[$to_entity_name]['depends'][$from_entity_name][$field_name]))
						throw new DMLinkException("'$from_entity_name' entity item referred to, but '$field_name' key field value not provided on insert into entity '$to_entity_name'.");
					
					$value = $data[$to_entity_name]['depends'][$from_entity_name][$field_name];
					
					$parent_key_values[$field_name] = $value;
				}
				
				
				// Check row count
				
				$c = $this->fromEntity->ORMTable()->getRowCount($parent_key_values);

				// if ($c == 0) throw new DMLinkException("'$from_entity_name' entity item with key values (" . implode(', ', array_map(function($a1value, $a2value){return "$a1value: $a2value";}, array_keys($parent_key_values), $parent_key_values)) . ") and referred to as parent was not found on insert into entity '$to_entity_name'.");
				if ($c == 0) throw new DMLinkException("'$from_entity_name' entity item with key values (" . implode_params($parent_key_values, array(', ', ': ')) . ") and referred to as parent was not found on insert into entity '$to_entity_name'.");
			}
			
			
			// Check upon required
			
			/*
			if ($this->fromRequired && $this->toRequired)
			{
			}
			elseif ($this->fromRequired) // only from required
			{
				
			}
			*/
			if ($this->toRequired) // link needs a parent
			{
				// if (empty($parent_key_values)) throw new DMLinkException("'$to_entity_name' entity is required, but no '$from_entity_name' entity item is referred to on insert into entity '$to_entity_name'.");
				if (empty($parent_key_values))
				{
					// Double insert required (new parent)
					
					if (! isset($data[$from_entity_name]))
						throw new DMLinkException("Double insert required on insert into entity '$to_entity_name', but no data entry provided for entity '$from_entity_name'.");
					
					if (! isset($data[$from_entity_name]['values']))
						throw new DMLinkException("Double insert required on insert into entity '$to_entity_name', but no values provided for entity '$from_entity_name'.");
					
					
					// Insert data for dependent parent
					$parent_key_values = $this->fromEntity->_insertItemData($data);
				}
				else
				{
					// Use provided parent key values; Nothing to do here
				}
			}
			else // nothing required
			{
				// Just insert values in DMEntity, nothing to do here
			}
			
			
			// Inject key values if any
			if (! empty($parent_key_values))
			{
				foreach ($this->dmFieldPairs as $dmFieldPair)
				{
					$dmFromField = $dmFieldPair[0];
					$dmToField = $dmFieldPair[1];
					
					$data[$to_entity_name]['values'][$dmToField->getName()] = $parent_key_values[$dmFromField->getName()];
				}
			}
			else
			{
				// Insert default value ??? It should be inserted by RDBMS automatically
			}
		}
		
		
		return $data;
	}
	/*
	static public function createFromConfig(DMDataModel $dm, $config)
	{
		$custom_field_map = isset($config['custom_field_map']) ? $config['custom_field_map'] : NULL;
		
		$from_entity = $dm->entity($config['from']);
		$to_entity = $dm->entity($config['to']);
		
		
		$dmLink = new self($dm, $config['name'], $from_entity, $config['from_required'], $to_entity, $config['to_required'], $custom_field_map);
		
		
		return $dmLink;
	}
	*/
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function createItemLink($from_pk, $to_pk)
	{
		$from_entity_name = $this->fromEntity->getName();
		$to_entity_name = $this->toEntity->getName();
		
		
		if ($this->fromRequired || $this->toRequired) throw new DMLinkException(__CLASS__  . '::' . __METHOD__ . "() not implemented when either entity is required");
		
			
		// Check entity items
		
		$c = $this->fromEntity->getItemCount($from_pk);

		if ($c == 0) throw new DMLinkException("'$from_entity_name' entity item with key values (" . implode_params($from_pk, array(', ', ': ')) . ") and referred to as parent was not found on link between entity '$from_entity_name' and entity '$to_entity_name'.");
		
		$c = $this->toEntity->getItemCount($to_pk);

		if ($c == 0) throw new DMLinkException("'$to_entity_name' entity item with key values (" . implode_params($to_pk, array(', ', ': ')) . ") and referred to as child was not found on link between entity '$from_entity_name' and entity '$to_entity_name'.");

		
		$link_set = array();
		foreach ($this->dmFieldPairs as $dmFieldPair)
		{
			$dmFromField = $dmFieldPair[0];
			$dmToField = $dmFieldPair[1];
			
			$link_set[$dmToField->getName()] = $from_pk[$dmFromField->getName()];
		}
		
		
		$this->toEntity->updateItemData($link_set, $to_pk);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function dropItemLink($from_pk, $to_pk)
	{
		$from_entity_name = $this->fromEntity->getName();
		$to_entity_name = $this->toEntity->getName();
		
		
		if ($this->fromRequired || $this->toRequired) throw new DMLinkException("linkItems() not implemented when either entity is required");


		// Check entity items
		
		$c = $this->fromEntity->getItemCount($from_pk);

		if ($c == 0) throw new DMLinkException("'$from_entity_name' entity item with key values (" . implode_params($from_pk, array(', ', ': ')) . ") and referred to as parent was not found on drop link between entity '$from_entity_name' and entity '$to_entity_name'.");
		
		$to_item_data = $this->toEntity->getItemDataAll($to_pk);

		if (empty($to_item_data)) throw new DMLinkException("'$to_entity_name' entity item with key values (" . implode_params($to_pk, array(', ', ': ')) . ") and referred to as child was not found on drop link between entity '$from_entity_name' and entity '$to_entity_name'.");

		
		$unlink_set = array();
		foreach ($this->dmFieldPairs as $field_pair)
		{
			$dmFromField = $field_pair[0];
			$dmToField = $field_pair[1];

			if ($to_item_data[$dmToField->getName()] != $from_pk[$dmFromField->getName()])
			{
				// items are not linked, so unlink is impossible
				$unlink_set = array();
				break;
			}

			$ormToField = $dmToField->ORMField();
			
			$unlink_set[$ormToField->getName()] = $ormToField->getDefaultValue(); // set link fields to default values
		}
		
		if (! empty($unlink_set))
		{
			$this->toEntity->updateItemData($unlink_set, $to_pk);
		}
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function dropAllToItemLinks($from_pk)
	{
		$from_entity_name = $this->fromEntity->getName();
		$to_entity_name = $this->toEntity->getName();
		
		
		if ($this->fromRequired || $this->toRequired) throw new DMLinkException("linkItems() not implemented when either entity is required");


		// Check entity items
		
		$c = $this->fromEntity->getItemCount($from_pk);

		if ($c == 0) throw new DMLinkException("'$from_entity_name' entity item with key values (" . implode_params($from_pk, array(', ', ': ')) . ") and referred to as parent was not found on drop link between entity '$from_entity_name' and entity '$to_entity_name'.");

		
		$unlink_set = array();
		$unlink_condition = array();
		foreach ($this->dmFieldPairs as $field_pair)
		{
			$dmFromField = $field_pair[0];
			$dmToField = $field_pair[1];

			$unlink_condition[$dmToField->getName()] = $from_pk[$dmFromField->getName()]; // set link fields to default values

			$ormToField = $dmToField->ORMField();
			
			$unlink_set[$ormToField->getName()] = $ormToField->getDefaultValue(); // set link fields to default values
		}
		
		if (! empty($unlink_set))
		{
			$this->toEntity->updateItemData($unlink_set, $unlink_condition);
		}
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function dropAllFromItemLinks($to_pk)
	{
		$from_entity_name = $this->fromEntity->getName();
		$to_entity_name = $this->toEntity->getName();
		
		
		if ($this->fromRequired || $this->toRequired) throw new DMLinkException("linkItems() not implemented when either entity is required");


		// Check entity items
		
		$c = $this->toEntity->getItemCount($to_pk);

		if ($c == 0) throw new DMLinkException("'$to_entity_name' entity item with key values (" . implode_params($to_pk, array(', ', ': ')) . ") and referred to as child was not found on drop link between entity '$from_entity_name' and entity '$to_entity_name'.");

		
		$unlink_set = array();
		foreach ($this->dmFieldPairs as $field_pair)
		{
			$dmFromField = $field_pair[0];
			$dmToField = $field_pair[1];

			$ormToField = $dmToField->ORMField();
			
			$unlink_set[$ormToField->getName()] = $ormToField->getDefaultValue(); // set link fields to default values
		}
		
		if (! empty($unlink_set))
		{
			$this->toEntity->updateItemData($unlink_set, $to_pk);
		}
	}
}

?>
