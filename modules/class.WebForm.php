<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* WebForms
*
*	@version	3.0
* @package modules
* @subpackage ui
*
*
* WebFormWidget configuration
*
*  Possible widget types:
*
*   stringhidden
*     Params: name, value, default_value, required, regexp, attrs
*
*   string
*     Params: name, value, default_value, required, regexp, attrs
*
*   password
*     Params: name, value, default_value, required, regexp, attrs
*
*   text
*     Params: name, value, default_value, required, cols, rows, regexp, attrs
*
*   file
*     Params: name, value, default_value, required, return_type, attrs
*       return_type = ( content | path )
*     
*   integerhidden
*     Params: name, value, default_value, required, attrs
*
*   integer
*     Params: name, value, default_value, required, attrs);
*
*   stringddl
*     Params: name, options, value, default_value, required, regexp, empty_value, empty_display, format, value_field, display_field, attrs
*       format = ( datalist | valuemap )
*
*   stringlist
*     Params: name, options, value, default_value, size, required, regexp, empty_value, empty_display, format, value_field, display_field, attrs
*
*   stringslist
*     Params: name, options, value, default_value, size, required, empty_value, empty_display, format, value_field, display_field, attrs
*
*   integerddl
*     Params: name, options, value, default_value, required, empty_value, empty_display, format, value_field, display_field, attrs
*     
*   integerlist
*     Params: name, options, value, default_value, size, required, empty_value, empty_display, format, value_field, display_field, attrs
*
*   integerslist
*     Params: name, options, value, default_value, size, required, empty_value, empty_display, format, value_field, display_field, attrs
*
*   date
*     Params: name, value, default_value, required, empty_display, start_year, end_year, format, attrs, postfixes
*     Defaults:
*       empty_display   '-'
*       start_year      NULL
*       end_year        NULL
*       format          'Y-m-d'
*       postfixes       array('_year', '_month', '_day')
*
*/

/**
* WebForm module exception
*/

class WebFormException extends Exception
{
}


abstract class WebFormField
{
	const EMPTY_VALUE = 0x01; 
	const WRONG_VALUE = 0x02; 


	protected $name;
	protected $value;
	protected $defaultValue;
	protected $required;
	
	protected $errorFlags = 0;

	protected $parentGroup = NULL;
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false)
	{
		if (empty($name)) throw new WebFormException("Field name empty");
		
		$this->name = $name;
		$this->value = $value;
		$this->defaultValue = $default_value;
		$this->required = $required;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getName()
	{
		return $this->name;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setValue($value)
	{
		$this->value = $value;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getValue()
	{
		return $this->value;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setDefaultValue($value)
	{
		$this->defaultValue = $value;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getDefaultValue()
	{
		return $this->defaultValue;
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setRequired($required)
	{
		$this->required = $required;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isRequired()
	{
		return $this->required;
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setErrorFlags($flags)
	{
		$this->errorFlags = $flags;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getErrorFlags()
	{
		return $this->errorFlags;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setParentGroup(WebFormFieldGroup $group)
	{
		$this->parentGroup = $group;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getParentGroup()
	{
		return $this->parentGroup;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function validate()
	{
		$flags = 0;
		

		if ($this->isRequired() && $this->isEmpty())
		{
			$flags |= WebFormField::EMPTY_VALUE;
		}
		

		$this->setErrorFlags($flags);
		
		
		return ($flags == 0);
	}

	
	abstract public function capture();
	abstract public function isEmpty();
}


/**
* Generic single value field
* Captures its value as string
*/

abstract class WebFormSingleValueField extends WebFormField
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/
	/*
	public function __construct($name, $value = NULL, $default_value = NULL, $required = false)
	{
		parent::__construct($name, $value, $default_value, $required);
	}
	*/
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		if (! is_null($this->defaultValue)) $value = get_cgi_value($this->name, 's', $this->defaultValue);
		else $value = get_cgi_value($this->name, 's');
		
		$this->value = $value;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isEmpty()
	{
		return (strlen($this->value) == 0);
	}
}


/**
* Generic multi value field
* Captures its value as array of strings
*/

abstract class WebFormMultiValueField extends WebFormField
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false)
	{
		parent::__construct($name, $value, $default_value, $required);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		if (! is_null($this->defaultValue)) $value = get_cgi_value($this->name, 'as', $this->defaultValue);
		else $value = get_cgi_value($this->name, 'as');
		
		$this->value = $value;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isEmpty()
	{
		return empty($this->value);
	}
}


/**
* Generic visible field
* Introduces title
*/
/*
abstract class WebFormVisibleField extends WebFormGenericField
{
	protected $title;
	
	
	public function __construct($title, $name, $value = NULL, $default_value = NULL, $required = false)
	{
		parent::__construct($name, $value, $default_value, $required);

		if (empty($title)) throw new Exception("Field title empty");
		
		$this->title = $title;
	}

	
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	public function getTitle()
	{
		return $this->title;
	}
}
*/


/**
* Generic regexp validateable single value field
* Introduces regexp
*/
abstract class WebFormSingleValueRegexpField extends WebFormSingleValueField
{
	protected $regexp;
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $regexp = '')
	{
		parent::__construct($name, $value, $default_value, $required);
		
		$this->regexp = $regexp;
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setRegexp($regexp)
	{
		$this->regexp = $regexp;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRegexp()
	{
		return $this->regexp;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function validate()
	{
		parent::validate();
		
		
		$flags = $this->getErrorFlags(); 
		

		if (! $this->isEmpty() && ! empty($this->regexp))
		{
			if (! preg_match($this->regexp, $this->value))
			{
				$flags |= WebFormField::WRONG_VALUE;
			}
		}


		$this->setErrorFlags($flags);

		
		return ($flags == 0);
	}
}


/**
* Single value fields
*/

/**
* Flag field
* Always has value (holds 1 or 0)
*/
class WebFormFlagField extends WebFormField
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = 0, $default_value = 0, $required = false)
	{
		$value = intval((bool)intval($value));
		$default_value = intval((bool)intval($default_value));
		
		parent::__construct($name, $value, $default_value, $required);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setValue($value)
	{
		$value = intval((bool)intval($value));

		$this->value = $value;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setDefaultValue($value)
	{
		$value = intval((bool)intval($value));
		
		$this->defaultValue = $value;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setErrorFlags($flags)
	{
		$flags &= ~ WebFormField::WRONG_VALUE; // exclude WRONG_VALUE
		
		$this->errorFlags = $flags;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		$value = get_cgi_value($this->name, 'i', $this->defaultValue);
		
		$this->value = $value;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isEmpty()
	{
		return !($this->value == 1);
	}
}

/**
* Option field
*/
class WebFormOptionField extends WebFormField
{
	public $checked;


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $checked = false)
	{
		parent::__construct($name, $value, $default_value, $required);

		$this->checked = $checked;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setChecked($checked)
	{
		$this->checked = $checked;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getChecked()
	{
		return $this->checked;
	}

	
	public function getValue()
	{
		return $this->checked ? $this->value : $this->defaultValue;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setErrorFlags($flags)
	{
		$flags &= ~ WebFormField::WRONG_VALUE; // exclude WRONG_VALUE
		
		$this->errorFlags = $flags;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		$value = get_cgi_value($this->name, 's');
		
		if ($value == $this->value)
		{
			$this->setChecked(true);
		}
		else
		{
			$this->setChecked(false);
		}
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isEmpty()
	{
		return ! $this->checked;
	}
}

/**
* String field
*/
class WebFormStringField extends WebFormSingleValueRegexpField
{
}

/**
* Integer field
*/
class WebFormIntegerField extends WebFormSingleValueField
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false)
	{
		// Cast to integer
		
		$value = ! is_null($value) ? intval($value) : $value;
		$default_value = ! is_null($default_value) ? intval($default_value) : $default_value;
		
		
		parent::__construct($name, $value, $default_value, $required);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setValue($value)
	{
		$this->value = intval($value);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		if (! is_null($this->defaultValue)) $value = get_cgi_value($this->name, 'i', $this->defaultValue);
		else $value = get_cgi_value($this->name, 'i');
		
		// Skip unexpected zero if any (nothing passed but $value became 0 after forced conversion)
		if ($value === 0 && $this->defaultValue !== 0)
		{
			$REQUEST =& _get_request();

			$cgi_value = $REQUEST[$this->name];
			
			if (strlen($cgi_value) == 0) $value = NULL;
		}

		$this->value = $value;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isEmpty()
	{
		return empty($this->value);
	}
}

/**
* Float field
*/
class WebFormFloatField extends WebFormSingleValueField
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false)
	{
		// Cast to float
		
		$value = ! is_null($value) ? floatval($value) : $value;
		$default_value = ! is_null($default_value) ? floatval($default_value) : $default_value;
		
		
		parent::__construct($name, $value, $default_value, $required);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setValue($value)
	{
		$this->value = floatval($value);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		if (! is_null($this->defaultValue)) $value = get_cgi_value($this->name, 'f', $this->defaultValue);
		else $value = get_cgi_value($this->name, 'f');

		// Skip unexpected zero if any (nothing passed but $value became 0 after forced conversion)
		if ($value === .0 && $this->defaultValue !== .0)
		{
			$REQUEST =& _get_request();

			$cgi_value = $REQUEST[$this->name];

			if (strlen($cgi_value) == 0) $value = NULL;
		}

		$this->value = $value;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isEmpty()
	{
		return empty($this->value);
	}
}

/**
* Date field
*/
class WebFormDateField extends WebFormSingleValueField
{
	private $postfixes;
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $postfixes = array('_year', '_month', '_day'))
	{
		parent::__construct($name, $value, $default_value, $required);
		
		$this->postfixes = $postfixes;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPostfixes()
	{
		return $this->postfixes;
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		$this->value = get_cgi_date($this->name, $this->defaultValue, $this->postfixes);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isEmpty()
	{
		return empty($this->value);
	}
}

/**
* Time field
*/
class WebFormTimeField extends WebFormSingleValueField
{
	private $postfixes;
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $postfixes = array('_hour', '_minute', '_second'))
	{
		parent::__construct($name, $value, $default_value, $required);
		
		$this->postfixes = $postfixes;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPostfixes()
	{
		return $this->postfixes;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		$this->value = get_cgi_time($this->name, $this->defaultValue, $this->postfixes);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isEmpty()
	{
		return empty($this->value);
	}
}

/**
* Date & time field
*/
class WebFormDateTimeField extends WebFormSingleValueField
{
	private $postfixes;
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $postfixes = array('_year', '_month', '_day', '_hour', '_minute', '_second'))
	{
		parent::__construct($name, $value, $default_value, $required);
		
		$this->postfixes = $postfixes;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPostfixes()
	{
		return $this->postfixes;
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		$this->value = get_cgi_datetime($this->name, $this->defaultValue, $this->postfixes);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isEmpty()
	{
		return empty($this->value);
	}
}

/**
* File field
*/
class WebFormFileField extends WebFormSingleValueField
{
	private $returnType;
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $return_type = 'path')
	{
		parent::__construct($name, $value, $default_value, $required);
		
		$this->returnType = $return_type;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getReturnType()
	{
		return $this->returnType;
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		$this->value = get_cgi_file($this->name, $this->returnType, $this->defaultValue);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isEmpty()
	{
		return empty($this->value);
	}
}


/**
* Multi value fields
*/

/**
* Strings field
*/
class WebFormStringsField extends WebFormMultiValueField
{
}

/**
* Integers field
*/
class WebFormIntegersField extends WebFormMultiValueField
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false)
	{
		// Cast to integer
		
		$value = ! is_null($value) ? array_map('intval', $value) : $value;
		$default_value = ! is_null($default_value) ? array_map('intval', $default_value) : $default_value;
		
		
		parent::__construct($name, $value, $default_value, $required);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setValue($value)
	{
		$this->value = array_map('intval', $value);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		if (! is_null($this->defaultValue)) $value = get_cgi_value($this->name, 'ai', $this->defaultValue);
		else $value = get_cgi_value($this->name, 'ai');
		
		$this->value = $value;
	}
}


class WebFormHelper
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function createField($name, $config)
	{
		$f = NULL;
		
		
		if (! isset($config['type'])) throw new WebFormException("Type not set for field '$name'");
		
		$type = $config['type'];
		$value = isset($config['value']) ? $config['value'] : NULL;
		$default_value = isset($config['default']) ? $config['default'] : NULL;
		$required = isset($config['required']) ? $config['required'] : false;
		$regexp = isset($config['regexp']) ? $config['regexp'] : '';
		
		switch ($type)
		{
			case 'flag':
				$f = new WebFormFlagField($name, $value, $default_value, $required);
			break;

			case 'option':
				$f = new WebFormOptionField($name, $value, $default_value, $required);
			break;

			case 'string':
				$f = new WebFormStringField($name, $value, $default_value, $required, $regexp);
			break;

			case 'integer':
				$f = new WebFormIntegerField($name, $value, $default_value, $required);
			break;

			case 'float':
				$f = new WebFormFloatField($name, $value, $default_value, $required);
			break;

			case 'date':
				if (isset($config['postfixes']))
				{
					$f = new WebFormDateField($name, $value, $default_value, $required, $config['postfixes']);
				}
				else
				{
					$f = new WebFormDateField($name, $value, $default_value, $required);
				}
			break;

			case 'time':
				if (isset($config['postfixes']))
				{
					$f = new WebFormTimeField($name, $value, $default_value, $required, $config['postfixes']);
				}
				else
				{
					$f = new WebFormTimeField($name, $value, $default_value, $required);
				}
			break;

			case 'datetime':
				if (isset($config['postfixes']))
				{
					$f = new WebFormDateTimeField($name, $value, $default_value, $required, $config['postfixes']);
				}
				else
				{
					$f = new WebFormDateTimeField($name, $value, $default_value, $required);
				}
			break;

			case 'file':
				if (isset($config['return_type']))
				{
					$f = new WebFormFileField($name, $value, $default_value, $required, $config['return_type']);
				}
				else
				{
					$f = new WebFormFileField($name, $value, $default_value, $required);
				}
			break;

			
			case 'strings':
				$f = new WebFormStringsField($name, $value, $default_value, $required);
			break;

			case 'integers':
				$f = new WebFormIntegersField($name, $value, $default_value, $required);
			break;

			default:
				throw new WebFormException("Unknown field type '$type'");
		}
		
		
		return $f;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addGroup($form, $name, $config)
	{
		// $name = $config['name'];
		$required = isset($config['required']) ? $config['required'] : false;
		$fields = $config['fields'];
		
		// if (! isset($config['type'])) throw new WebFormException("Type not set for field '$name'");
		
		$g = new WebFormFieldGroup($name, $required);
		
		foreach ($fields as $name => $field_config)
		{
			$f = WebFormHelper::createField($name, $field_config);
			
			$g->addField($f);
		}
		
		$form->addGroup($g);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addField($form, $name, $config)
	{
		$f = WebFormHelper::createField($name, $config);
		
		$form->addField($f);
	}
}


class WebFormFieldGroup // extends WebFormItem
{
	private $name;
	private $required;
	
	private $errorFlags = 0;

	private $fields;

	
		
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $required = false)
	{
		if (empty($name)) throw new WebFormException("Group name empty");

		$this->name = $name;
		$this->required = $required;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getName()
	{
		return $this->name;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setRequired($required)
	{
		$this->required = $required;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isRequired()
	{
		return $this->required;
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setErrorFlags($flags)
	{
		$this->errorFlags = $flags;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getErrorFlags()
	{
		return $this->errorFlags;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getFields()
	{
		return $this->fields;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addField(WebFormField $field)
	{
		$name = $field->getName();

		
		if (isset($this->fields[$name])) throw new WebFormException("Duplicate field name '$name'");

		
		$field->setParentGroup($this);

		$this->fields[$name] = $field;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function removeField($field)
	{
		if ($field instanceof WebFormField)
		{
			$name = $field->getName();
		}
		else
		{
			$name = $field;
		}
		

		if (! isset($this->fields[$name])) throw new WebFormException("Field name '$name' not found");
		

		$field = $this->fields[$name];
		
		$field->setParentGroup(NULL);
		
		unset($this->fields[$name]);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isEmpty()
	{
		$group_empty = true;

		foreach ($this->fields as $field)
		{
			if (! $field->isEmpty())
			{
				$group_empty = false;
				break;
			}
		}
		
		return $group_empty;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function validate()
	{
		$flags = 0;
		
		
		$group_empty = true;
		$group_valid = true;
		
		foreach ($this->fields as $field)
		{
			if (! $field->validate()) $group_valid = false;
			
			if (! $field->isEmpty()) $group_empty = false;
		}
		
		
		if ($this->isRequired() && $group_empty)
		{
			$flags |= WebFormField::EMPTY_VALUE;
		}
		
		if (! $group_valid)
		{
			$flags |= WebFormField::WRONG_VALUE;
		}
		
		
		$this->setErrorFlags($flags);
		
		
		return ($flags == 0);
	}
}


class WebForm
{
	private $fields;
	private $groups;


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($configs = NULL)
	{
		if (! is_null($configs))
		{
			foreach ($configs as $name => $config)
			{
				if (isset($config['fields'])) // assume group
				{
					WebFormHelper::addGroup($this, $name, $config);
				}
				else // assume field
				{
					WebFormHelper::addField($this, $name, $config);
				}
			}
		}
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addField(WebFormField $field)
	{
		$name = $field->getName();
		
		if (isset($this->fields[$name])) throw new WebFormException("Duplicate field name '$name'");
		
		$this->fields[$name] = $field;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function removeField($field)
	{
		if ($field instanceof WebFormField)
		{
			$name = $field->getName();
		}
		else
		{
			$name = $field;
		}
		
		
		if (! isset($this->fields[$name])) throw new WebFormException("Field name '$name' not found");

		
		$field = $this->fields[$name];
		
		
		$group = $field->getParentGroup();
		
		if (! is_null($group))
		{
			$group->removeField($field);
		}
		
		
		unset($this->fields[$name]);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getField($name)
	{
		$field = NULL;
		
		if (isset($this->fields[$name]))
		{
			$field = $this->fields[$name];
		}
		
		return $field;
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getFields()
	{
		return $this->fields;
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addGroup(WebFormFieldGroup $group)
	{
		$name = $group->getName();

		if (isset($this->groups[$name])) throw new WebFormException("Duplicate group name '$name'");
		

		$fields = $group->getFields();
		
		foreach ($fields as $field)
		{
			$this->addField($field);
		}
		
		$this->groups[$name] = $group;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function removeGroup($group)
	{
		if ($group instanceof WebFormFieldGroup)
		{
			$name = $group->getName();
		}
		else
		{
			$name = $group;
		}
		
		if (! isset($this->groups[$name])) throw new WebFormException("Group name '$name' not found");

		
		$group = $this->groups[$name];
		
		
		$fields = $group->getFields();
		
		foreach ($fields as $field)
		{
			$this->removeField($field);
		}

		
		unset($this->groups[$name]);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getGroups()
	{
		return $this->groups;
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setValues($values)
	{
		foreach ($values as $name => $value)
		{
			if (isset($this->fields[$name]))
			{
				$this->fields[$name]->setValue($value);
			}
		}
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setValue($name, $value)
	{
		if (! isset($this->fields[$name])) throw new WebFormException("Field '$name' not found");
		
		$this->fields[$name]->setValue($value);
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getValues()
	{
		$values = array();
		
		
		foreach ($this->fields as $name => $field)
		{
			$values[$name] = $field->getValue();
		}

		
		return $values;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getValue($name)
	{
		if (! isset($this->fields[$name])) throw new WebFormException("Field '$name' not found");
		
		return $this->fields[$name]->getValue();
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		foreach ($this->fields as $field)
		{
			$field->capture();
		}
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function validate()
	{
		$form_valid = true;
		

		if (! empty($this->fields))
		{
			foreach ($this->fields as $field)
			{
				if (! $field->validate()) $form_valid = false;
			}
		}

		if (! empty($this->groups))
		{
			foreach ($this->groups as $group)
			{
				if (! $group->validate()) $form_valid = false;
			}
		}

		
		return $form_valid;
	}
}


/**
* WIDGETS (next layer from WebFormField, WebForm)
*/

class WebFormWidgetException extends Exception
{
}

interface WebFormFieldWidget
{
	public function getWidgetData();
}


/**
* Hidden Flag
*/
class WebFormFlagHiddenFieldWidget extends WebFormFlagField implements WebFormFieldWidget
{
	public $type = 'hidden';
	
	public $attrs;
	public $addClass;

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $attrs = '', $add_class = false)
	{
		parent::__construct($name, $value, $default_value, $required);
		
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData($this);
	}
}

/**
* Flag
*/
class WebFormFlagFieldWidget extends WebFormFlagHiddenFieldWidget
{
	public $type = 'flag';
}

/**
* Hidden Option
*/
class WebFormOptionHiddenFieldWidget extends WebFormOptionField implements WebFormFieldWidget
{
	public $type = 'hidden';
	
	public $attrs;
	public $addClass;

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $checked = false, $attrs = '', $add_class = false)
	{
		parent::__construct($name, $value, $default_value, $required, $checked);
		
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData
		(
			$this,
			array
			(
				'value' => $this->value,
				'checked' => $this->getChecked()
			)
		);
	}
}

/**
* Option
*/
class WebFormOptionFieldWidget extends WebFormOptionHiddenFieldWidget
{
	public $type = 'option';
}

/**
* Hidden String
*/
class WebFormStringHiddenFieldWidget extends WebFormStringField implements WebFormFieldWidget
{
	public $type = 'hidden';

	public $attrs;
	public $addClass;

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $regexp = '', $attrs = '', $add_class = false)
	{
		parent::__construct($name, $value, $default_value, $required, $regexp);

		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData($this);
	}
}

/**
* String
*/
class WebFormStringFieldWidget extends WebFormStringHiddenFieldWidget
{
	public $type = 'string';
}

/**
* Password
*/
class WebFormPasswordFieldWidget extends WebFormStringHiddenFieldWidget
{
	public $type = 'password';
}

/**
* Text
*/
class WebFormTextFieldWidget extends WebFormStringFieldWidget
{
	public $type = 'text';
	
	private $cols;
	private $rows;

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $cols = 25, $rows = 3, $regexp = '', $attrs = '', $add_class = false)
	{
		parent::__construct($name, $value, $default_value, $required, $regexp, $attrs, $add_class);
		
		$this->cols = $cols;
		$this->rows = $rows;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData
		(
			$this,
			array
			(
				'cols' => $this->cols,
				'rows' => $this->rows
			)
		);
	}
}

/**
* File
*/
class WebFormFileFieldWidget extends WebFormFileField implements WebFormFieldWidget
{
	public $type = 'file';
	
	public $attrs;
	public $addClass;

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $return_type = 'path', $attrs = '', $add_class = false)
	{
		parent::__construct($name, $value, $default_value, $required, $return_type);
		
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData($this);
	}
}
		
/**
* Hidden Integer
*/
class WebFormIntegerHiddenFieldWidget extends WebFormIntegerField implements WebFormFieldWidget
{
	public $type = 'hidden';

	public $attrs;
	public $addClass;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $attrs = '', $add_class = false)
	{
		parent::__construct($name, $value, $default_value, $required);
		
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData($this);
	}
}

/**
* Integer
*/
class WebFormIntegerFieldWidget extends WebFormIntegerHiddenFieldWidget
{
	public $type = 'string';
}


/**
* Hidden Float
*/
class WebFormFloatHiddenFieldWidget extends WebFormFloatField implements WebFormFieldWidget
{
	public $type = 'hidden';

	public $attrs;
	public $addClass;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $regexp = '', $attrs = '', $add_class = false)
	{
		parent::__construct($name, $value, $default_value, $required, $regexp);
		
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData($this);
	}
}

/**
* Float
*/
class WebFormFloatFieldWidget extends WebFormFloatHiddenFieldWidget
{
	public $type = 'string';
}


/*
class WebFormTextFieldWidget extends WebFormStringField implements WebFormFieldWidget
{
	private $attrs;

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $regexp = '', $attrs = '')
	{
		parent::__construct($name, $value, $default_value, $required, $regexp);
		
		$this->attrs = $attrs;
	}
}
*/

/**
* Date
*/
class WebFormDateFieldWidget extends WebFormDateField implements WebFormFieldWidget
{
	public $type = 'date';
	
	private $emptyDisplay;
	private $startYear;
	private $endYear;
	private $format;
	public $attrs;
	public $addClass;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $value = NULL, $default_value = NULL, $required = false, $empty_display = '-',
		$start_year = NULL, $end_year = NULL, $format = 'Y-m-d', $attrs = '', $postfixes = array('_year', '_month', '_day'), $add_class = false)
	{
		parent::__construct($name, $value, $default_value, $required, $postfixes);

		$this->emptyDisplay = $empty_display;
		$this->startYear = $start_year;
		$this->endYear = $end_year;
		$this->format = $format;
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData
		(
			$this,
			array
			(
				'emptydisplay' => $this->emptyDisplay,
				'start_year' => $this->startYear,
				'end_year' => $this->endYear,
				'format' => $this->format,
				'postfixes' => $this->getPostfixes()
			)
		);
	}
}

/**
* String DDL
*/
class WebFormStringDDLFieldWidget extends WebFormStringField implements WebFormFieldWidget
{
	public $type = 'ddl';

	private $options;
	
	private $emptyValue;
	private $emptyDisplay;
	private $format;
	private $valueField;
	private $displayField;
	public $attrs;
	public $addClass;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $options, $value = NULL, $default_value = NULL, $required = false, $regexp = '',
		$empty_value = '', $empty_display = '-', $format = 'valuemap', $value_field = '', $display_field = '', $attrs = '', $add_class = false)
	{
		if (empty($options)) throw new WebFormWidgetException("Field options empty");
		
		parent::__construct($name, $value, $default_value, $required, $regexp);

		$this->options = $options;
		$this->emptyValue = $empty_value;
		$this->emptyDisplay = $empty_display;
		$this->format = $format;
		$this->valueField = $value_field;
		$this->displayField = $display_field;
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData
		(
			$this,
			array
			(
				'options' => $this->options,
				'emptyvalue' => $this->emptyValue,
				'emptydisplay' => $this->emptyDisplay,
				'format' => $this->format,
				'valuefield' => $this->valueField,
				'displayfield' => $this->displayField
			)
		);
	}
}

/**
* String List
*/
class WebFormStringListFieldWidget extends WebFormStringField implements WebFormFieldWidget
{
	public $type = 'list';
	
	private $options;
	
	private $size;

	private $emptyValue;
	private $emptyDisplay;
	private $format;
	private $valueField;
	private $displayField;
	public $attrs;
	public $addClass;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $options, $value = NULL, $default_value = NULL, $size = 3, $required = false, $regexp = '',
		$empty_value = '', $empty_display = '-', $format = 'valuemap', $value_field = '', $display_field = '', $attrs = '', $add_class = false)
	{
		if (empty($options)) throw new WebFormWidgetException("Field options empty");
		
		parent::__construct($name, $value, $default_value, $required, $regexp);

		$this->options = $options;
		$this->size = $size;
		$this->emptyValue = $empty_value;
		$this->emptyDisplay = $empty_display;
		$this->format = $format;
		$this->valueField = $value_field;
		$this->displayField = $display_field;
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		parent::capture();

		// Nullify empty value
		if ($this->isEmpty()) $this->value = NULL;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData
		(
			$this,
			array
			(
				'options' => $this->options,
				'multiselect' => false,
				'size' => $this->size,
				'emptyvalue' => $this->emptyValue,
				'emptydisplay' => $this->emptyDisplay,
				'format' => $this->format,
				'valuefield' => $this->valueField,
				'displayfield' => $this->displayField
			)
		);
	}
}

/**
* Strings List
*/
class WebFormStringsListFieldWidget extends WebFormStringsField implements WebFormFieldWidget
{
	public $type = 'list';

	private $options;
	
	private $size;

	private $emptyValue;
	private $emptyDisplay;
	private $format;
	private $valueField;
	private $displayField;
	public $attrs;
	public $addClass;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $options, $value = NULL, $default_value = NULL, $size = 3, $required = false,
		$empty_value = '', $empty_display = '-', $format = 'valuemap', $value_field = '', $display_field = '', $attrs = '', $add_class = false)
	{
		if (empty($options)) throw new WebFormWidgetException("Field options empty");
		
		parent::__construct($name, $value, $default_value, $required);

		$this->options = $options;
		$this->size = $size;
		$this->emptyValue = $empty_value;
		$this->emptyDisplay = $empty_display;
		$this->format = $format;
		$this->valueField = $value_field;
		$this->displayField = $display_field;
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function capture()
	{
		parent::capture();

		// Remove empty strings
		if (is_array($this->value))
		{
			foreach ($this->value as $k => $v)
			{
				if (strlen($v) == 0) unset($this->value[$k]);
			}
		}
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData
		(
			$this,
			array
			(
				'options' => $this->options,
				'multiselect' => true,
				'size' => $this->size,
				'emptyvalue' => $this->emptyValue,
				'emptydisplay' => $this->emptyDisplay,
				'format' => $this->format,
				'valuefield' => $this->valueField,
				'displayfield' => $this->displayField
			)
		);
	}
}

/**
* Integer DDL
*/
class WebFormIntegerDDLFieldWidget extends WebFormIntegerField implements WebFormFieldWidget
{
	public $type = 'ddl';

	private $options;
	
	private $emptyValue;
	private $emptyDisplay;
	private $format;
	private $valueField;
	private $displayField;
	public $attrs;
	public $addClass;
	private $lookup; // function to call to retrieve title for actual value

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $options, $value = NULL, $default_value = NULL, $required = false,
		$empty_value = '', $empty_display = '-', $format = 'valuemap', $value_field = 'id', $display_field = 'name', $attrs = '', $add_class = false, $lookup = NULL)
	{
		if (empty($options)) throw new WebFormWidgetException("Field options empty");
		
		
		// Cast to integer
		
		switch ($format)
		{
			case 'datalist':
				foreach ($options as $i => $option)
				{
					$options[$i][$value_field] = intval($option[$value_field]);
				}
			break;
			
			default: // 'valuemap'
				$_options = array();
				
				foreach ($options as $i => $option)
				{
					$_options[intval($i)] = $option;
				}
			break;
		}
		
		
		parent::__construct($name, $value, $default_value, $required);

		$this->options = $options;
		$this->emptyValue = $empty_value;
		$this->emptyDisplay = $empty_display;
		$this->format = $format;
		$this->valueField = $value_field;
		$this->displayField = $display_field;
		$this->attrs = $attrs;
		$this->addClass = $add_class;
		$this->lookup = $lookup;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		$custom_data = array
		(
			'options' => $this->options,
			'emptyvalue' => $this->emptyValue,
			'emptydisplay' => $this->emptyDisplay,
			'format' => $this->format,
			'valuefield' => $this->valueField,
			'displayfield' => $this->displayField
		);

		if (is_callable($this->lookup))
		{
			$value = $this->getValue();
			$custom_data['title'] = call_user_func($this->lookup, $value);
		}
		
		return WebFormWidgetHelper::getWidgetData
		(
			$this, $custom_data
		);
	}
}

/**
* Integer List
*/
class WebFormIntegerListFieldWidget extends WebFormIntegerField implements WebFormFieldWidget
{
	public $type = 'list';
	
	private $options;
	
	private $size;

	private $emptyValue;
	private $emptyDisplay;
	private $format;
	private $valueField;
	private $displayField;
	public $attrs;
	public $addClass;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $options, $value = NULL, $default_value = NULL, $size = 3, $required = false,
		$empty_value = '', $empty_display = '-', $format = 'valuemap', $value_field = '', $display_field = '', $attrs = '', $add_class = false)
	{
		if (empty($options)) throw new WebFormWidgetException("Field options empty");
		

		// Cast to integer
		
		switch ($format)
		{
			case 'datalist':
				foreach ($options as $i => $option)
				{
					$options[$i][$value_field] = intval($option[$value_field]);
				}
			break;
			
			default: // 'valuemap'
				$_options = array();
				
				foreach ($options as $i => $option)
				{
					$_options[intval($i)] = $option;
				}
			break;
		}


		parent::__construct($name, $value, $default_value, $required);

		$this->options = $options;
		$this->size = $size;
		$this->emptyValue = $empty_value;
		$this->emptyDisplay = $empty_display;
		$this->format = $format;
		$this->valueField = $value_field;
		$this->displayField = $display_field;
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData
		(
			$this,
			array
			(
				'options' => $this->options,
				'multiselect' => false,
				'size' => $this->size,
				'emptyvalue' => $this->emptyValue,
				'emptydisplay' => $this->emptyDisplay,
				'format' => $this->format,
				'valuefield' => $this->valueField,
				'displayfield' => $this->displayField
			)
		);
	}
}

/**
* Integers List
*/
class WebFormIntegersListFieldWidget extends WebFormIntegersField implements WebFormFieldWidget
{
	public $type = 'list';

	private $options;
	
	private $size;

	private $emptyValue;
	private $emptyDisplay;
	private $format;
	private $valueField;
	private $displayField;
	public $attrs;
	public $addClass;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $options, $value = NULL, $default_value = NULL, $size = 3, $required = false,
		$empty_value = '', $empty_display = '-', $format = 'valuemap', $value_field = '', $display_field = '', $attrs = '', $add_class = false)
	{
		if (empty($options)) throw new WebFormWidgetException("Field options empty");
		

		// Cast to integer
		
		switch ($format)
		{
			case 'datalist':
				foreach ($options as $i => $option)
				{
					$options[$i][$value_field] = intval($option[$value_field]);
				}
			break;
			
			default: // 'valuemap'
				$_options = array();
				
				foreach ($options as $i => $option)
				{
					$_options[intval($i)] = $option;
				}
			break;
		}


		parent::__construct($name, $value, $default_value, $required);

		$this->options = $options;
		$this->size = $size;
		$this->emptyValue = $empty_value;
		$this->emptyDisplay = $empty_display;
		$this->format = $format;
		$this->valueField = $value_field;
		$this->displayField = $display_field;
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData
		(
			$this,
			array
			(
				'options' => $this->options,
				'multiselect' => true,
				'size' => $this->size,
				'emptyvalue' => $this->emptyValue,
				'emptydisplay' => $this->emptyDisplay,
				'format' => $this->format,
				'valuefield' => $this->valueField,
				'displayfield' => $this->displayField
			)
		);
	}
}

/**
* Integer group
*/
class WebFormIntegerGroupFieldWidget extends WebFormIntegerField implements WebFormFieldWidget
{
	public $type = 'group';

	private $options;

	private $emptyValue;
	private $emptyDisplay;
	private $format;
	private $valueField;
	private $displayField;
	public $attrs;
	public $addClass;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $options, $value = NULL, $default_value = NULL, $required = false,
		$empty_value = '', $empty_display = '-', $format = 'valuemap', $value_field = '', $display_field = '', $attrs = '', $add_class = false)
	{
		if (! isset($options)) throw new WebFormWidgetException("Field options not set");
		

		// Cast to integer
		
		switch ($format)
		{
			case 'datalist':
				foreach ($options as $i => $option)
				{
					$options[$i][$value_field] = intval($option[$value_field]);
				}
			break;
			
			default: // 'valuemap'
				$_options = array();
				
				foreach ($options as $i => $option)
				{
					$_options[intval($i)] = $option;
				}
			break;
		}


		parent::__construct($name, $value, $default_value, $required);

		$this->options = $options;
		$this->emptyValue = $empty_value;
		$this->emptyDisplay = $empty_display;
		$this->format = $format;
		$this->valueField = $value_field;
		$this->displayField = $display_field;
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData
		(
			$this,
			array
			(
				'options' => $this->options,
				'multiselect' => false,
				'emptyvalue' => $this->emptyValue,
				'emptydisplay' => $this->emptyDisplay,
				'format' => $this->format,
				'valuefield' => $this->valueField,
				'displayfield' => $this->displayField
			)
		);
	}
}

/**
* Integers group
*/
class WebFormIntegersGroupFieldWidget extends WebFormIntegersField implements WebFormFieldWidget
{
	public $type = 'group';

	private $options;
	
	private $emptyValue;
	private $emptyDisplay;
	private $format;
	private $valueField;
	private $displayField;
	public $attrs;
	public $addClass;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $options, $value = NULL, $default_value = NULL, $required = false,
		$empty_value = '', $empty_display = '-', $format = 'valuemap', $value_field = '', $display_field = '', $attrs = '', $add_class = false)
	{
		if (! isset($options)) throw new WebFormWidgetException("Field options not set");
		

		// Cast to integer
		
		switch ($format)
		{
			case 'datalist':
				foreach ($options as $i => $option)
				{
					$options[$i][$value_field] = intval($option[$value_field]);
				}
			break;
			
			default: // 'valuemap'
				$_options = array();
				
				foreach ($options as $i => $option)
				{
					$_options[intval($i)] = $option;
				}
			break;
		}


		parent::__construct($name, $value, $default_value, $required);

		$this->options = $options;
		$this->emptyValue = $empty_value;
		$this->emptyDisplay = $empty_display;
		$this->format = $format;
		$this->valueField = $value_field;
		$this->displayField = $display_field;
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}

	protected function getOptionValues()
	{
		$values = array();
		
		switch ($this->format)
		{
			case 'datalist':
				foreach ($this->options as $option)
				{
					$values[] = $option[$this->valueField];
				}
			break;
			
			default: // 'valuemap'
				$values = array_keys($this->options);
			break;
		}
		
		return $values;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		// Discover whether all options are selected
		
		$all = false;
		
		
		$option_values = $this->getOptionValues();
		$values = $this->getValue();
		
		if (is_null($values))
		{
			if (empty($option_values))
			{
				$all = true;
			}
		}
		else
		{
			sort($option_values);
			sort($values);
			
			$all = ($option_values == $values);
		}
		
		
		return WebFormWidgetHelper::getWidgetData
		(
			$this,
			array
			(
				'options' => $this->options,
				'multiselect' => true,
				'emptyvalue' => $this->emptyValue,
				'emptydisplay' => $this->emptyDisplay,
				'format' => $this->format,
				'valuefield' => $this->valueField,
				'displayfield' => $this->displayField,
				
				'all' => $all
			)
		);
	}
}

/**
* String group
*/
class WebFormStringGroupFieldWidget extends WebFormStringField implements WebFormFieldWidget
{
	public $type = 'group';

	private $options;

	private $emptyValue;
	private $emptyDisplay;
	private $format;
	private $valueField;
	private $displayField;
	public $attrs;
	public $addClass;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $options, $value = NULL, $default_value = NULL, $required = false,
		$empty_value = '', $empty_display = '-', $format = 'valuemap', $value_field = '', $display_field = '', $attrs = '', $add_class = false)
	{
		if (! isset($options)) throw new WebFormWidgetException("Field options not set");
		

		// Cast to integer
		/*
		switch ($format)
		{
			case 'datalist':
				foreach ($options as $i => $option)
				{
					$options[$i][$value_field] = intval($option[$value_field]);
				}
			break;
			
			default: // 'valuemap'
				$_options = array();
				
				foreach ($options as $i => $option)
				{
					$_options[intval($i)] = $option;
				}
			break;
		}
		*/

		parent::__construct($name, $value, $default_value, $required);

		$this->options = $options;
		$this->emptyValue = $empty_value;
		$this->emptyDisplay = $empty_display;
		$this->format = $format;
		$this->valueField = $value_field;
		$this->displayField = $display_field;
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData
		(
			$this,
			array
			(
				'options' => $this->options,
				'multiselect' => false,
				'emptyvalue' => $this->emptyValue,
				'emptydisplay' => $this->emptyDisplay,
				'format' => $this->format,
				'valuefield' => $this->valueField,
				'displayfield' => $this->displayField
			)
		);
	}
}

/**
* Strings group
*/
class WebFormStringsGroupFieldWidget extends WebFormStringsField implements WebFormFieldWidget
{
	public $type = 'group';

	private $options;

	private $emptyValue;
	private $emptyDisplay;
	private $format;
	private $valueField;
	private $displayField;
	public $attrs;
	public $addClass;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $options, $value = NULL, $default_value = NULL, $required = false,
		$empty_value = '', $empty_display = '-', $format = 'valuemap', $value_field = '', $display_field = '', $attrs = '', $add_class = false)
	{
		if (! isset($options)) throw new WebFormWidgetException("Field options not set");
		

		// Cast to integer
		/*
		switch ($format)
		{
			case 'datalist':
				foreach ($options as $i => $option)
				{
					$options[$i][$value_field] = intval($option[$value_field]);
				}
			break;
			
			default: // 'valuemap'
				$_options = array();
				
				foreach ($options as $i => $option)
				{
					$_options[intval($i)] = $option;
				}
			break;
		}
		*/

		parent::__construct($name, $value, $default_value, $required);

		$this->options = $options;
		$this->emptyValue = $empty_value;
		$this->emptyDisplay = $empty_display;
		$this->format = $format;
		$this->valueField = $value_field;
		$this->displayField = $display_field;
		$this->attrs = $attrs;
		$this->addClass = $add_class;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return WebFormWidgetHelper::getWidgetData
		(
			$this,
			array
			(
				'options' => $this->options,
				'multiselect' => true,
				'emptyvalue' => $this->emptyValue,
				'emptydisplay' => $this->emptyDisplay,
				'format' => $this->format,
				'valuefield' => $this->valueField,
				'displayfield' => $this->displayField
			)
		);
	}
}


class WebFormFieldWidgetGroup extends WebFormFieldGroup
{
	/**
	*
	*
	* @param WebFormField $field  actually expects subclass of WebFormFieldWidget, but declaration should be compatible with superclass
	*
	* @return
	*/

	// public function addField(WebFormFieldWidget $field)
	public function addField(WebFormField $field)
	{
		xbf_ensure_param_class($field, 'WebFormFieldWidget', true, 'WebFormWidgetException');
		
		
		parent::addField($field);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		return array
		(
			'name' => $this->getName(),
			'required' => $this->isRequired(),
			'error_flags' => $this->getErrorFlags()
		);
	}
}


class WebFormWidgetHelper
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function createField($name, $config)
	{
		$f = NULL;
		
		
		if (! isset($config['type'])) throw new WebFormException("Type not set for field '$name'");
		
		$type = $config['type'];
		$value = isset($config['value']) ? $config['value'] : NULL;
		$default_value = isset($config['default']) ? $config['default'] : NULL;
		$required = isset($config['required']) ? $config['required'] : false;
		$regexp = isset($config['regexp']) ? $config['regexp'] : '';
		$attrs = isset($config['attrs']) ? $config['attrs'] : '';

		$add_class = isset($config['add_class']) ? $config['add_class'] : false;

		switch ($type)
		{
			case 'flaghidden':
				$f = new WebFormFlagHiddenFieldWidget($name, $value, $default_value, $required, $attrs, $add_class);
			break;          

			case 'flag':
				$f = new WebFormFlagFieldWidget($name, $value, $default_value, $required, $attrs, $add_class);
			break;          

			case 'optionhidden':
				$checked = isset($config['checked']) ? $config['checked'] : false;
				
				$f = new WebFormOptionHiddenFieldWidget($name, $value, $default_value, $required, $checked, $attrs, $add_class);
			break;
			
			case 'option':
				$checked = isset($config['checked']) ? $config['checked'] : false;
				
				$f = new WebFormOptionFieldWidget($name, $value, $default_value, $required, $checked, $attrs, $add_class);
			break;
			
			case 'stringhidden':
				$f = new WebFormStringHiddenFieldWidget($name, $value, $default_value, $required, $regexp, $attrs, $add_class);
			break;          

			case 'string':
				$f = new WebFormStringFieldWidget($name, $value, $default_value, $required, $regexp, $attrs, $add_class);
			break;

			case 'password':
				$f = new WebFormPasswordFieldWidget($name, $value, $default_value, $required, $regexp, $attrs, $add_class);
			break;

			case 'text':
				$cols = isset($config['cols']) ? $config['cols'] : 25;
				$rows = isset($config['rows']) ? $config['rows'] : 3;

				$f = new WebFormTextFieldWidget($name, $value, $default_value, $required, $cols, $rows, $regexp, $attrs, $add_class);
			break;

			case 'file':
				$return_type = isset($config['return_type']) ? $config['return_type'] : 'path';
			
				$f = new WebFormFileFieldWidget($name, $value, $default_value, $required, $return_type, $attrs, $add_class);
			break;
			
			case 'integerhidden':
				$f = new WebFormIntegerHiddenFieldWidget($name, $value, $default_value, $required, $attrs, $add_class);
			break;          

			case 'integer':
				$f = new WebFormIntegerFieldWidget($name, $value, $default_value, $required, $attrs, $add_class);
			break;          

			case 'floathidden':
				$f = new WebFormFloatHiddenFieldWidget($name, $value, $default_value, $required, $attrs, $add_class);
			break;          

			case 'float':
				$f = new WebFormFloatFieldWidget($name, $value, $default_value, $required, $attrs, $add_class);
			break;          
			/*
			case 'integer':
				$f = new WebFormIntegerFieldWidget($name, $value, $default_value, $required);
			break;
			*/
			case 'stringddl':
				if (! isset($config['options'])) throw new WebFormWidgetException("Options not set for field '$name'");
				
				$empty_value = isset($config['empty_value']) ? $config['empty_value'] : '';
				$empty_display = isset($config['empty_display']) ? $config['empty_display'] : '-';
				$format = isset($config['format']) ? $config['format'] : 'valuemap';
				$value_field = isset($config['value_field']) ? $config['value_field'] : 'id';
				$display_field = isset($config['display_field']) ? $config['display_field'] : 'name';
				
				$f = new WebFormStringDDLFieldWidget($name, $config['options'], $value, $default_value, $required, $regexp,
					$empty_value, $empty_display, $format, $value_field, $display_field, $attrs, $add_class);
			break;

			case 'stringlist':
				if (! isset($config['options'])) throw new WebFormWidgetException("Options not set for field '$name'");
				
				$size = isset($config['size']) ? $config['size'] : '3';
				$empty_value = isset($config['empty_value']) ? $config['empty_value'] : '';
				$empty_display = isset($config['empty_display']) ? $config['empty_display'] : '-';
				$format = isset($config['format']) ? $config['format'] : 'valuemap';
				$value_field = isset($config['value_field']) ? $config['value_field'] : 'id';
				$display_field = isset($config['display_field']) ? $config['display_field'] : 'name';
				
				$f = new WebFormStringListFieldWidget($name, $config['options'], $value, $default_value, $size, $required, $regexp,
					$empty_value, $empty_display, $format, $value_field, $display_field, $attrs, $add_class);
			break;

			case 'stringslist':
				if (! isset($config['options'])) throw new WebFormWidgetException("Options not set for field '$name'");
				
				$size = isset($config['size']) ? $config['size'] : '3';
				$empty_value = isset($config['empty_value']) ? $config['empty_value'] : '';
				$empty_display = isset($config['empty_display']) ? $config['empty_display'] : '-';
				$format = isset($config['format']) ? $config['format'] : 'valuemap';
				$value_field = isset($config['value_field']) ? $config['value_field'] : 'id';
				$display_field = isset($config['display_field']) ? $config['display_field'] : 'name';
				
				$f = new WebFormStringsListFieldWidget($name, $config['options'], $value, $default_value, $size, $required,
					$empty_value, $empty_display, $format, $value_field, $display_field, $attrs, $add_class);
			break;

			case 'stringgroup':
				if (! isset($config['options'])) throw new WebFormWidgetException("Options not set for field '$name'");
				
				$empty_value = isset($config['empty_value']) ? $config['empty_value'] : '';
				$empty_display = isset($config['empty_display']) ? $config['empty_display'] : '-';
				$format = isset($config['format']) ? $config['format'] : 'valuemap';
				$value_field = isset($config['value_field']) ? $config['value_field'] : 'id';
				$display_field = isset($config['display_field']) ? $config['display_field'] : 'name';
				
				$f = new WebFormStringGroupFieldWidget($name, $config['options'], $value, $default_value, $required,
					$empty_value, $empty_display, $format, $value_field, $display_field, $attrs, $add_class);
			break;

			case 'stringsgroup':
				if (! isset($config['options'])) throw new WebFormWidgetException("Options not set for field '$name'");
				
				$empty_value = isset($config['empty_value']) ? $config['empty_value'] : '';
				$empty_display = isset($config['empty_display']) ? $config['empty_display'] : '-';
				$format = isset($config['format']) ? $config['format'] : 'valuemap';
				$value_field = isset($config['value_field']) ? $config['value_field'] : 'id';
				$display_field = isset($config['display_field']) ? $config['display_field'] : 'name';
				
				$f = new WebFormStringsGroupFieldWidget($name, $config['options'], $value, $default_value, $required,
					$empty_value, $empty_display, $format, $value_field, $display_field, $attrs, $add_class);
			break;

			case 'integerddl':
				if (! isset($config['options'])) throw new WebFormWidgetException("Options not set for field '$name'");
				
				$empty_value = isset($config['empty_value']) ? $config['empty_value'] : '';
				$empty_display = isset($config['empty_display']) ? $config['empty_display'] : '-';
				$format = isset($config['format']) ? $config['format'] : 'valuemap';
				$value_field = isset($config['value_field']) ? $config['value_field'] : 'id';
				$display_field = isset($config['display_field']) ? $config['display_field'] : 'name';
				$lookup = isset($config['lookup']) ? $config['lookup'] : NULL;
				
				$f = new WebFormIntegerDDLFieldWidget($name, $config['options'], $value, $default_value, $required,
					$empty_value, $empty_display, $format, $value_field, $display_field, $attrs, $add_class, $lookup);
			break;
			
			case 'integerlist':
				if (! isset($config['options'])) throw new WebFormWidgetException("Options not set for field '$name'");
				
				$size = isset($config['size']) ? $config['size'] : '3';
				$empty_value = isset($config['empty_value']) ? $config['empty_value'] : '';
				$empty_display = isset($config['empty_display']) ? $config['empty_display'] : '-';
				$format = isset($config['format']) ? $config['format'] : 'valuemap';
				$value_field = isset($config['value_field']) ? $config['value_field'] : 'id';
				$display_field = isset($config['display_field']) ? $config['display_field'] : 'name';
				
				$f = new WebFormIntegerListFieldWidget($name, $config['options'], $value, $default_value, $size, $required,
					$empty_value, $empty_display, $format, $value_field, $display_field, $attrs, $add_class);
			break;

			case 'integerslist':
				if (! isset($config['options'])) throw new WebFormWidgetException("Options not set for field '$name'");
				
				$size = isset($config['size']) ? $config['size'] : '3';
				$empty_value = isset($config['empty_value']) ? $config['empty_value'] : '';
				$empty_display = isset($config['empty_display']) ? $config['empty_display'] : '-';
				$format = isset($config['format']) ? $config['format'] : 'valuemap';
				$value_field = isset($config['value_field']) ? $config['value_field'] : 'id';
				$display_field = isset($config['display_field']) ? $config['display_field'] : 'name';
				
				$f = new WebFormIntegersListFieldWidget($name, $config['options'], $value, $default_value, $size, $required,
					$empty_value, $empty_display, $format, $value_field, $display_field, $attrs, $add_class);
			break;

			case 'integergroup':
				if (! isset($config['options'])) throw new WebFormWidgetException("Options not set for field '$name'");
				
				$empty_value = isset($config['empty_value']) ? $config['empty_value'] : '';
				$empty_display = isset($config['empty_display']) ? $config['empty_display'] : '-';
				$format = isset($config['format']) ? $config['format'] : 'valuemap';
				$value_field = isset($config['value_field']) ? $config['value_field'] : 'id';
				$display_field = isset($config['display_field']) ? $config['display_field'] : 'name';
				
				$f = new WebFormIntegerGroupFieldWidget($name, $config['options'], $value, $default_value, $required,
					$empty_value, $empty_display, $format, $value_field, $display_field, $attrs, $add_class);
			break;

			case 'integersgroup':
				if (! isset($config['options'])) throw new WebFormWidgetException("Options not set for field '$name'");
				
				$empty_value = isset($config['empty_value']) ? $config['empty_value'] : '';
				$empty_display = isset($config['empty_display']) ? $config['empty_display'] : '-';
				$format = isset($config['format']) ? $config['format'] : 'valuemap';
				$value_field = isset($config['value_field']) ? $config['value_field'] : 'id';
				$display_field = isset($config['display_field']) ? $config['display_field'] : 'name';
				
				$f = new WebFormIntegersGroupFieldWidget($name, $config['options'], $value, $default_value, $required,
					$empty_value, $empty_display, $format, $value_field, $display_field, $attrs, $add_class);
			break;
			
			/*
			case 'integerddl':
				if (! isset($config['options'])) throw new WebFormWidgetException("Options not set for field '$name'");
				
				$f = new WebFormIntegerDDLFieldWidget($name, $config['options'], $value, $default_value, $required);
			break;
			*/

			case 'date':
				$empty_display = isset($config['empty_display']) ? $config['empty_display'] : '-';
				$start_year = isset($config['start_year']) ? $config['start_year'] : NULL;
				$end_year = isset($config['end_year']) ? $config['end_year'] : NULL;
				$format = isset($config['format']) ? $config['format'] : 'Y-m-d';
				$postfixes = isset($config['postfixes']) ? $config['postfixes'] : array('_year', '_month', '_day');
			
				$f = new WebFormDateFieldWidget($name, $value, $default_value, $required, $empty_display, $start_year, $end_year,
					$format, $attrs, $postfixes, $add_class);
			break;
			/*
			case 'time':
				if (isset($config['postfixes']))
				{
					$f = new WebFormTimeField($name, $value, $default_value, $required, $config['postfixes']);
				}
				else
				{
					$f = new WebFormTimeField($name, $value, $default_value, $required);
				}
			break;

			case 'datetime':
				if (isset($config['postfixes']))
				{
					$f = new WebFormDateTimeField($name, $value, $default_value, $required, $config['postfixes']);
				}
				else
				{
					$f = new WebFormDateTimeField($name, $value, $default_value, $required);
				}
			break;

			case 'file':
				if (isset($config['return_type']))
				{
					$f = new WebFormFileField($name, $value, $default_value, $required, $config['return_type']);
				}
				else
				{
					$f = new WebFormFileField($name, $value, $default_value, $required);
				}
			break;

			
			case 'strings':
				$f = new WebFormStringsField($name, $value, $default_value, $required);
			break;

			case 'integers':
				$f = new WebFormIntegersField($name, $value, $default_value, $required);
			break;
			*/		
			default:
				throw new WebFormWidgetException("Unknown field type '$type'");
		}
		
		
		return $f;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function addGroup($form, $name, $config)
	{
		// $name = $config['name'];
		$required = isset($config['required']) ? $config['required'] : false;
		$fields = $config['fields'];
		
		// if (! isset($config['type'])) throw new WebFormException("Type not set for field '$name'");
		
		$g = new WebFormFieldWidgetGroup($name, $required);
		
		foreach ($fields as $name => $field_config)
		{
			$f = WebFormWidgetHelper::createField($name, $field_config);
			
			$g->addField($f);
		}
		
		$form->addGroup($g);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function addField($form, $name, $config)
	{
		$f = WebFormWidgetHelper::createField($name, $config);
		
		$form->addField($f);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function getWidgetData(WebFormFieldWidget $widget, Array $custom_data = NULL)
	{
		$data = array
		(
			'type' => $widget->type,
			
			'name' => $widget->getName(),
			'value' => $widget->getValue(),
			'attrs' => $widget->attrs,
			'add_class' => $widget->addClass,
			// 'required' => $this->isRequired(),
			'required' => $widget->isRequired(),
			// 'error_flags' => $this->getErrorFlags()
			'error_flags' => $widget->getErrorFlags()
		);
		
		if (! is_null($custom_data))
		{
			$data = array_merge($data, $custom_data);
		}

		return $data; 
	}
}


class WebFormWidget extends WebForm
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($configs = NULL)
	{
		if (! is_null($configs))
		{
			foreach ($configs as $name => $config)
			{
				if (isset($config['fields'])) // assume group
				{
					WebFormWidgetHelper::addGroup($this, $name, $config);
				}
				else // assume field
				{
					WebFormWidgetHelper::addField($this, $name, $config);
				}
			}
		}
	}
	
	/**
	*
	*
	* @param WebFormField $field  actually expects WebFormFieldWidget, but declaration should be compatible with superclass
	*
	* @return
	*/

	public function addField(WebFormField $field)
	{
		xbf_ensure_param_class($field, 'WebFormFieldWidget', true, 'WebFormWidgetException');
		
		
		parent::addField($field);
	}
	
	/**
	*
	*
	* @param WebFormFieldGroup $group  actually expects WebFormFieldWidgetGroup, but declaration should be compatible with superclass
	*
	* @return
	*/

	public function addGroup(WebFormFieldGroup $group)
	{
		xbf_ensure_param_class($group, 'WebFormFieldWidgetGroup', false, 'WebFormWidgetException');
		

		parent::addGroup($group);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidgetData()
	{
		$data = array('fields' => array(), 'groups' => array());
		

		$fields = $this->getFields();
		foreach ($fields as $name => $field)
		{
			$data['fields'][$name] = $field->getWidgetData();
		}


		$groups = $this->getGroups();

		if (! empty($groups))
		{
			foreach ($groups as $name => $group)
			{
				$data['groups'][$name] = $group->getWidgetData();
			}
		}


		return $data;
	}

/*	
	public function getFormData()
	{
		$data = array('fields' => array(), 'groups' => array());
		
		
		foreach ($this->fields as $name => $field)
		{
			$data['fields'][$name] = array
			(
				'name' => $name,
				'value' => $field->getValue(),
				'required' => $field->isRequired(),
				'error_flags' => $field->getErrorFlags()
			);
		}

		foreach ($this->groups as $name => $group)
		{
			$data['groups'][$name] = array
			(
				'name' => $name,
				'required' => $group->isRequired(),
				'error_flags' => $group->getErrorFlags()
			);
		}
		
		return $data;
	}
*/		
}

?>
