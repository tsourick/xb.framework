<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

require_once('class.DMEntity.php');


require_once('class.DMPlainEntityBase.php');
if (PHP_VERSION >= '5.3')
{
	include('class.DMPlainEntity.php_5_3.php');
}
else
{
	include('class.DMPlainEntity.php_5_2.php');
}

?>
