<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage wrapper
*/


/**
* Services_JSON wrapper class
*
* @version 1.0
*/

class JSON
{
	private static $instance = NULL;

	// private static $systemConfig = NULL;

	private static $json = NULL;
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __clone()
	{
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct()
	{
		if (! is_null(self::$instance)) throw new Exception(__CLASS__ . ' singleton already created');
		
		// if (is_null(self::$systemConfig)) throw new Exception('System config should be set before initialization');


		$class_dir = Framework::get('services_json/install_dir');
		$class_path = $class_dir . 'Services_JSON.php';

		if (! is_readable($class_path)) throw new Exception("'$class_path' is not readable");
		
		include_once($class_path);
		
		
		// Create Services_JSON instance
		
		self::$json = new Services_JSON;


		self::$instance = $this;
	}
	/*
	public static function setSystemConfig($c)
	{
		$r = false;
		
		if (is_null(self::$instance))
		{
			self::$systemConfig = $c;
			$r = true;
		}
		
		return $r;
	}
*/
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function encode($var, $use = 0) // see Services_JSON's $use parameter
	{
		self::$json->use = $use;
		return self::$json->encode($var);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function decode($var, $use = 0)
	{
		self::$json->use = $use;
		return self::$json->decode($var);
	}
}

// JSON::setSystemConfig(SystemConfig::getObject());
new JSON();

?>