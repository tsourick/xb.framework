<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage orm
*/

require_once('class.ORMFieldType.php');
require_once('class.SQL.php');


class ORMFieldException extends ORMException
{
}

class ORMField
{
	private $ormTable; // Parent table reference

	private $name; // string
	
	private $ormFieldType; // ORMFieldType
	
	private $null; // boolean
	private $default; // value or NULL
	private $auto_increment; // boolean
	private $comment; // string
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(ORMTable $ormTable, $name, ORMFieldType $ormFieldType, $null = false, $default = NULL, $auto_increment = false, $comment = '')
	{
		$this->ormTable = $ormTable;

		$this->name = $name;
		
		$this->ormFieldType = $ormFieldType;
		
		$this->null = $null;
		$this->default = $default;
		$this->auto_increment = $auto_increment;
		$this->comment = $comment;
		
		
		$ormTable->addField($this);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getName()
	{
		return $this->name;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPreparedName()
	{
		return SQL::prepare_name($this->name);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPreparedNameValue($value)
	{
		return SQL::prepare_name($this->name) . ' = ' . SQL::prepare_value($value);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getFullName()
	{
		return $this->ormTable->getName() . '.' . $this->name;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPreparedFullName()
	{
		return $this->ormTable->getPreparedName() . '.' . $this->getPreparedName();
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isNull()
	{
		return $this->null;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getDefaultValue()
	{
		return $this->default;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isAutoincrement()
	{
		return $this->auto_increment;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getComment()
	{
		return $this->comment;
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setName($name)
	{
		if (strcmp($this->name, $name) != 0)
		{
			$old_name = $this->name;

			$this->name = $name;
			
			$this->ormTable->remapField($this, $old_name); // Update field name upstream
		}
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setORMFieldType(ORMFieldType $ormFieldType)
	{
		$this->ormFieldType = $ormFieldType;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setNull($null)
	{
		$this->null = $null;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setDefaultValue($default)
	{
		$this->default = $default;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setAutoincrement($auto_increment)
	{
		$this->auto_increment = $auto_increment;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setComment($comment)
	{
		$this->comment = $comment;
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function table()
	{
		return $this->ormTable;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getNextValue()
	{
		if (! $this->auto_increment) throw new ORMFieldException("Could not give next value for non-autoincrement field.");
		
		$row = $this->ormTable->getRowDataCustom('IF(MAX(' . $this->getPreparedName(). ') IS NOT NULL, MAX(' . $this->getPreparedName(). ') + 1, 1) AS next_value', '1 FOR UPDATE');

		return $row['next_value'];
	}
	
	
	/**
	*
		Config structure
		
		array
		(
			'name' => '<field name>',
			'type' => array
			{
				'name' => '(BIT |TINYINT |SMALLINT |MEDIUMINT |INT |INTEGER |BIGINT |REAL |DOUBLE |FLOAT |DECIMAL |NUMERIC| ...)', 					
				'length' => (<type length>|NULL), // if applicable
				'decimals' => (<decimal count>|NULL), // if applicable
				'unsigned' => (true|false|NULL), // if applicable
			),
			'null' => (true|false),
			'default' => (<default value>|NULL),
			'auto_increment' => (true|false),
			'comment' => '<comment on field>'
		)
	*/
	static public function createFromConfig(ORMTable $ormTable, $config)
	{
		$name = $config['name'];
		
		/**
		* Config structure:
		* @see ORMFieldType->createFromConfig()
		*/
		$ormFieldType = ORMFieldType::createFromConfig($config['type']);
		
		$null = isset($config['null']) ? (bool)$config['null'] : false;
		$default = isset($config['default']) ? $config['default'] : NULL;
		$auto_increment = isset($config['auto_increment']) ? (bool)$config['auto_increment'] : false;
		$comment = isset($config['comment']) ? (string)$config['comment'] : '';
		
		$ormField = new self($ormTable, $name, $ormFieldType, $null, $default, $auto_increment, $comment);
		
		return $ormField;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getConfig()
	{
		return array
		(
			'name' => $this->name,
			'type' => $this->ormFieldType->getConfig(),
			'null' => $this->null,
			'default' => $this->default,
			'auto_increment' => $this->auto_increment,
			'comment' => $this->comment
		);
	}


	/*
	public function _createDDLSource()
	{
		$ddl = SQL::prepare_name($this->name) . ' ' . $this->ormFieldType->_createDDLSource();

		$ddl .= $this->null ? ' NULL' : ' NOT NULL';
		
		if (! is_null($this->default))
		{
			$ddl .= ' DEFAULT ' . SQL::prepare_value($this->default);
		}
		/ *
		if ($this->auto_increment)
		{
			$ddl .= ' AUTO_INCREMENT';
		}
		* /
		if (! empty($this->comment))
		{
			$ddl .= ' COMMENT \'' . SQL::escape($this->comment) . '\'';
		}
		
		
		return $ddl;
	}
	*/

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function _SQLDDLGetDefinition()
	{
		$ddl = SQL::prepare_name($this->name) . ' ' . $this->ormFieldType->_SQLDDLGetDefinition();

		$ddl .= $this->null ? ' NULL' : ' NOT NULL';
		
		if (! is_null($this->default))
		{
			$ddl .= ' DEFAULT ' . SQL::prepare_value($this->default);
		}
		/*
		if ($this->auto_increment)
		{
			$ddl .= ' AUTO_INCREMENT';
		}
		*/
		if (! empty($this->comment))
		{
			$ddl .= ' COMMENT \'' . SQL::escape($this->comment) . '\'';
		}
		
		
		return $ddl;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetCreateAutoincrementQuerySet()
	{
		$qset = array();
		
		
		$q = 'ALTER TABLE ' . $this->ormTable->getPreparedName() . ' MODIFY COLUMN ';

		$q .= $this->_SQLDDLGetDefinition() . ' AUTO_INCREMENT';
		
		$qset[] = $q;
		
		
		return $qset;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetAddQuerySet()
	{
		$qset = array();
		

		$q = 'ALTER TABLE ' . $this->ormTable->getPreparedName() . ' ADD COLUMN ' . $this->_SQLDDLGetDefinition();

		$qset[] = $q;

		
		return $qset;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetModifyQuerySet()
	{
		$qset = array();
		

		$q = 'ALTER TABLE ' . $this->ormTable->getPreparedName() . ' MODIFY COLUMN ' . $this->_SQLDDLGetDefinition();

		$qset[] = $q;

		
		return $qset;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetChangeQuerySet($old_name)
	{
		$qset = array();
		

		$q = 'ALTER TABLE ' . $this->ormTable->getPreparedName() . ' CHANGE COLUMN ' . SQL::prepare_name($old_name) . ' ' . $this->_SQLDDLGetDefinition();

		$qset[] = $q;

		
		return $qset;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetDropQuerySet()
	{
		$qset = array();
		

		$q = 'ALTER TABLE ' . $this->ormTable->getPreparedName() . ' DROP COLUMN ' . $this->getPreparedName();

		$qset[] = $q;

		
		return $qset;
	}
}


?>
