<?php

abstract class TableDataSource
{
	const EXCEL_FILE_TYPE = 1;
	const CSV_FILE_TYPE = 2;
	
	private $columns;

	
	public function setColumns($columns)
	{
		$this->columns = $columns;
	}
	
	abstract public function readRows($start_row_index = NULL, $row_count = NULL);
}

class TableDataSourceExcelFile extends TableDataSource 
{
	private $filePath;

	private $reader;
	private $readerFilter;
	
	private $columnNames;
	
	
	public function __construct($file_path, $source_options = array())
	{
		$this->filePath = $file_path;
	
		
		include_once($source_options['phpexcel_dir'] . 'Classes/PHPExcel.php');
		
		
		$reader = new PHPExcel_Reader_Excel2007();
		$reader->setReadDataOnly(true);
		

		include_once('class.PHPExcelChunkReadFilter.php');
		
		// $filter = new TableDataSourceExcel_ChunkReadFilter();
		$filter = new PHPExcelChunkReadFilter();
		$reader->setReadFilter($filter);
		

		$this->reader = $reader;
		$this->readerFilter = $filter;
	}
	
	
	/**
	* Every column can be given by char (excel native notation) or by 0-based index.
	*/
	public function setColumns($columns)
	{
		$alpha = range('A', 'Z');
		
		// $column_indexes = array();
		$column_names = array();
		
		foreach ($columns as $column_key => $column_name)
		{
			if (is_string($column_name))
			{
				$column_names[$column_key] = $column_name;
			}
			else
			{
				$column_names[$column_key] = $alpha[$column_name];
			}
		}
		

		parent::setColumns($columns);
		
		$this->readerFilter->setColumns($column_names);


		$this->columnNames = $column_names;
	}

	public function readRows($start_row_index = NULL, $row_count = NULL)
	{
		$rows = array();
		

		$this->readerFilter->setRows($start_row_index + 1, $row_count); // excel indexes start from 1 
		$pe = $this->reader->load($this->filePath);
		
		
		$sheet = $pe->getSheet(0);

		$highest_row_index = $sheet->getHighestRow();
		for ($row_index = $start_row_index; ($row_index + 1) <= $highest_row_index; $row_index++) // excel indexes start from 1
		{
			foreach ($this->columnNames as $column_key => $column_name)
			{
				$value = $sheet->getCell($column_name . ($row_index + 1))->getValue(); // excel indexes start from 1

				$column_index = is_string($column_key) ? $column_key : $column_name;
				$rows[$row_index][$column_index] = $value;
			}
		}

		$pe->disconnectWorksheets(); 
		unset($pe);
		
		
		return $rows;
	}
	
	public function oneToZeroBased($index)
	{
		return $index--;
	}
	public function zeroToOneBased($index)
	{
		return $index++;
	}
	// public function indexBeforeEnd($index)
	public function blockHasData($index, $row_count)
	{
		$result = false;
		
		// try to read next block
		// $this->readerFilter->setRows($index, 2); // try to read from index plus one row more
		$this->readerFilter->setRows($this->zeroToOneBased($index), $row_count); // try to read next block
		$pe = $this->reader->load($this->filePath);
		
		
		$sheet = $pe->getSheet(0);
		
		$highest_row_index = $this->oneToZeroBased($sheet->getHighestRow());
		if ($highest_row_index >= $index)
		{
			if ($highest_row_index > 0)
			{
				$result = true;
			}
			else
			{
				// NOTE: empty block always has highest excel index "one" (zero-based "zero")
				// TODO: additional check needed here 
				$result = true;
			}
		}
		// wdebug("max index, index: $highest_row_index, $index");
		
		$pe->disconnectWorksheets(); 
		unset($pe);
		
		
		return $result;
	}
	
	public function close()
	{
		return true;
	}
}

class TableDataSourceCSVFile extends TableDataSource
{
	private $filePath;
	private $fileEncoding;
	
	private $delimiter;
	
	// private $fileHandle = NULL;
	// private $position;
	// private $rowIndex = 0;
	private $columnIndexes;


	public function __construct($file_path, $source_options = array())
	{
		$this->filePath = $file_path;
		
		$so = $source_options;
		$this->fileEncoding = array_get_value($so, 'file_encoding', "utf-8"); 
		$this->delimiter = array_get_value($so, 'delimiter', "\t"); 
	}
	
	/**
	* <key to map to> => <column index in file>
	*/
	public function setColumns($columns)
	{
		/*
		$column_indexes = array();
		
		foreach ($columns as $column_key => $column_index)
		{
			$column_indexes[$column_key] = $column_index;
		}

		$this->columnIndexes = $column_indexes;
		*/
		
		$this->columnIndexes = $columns;
	}
	
	public function parseRow($row)
	{
		$values = array();
		
		if (! empty($row))
		{
			if ($this->fileEncoding != 'utf-8')
			{
				$row = iconv($this->fileEncoding, 'utf-8', $row);
			}
			
			$row = explode($this->delimiter, $row);
			
	
			foreach ($this->columnIndexes as $column_key => $column_index)
			{
				$value = array_key_exists($column_index, $row) ? $row[$column_index] : NULL;
				
				$column_index = is_string($column_key) ? $column_key : $column_index;
				$values[$column_index] = $value;
			}
		}
			
		return $values;
	}
	
	public function readRows($start_row_index = NULL, $row_count = NULL)
	{//wdebug("read rows: $start_row_index, $row_count");
		$rows = array();
		
		
		$h = @fopen($this->filePath, "r");
		
		if ($h)
		{
			$row_index = 0;
			while (($row = fgets($h)) !== false)
			{
				// Stop after end row index
				if ($row_index >= ($start_row_index + $row_count)) break;

				// Store rows after start row index
				if ($start_row_index <= $row_index && $row_index < ($start_row_index + $row_count))
				{
					$row = $this->parseRow($row);
					if (! empty($row))
					{
						$rows[$row_index] = $row;
					}
				}
				
				$row_index++;
			}
			
			fclose($h);
		}
		// wdebug($rows);
		
		return $rows;
	}
	
	public function blockHasData($start_row_index, $row_count)
	{
		$has_data = false;
		
		
		$h = @fopen($this->filePath, "r");
		
		if ($h)
		{
			$row_index = 0;
			while (($row = fgets($h)) !== false)
			{
				// Stop after end row index
				if ($row_index >= ($start_row_index + $row_count)) break;

				// Try rows after start row index
				if ($start_row_index <= $row_index && $row_index < ($start_row_index + $row_count))
				{
					if (! empty($row))
					{
						$has_data = true;
						break;
					}
				}
				
				$row_index++;
			}
			
			fclose($h);
		}

		
		return $has_data;
	}
	/*
	public function close()
	{
		fclose($this->fileHandle);
		
		$this->fileHandle = NULL;
	}
	*/
}

class TableDataReader
{
	private $chunkTimeLimit = 100;
	private $chunkRowCount = 10;
	
	private $source;
	
	private $rowIndex = 0;
	
	
	public function __construct($data, $type = NULL, $source_options = array())
	{
		if (is_null($type)) $type = $this->detectDataType($data);
		
		switch ($type)
		{
			case TableDataSource::EXCEL_FILE_TYPE:
				$this->source = new TableDataSourceExcelFile($data, $source_options);
			break;
			
			case TableDataSource::CSV_FILE_TYPE:
				$this->source = new TableDataSourceCSVFile($data, $source_options);
			break;
		}
	}
	
	
	private function detectDataType($data)
	{
		$type = '';
		
		// TODO: implement detection
		
		if (baseext($data) == 'xlsx')
		{
			$type = TableDataSource::EXCEL_FILE_TYPE;
		}
		elseif (baseext($data) == 'csv')
		{
			$type = TableDataSource::CSV_FILE_TYPE;
		}
		else
		{
			throw new Exception("Unrecognized table data format");
		}
		
		return $type;
	}
	
	public function setChunkRowCount($count)
	{
		$this->chunkRowCount = $count;
	}
	
	public function setChunkTimeLimit($seconds)
	{
		$this->chunkTimeLimit = $seconds;
	}

	/**
	* Order of columns is preserved in the result dataset.
	* Indexes corresponding to columns in the result may not be preserved depending on data source,
	* except when mapping is used.
	* Indexes preserved when mapping is used.
	* String key should be set for a column to use mapping on that column.
	*/
	public function setColumns($columns)
	{
		$this->source->setColumns($columns);
	}
	

	/**
	* @param mixed $start start index or NULL to read from current position
	* @param mixed $count row count to read or NULL to read default count of rows
	*/
	public function readRows($start_row_index = NULL, $row_count = NULL)
	{//wdebug("<< readRows()");
		set_time_limit($this->chunkTimeLimit);
		
		
		if (is_null($start_row_index)) $start_row_index = $this->rowIndex;
		if (is_null($row_count)) $row_count = $this->chunkRowCount;
		
		//wdebug("start_row_index: $start_row_index; row_count: $row_count");
		$rows = $this->source->readRows($start_row_index, $row_count);
		

		$row_index = $start_row_index + $row_count;//wdebug("next row_index: $row_index");
		if (! $this->source->blockHasData($row_index, $row_count)) $row_index = -1;
		//wdebug("real row_index: $row_index");
		$this->rowIndex = $row_index;

		//wdebug("readRows() >>");
		return $rows; 
	}
	
	public function isLastBlock()
	{
		return $this->rowIndex == -1;
	}
	
	public function getRowIndex()
	{
		return $this->rowIndex;
	}
	/*
	public function close()
	{
		$this->source->close();
	}
	*/
}
?>
