<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
*/

class SMSCoinException extends Exception
{
}

class SMSCoin
{
	private $install_dir;
	private $base_url;
	

	public function __construct($install_dir = '', $base_url = '')
	{
		$this->install_dir = dirpath($install_dir);
		$this->base_url = dirpath($base_url);

		include($this->install_dir . 'lib/config.php');
	}
	
	public function isCodeValid($pair)
	{
		$key = KEY_ID;		
		
		
		$do_die = 0;
		if (isset($pair) && $pair !='' && strlen($pair)<=10)
		{
			$db = new mini_sql();
	
			// here we check for key_pair if exist
			$result = $db->query("SELECT * FROM skeys
				WHERE k_status='1'
					AND k_pair='".addslashes($pair)."'
					AND k_key='".addslashes(intval($key))."'");
	
			if ($db->num_rows($result) === 1) {
				$data = $db->fetch($result);
				if ($data['k_first_access'] == '0') {
					$do_die = 1;
				} elseif ($data['k_timeout'] == 0 || ($data['k_first_access']+$data['k_timeout']*60)>time()) {
					if ($data['k_limit_start'] > 0) {
						if ($data['k_limit_current'] > 0) {
							$do_die = 1;
						}
					} else {
						$do_die = 1;
					}
				}
			} else {
				### пользователь ввел неправильный пароль ###
				$do_die = 2;
			}
		}
		
		
		return (bool)($do_die == 1);
	}

	public function useCode($pair)
	{
		$key = KEY_ID;		
		
		
		$do_die = 0;
		if (isset($pair) && $pair !='' && strlen($pair)<=10) {
			$db = new mini_sql();
	
			// here we check for key_pair if exist
			$result = $db->query("SELECT * FROM skeys
				WHERE k_status='1'
					AND k_pair='".addslashes($pair)."'
					AND k_key='".addslashes(intval($key))."'");
	
			if ($db->num_rows($result) === 1) {
				$data = $db->fetch($result);
				if ($data['k_first_access'] == '0') {
					$db->query("UPDATE skeys
						SET k_first_access='".time()."', k_first_ip='".addslashes($_SERVER["REMOTE_ADDR"])."',
							k_first_from='".addslashes($_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"])."'".($data['k_limit_current'] > 0 ? ", k_limit_current=k_limit_current-1" : "")."
						WHERE k_pair='".addslashes($pair)."' AND k_key='".addslashes(intval($key))."'");
					$do_die = 1;
				} elseif ($data['k_timeout'] == 0 || ($data['k_first_access']+$data['k_timeout']*60)>time()) {
					if ($data['k_limit_start'] > 0) {
						if ($data['k_limit_current'] > 0) {
							$db->query("UPDATE skeys SET k_limit_current=k_limit_current-1
								WHERE k_pair='".addslashes($pair)."'
									AND k_key='".intval($key)."' AND k_limit_current>0");
							$do_die = 1;
						}
					} else {
						$do_die = 1;
					}
				}
			} else {
				### пользователь ввел неправильный пароль ###
				$do_die = 2;
			}
		}
		
		
		return (bool)($do_die == 1);
	}
}
?>
