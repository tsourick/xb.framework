<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @version 1.0
* @package modules
* @subpackage wrapper
*/


/**
* Class exception
*/

class CaptchaException extends Exception
{
}


/**
* Captcha wrapper class
*/

class Captcha
{
	private $session = NULL;
	
	
	public function __construct($session)
	{
		$this->session = $session;
	}
	
	
	private function makeSessionKeyName($name)
	{
		return 'captcha[' . $name . ']';
	}
	
	public function makeAndSend($name = 'default')
	{
		xbf_http_nocache();


		$install_dir = Framework::get('kcaptcha/install_dir');
		$class_path = $install_dir . 'kcaptcha.php';


		include_once($class_path);

		$kcaptcha = new KCAPTCHA();

		$value = $kcaptcha->getKeyString();

		$this->session->set($this->makeSessionKeyName($name), $value);
	}
	
	public function getValue($name = 'default')
	{
		return $this->session->get($this->makeSessionKeyName($name));
	}
	
	public function clearValue($name = 'default')
	{
		$this->session->clear($this->makeSessionKeyName($name));
	}
	
	public function cutValue($name = 'default')
	{
		$value = $this->getValue($name);
		
		$this->clearValue($name);
		
		return $value;
	}
}
?>
