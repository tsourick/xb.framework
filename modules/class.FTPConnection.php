<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* FTPConnection class
*
* @version 1.0
* @package modules
* @subpackage ftp
*/


/**
* undocumented class
*/

class FTPConnectionException extends Exception
{
}


/**
* undocumented class
*/

class FTPConnection
{
	private $host = 'localhost';
	private $port = '21';
	private $user = 'anonymous';
	private $pwd = '';
	private $timeout = 10;
	private $pasv = false;

	private $ftp;
	
	private $cwd_backup; // used in save_cwd()/restore_cwd()
	private $_cwd_backup; // used in _save_cwd()/_restore_cwd()
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($host = NULL, $port = NULL, $user = NULL, $pwd = NULL, $timeout = NULL, $pasv = NULL)
	{
		if (! is_null($host)) $this->host = $host;
		if (! is_null($port)) $this->port = $port;
		if (! is_null($user)) $this->user = $user;
		if (! is_null($pwd)) $this->pwd = $pwd;
		if (! is_null($timeout)) $this->timeout = $timeout;
		if (! is_null($pasv)) $this->pasv = $pasv;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function open($host = NULL, $port = NULL, $user = NULL, $pwd = NULL, $timeout = NULL, $pasv = NULL)
	{
		$result = true;
		

		if (is_null($host))
		{
			if (is_null($this->host)) throw new FTPConnectionException("Host not specified!");

			$host = $this->host;
		}
		else $this->host = $host;
		
		if (is_null($port))
		{
			if (is_null($this->port)) throw new FTPConnectionException("Port not specified!");

			$port = $this->port;
		}
		else $this->port = $port;

		if (is_null($user))
		{
			if (is_null($this->user)) throw new FTPConnectionException("User not specified!");

			$user = $this->user;
		}
		else $this->user = $user;

		if (is_null($pwd))
		{
			if (is_null($this->pwd)) throw new FTPConnectionException("Password not specified!");

			$pwd = $this->pwd;
		}
		else $this->pwd = $pwd;

		if (is_null($timeout))
		{
			if (is_null($this->timeout)) throw new FTPConnectionException("Timeout value not specified!");

			$timeout = $this->timeout;
		}
		else $this->timeout = $timeout;

		if (is_null($pasv))
		{
			if (is_null($this->pasv)) throw new FTPConnectionException("Transmission mode not specified!");

			$pasv = $this->pasv;
		}
		else $this->pasv = $pasv;


		if (! $this->ftp = ftp_connect($host, $port, $timeout)) throw new FTPConnectionException("Could not connect to FTP server");
		
		if (! ftp_login($this->ftp, $user, $pwd)) throw new FTPConnectionException("Could not login to FTP server");

		if ($pasv)
		{
			if (! ftp_pasv($this->ftp, true)) throw new FTPConnectionException("Could not PASV to FTP server");
		}
		
		
		return $result;
	}
	

	/*
	* Underlayer wrappers
	*/
	
	public function ftp_chdir($dir)
	{
		return ftp_chdir($this->ftp, $dir);
	}
	
	public function ftp_pwd()
	{
		return ftp_pwd($this->ftp);
	}
	
	public function ftp_mkdir($dir)
	{
		return ftp_mkdir($this->ftp, $dir);
	}
	
	public function ftp_rmdir($dir)
	{
		return ftp_rmdir($this->ftp, $dir);
	}
	
	public function ftp_delete($path)
	{
		return ftp_delete($this->ftp, $path);
	}
	
	public function ftp_systype()
	{
		return ftp_systype($this->ftp);
	}
	
	public function ftp_size($file)
	{
		return ftp_size($this->ftp, $file);
	}
	
	public function ftp_nlist($directory_name)
	{
		return ftp_nlist($this->ftp, $directory_name);
	}

	public function ftp_rawlist($directory_name)
	{
		return ftp_rawlist($this->ftp, $directory_name);
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function chdir($dir)
	{
		return $this->ftp_chdir($dir);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function cwd()
	{
		return $this->ftp_pwd();
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function mkdir($path)
	{
		$this->_save_cwd();
		

		$path = dirpath($path, '/');
		$path = rtrim($path, '/');
		$dirs = explode('/', $path);
		
		foreach ($dirs as $dir)
		{
			if (empty($dir)) // assume root
			{
				$this->chdir('/');
				continue;
			}
			
			if (! $this->file_exists($dir))
			{
				$this->ftp_mkdir($dir);
			}
			
			$this->ftp_chdir($dir);
		}
		

		$this->_restore_cwd();
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function rmdir($dir)
	{
		return $this->ftp_rmdir($dir);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function put($remote_file, $local_file, $mode)
	{
		return ftp_put($this->ftp, $remote_file, $local_file, $mode);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function fput($remote_file, $handle, $mode, $startpos = 0)
	{
		return ftp_fput($this->ftp, $remote_file, $handle, $mode, $startpos);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function delete($path)
	{
		return $this->ftp_delete($path);
	}
	

	// FOR PRIVATE USE
	private function _save_cwd()
	{
		$this->_cwd_backup = $this->cwd();
	}
	
	private function _restore_cwd()
	{
		$this->chdir($this->_cwd_backup);
	}
	
	
	public function save_cwd()
	{
		$this->cwd_backup = $this->cwd();
	}
	
	public function restore_cwd()
	{
		$this->chdir($this->cwd_backup);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function nlist($directory_name)
	{
		$result = array();
		
		
		$nlist = $this->ftp_nlist($directory_name);
		
		if (! $nlist) throw new FTPConnectionException("Could not NLIST");

		
		$result = array_map('basename', $nlist);
		
		
		return $result;
	}
	
	private function need_chdir($dirpath)
	{
		return true;
	}
	
	public function rawlist($dirpath)
	{
		$list = $this->ftp_rawlist($dirpath);
		
		if (! $list) throw new FTPConnectionException("Could not RAWLIST");


		return $list;
	}
	
	public function ls($dirpath)
	{
		$list = array();
		

		$rawlist = $this->rawlist($dirpath);


		// Parse raw list
		
		$cols = array("permissions", "number", "owner", "group", "size", "month", "day", "time", "name");
		 
		foreach ($rawlist as $i => $row)
		{
			$item = array();
			
			$values = explode(" ", preg_replace('!\s+!', ' ', $row), 9);
			
			foreach($values as $j => $value)
			{
				$item[$cols[$j]] = $value;
			}
			
			$list[] = $item;
		}
		
		
			
		return $list;
	}
	
	/**
	* Tests if file or directory exists
	*/
	public function file_exists($path)
	{
		$exists = false;
		
		$path = trim($path);
		
		if ($path == '/') return true;
		
		$parent_dirpath = filedirpath($path, '/');
		$item_name = basename($path);

		$list = $this->ls($parent_dirpath);
		
		foreach ($list as $item)
		{
			if ($item['name'] == $item_name)
			{
				$exists = true;
				break;
			}
		}
		
		return $exists;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function close()
	{
		return ftp_close($this->ftp);
	}
}
?>
