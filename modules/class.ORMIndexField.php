<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage orm
*/

require_once('class.SQL.php');


class ORMIndexFieldException extends ORMException
{
}

class ORMIndexField
{
	private $ormField; // reference to ORMField
	// private $name; // reference to fields' name
	
	private $length; // number or NULL


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(ORMField $ormField, $length = NULL)
	{
		$this->ormField = $ormField;

		$this->length = $length;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getName()
	{
		return $this->ormField->getName();
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getORMField()
	{
		return $this->ormField;
	}
	
	/**
	*
		Config structure
		
		array
		(
			'name' => '<field name>',
			'length' => (<index length>|NULL)
		)
			
	*/
	static public function createFromConfig(ORMField $ormField, $config)
	{
		$length = isset($config['length']) ? $config['length'] : NULL;
		
		$ormIndexField = new self($ormField, $length);
		
		return $ormIndexField;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getConfig()
	{
		return array
		(
			'name' => $this->getName(), 'length' => $this->length
		);
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function _SQLDDLGetDefinition()
	{
		$ddl = SQL::prepare_name($this->getName());
		
		if (! is_null($this->length)) $ddl .= '(' . $this->length . ')';
		
		return $ddl;
	}
}


?>
