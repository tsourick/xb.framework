<?php

class PHPExcelChunkReadFilter implements PHPExcel_Reader_IReadFilter
{
	private $start_row = 0;
	private $end_row = 0;
	private $columns;
	
 
	public function __construct()
	{
		$this->columns = range('A', 'Z');
	}
	
	
	public function setRows($start_row, $chunk_size)
	{ 
		$this->start_row = $start_row; 
		$this->end_row = $start_row + $chunk_size - 1;
	} 
	
	public function setColumns($columns)
	{
		$this->columns = $columns;
	}

	public function readCell($column, $row, $worksheetName = '')
	{
		$read = false;
		

		if (($row >= $this->start_row && $row <= $this->end_row))
		{ 
			if (in_array($column, $this->columns))
			{ 
				$read = true;
			}
		}

		
		return $read;
	} 
}

?>
