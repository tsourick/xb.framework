<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

abstract class DMLinkBase
{
	protected $dm = NULL; // Data model

	protected $dbc = NULL; // DBConnection instance

	protected $name; // Entity name string

	protected $type;

	
	public $fromEntity = NULL;
	public $fromRequired = false;
	
	public $toEntity = NULL;
	public $toRequired = false;
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DMDataModel $dm, $name, DMEntity $from_entity, $from_required, DMEntity $to_entity, $to_required, $custom_field_map = NULL)
	{
		$this->dm = $dm;
		
		$this->dbc = $dm->getDBConnection();

		$this->name = $name;
		
		
		$dm->addLink($this);
	}
	
	// THIS IS NOT A LANGUAGE DESTRUCTOR
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function destruct()
	{
		$this->dm->removeLink($this);
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getName()
	{
		return $this->name;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getType()
	{
		return $this->type;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function dataModel()
	{
		return $this->dm;
	}	


	/**
	*
		Config structure
		
		array
		(
			'name' => '<link name>',
			'from' => '<parent entity name>',
			'from_required' => (true|false),
			'to' => '<child entity name>',
			'to_required' => (true|false)
		)
	*/
	/*
	static public function createFromConfig(DMDataModel $dm, $config)
	{
		$custom_field_map = isset($config['custom_field_map']) ? $config['custom_field_map'] : NULL;
		
		$from_entity = $dm->entity($config['from']);
		$to_entity = $dm->entity($config['to']);
		
		
		$dmLink = new self($dm, $config['name'], $from_entity, $config['from_required'], $to_entity, $config['to_required'], $custom_field_map);
		
		
		return $dmLink;
	}
	*/


	abstract public function checkAndAdjustForDelete($actOn, $condition = NULL);
	abstract public function checkAndAdjustForInsert($actOn, $data);
	

	// Override this method in derived classes to provide appropriate functionality
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function remapEntity(DMEntity $dmEntity, $old_name)
	{
		if ($this->fromEntity != $dmEntity && $this->toEntity != $dmEntity) throw new DMLinkException("Could not remap entity '" . $dmEntity->getName()  . "' from name '" . $old_name . "' because entity is not used in this link.");

		
		if (strcmp($old_name, $dmEntity->getName()) != 0)
		{
			// remap
		}
	}
}

?>
