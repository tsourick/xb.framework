<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
*/

/**
* Message localization
*/

/**
* Default Lang class object to use with t() function.
* It is overwrited every time new Lang object is instantiated.
*/

$RUNTIME['lang'] = NULL;


/**
* Lang class exception
*/

class LangException extends Exception
{
}

/**
* Lang class
*/

class Lang
{
	// private $installDir = NULL;
	private $dirs = array();
	private $messages = array();
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/
/*
	public function __construct($install_dir = NULL, $lang = NULL)
	{
		global $RUNTIME;
		
	
		$lang_config = Framework::get('lang');
		
		
		if (! is_null($install_dir))
		{
			$this->setInstallDir($install_dir);
		}
		elseif (isset($lang_config['install_dir']) && ! empty($lang_config['install_dir']))
		{
			$this->setInstallDir($lang_config['install_dir']);
		}
		
		
		if (! is_null($lang))
		{
			$this->useLanguage($lang);
		}
		elseif (isset($lang_config['default_language']) &&! empty($lang_config['default_language']))
		{
			$this->useLanguage($lang_config['default_language']);
		}
		
		
		$RUNTIME['lang'] = $this;
	}
*/	
	public function __construct($dir = NULL, $lang = NULL)
	{
		global $RUNTIME;
		
	
		$lang_config = Framework::get('lang');
		
		
		if (! is_null($dir))
		{
			$this->addDir($dir);
		}
		elseif (isset($lang_config['dir']) && ! empty($lang_config['dir']))
		{
			$this->addDir($lang_config['dir']);
		}
		
		
		if (! is_null($lang))
		{
			$this->useLanguage($lang);
		}
		elseif (isset($lang_config['default_language']) &&! empty($lang_config['default_language']))
		{
			$this->useLanguage($lang_config['default_language']);
		}
		
		
		$RUNTIME['lang'] = $this;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/
/*
	public function setInstallDir($dir)
	{
		$dir = dirpath($dir);
		

		if (! is_readable($dir)) throw new LangException("'{$dir}' is not readable");
		

		$this->installDir = $dir;
	}
*/
	public function addDir($dir)
	{
		$dir = dirpath($dir);
		
		if (array_search($dir, $this->dirs) === false)
		{
			if (! is_readable($dir)) throw new LangException("'{$dir}' is not readable");
			
	
			$this->dirs[] = $dir;
		}
	}
	
	/**
	*
	* @param $lang string lang code of the form xx or xx_XX to set language or NULL to clear language
	*/
	/*
	public function useLanguage($lang = NULL)
	{
		if (is_null($lang))
		{
			// Clear lang
			
			$this->messages = NULL;
		}
		else
		{
			// Set lang
			
			
			// Check for lang dir
			if (is_null($this->installDir)) throw new LangException("Lang install dir not given or unreadable");
			

			$parts = explode('_', $lang, 2);
			$attached = false;
			
			if (isset($parts[1]))
			{
				// Try full code
				
				$parts[1] = strtolower($parts[1]);
				
				$file_path = $this->installDir . $parts[0] . '_' . $parts[1] . '.php';
				$attached = $this->useFile($file_path);
			}
			
			if (! $attached)
			{
				// Try short code
				
				$file_path = $this->installDir . $parts[0] . '.php';
				$attached = $this->useFile($file_path);
			}
			
			
			if (! $attached)
			{
				throw new LangException("Language '{$lang}' not found");
			}
		}
	}
	*/
	public function useLanguage($lang = NULL)
	{
		if (is_null($lang))
		{
			// Clear lang
			
			$this->messages = array();
		}
		else
		{
			// Set lang
			
			
			// Check for lang dir
			if (empty($this->dirs)) throw new LangException("No lang dirs are set");
			

			foreach ($this->dirs as $dir)
			{
				$parts = explode('_', $lang, 2);
				$attached = false;
				
				if (isset($parts[1]))
				{
					// Try full code
					
					$parts[1] = strtolower($parts[1]);
					
					$file_path = $dir . $parts[0] . '_' . $parts[1] . '.php';
					$attached = $this->useFile($file_path);
				}
				
				if (! $attached)
				{
					// Try short code
					
					$file_path = $dir . $parts[0] . '.php';
					$attached = $this->useFile($file_path);
				}
				
				
				if (! $attached)
				{
					throw new LangException("Language '{$lang}' not found at '{$dir}'");
					// trigger_error('test', E_USER_WARNING);
				}
			}
		}
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	private function useFile($file_path)
	{
		$r = false;
		
		
		if (is_readable($file_path))
		{
			include($file_path);

			$this->messages = array_merge($this->messages, $messages);
			
			$r = true;
		}
		
		
		return $r;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function t($text, &$found = NULL)
	{
		$found = false;
		
		if (! empty($this->messages))
		{
			if (isset($this->messages[$text]))
			{
				$text = $this->messages[$text];
				$found = true;
			}
		}
		
		return $text;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function export()
	{
		return $this->messages;
	}
}


function t($text, Lang $lang = NULL)
{
	if (! is_null($lang))
	{
		return $lang->t($text);
	}
	else
	{
		global $RUNTIME;
		
		if (! isset($RUNTIME['lang'])) throw new LangException("No any Lang object created to use with t() without \$lang");
			

		return $RUNTIME['lang']->t($text);
	}
}
?>