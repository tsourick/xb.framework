<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage orm
*/

require_once('class.ORMIndexField.php');


class ORMIndexException extends ORMException
{
}

class ORMIndex
{
	private $ormTable; // Parent table reference
	
	private $name;
	
	private $type; // Name of type
	private $ormIndexFields; // Array of table fields


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(ORMTable $ormTable, $name, $type, $ormIndexFields = array())
	{
		$this->ormTable = $ormTable;
		
		$this->name = $name;
		
		$this->type = $type;
		$this->ormIndexFields = $ormIndexFields;
		
		
		$ormTable->addIndex($this);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getName()
	{
		return $this->name;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function table()
	{
		return $this->ormTable;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getType()
	{
		return $this->type;
	}
	
	
/**
	Config structure
	
	array
	(
		'name' => '(PRIMARY|<index name>)',
		'type' => '(PRIMARY|UNIQUE|INDEX)',
		'fields' => array
		(
			array('name' => '<field name>', 'length' => (<index length>|NULL))
		)
	)
*/
	static public function createFromConfig(ORMTable $ormTable, $config)
	{
		$ormIndex = new self($ormTable, $config['name'], $config['type']);
		
		foreach ($config['fields'] as $field_config)
		{
			$ormIndexField = $ormIndex->createIndexFieldFromConfig($field_config);
			
			$ormIndex->addField($ormIndexField);
		}

		return $ormIndex;
	}

	/**
	* Config structure:
	* @see ORMIndexField->createFromConfig()
	*/
	public function createIndexFieldFromConfig($config)
	{
		$ormField = $this->ormTable->field($config['name']);
		
		$ormIndexField = ORMIndexField::createFromConfig($ormField, $config);
		
		return $ormIndexField;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getConfig()
	{
		$config = array
		(
			'name' => $this->name,
			'type' => $this->type,
			'fields' => array()
		);
		
		foreach ($this->ormIndexFields as $field)
		{
			$config['fields'][] = $field->getConfig();
		}
		
		
		return $config;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addField(ORMIndexField $ormIndexField)
	{
		$this->ormIndexFields[$ormIndexField->getName()] = $ormIndexField;
	}
	
	/*
	public function _createDDLSource()
	{
		$ddl = '';
		
		return $ddl;
	}
	*/
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function fields()
	{
		return $this->ormIndexFields;
	}
	
	/*
	public function createDDLSource()
	{
		$ddl = '';
		
		
		if ($this->type == 'PRIMARY')
		{
			$ddl = 'ALTER TABLE ' . SQL::prepare_name($this->ormTable->getName()) . ' ADD PRIMARY KEY ';
			
			$indexFieldsDDL = array();
			foreach ($this->ormIndexFields as $ormIndexField)
			{
				$indexFieldsDDL[] = $ormIndexField->_createDDLSource();
			}
			
			$ddl .= '(' . implode(', ', $indexFieldsDDL) . ');';
		}
		else
		{
			$index_type = ($this->type != 'INDEX' ? $this->type . ' ' : '');
			$ddl = 'CREATE ' . $index_type . 'INDEX ' . SQL::prepare_name($this->name) . ' ON ' . SQL::prepare_name($this->ormTable->getName()) . ' ';

			$indexFieldsDDL = array();
			foreach ($this->ormIndexFields as $ormIndexField)
			{
				$indexFieldsDDL[] = $ormIndexField->_createDDLSource();
			}

			$ddl .= '(' . implode(', ', $indexFieldsDDL) . ');';
    }    

		
		return $ddl;
	}
	*/
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function SQLDDLGetCreateQuerySet()
	{
		$qset = array();
		
		
		if ($this->type == 'PRIMARY')
		{
			$q = 'ALTER TABLE ' . SQL::prepare_name($this->ormTable->getName()) . ' ADD PRIMARY KEY ';
			
			$indexFieldsDDL = array();
			foreach ($this->ormIndexFields as $ormIndexField)
			{
				$indexFieldsDDL[] = $ormIndexField->_SQLDDLGetDefinition();
			}
			
			$q .= '(' . implode(', ', $indexFieldsDDL) . ')';
		}
		else
		{
			$index_type = ($this->type != 'INDEX' ? $this->type . ' ' : '');
			$q = 'CREATE ' . $index_type . 'INDEX ' . SQL::prepare_name($this->name) . ' ON ' . SQL::prepare_name($this->ormTable->getName()) . ' ';

			$indexFieldsDDL = array();
			foreach ($this->ormIndexFields as $ormIndexField)
			{
				$indexFieldsDDL[] = $ormIndexField->_SQLDDLGetDefinition();
			}

			$q .= '(' . implode(', ', $indexFieldsDDL) . ')';
    }    


    $qset[] = $q;
		
    
		return $qset;
	}
}

?>