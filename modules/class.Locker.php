<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2014
*
* @package modules
*/

/**
* Locker class
*
* Implements simple locking (file-based)
*
*/

class Locker
{
	private $dirPath;
	private $defaultTimeout;
	
	
	public function __construct($dir_path, $default_timeout = 30)
	{
		// $install_dir = Framework::get('locker/install_dir');
		
		$this->dirPath = dirpath($dir_path);
		$this->defaultTimeout = is_null($default_timeout) ? intval(ini_get('max_execution_time')) : $default_timeout;
	}
	
	private function makeLockFileName($name)
	{
		return md5($name) . '.lock';
	}
	
	private function makeLockFilePath($name)
	{
		$file_name = $this->makeLockFileName($name);
		return $this->dirPath . $file_name;
	}
	
	
	public function lock($name, $timeout = NULL)
	{
		$r = false;
		//dump("call lock($name, $timeout)");
		 
		if (empty($name)) throw new Exception("lock name empty");
		
		
		if (! $this->locked($name))
		{
			$timeout = is_null($timeout) ? $this->defaultTimeout : $timeout;
			
			// dump("locking for max $timeout seconds...");
			
			$till = microtime(true) + $timeout;
			
			$data = compact('till', 'name');
			$data = serialize($data);
			$r = file_put_contents($this->makeLockFilePath($name), $data);
			
			dump("'$name' locked for max $timeout seconds...");
		}
		
		
		return $r;
	}
	
	public function unlock($name)
	{
		//dump("call unlock($name)");
		
		if (empty($name)) throw new Exception("lock name empty");
		
		$r = unlink($this->makeLockFilePath($name));
		
		if ($r)
		{
			dump("'$name' unlocked.");
		}
		
		return $r;
	}
	
	public function locked($name)
	{
		//dump("call locked($name)");
		$locked = false;
		
		
		if (empty($name)) throw new Exception("lock name empty");
		
		
		$now = microtime(true);
		
		$file_path = $this->makeLockFilePath($name);
		//dump("$file_path");
		if (file_exists($file_path))
		{
			//dump("exists");

			$data = file_get_contents($file_path);
			
			$data = unserialize($data);
			
			
			$diff = $data['till'] - $now;
			// wechos('diff: ' . $diff);
			
			$locked = ($diff > 0);

			// $locked = true;
		}
		
		return $locked;
	}
}

?>
