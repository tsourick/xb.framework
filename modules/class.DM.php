<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

// require_once('class.ORM.php');
Framework::useClass('ORM');

class DMException extends Exception
{
}

// require_once('class.DMDataModel.php');
Framework::useClass('DMDataModel');

?>