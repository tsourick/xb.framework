<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

require_once('class.DMLink.php');


class DMLinkEndPoint
{
	private $dmLink = NULL;
	
	private $isFrom = false;
	private $isTo = false;

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DMLink $dmLink, $isFrom, $isTo)
	{
		$this->dmLink = $dmLink;
		
		$this->isFrom = $isFrom;
		$this->isTo = $isTo;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function link()
	{
		return $this->dmLink;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isFrom()
	{
		return $this->isFrom;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function isTo()
	{
		return $this->isTo;
	}
}

?>
