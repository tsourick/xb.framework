<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

class DMManyToManyLinkBase extends DMLinkThroughTable
{
	// private $ormFromFields = array();
	// private $ormToFields = array();

	// private $fromFieldPairs = array(); // (0: (<from DMField>), 1: (<to link ORMField>)), ...
	// private $toFieldPairs = array(); // (0: (<from DMField>), 1: (<to link ORMField>)), ...
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DMDataModel $dm, $name, DMEntity $from_entity, $from_required, DMEntity $to_entity, $to_required, $custom_field_map = NULL)
	// public function __construct(DMDataModel $dm, $config)
	{
		// parent::__construct($dm, $config);
		parent::__construct($dm, $name, $from_entity, $from_required, $to_entity, $to_required, $custom_field_map);
		
		
		$this->type = 'n:n';
		
		
		// $from_entity = $dm->entity($config['from']);
		// $to_entity = $dm->entity($config['to']);


		// Create link table
		
		$this->createLinkTable($from_entity, $to_entity);
		
		
		// Add index to link table
		
		$index_config = array();
		
		$index_config['name'] = 'PRIMARY';
		$index_config['type'] = 'PRIMARY';
		
		foreach ($this->ormTable->fields() as $field)
		{
			$index_config['fields'][]['name'] = $field->getName();
		}
		
		$this->ormTable->createIndexFromConfig($index_config);
		
		
		// Register link in entities through endpoints

		$from_entity->addLinkEndPoint(new DMLinkEndPoint($this, true, false));
		$to_entity->addLinkEndPoint(new DMLinkEndPoint($this, false, true));
		
		
		// Finalize construction
		
		$this->fromEntity = $from_entity;
		$this->fromRequired = $from_required;
		
		$this->toEntity = $to_entity;
		$this->toRequired = $to_required;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function checkAndAdjustForDelete($actOn, $condition = NULL)
	{
		// wechos('checkAndAdjustForDelete: ');
		// wdebug($condition);
		if ($actOn == 'from')
		{
			$local_entity = $this->fromEntity;
			$foreign_entity = $this->toEntity;
			$foreign_entity_required = $this->toRequired;
			
			$local_entity_name = $this->fromEntity->getName();
			$foreign_entity_name = $this->toEntity->getName();
			
			$local_field_pairs =& $this->fromFieldPairs;
			$foreign_field_pairs =& $this->toFieldPairs;
		}
		else
		{
			$local_entity = $this->toEntity;
			$foreign_entity = $this->fromEntity;
			$foreign_entity_required = $this->fromRequired;

			$local_entity_name = $this->toEntity->getName();
			$foreign_entity_name = $this->fromEntity->getName();

			$local_field_pairs =& $this->toFieldPairs;
			$foreign_field_pairs =& $this->fromFieldPairs;
		}
		
		
		// Get local entity items' (deleted items') pk values
		
		$local_entity_pk_names = array();
		
		foreach ($local_field_pairs as $field_pair)
		{
			$local_entity_pk_names[] = $field_pair[0]->getName(); // dmField
		}
		
		$local_entity_pk_values = $local_entity->getItemDataList($local_entity_pk_names, $condition);
		
		
		if (! empty($local_entity_pk_values)) // there is something to delete
		{
			// Create link conditions
			
			$link_condition = array();
			$link_exclude_condition = array();
			
			foreach ($local_entity_pk_values as $pk_data)
			{
				$and_condition = array();
				foreach ($local_field_pairs as $field_pair)
				{
					$entity_field = $field_pair[0];
					$link_field = $field_pair[1];
					
					$and_condition[] = $link_field->getPreparedName() . ' = ' . SQL::prepare_value($pk_data[$entity_field->getName()]);
				}
				
				assert(! empty($and_condition)); // prevent delete all the table data
				
				$link_condition[] = implode(' AND ', $and_condition);
			}
			
			assert(! empty($link_condition)); // prevent delete all the table data
			
			$link_exclude_condition = 'NOT (' . implode(') AND NOT (', $link_condition) . ')';
			$link_condition = '(' . implode(') OR (', $link_condition) . ')';


			// Check local item count bound to deleted foreign items upon requried
			if ($foreign_entity_required)
			{
				// wechos('foreign entity required');
				// Get foreign link field values (foreign pk values)
				
				$foreign_link_field_values = array();
				
				$foreign_link_field_names = array();
				foreach ($foreign_field_pairs as $field_pair)
				{
					$foreign_link_field_names[] = $field_pair[1]->getName();
				}
				
				$foreign_link_field_values = $this->ormTable->getRowDataList($foreign_link_field_names, $link_condition);
				// wechos('foreign_link_field_values: ');
				// wdebug($foreign_link_field_values);
				if (! empty($foreign_link_field_values)) // there is something bound to deleted items
				{
					$foreign_link_field_values = array_unique_multi($foreign_link_field_values); // remove redundant values
					// wechos('foreign_link_field_values after remove redundant: ');
					// wdebug($foreign_link_field_values);
					// For every foreign item get count of local items which those would have bound to after delete
					// and schedule those foreign items for delete
					
					$foreign_entity_delete_condition = array();
				
					foreach ($foreign_link_field_values as $link_data)
					{
						$extra_bound_link_condition = array();
						$foreign_entity_pk_values_condition = array();
						
						foreach ($foreign_field_pairs as $field_pair)
						{
							$entity_field = $field_pair[0];
							$link_field = $field_pair[1];
							
							$extra_bound_link_condition[] = $link_field->getPreparedName() . ' = ' . SQL::prepare_value($link_data[$link_field->getName()]);
							$foreign_entity_pk_values_condition[] = $entity_field->ORMField()->getPreparedName() . ' = ' . SQL::prepare_value($link_data[$link_field->getName()]);
						}
						// wechos('creating conditions for row: ');
						// wdebug($link_data);
						// wechos('extra_bound_link_condition[] : ');
						// wdebug($extra_bound_link_condition);
						// wechos('foreign_entity_pk_values_condition[] : ');
						// wdebug($foreign_entity_pk_values_condition);

						$extra_bound_link_condition = $link_exclude_condition . ' AND ' . implode(' AND ', $extra_bound_link_condition);
						// wechos('extra_bound_link_condition : ');
						// wdebug($extra_bound_link_condition);
						
						
						// Check count of extra items
						
						$extra_bound_count = $this->ormTable->getRowCount($extra_bound_link_condition);
						// wechos('extra_bound_count: ' . $extra_bound_count);
						if ($extra_bound_count == 0)
						{
							$foreign_entity_delete_condition[] = implode(' AND ', $foreign_entity_pk_values_condition);
						}
					}
					
					
					// Delete orphan items if any
					if (! empty($foreign_entity_delete_condition))
					{
						$foreign_entity_delete_condition = '(' . implode(') OR (', $foreign_entity_delete_condition) . ')';
						// wechos('foreign_entity_delete_condition: ');
						// wdebug($foreign_entity_delete_condition);
						$foreign_entity->_deleteItemData($foreign_entity_delete_condition);
					}
				}
			}
	
			
			// Delete links
			
			$this->ormTable->delete($link_condition);
		}
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function checkAndAdjustForInsert($actOn, $data)
	{
		if ($actOn == 'from')
		{
			$local_entity = $this->fromEntity;
			$foreign_entity = $this->toEntity;
			$local_entity_required = $this->fromRequired;
			
			$local_entity_name = $this->fromEntity->getName();
			$foreign_entity_name = $this->toEntity->getName();
			
			$local_field_pairs =& $this->fromFieldPairs;
			$foreign_field_pairs =& $this->toFieldPairs;
		}
		else
		{
			$local_entity = $this->toEntity;
			$foreign_entity = $this->fromEntity;
			$local_entity_required = $this->toRequired;

			$local_entity_name = $this->toEntity->getName();
			$foreign_entity_name = $this->fromEntity->getName();

			$local_field_pairs =& $this->toFieldPairs;
			$foreign_field_pairs =& $this->fromFieldPairs;
		}
		
		
		// Check dependency if any
		
		$foreign_key_values = array();
			
		if (isset($data[$local_entity_name]['depends']) && isset($data[$local_entity_name]['depends'][$foreign_entity_name]))
		{
			// Check foreign row

			
			// Create condition
			foreach ($foreign_field_pairs as $field_pair)
			{
				$foreign_field_name = $field_pair[0]->getName();
				
				if (! isset($data[$local_entity_name]['depends'][$foreign_entity_name][$foreign_field_name]))
					throw new DMLinkException("'$foreign_entity_name' entity item referred to, but '$foreign_field_name' key field value not provided on insert into entity '$local_entity_name'.");
				
				$value = $data[$local_entity_name]['depends'][$foreign_entity_name][$foreign_field_name];
				
				$foreign_key_values[$foreign_field_name] = $value;
			}
			
			
			// Check row count
			
			$c = $foreign_entity->ORMTable()->getRowCount($foreign_key_values);

			// if ($c == 0) throw new DMLinkException("'$foreign_entity_name' entity item with key values (" . implode(', ', array_map(function($a1value, $a2value){return "$a1value: $a2value";}, array_keys($foreign_key_values), $foreign_key_values)) . ") and referred to as foreign was not found on insert into entity '$local_entity_name'.");
			if ($c == 0) throw new DMLinkException("'$foreign_entity_name' entity item with key values (" . implode_params($foreign_key_values, array(', ', ': ')) . ") and referred to as foreign was not found on insert into entity '$local_entity_name'.");
		}
			

		// Check upon required
		
		if ($local_entity_required)
		{
			if (empty($foreign_key_values))
			{
				// Double insert required (new foreign)
				
				if (! isset($data[$foreign_entity_name]))
					throw new DMLinkException("Double insert required on insert into entity '$local_entity_name', but no data entry provided for entity '$foreign_entity_name'.");
				
				if (! isset($data[$foreign_entity_name]['values']))
					throw new DMLinkException("Double insert required on insert into entity '$local_entity_name', but no values provided for entity '$foreign_entity_name'.");
				
				
				// Insert data for dependent foreign
				$foreign_key_values = $foreign_entity->_insertItemData($data);
			}
			else
			{
				// Use provided foreign key values; Nothing to do here
			}
		}

		
		// Add link if any
		
		if (! empty($foreign_key_values))
		{
			$link_values = array();
			
			foreach ($local_field_pairs as $field_pair)
			{
				$entity_field = $field_pair[0];
				$link_field = $field_pair[1];
				
				$link_values[$link_field->getName()] = $data[$local_entity_name]['values'][$entity_field->getName()];
			}
			
			foreach ($foreign_field_pairs as $field_pair)
			{
				$entity_field = $field_pair[0];
				$link_field = $field_pair[1];
				
				$link_values[$link_field->getName()] = $foreign_key_values[$entity_field->getName()];
			}


			$this->ormTable->ins($link_values);
		}
				
		
		return $data;
	}
	/*
	static public function createFromConfig(DMDataModel $dm, $config)
	{
		$custom_field_map = isset($config['custom_field_map']) ? $config['custom_field_map'] : NULL;
		
		$from_entity = $dm->entity($config['from']);
		$to_entity = $dm->entity($config['to']);
		
		
		$dmLink = new self($dm, $config['name'], $from_entity, $config['from_required'], $to_entity, $config['to_required'], $custom_field_map);
		
		
		return $dmLink;
	}
	*/
	
	
	/**
	* raw - if true, return whole result as is with joined fields. false - return only entity fields (not implemented)
	*
	* @param
	* @param
	*
	* @return
	*/

	private function _getLinkedItemDataList($direction, $raw, $all, $fields, $left_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		if (is_null($condition))
		{
			$condition = array();
		}


		// Convert condition to array
		
		if (is_string($condition))
		{
			$condition = array($condition);
		}
		else
		{
			// assume array
		}


		if ($direction == 'right') // exec as the original function was written
		{
			$dmEntity = $this->toEntity;

			$fromFieldPairs = $this->fromFieldPairs();
			$toFieldPairs = $this->toFieldPairs();
		}
		else // inverse
		{
			$dmEntity = $this->fromEntity;

			$fromFieldPairs = $this->toFieldPairs();
			$toFieldPairs = $this->fromFieldPairs();
		}


		$ormLinkTable = $this->ormTable;


		// Create ON condition
		
		$_on = array();
		
		foreach ($toFieldPairs as $i => $field_pair)
		{
			$dmEntityField = $field_pair[0];
			$ormLinkField = $field_pair[1];
		
			$_on[] = 'lnk.' . $ormLinkField->getPreparedName() . ' = e.' . $dmEntityField->ORMField()->getPreparedName();
		}
		

		// Create condition
		
		foreach ($fromFieldPairs as $i => $field_pair)
		{
			$dmEntityField = $field_pair[0];
			$ormLinkField = $field_pair[1];

			$value = $left_pk_values[$dmEntityField->getName()];
			
			$condition['lnk.' . $ormLinkField->getName()] = $value;
		}
		
		if ($all)
		{
			$_select = 'e.*';
		}
		else
		{
			if ($raw) // select as is; let caller choose any field
			{
				$_select = $fields;
			}
			else // select only entity fields; hide other fields from caller
			{
				// need to add aliases to fields
				not_implemented();
			}
		}
		
		$_from = $ormLinkTable->getPreparedName() . ' AS lnk INNER JOIN ' . $dmEntity->ORMTable()->getPreparedName() . ' AS e ON ' . implode(' AND ', $_on);


		return $this->dbc->getRowsCustom($_select, $_from, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRightItemDataListAll($left_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->_getLinkedItemDataList('right', false, true, NULL, $left_pk_values, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
	
	public function getLeftItemDataListAll($right_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->_getLinkedItemDataList('left', false, true, NULL, $right_pk_values, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	* raw - return whole result as is with joined fields
	*/
	public function getLeftItemDataListRaw($fields, $right_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->_getLinkedItemDataList('left', true, false, $fields, $right_pk_values, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
	
	public function getRightItemDataListRaw($fields, $right_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->_getLinkedItemDataList('right', true, false, $fields, $right_pk_values, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
	
	
	private function _getUnlinkedItemDataListRaw($direction, $fields, $right_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		if (is_null($condition))
		{
			$condition = array();
		}
		
		
		// Convert condition to array
		
		if (is_string($condition))
		{
			$condition = array($condition);
		}
		else
		{
			// assume array
		}


		if ($direction == 'right') // inverse
		{
			$dmEntity = $this->toEntity;
			
			$fromFieldPairs = $this->toFieldPairs();
			$toFieldPairs = $this->fromFieldPairs();
		}
		else // exec as the original function was written
		{
			$dmEntity = $this->fromEntity;
			
			$fromFieldPairs = $this->fromFieldPairs();
			$toFieldPairs = $this->toFieldPairs();
		}
		

		$ormLinkTable = $this->ormTable;

		
		// Create subquery link condition
		
		$_sub_on = array();

		foreach ($toFieldPairs as $i => $field_pair)
		{
			$dmEntityField = $field_pair[0];
			$ormLinkField = $field_pair[1];
			
			$_link_right_field_name = $ormLinkField->getPreparedName();
			
			$right_pk_value = $right_pk_values[$dmEntityField->getName()];
			$_right_pk_value = SQL::prepare_value($right_pk_value);

			
			$_sub_on[] = "{$_link_right_field_name} = {$_right_pk_value}";
		}
		
		
		// Create query link condition and condition
		
		$_on = array();
		
		foreach ($fromFieldPairs as $i => $field_pair)
		{
			$dmEntityField = $field_pair[0];
			$ormLinkField = $field_pair[1];
			
			$_link_left_field_name = $ormLinkField->getPreparedName();
			$_left_entity_field_name = $dmEntityField->ORMField()->getPreparedName();

			
			$_on[] = "lnk.{$_link_left_field_name} = e.{$_left_entity_field_name}";
			
			
			$condition[] = "lnk.{$_link_left_field_name} IS NULL";
		}
		
		
		$_entity_table_name = $dmEntity->ORMTable()->getPreparedName();
		$_link_table_name = $ormLinkTable->getPreparedName();
		
		$_from =
		$_entity_table_name . ' e LEFT JOIN
		(
			SELECT * FROM ' . $_link_table_name . ' WHERE ' . implode(' AND ', $_sub_on) . '
		) lnk ON ' . implode(' AND ', $_on);
		
		
		return $this->dbc->getRowsCustom($fields, $_from, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
	
	/**
	* raw - return whole result as is with joined fields.
	*/
	public function getLeftUnlinkedItemDataListRaw($fields, $right_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->_getUnlinkedItemDataListRaw('left', $fields, $right_pk_values, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}

	/**
	* raw - return whole result as is with joined fields.
	*/
	public function getRightUnlinkedItemDataListRaw($fields, $left_pk_values, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->_getUnlinkedItemDataListRaw('right', $fields, $left_pk_values, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
}

?>
