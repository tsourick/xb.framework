<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage orm
*/

class ORMFieldTypeException extends ORMException
{
}

class ORMFieldType
{
	private $name;
	
	private $length; // number or NULL
	private $decimals; // number or NULL
	private $unsigned; // boolean or NULL
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $length = NULL, $decimals = NULL, $unsigned = NULL)
	{
		$this->name = $name;
		
		$this->length = $length;
		$this->decimals = $decimals;
		$this->unsigned = $unsigned;
	}
	
	
	/**
	*
		Config structure
		
		array
		(
			'name' => '(BIT |TINYINT |SMALLINT |MEDIUMINT |INT |INTEGER |BIGINT |REAL |DOUBLE |FLOAT |DECIMAL |NUMERIC| ...)', 					
			'length' => (<type length>|NULL), // if applicable
			'decimals' => (<decimal count>|NULL), // if applicable
			'unsigned' => (true|false|NULL), // if applicable
		)
	*/
	static public function createFromConfig($config)
	{
		$name = $config['name'];
		
		$length = isset($config['length']) ? $config['length'] : NULL;
		$decimals = isset($config['decimals']) ? $config['decimals'] : NULL;
		$unsigned = isset($config['unsigned']) ? $config['unsigned'] : NULL;
		
		$ormFieldType = new self($name, $length, $decimals, $unsigned);

		return $ormFieldType;
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getConfig()
	{
		return array
		(
			'name' => $this->name, 					
			'length' => $this->length,
			'decimals' => $this->decimals,
			'unsigned' => $this->unsigned
		);
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function _SQLDDLGetDefinition()
	{
		$ddl = $this->name;
		
		if (! is_null($this->decimals))
		{
			assert(! is_null($this->length));
			
			$ddl .= '(' . $this->length . ', ' . $this->decimals . ')';
		}
		elseif (! is_null($this->length))
		{
			$ddl .= '(' . $this->length . ')';
		}
		
		if (! is_null($this->unsigned))
		{
			if ($this->unsigned)
			{
				$ddl .= ' UNSIGNED';
			}
			else
			{
				$ddl .= ' SIGNED';
			}
		}
		
		return $ddl;
	}
}


?>
