<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

require_once('class.DMField.php');


class DMEntityException extends DMException
{
}


require_once('class.DMEntityBase.php');
if (PHP_VERSION >= '5.3')
{
	include('class.DMEntity.php_5_3.php');
}
else
{
	include('class.DMEntity.php_5_2.php');
}

?>
