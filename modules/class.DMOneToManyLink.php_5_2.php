<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

class DMOneToManyLink extends DMOneToManyLinkBase
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function createFromConfig(DMDataModel $dm, $config, $__class__= __CLASS__)
	{
		return parent::createFromConfig($dm, $config, $__class__);		
	}
}

?>
