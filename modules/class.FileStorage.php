<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
*/

/**
* undocumented class
*/

class FileException extends Exception
{
}

class FileHelper
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function getMimeType($path)
	{
		$mt = '';
		
		// try mime_content_type()
		if (function_exists('mime_content_type'))
		{
			$mt = mime_content_type($path);
		}
		
		// try getimagesize()
		if (empty($mt))
		{
			if ($info = @getimagesize($path))
			{
				if (! empty($info['mime'])) $mt = $info['mime'];
			}
		}
		
		// if (empty($mt)) $mt = 'application/octet-stream';
		
		return $mt;
	}
}

class ImageHelper
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function getDimensions($path)
	{
		list($width, $height) = @getimagesize($path);
		
		return array($width, $height);
	}
}


abstract class File
{
	protected $name;
	private $size;
	private $mimeType;

	protected $defaultMimeType = 'application/octet-stream';
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $size, $mimeType)
	{
		$this->name = $name;
		$this->size = $size;
		$this->mimeType = $mimeType;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getName()
	{
		return $this->name;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setSize($size)
	{
		$this->size = $size;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getSize()
	{
		return $this->size;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setMimeType($mime_type)
	{
		$this->mimeType = $mime_type;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getMimeType()
	{
		return $this->mimeType;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setDefaultMimeType($mime_type)
	{
		$this->defaultMimeType = $mime_type;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getDefaultMimeType()
	{
		return $this->defaultMimeType;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	abstract public function send();
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	abstract public function getContents();
}

class FileDBMeta extends File
{
	private $dbConnection = NULL;
	private $tableName;
	private $storageDir;
	
	private $id;
	private $physicalName;
	
	private $contents = null;
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DBConnection $db_connection, $table_name, $storage_dir, $id)
	{
		$metaInfo = $db_connection->getRowAll($table_name, compact('id'));

		if (empty($metaInfo)) throw new FileException("File record id:{$id} not found in '{$table_name}'");
		
		parent::__construct($metaInfo['name'], $metaInfo['size'], $metaInfo['mime_type']);
		
		$this->id = $id;
		$this->physicalName = $metaInfo['physical_name'];

		$this->dbConnection = $db_connection;
		$this->tableName = $table_name;
		$this->storageDir = $storage_dir;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getId()
	{
		return $this->id;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPhysicalName()
	{
		return $this->physicalName;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function save()
	{
		$id = $this->getId();
		$name = $this->getName();
		$size = $this->getSize();
		$mime_type = $this->getMimeType();

		
		$row = $this->dbConnection->getRowAll($this->tableName, compact('id'));
		
		if (empty($row)) throw new FileException("File record [{$id}] not found in '{$this->tableName}'");
		
		// update

		file_put_contents($this->storageDir . $row['physical_name'], $this->getContents());
			
		$this->dbConnection->update($this->tableName, compact('name', 'size', 'mime_type'), compact('id'));

		return $id;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setContents($contents)
	{
		$this->setSize(strlen($contents));
		
		$this->contents = $contents;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getContents()
	{
		if (is_null($this->contents))
		{
			$this->contents = file_get_contents($this->storageDir . $this->physicalName);
		}
		
		return $this->contents;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function send($disposition = 'inline', $cache = false)
	{
		// $content = file_get_contents($this->storageDir . $this->physicalName);
		$content = $this->getContents();

		if (! $cache) xbf_http_nocache();

		xbf_http_send_file($this->getName(), $this->getMimeType(), $content, $this->getSize(), $disposition);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function updateFromFile($path, $name = '')
	{
		if (! is_readable($path)) throw new FileException("'{path}' is not readable");
		
		$id = $this->getId();

		if (empty($name)) $name = basename($path);
		$size = filesize($path);
		$mime_type = FileHelper::getMimeType($path);
		if (empty($mime_type)) $mime_type = $this->defaultMimeType;


		// update

		$physical_name = $this->getPhysicalName();
		
		$target_path = $this->storageDir . $physical_name;

		if (! @copy($path, $target_path)) throw new FileException("File copy failed from {$path} to {$target_path}");


		$r = $this->dbConnection->update($this->tableName, compact('name', 'size', 'mime_type'), compact('id'));

		// if (! $r) throw new FileException("File record ('{$target_path}') update failed for record [$id] in '{$this->tableName}'");
		

		return $id;
	}
}


abstract class FileDBStorage
{
	protected $dbConnection = NULL;
	protected $tableName;

	protected $defaultMimeType = 'application/octet-stream';
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DBConnection $db_connection, $table_name)
	{
		$this->dbConnection = $db_connection;
		$this->tableName = $table_name;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setDefaultMimeType($mime_type)
	{
		$this->defaultMimeType = $mime_type;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getDefaultMimeType()
	{
		return $this->defaultMimeType;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/
	
	abstract public function getFile($key);

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/
	
	abstract public function saveFile($file);
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/
	
	abstract public function createFileFromPath($path);
}

class FileDBMetaStorage extends FileDBStorage
{
	protected $storageDir;
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DBConnection $db_connection, $table_name, $storage_dir)
	{
		parent::__construct($db_connection, $table_name);
		
		if (! is_writable($storage_dir)) throw new FileException("'{$storage_dir}' is not writable");

		$this->storageDir = $storage_dir;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getFile($id)
	{
		return new FileDBMeta($this->dbConnection, $this->tableName, $this->storageDir, $id);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function saveFile(File $file)
	{
		$name = $file->getName();
		$size = $file->getSize();
		$mime_type = $file->getMimeType();

		$row = $this->dbConnection->getRowAll($this->tableName, compact('name'));
		
		if (! empty($row))
		{
			// update

			file_put_contents($this->storageDir . $row['physical_name'], $file->getContents());
			
			$this->dbConnection->update($this->tableName, compact('name', 'size', 'mime_type'), array('id' => $row['id']));

			$id = $row['id'];
		}
		else
		{
			$physical_name = str_rand(32);

			file_put_contents($this->storageDir . $physical_name, $file->getContents());
			
			$this->dbConnection->ins($this->tableName, compact('name', 'size', 'mime_type', 'physical_name'));

			$id = $this->dbConnection->lastInsertId();
		}
		
		return new FileDBMeta($this->dbConnection, $this->tableName, $this->storageDir, $id);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function createFileFromPath($path, $name = '')
	{
		if (! is_readable($path)) throw new FileException("'{$path}' is not readable");
		
		if (empty($name)) $name = basename($path);
		$size = filesize($path);
		$mime_type = FileHelper::getMimeType($path);
		if (empty($mime_type)) $mime_type = $this->defaultMimeType;
		$physical_name = str_rand(32);
		
		$target_path = $this->storageDir . $physical_name;

		if (! @copy($path, $target_path)) throw new FileException("File copy failed from {$path} to {$target_path}");
		
		
		$this->dbConnection->ins($this->tableName, compact('name', 'size', 'mime_type', 'physical_name'));
		
		$id = $this->dbConnection->lastInsertId();
		
		return new FileDBMeta($this->dbConnection, $this->tableName, $this->storageDir, $id);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function deleteFilesById($ids)
	{
		if (! is_array($ids)) $ids = array($ids);
		
		
		$physical_names = $this->dbConnection->getRows('physical_name', $this->tableName, SQL::in('id', $ids), '', '', NULL, '', 'physical_name');
		
		
		$this->dbConnection->delete($this->tableName, NULL, SQL::in('id', $ids));

		
		foreach ($physical_names as $n)
		{
			$file_path = $this->storageDir . $n;
	
			if (! @unlink($file_path)) throw new FileException("Could not delete file at '$file_path'");
		}
	}
}


abstract class Image extends File
{
	private $width;
	private $height;
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($name, $size, $mimeType, $width, $height)
	{
		parent::__construct($name, $size, $mimeType);
		
		$this->width = $width;
		$this->height = $height;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setWidth($width)
	{
		$this->width = $width;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getWidth()
	{
		return $this->width;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setHeight($height)
	{
		$this->height = $height;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getHeight()
	{
		return $this->height;
	}
}

class ImageDBMeta extends Image
{
	private $dbConnection = NULL;
	private $tableName;
	private $storageDir;
	
	private $id;
	private $physicalName;
	
	private $contents = null;

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DBConnection $db_connection, $table_name, $storage_dir, $id)
	{
		$metaInfo = $db_connection->getRowAll($table_name, compact('id'));

		if (empty($metaInfo)) throw new FileException("File meta info not found");
		
		
		parent::__construct($metaInfo['name'], $metaInfo['size'], $metaInfo['mime_type'], $metaInfo['width'], $metaInfo['height']);
		
		$this->id = $id;
		$this->physicalName = $metaInfo['physical_name'];

		$this->dbConnection = $db_connection;
		$this->tableName = $table_name;
		$this->storageDir = $storage_dir;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getId()
	{
		return $this->id;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPhysicalName()
	{
		return $this->physicalName;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function save()
	{
		$id = $this->getId();
		$name = $this->getName();
		$size = $this->getSize();
		$mime_type = $this->getMimeType();
		$width = $this->getWidth();
		$height = $this->getHeight();

		$row = $this->dbConnection->getRowAll($this->tableName, compact('id'));
		
		if (empty($row)) throw new FileException("File record [{$id}] not found in '{$this->tableName}'");
		
		// update

		file_put_contents($this->storageDir . $row['physical_name'], $this->getContents());
			
		$this->dbConnection->update($this->tableName, compact('name', 'size', 'mime_type', 'width', 'height'), compact('id'));

		return $id;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setContents($contents)
	{
		$this->setSize(strlen($contents));
		
		$this->contents = $contents;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getContents()
	{
		if (is_null($this->contents))
		{
			$this->contents = file_get_contents($this->storageDir . $this->physicalName);
		}
		
		return $this->contents;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function send($disposition = 'inline', $cache = false)
	{
		// $content = file_get_contents($this->storageDir . $this->physicalName);
		$content = $this->getContents();

		if (! $cache) xbf_http_nocache();

		xbf_http_send_file($this->getName(), $this->getMimeType(), $content, $this->getSize(), $disposition);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function updateFromFile($path, $name = '')
	{
		if (! is_readable($path)) throw new FileException("'{path}' is not readable");
		
		$id = $this->getId();

		if (empty($name)) $name = basename($path);
		$size = filesize($path);
		$mime_type = FileHelper::getMimeType($path);
		if (empty($mime_type)) $mime_type = $this->defaultMimeType;
		list($width, $height) = ImageHelper::getDimensions($path);


		// update

		$physical_name = $this->getPhysicalName();
		
		$target_path = $this->storageDir . $physical_name;

		if (! @copy($path, $target_path)) throw new FileException("File copy failed from {$path} to {$target_path}");


		$r = $this->dbConnection->update($this->tableName, compact('name', 'size', 'mime_type', 'width', 'height'), compact('id'));

		// if (! $r) throw new FileException("File record ('{$target_path}') update failed for record [$id] in '{$this->tableName}'");
		

		return $id;
	}
}

abstract class ImageDBStorage extends FileDBStorage
{
}

class ImageDBMetaStorage extends ImageDBStorage
{
	protected $storageDir;


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DBConnection $db_connection, $table_name, $storage_dir)
	{
		parent::__construct($db_connection, $table_name);
		
		if (! is_writable($storage_dir)) throw new FileException("'{$storage_dir}' is not writable");

		$this->storageDir = $storage_dir;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getFile($id)
	{
		return new ImageDBMeta($this->dbConnection, $this->tableName, $this->storageDir, $id);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function saveFile(Image $file)
	{
		$name = $file->getName();
		$size = $file->getSize();
		$mime_type = $file->getMimeType();
		$width = $file->getWidth();
		$height = $file->getHeight();

		$row = $this->dbConnection->getRowAll($this->tableName, compact('name'));
		
		if (! empty($row))
		{
			// update

			file_put_contents($this->storageDir . $row['physical_name'], $file->getContents());
			
			$this->dbConnection->update($this->tableName, compact('name', 'size', 'mime_type', 'width', 'height'), array('id' => $row['id']));

			$id = $row['id'];
		}
		else
		{
			$physical_name = str_rand(32);

			file_put_contents($this->storageDir . $physical_name, $file->getContents());
			
			$this->dbConnection->ins($this->tableName, compact('name', 'size', 'mime_type', 'physical_name', 'width', 'height'));

			$id = $this->dbConnection->lastInsertId();
		}
		
		return new ImageDBMeta($this->dbConnection, $this->tableName, $this->storageDir, $id);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function createFileFromPath($path, $name = '')
	{
		if (! is_readable($path)) throw new FileException("'{$path}' is not readable");

		if (empty($name)) $name = basename($path);
		$size = filesize($path);
		$mime_type = FileHelper::getMimeType($path);
		if (empty($mime_type)) $mime_type = $this->defaultMimeType;
		list($width, $height) = ImageHelper::getDimensions($path);
		$physical_name = str_rand(32);
		
		$target_path = $this->storageDir . $physical_name;

		if (! @copy($path, $target_path)) throw new FileException("File copy failed from {$path} to {$target_path}");
		
		
		$this->dbConnection->ins($this->tableName, compact('name', 'size', 'mime_type', 'physical_name', 'width', 'height'));
		
		$id = $this->dbConnection->lastInsertId();
		
		return new ImageDBMeta($this->dbConnection, $this->tableName, $this->storageDir, $id);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function deleteFilesById($ids)
	{
		if (! is_array($ids)) $ids = array($ids);


		$physical_names = $this->dbConnection->getRows('physical_name', $this->tableName, SQL::in('id', $ids), '', '', NULL, '', 'physical_name');


		$this->dbConnection->delete($this->tableName, NULL, SQL::in('id', $ids));

		
		foreach ($physical_names as $n)
		{
			$file_path = $this->storageDir . $n;
	
			if (! @unlink($file_path)) throw new FileException("Could not delete file at '$file_path'");
		}
	}
}

?>
