<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2013
*
* @package modules
* @subpackage tools
*/


class TextTools
{
	private static $_1_2 = array
	(
		1 => 'одна',
		2 => 'две'	
	);

	private static $_1_19 = array
	(
		1 => 'один',
		2 => 'два',
		3 => 'три',
		4 => 'четыре',
		5 => 'пять',
		6 => 'шесть',
		7 => 'семь',
		8 => 'восемь',
		9 => 'девять',
		10 => 'десять',
		11 => 'одиннацать',
		12 => 'двенадцать',
		13 => 'тринадцать',
		14 => 'четырнадцать',
		15 => 'пятнадцать',
		16 => 'шестнадцать',
		17 => 'семнадцать',
		18 => 'восемнадцать',
		19 => 'девятнадцать'
	);
	
	private static $des = array
	(
		2 => 'двадцать',
		3 => 'тридцать',
		4 => 'сорок',
		5 => 'пятьдесят',
		6 => 'шестьдесят',
		7 => 'семьдесят',
		8 => 'восемьдесят',
		9 => 'девяносто'
	);
	
	private static $hang = array
	(
		1 => 'сто',
		2 => 'двести',
		3 => 'триста',
		4 => 'четыреста',
		5 => 'пятьсот',
		6 => 'шестьсот',
		7 => 'семьсот',
		8 => 'восемьсот',
		9 => 'девятьсот'
	);
		
	private static $namerub = array
	(
		1 => 'рубль',
		2 => 'рубля',
		3 => 'рублей'
	);

	private static $nametho = array
	(
		1 => 'тысяча',
		2 => 'тысячи',
		3 => 'тысяч'
	);
		
	private static $namemil = array
	(
		1 => 'миллион',
		2 => 'миллиона',
		3 => 'миллионов'
	);
		
	private static $namemrd = array
	(
		1 => 'миллиард',
		2 => 'миллиарда',
		3 => 'миллиардов'
	);     
		
	private static $kopeek = array
	(
		1 => 'копейка',
		2 => 'копейки',
		3 => 'копеек'
	);     

		
	static public function rubtostr($L)
	{
		$rubles = true;
		
		$_1_2 = self::$_1_2;
		$_1_19 = self::$_1_19;                                                                        
		$des = self::$des;
		$hang = self::$hang;
		$namerub = self::$namerub;
		$nametho = self::$nametho;                                                                               
		$namemil = self::$namemil;
		$namemrd = self::$namemrd;
		$kopeek = self::$kopeek;

		
		$s = array();
		$s1 = '';


		$kop = intval(bcsub($L, intval($L), 3) * 100);
		$L = intval($L);
		
		
		$divider = 1000000000;
		if ($L >= $divider)
		{
			//$many = 0;
			$s[] = self::semantic(intval($L / $divider), $declension, 3);
			$s[] = $namemrd[$declension];
			$L%= $divider;
		}
	
		$divider = 1000000;
		if ($L >= $divider)
		{
			// $many = 0;
			$s[] = self::semantic(intval($L / $divider), $declension, 2);
			$s[] = $namemil[$declension];
			$L %= $divider;
			
			if ($L == 0)
			{
				$s[] = ($rubles) ? 'рублей ' : '';
			}
		}

		$divider = 1000;
		if ($L >= $divider)
		{
			//$many = 0;
			$s[] = self::semantic(intval($L / $divider), $declension, 1);
			$s[] = $nametho[$declension];
			$L %= 1000;
			
			if ($L == 0)
			{
				$s[] = ($rubles) ? 'рублей ' : '';
			}
		}                                                      
	                                                         
		
		if ($L != 0)                                           
		{
			//$many = 0;
			$s[] = self::semantic($L, $declension, 0);
			$s[] = ($rubles) ? $namerub[$declension] : '';
		}

		
		if ($kop > 0)
		{
			//$many = 0;
			$s[] = self::semantic($kop, $declension, 1);
			$s[] = $kopeek[$declension];
		}
		else
		{
			$s[] = ($rubles) ? 'ноль копеек ' : '';
		}

		
		return implode(' ', $s);
	}                                                     
	
	static private function semantic($i, &$declension, $f)
	{
		// global $_1_2, $_1_19, $des, $hang, $namerub, $nametho, $namemil, $namemrd;
		$_1_2 = self::$_1_2;
		$_1_19 = self::$_1_19;
		$des = self::$des;
		$hang = self::$hang;
		$namerub = self::$namerub;
		$nametho = self::$nametho;
		$namemil = self::$namemil;
		$namemrd = self::$namemrd;
		$kopeek = self::$kopeek;

		
		$words = array();
		

		$fl = 0;

		if ($i >= 100)
		{
			$jkl = intval($i / 100);
			$words[] = $hang[$jkl];
			$i %= 100;
		}

		if ($i >= 20)
		{
			$jkl = intval($i / 10);
			$words[] = $des[$jkl];
			$i %= 10;
			$fl = 1;
		}
		

		switch($i)
		{
			case 1:
				$declension=1;
			break;
			
			case 2:
			case 3:
			case 4:
				$declension=2;
			break;
			
			default:
				$declension=3;
		}
		
		if ($i)
		{
			if ( $i < 3 && $f > 0 )
			{
				if ( $f >= 2 )
				{
					$words[] = $_1_19[$i];
				}
				else
				{
					$words[] = $_1_2[$i];
				}
			}
			else
			{
				$words[] = $_1_19[$i];
			}
		}
		
		
		return implode(' ', $words);
	}
	
	
  static private $translitCyr = array
  (
    'ж',  'ч',  'щ',   'ш',  'ю',  'я',  'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ы', 'ь', 'э',
    'Ж',  'Ч',  'Щ',   'Ш',  'Ю',  'Я',  'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ъ', 'Ы', 'Ь', 'Э'
   );
  static private $translitLatin = array
  (
    'zh', 'ch', 'shch', 'sh', 'yu', 'ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'kh', 'ts', '',  'i', '',  'e',
    'Zh', 'Ch', 'Shch', 'Sh', 'Yu', 'YA', 'A', 'B', 'V', 'G', 'D', 'E', 'E', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'Kh', 'Ts', '',  'I', '',  'E'
  );
				
	static public function translitCyrToLatin($s)
	{
		$s = str_replace(self::$translitCyr, self::$translitLatin, $s);                                   
		                                                        
		return $s;
	}
	
	static public function translitLatinToCyr($s)
	{
		$s = str_replace(self::$translitLatin, self::$translitCyr, $s);      
		
		return $s;
	}
}

?>
