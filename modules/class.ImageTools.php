<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
*/

/**
* undocumented class
*/

class ImageToolsException extends Exception
{
}

class ImageTools
{
	// Please, note that the quality only means it for jpeg, while for png it actually means compression 
	static public $quality = ['jpeg' => NULL, 'png' => NULL]; // PHP's defaults tend to be ['jpeg' => 75, 'png' => 6]
	static private $_prevQuality = NULL;
	
	/**
	* Set default compression quality 
	*
	* @param string|array graphics file format (i.e. jpeg, png, gif etc) or config array (type => value)
	* [ @param int $quality quality value for single type call format]  
	*/
	static public function setDefaultQuality($values, $quality = NULL)
	{
		if (! is_array($values))
		{
			if (is_null($quality)) throw new Exception("quality value not given for type '{$values}'");
			
			$values = [$values => $quality];
		}
		
		self::saveDefaultQuality();
		
		foreach ($values as $type => $quality)
		{
			self::$quality[$type] = $quality;
		}
	}
	
	static public function saveDefaultQuality()
	{
		self::$_prevQuality = self::$quality;
	}
	
	static public function restoreDefaultQuality()
	{
		if (! is_null(self::$_prevQuality)) self::$quality = self::$_prevQuality;
	}
	
	
	/**
	* backwards compat.
	*/
	static public function shrinkImageFile($path, $frame_width, $frame_height, $preserve_aspect_ratio = true)
	{
		return ImageTools::fitImageFile($path, $frame_width, $frame_height, $preserve_aspect_ratio);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function fitReduceOnlyImageFile($path, $frame_width, $frame_height, $preserve_aspect_ratio = true)
	{
		if (! $meta = @getimagesize($path)) throw new ImageToolsException("File not found at '{$path}'");
		
		
		list($width, $height) = $meta;

		$fit = ( (is_null($frame_width) || $width <= $frame_width) && (is_null($frame_height) || $height <= $frame_height) ) ? true : false;

		if (! $fit)
		{
			return ImageTools::fitImageFile($path, $frame_width, $frame_height, $preserve_aspect_ratio);
		}
		else
		{
			return $meta;
		}
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function fitImageFile($path, $frame_width, $frame_height, $preserve_aspect_ratio = true)
	{
		if (! $meta = @getimagesize($path)) throw new ImageToolsException("File not found at '{$path}'");
		
		
		list($width, $height) = $meta;
		
		if (! is_null($frame_width) && ! is_null($frame_height)) // Frame mode
		{
			if ($preserve_aspect_ratio)
			{
				$ratios = array($frame_width / $width, $frame_height / $height);
				
				$ratio = min($ratios);
				
				$new_width  = intval($width * $ratio);
				$new_height = intval($height * $ratio);
			}
			else
			{
				$new_width  = $frame_width;
				$new_height = $frame_height;
			}
		}
		elseif (! is_null($frame_width)) // Width mode
		{
			if ($preserve_aspect_ratio)
			{
				$ratio = $frame_width / $width;

				$new_width  = intval($width * $ratio);
				$new_height = intval($height * $ratio);
			}
			else
			{
				$new_width  = $frame_width;
				$new_height = $height;
			}
		}
		elseif (! is_null($frame_height)) // Height mode
		{
			if ($preserve_aspect_ratio)
			{
				$ratio = $frame_height / $height;
				
				$new_width  = intval($width * $ratio);
				$new_height = intval($height * $ratio);
			}
			else
			{
				$new_width  = $width;
				$new_height = $frame_height;
			}
		}
		else
		{
			throw new ImageToolsException("Frame width and height are NULL");
		}

		$has_changes = ($new_width != $width) || ($new_height != $height);
		return $has_changes ? ImageTools::resample($meta['mime'], $path, 0, 0, $width, $height, 0, 0, $new_width, $new_height) : $meta;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function fitCropImageFile($path, $frame_width, $frame_height)
	{
		if (! $meta = @getimagesize($path)) throw new ImageToolsException("File not found at '{$path}'");
		
		
		list($width, $height) = $meta;
		
		
		$width_ratio = $frame_width / $width;
		$height_ratio = $frame_height / $height;
		
		if ($height_ratio <= $width_ratio)
		{
			// vertical crop

			$ratio = $width_ratio; // get max ratio
			
			$cropped_width = $width; // original width
			$cropped_height = $frame_height / $ratio; // cropped height

			$src_x = 0;
			$src_y = intval(($height - $cropped_height) / 2); // center cropped block height vertically
		}
		elseif ($width_ratio < $height_ratio)
		{
			// horizontal crop
			
			$ratio = $height_ratio; // get max ratio
			
			$cropped_width = $frame_width / $ratio; // cropped width
			$cropped_height = $height; // original height

			$src_x = intval(($width - $cropped_width) / 2); // center cropped block width horizontally
			$src_y = 0;
		}
		
		$new_width = $frame_width;
		$new_height = $frame_height;
		

		return ImageTools::resample($meta['mime'], $path, $src_x, $src_y, $cropped_width, $cropped_height, 0, 0, $new_width, $new_height);
	}
	
	
	static private function resample($mime_type, $path, $src_x, $src_y, $src_width, $src_height, $dst_x, $dst_y, $dst_width, $dst_height)
	{
		$create_function_name = '';
		$save_alpha      = false;
		$transp_by_index = false;
		
		$use_quality = null;
		
		switch ($mime_type)
		{
			case 'image/png':
				$create_function_name = 'imagecreatefrompng';
				$save_function_name = 'imagepng';
					
				$save_alpha = true;

				$use_quality = self::$quality['png'];
			break;
			
			case 'image/gif':
				$create_function_name = 'imagecreatefromgif';
				$save_function_name = 'imagegif';
				
				$transp_by_index = true;
			break;

			case 'image/jpeg':
			case 'image/jpg':
				$create_function_name = 'imagecreatefromjpeg';
				$save_function_name = 'imagejpeg';
				
				$use_quality = self::$quality['jpeg'];
			break;

			default:
				throw new ImageToolsException('Unsupported mime type. Resize could not be done.');
		}

		
		$src_image = $create_function_name($path);

		
		$dst_image = imagecreatetruecolor($dst_width, $dst_height);


		if ($save_alpha) // transparency for PNG
		{
			imagealphablending($dst_image, false);      
			imagesavealpha($dst_image, true);
		}
		
		
		if ($transp_by_index) // transparency for GIF (non-animated)
		{
			$src_tindex = imagecolortransparent($src_image);
			if ($src_tindex !== -1) // has trans. color
			{
				$tcolor = imagecolorsforindex($src_image, $src_tindex); // get trans. color from source
				
				$dst_tindex = imagecolorallocate($dst_image, $tcolor['red'], $tcolor['green'], $tcolor['blue']); // add trans. color to dest
				imagecolortransparent($dst_image, $dst_tindex); // set as 'transparent' by index
				
				imagefill($dst_image, 0, 0, $dst_tindex); // fill background with trans. color just in case
			}
		}
		

		imagecopyresampled($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_width, $dst_height, $src_width, $src_height);
		
		
		if ($use_quality) $save_function_name($dst_image, $path, $use_quality);
		else $save_function_name($dst_image, $path);
		
		imagedestroy($src_image);
		imagedestroy($dst_image);
		

		return @getimagesize($path);
	}
}
?>
