<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage tparser
*/

require_once('class.WebForm.php');


/**
* TPARSER_SMARTY class
*
* @author Roman V Tsourick <rvt@ok.ru>
* @version 1.0
*/

/**
* Class exception
*/

class TParserSmartyException extends Exception
{
}

class TParserSmarty implements TParserInterface
{
	// private $application = NULL;
	
	private $smarty;
	
	private $template_dir;
	private $theme_dir = '';
	private $default_theme_dir = '';
	
	private $string_template_data;
	
	private $area_enumerator_data;
	
	private $area_handler = NULL;
	
	private $debug = false;
	
	protected $debug_log_disabled = true;
	// protected $debug_log_blocks = array('info', 'info::init');
	protected $debug_log_blocks = array('info');
	

	private $minify_css = false; // default; set by config option
	private $minify_tpl = false; // default; set by config option

	
	private $minify_debug_css = false;             // force CSS minify when debug is on
	private $minify_debug_css_no_recompile = true; // stop "force compile" on every fetch; prevents race errors;
	                                               // should be set ON once after return from "minify debug CSS" to recompile back to unminified
	                                               
	private $minify_debug_tpl = false;             // force TPL minify when debug is on
	private $minify_debug_tpl_no_recompile = true; // stop "force compile" on every fetch; prevents race errors;
	                                               // should be set ON once after return from "minify debug TPL" to recompile back to unminified
	                                               
	
	protected function debug_log($message, $block)
	{
		if (! $this->debug_log_disabled)
		{
			if (in_array($block, $this->debug_log_blocks))
			{
				dump('framework.modules.' . get_class($this) . ': ' . $message, $block);
			}
		}
	}
	

	// public function __construct($application, $template_dir = '')
	public function __construct($template_dir = '', $theme = '', $default_theme = '')
	{
		$this->debug_log("Constructing TParser instance of '" . __CLASS__ . "'", 'info::init');
		/*
		$this->application = $application;

		
		$c = $this->application->getSystemConfig();
		*/
		$class_dir = Framework::get('tparser_smarty/smarty_dir');
		
		$ver = '3.1';
		$class_path = $class_dir . 'SmartyBC.class.php';
		if (! file_exists($class_path))
		{
			$ver = '3.0';
			$class_path = $class_dir . 'Smarty.class.php';
		}

		// if (! is_readable($class_path)) throw new TParserSmartyException("'$class_path' is not readable");
		
		include_once($class_path);
		
		
		// Create smarty instance
		
		$this->smarty = $ver == '3.1' ? new SmartyBC : new Smarty;

		$this->smarty->php_handling = Smarty::PHP_ALLOW;
		if ($ver == '3.0') $this->smarty->allow_php_tag = true;
		$this->smarty->error_reporting = E_ALL ^ E_NOTICE;
		
		
		$this->minify_tpl = Framework::get('tparser_smarty/minify_tpl', false, false);
		$this->minify_css = Framework::get('tparser_smarty/minify_css', false, false);
		
		
		/*
		// Use given dir for templates and own internal dirs for smarty features
		
		if (! empty($template_dir))
		{
			$template_dir = dirpath($template_dir);
		}
		else
		{
			$template_dir = dirpath(dirname(realpath(__FILE__))) . 'smarty_templates/';
		}
		
		if (! is_readable($template_dir)) throw new TParserSmartyException("Template dir '$template_dir' is not readable");
		

		$this->smarty->template_dir = $template_dir;
		$this->smarty->compile_dir = $template_dir . 'c/';


		// store template dir
		$this->template_dir = $template_dir;
		
		$this->debug_log("Template dir: '{$template_dir}'", 'info::init');
		*/
		$this->setTemplateDir($template_dir);
		
		
		// setup theming
		/*
		if (! empty($theme))
		{
			$theme_dir = dirpath(dirpath($this->template_dir) . $theme);
			
			if (! is_readable($theme_dir)) throw new TParserSmartyException("Theme dir '$theme_dir' is not readable");
			
			$this->theme_dir = $theme_dir;
			
	
			if (! empty($default_theme))
			{
				$default_theme_dir = dirpath(dirpath($this->template_dir) . $default_theme);
				
				if (! is_readable($default_theme_dir)) throw new TParserSmartyException("Default theme dir '$default_theme_dir' is not readable");
				
				$this->default_theme_dir = $default_theme_dir;
			}


			$this->smarty->template_dir = $theme_dir;
		}
		else
		{
			$this->smarty->template_dir = $template_dir;
		}
		*/
		
		
		// setup theming
		if (! empty($theme))
		{
			$this->debug_log("Theming on", 'info::init');
			

			$theme_dir = dirpath(dirpath($this->template_dir) . $theme);
			
			if (! is_readable($theme_dir)) throw new TParserSmartyException("Theme dir '$theme_dir' is not readable");
			
			$this->theme_dir = $theme_dir;

			
			$this->debug_log("Theme dir: {$theme_dir}", 'info::init');

			
			if (! empty($default_theme))
			{
				$default_theme_dir = dirpath(dirpath($this->template_dir) . $default_theme);
				
				if (! is_readable($default_theme_dir)) throw new TParserSmartyException("Default theme dir '$default_theme_dir' is not readable");
				
				$this->default_theme_dir = $default_theme_dir;

				
				$this->debug_log("Default theme dir: {$default_theme_dir}", 'info::init');
			}
		}
		else
		{
			$this->debug_log("Theming off", 'info::init');
		}
		
		
		// Set default template handler in order to find right template from theme on {include} and etc 
		$this->smarty->default_template_handler_func = array($this, 'defaultTemplateHandler');


		// custom plugins dir
		$this->smarty->plugins_dir = array
		(
			$class_dir . 'plugins/', // standard plugins
			dirpath(dirname(realpath(__FILE__))) . 'smarty_plugins/' // custom plugins
		);


		// register smarty resource to parse strings 
		$this->register_resource
		(
			'str',
			array
			(
				// $this,
				array($this, '_string_resource_get_template'), 
				array($this, '_string_resource_get_timestamp'), 
				array($this, '_string_resource_get_secure'), 
				array($this, '_string_resource_get_trusted') 
			 )
		); 


		$this->register_function('html_options2',   array($this, '_smarty_html_options2'));
 
		$this->register_function('WFHiddenField',   array($this, '_WFHiddenField'));
		$this->register_function('WFCheckboxField', array($this, '_WFCheckboxField')); // obsolete, use OptionField instead
		$this->register_function('WFOptionField',   array($this, '_WFOptionField'));
		$this->register_function('WFFlagField',     array($this, '_WFFlagField'));
		$this->register_function('WFStringField',   array($this, '_WFStringField'));
		$this->register_function('WFPasswordField', array($this, '_WFPasswordField'));
		$this->register_function('WFTextField',     array($this, '_WFTextField'));
		$this->register_function('WFFileField',     array($this, '_WFFileField'));
		$this->register_function('WFDateField',     array($this, '_WFDateField'));
		$this->register_function('WFTimeField',     array($this, '_WFTimeField'));                  
		$this->register_function('WFDDLField',      array($this, '_WFDDLField'));
		$this->register_function('WFListField',     array($this, '_WFListField'));
		$this->register_function('WFGroupField',    array($this, '_WFGroupField'));
 
		$this->register_function('WFFieldWidget',   array($this, '_WFFieldWidget'));
		

		$this->register_block('WFFieldEmpty', array($this, '_WFFieldEmpty')); 
		$this->register_block('WFFieldWrong', array($this, '_WFFieldWrong')); 
	}
	
	public function register_function($name, $callback)
	{
		$this->smarty->registerPlugin('function', $name, $callback);
	}
	
	public function register_block($name, $callback)
	{
		$this->smarty->registerPlugin('block', $name, $callback);
	}

	public function register_modifier($name, $callback)
	{
		$this->smarty->registerPlugin('modifier', $name, $callback);
	}

	public function register_resource($name, $callbacks)
	{
		$this->smarty->registerResource($name, $callbacks);
	}
	
	
	/*
	protected function getTemplateActualPath($template_path)
	{
		$path = false;
		dump($template_path);
		
		if (fs_is_absolute_path($template_path))
		{
			if (file_exists($template_path))
			{
				$path = $template_path;
			}
			else throw new Exception("Template file not found at absolute path '{$template_path}'");
		}
		else
		{
			if (! empty($this->theme_dir))
			{
				// theming is on
				if (file_exists(dirpath($this->theme_dir) . $template_path))
				{
					$path = dirpath($this->theme_dir) . $template_path;
				}
				elseif (! empty($this->default_theme_dir) && file_exists(dirpath($this->default_theme_dir) . $template_path))
				{
					$path = dirpath($this->default_theme_dir) . $template_path;
				}
				else throw new Exception("Template file not found in neither theme nor default theme at path '{$template_path}'");
			}
			else
			{
				// theming is off
				
				if (file_exists(dirpath($this->template_dir) . $template_path))
				{
					$path = dirpath($this->template_dir) . $template_path;
				}
				else throw new Exception("Template file not found at path '{$template_path}'");
			}
		}


		return $path;
	}
	*/

	public function getTemplateActualPath($template_path)
	{
		$path = false;

		
		if (fs_is_absolute_path($template_path))
		{
			if (file_exists($template_path))
			{
				$path = $template_path;
			}
			else throw new Exception("Template file not found at absolute path '{$template_path}'");
			
			
			$this->debug_log("Absolute template path detected '{$template_path}'", 'info');
		}
		else
		{
			if (! empty($this->theme_dir))
			{
				// theming is on
				if (file_exists(dirpath($this->theme_dir) . $template_path))
				{
					$path = dirpath($this->theme_dir) . $template_path;
				}
				elseif (! empty($this->default_theme_dir))
				{
					if (file_exists(dirpath($this->default_theme_dir) . $template_path))
					{
						$path = dirpath($this->default_theme_dir) . $template_path;
					}
					else throw new Exception("Template file not found in neither theme nor default theme at path '{$template_path}'");

					
					$this->debug_log("Default theme template path detected '{$template_path}'", 'info');
				}
				else throw new Exception("Template file not found in theme at path '{$template_path}'");
			}
			else
			{
				// theming is off
				
				if (file_exists(dirpath($this->template_dir) . $template_path))
				{
					$path = dirpath($this->template_dir) . $template_path;
				}
				else throw new Exception("Template file not found at path '{$template_path}'");
			}
		}


		return $path;
	}


	public function defaultTemplateHandler($resource_type, $resource_name, &$template_source, &$template_timestamp, $smarty_obj)
	{
		$template_path = false;
		
		
		if ($resource_type == 'file')
		{
			if (! is_readable($resource_name))
			{
				$template_path = $this->getTemplateActualPath($resource_name);

				$template_source = file_get_contents($template_path);
				$template_timestamp = filemtime($template_path);
			}
		}
		
		
		return $template_path;
	}


	public function setTemplateDir($template_dir = '')
	{
		// Use given dir for templates and own internal dirs for smarty features
		
		if (! empty($template_dir))
		{
			$template_dir = dirpath($template_dir);
		}
		else
		{
			$template_dir = dirpath(dirname(realpath(__FILE__))) . 'smarty_templates/';
		}
		
		if (! is_readable($template_dir)) throw new TParserSmartyException("Template dir '$template_dir' is not readable");
		
		$this->smarty->template_dir = $template_dir;
		$this->smarty->compile_dir = $template_dir . 'c/';


		// store template dir
		$this->template_dir = $template_dir;

		$this->debug_log("Template dir: '{$template_dir}'", 'info::init');
	}
	
	
	public function debug($debug = NULL)
	{
		if ($debug !== NULL)
		{
			settype($debug, 'boolean');
			
			$this->debug = $debug;
			
			return true;
		}
		else return $this->debug;
	}


	public function _string_resource_get_template($tpl_name, &$tpl_source, $smarty_obj) 
	{
		$tpl_source = $this->string_template_data['contents'];

		
		return true; 
	} 
	
	public function _string_resource_get_timestamp($tpl_name, &$tpl_timestamp, $smarty_obj) 
	{ 
		$tpl_timestamp = $this->string_template_data['timestamp'];
		
		
		return true; 
	} 
	
	public function _string_resource_get_secure($tpl_name, $smarty_obj) 
	{ 
		return true; 
	} 
	
	public function _string_resource_get_trusted($tpl_name, $smarty_obj)
	{
	} 
	
	private function htmlSelect($name, $options, $values = array(), $multiselect = false, $size = 1, $required = false, $emptyvalue = '', $emptydisplay = '-', $format = 'valuemap', $valuefield = 'id', $displayfield = 'name', $attrs = '', $titles = array())
	{
		$html_options = array();
		

		if (! is_array($values))
		{
			$values = array($values);
		}

		
		if (! $required)
		{
			$selected = in_array($emptyvalue, $values, true) ? ' selected="selected"' : '';
			
			$html_options[] = '<option value="' . html_special_chars($emptyvalue) . '"' . $selected . '>' . html_special_chars($emptydisplay) . '</option>' . "\n";
		}

		if ($format == 'datalist')
		{
			foreach ($options as $option)
			{
				$v = $option[$valuefield];
				
				$selected = in_array($v, $values, true) ? ' selected="selected"' : '';
				
				$html_options[] = '<option value="' . html_special_chars($v) . '"' . $selected . '>' . html_special_chars($option[$displayfield]) . '</option>' . "\n";
			}
		}
		else
		{
			foreach ($options as $i => $option)
			{
				$selected = in_array($i, $values, true) ? ' selected="selected"' : '';
				
				$html_options[] = '<option value="' . html_special_chars($i) . '"' . $selected . '>' . html_special_chars($option) . '</option>' . "\n";
			}
		}
		
		
		$multiple = $multiselect ? ' multiple="multiple"' : '';
		$size = $size > 1 ? ' size="' . $size . '"' : '';
		$attrs = strlen($attrs) > 0 ? ' ' . $attrs : '';
		
		include_once('class.JSON.php');
		
		
		$_data_values = '';
		if (! empty($values))
		{
			$_data_values = ' data-values="' . htmlspecialchars(JSON::encode($values)) . '"';
		}
		$_data_titles = '';
		if (! is_null($titles))
		{
			$_data_titles = ' data-titles="' . htmlspecialchars(JSON::encode($titles)) . '"';
		}
			
		return '<select name="' . html_special_chars($name) . '"' . $multiple . $size . $attrs . $_data_values . $_data_titles . '>' . implode('', $html_options) . '</select>';
	}


	public function _WFHiddenField($params, $smarty)
	{
		if (empty($params['name'])) $smarty->trigger_error('name empty');

		$n = $params['name'];
		$v = isset($params['value']) ? $params['value'] : '';
		$a = isset($params['attrs']) ? ' ' . $params['attrs'] : '';
		$title = isset($params['title']) ? $params['title'] : NULL;
		
		$_data_title = '';
		if (! is_null($title))
		{
			$_data_title = ' data-title="' . htmlspecialchars($title) . '"';
		}
		
		
		return '<input type="hidden" name="' . html_special_chars($n) . '" value="' . html_special_chars($v) . '"' . $a . $_data_title . ' />';
	}
	
	public function _WFCheckboxField($params, $smarty)
	{
		return $this->_WFOptionField($params, $smarty);
	}
	public function _WFOptionField($params, $smarty)
	{
		if (empty($params['name'])) $smarty->trigger_error('name empty');

		$n = $params['name'];
		$v = isset($params['value']) ? $params['value'] : '';
		$c = (isset($params['checked']) && $params['checked']) ? ' checked="checked"' : '';
		$a = isset($params['attrs']) ? ' ' . $params['attrs'] : '';
		
		return '<input type="checkbox" name="' . html_special_chars($n) . '" value="' . html_special_chars($v) . '"' . $c . $a . ' />';
	}

	public function _WFFlagField($params, $smarty)
	{
		if (empty($params['name'])) $smarty->trigger_error('name empty');

		$n = $params['name'];
		$c = (isset($params['value']) && $params['value']) ? ' checked="checked"' : '';
		$a = isset($params['attrs']) ? ' ' . $params['attrs'] : '';
		
		return '<input type="checkbox" name="' . html_special_chars($n) . '" value="1"' . $c . $a . ' />';
	}

	public function _WFStringField($params, $smarty)
	{
		if (empty($params['name'])) $smarty->trigger_error('name empty');

		$n = $params['name'];
		$v = isset($params['value']) ? $params['value'] : '';
		$a = isset($params['attrs']) ? ' ' . $params['attrs'] : '';

		return '<input type="text" name="' . html_special_chars($n) . '" value="' . html_special_chars($v) . '"' . $a . ' />';
	}
	
	public function _WFPasswordField($params, $smarty)
	{
		if (empty($params['name'])) $smarty->trigger_error('name empty');

		$n = $params['name'];
		$v = isset($params['value']) ? $params['value'] : '';
		$a = isset($params['attrs']) ? ' ' . $params['attrs'] : '';
		
		return '<input type="password" name="' . html_special_chars($n) . '" value="' . html_special_chars($v) . '"' . $a . ' />';
	}

	public function _WFTextField($params, $smarty)
	{
		if (empty($params['name'])) $smarty->trigger_error('name empty');

		$n = $params['name'];
		$v = isset($params['value']) ? $params['value'] : '';
		$a = isset($params['attrs']) ? ' ' . $params['attrs'] : '';
		$c = isset($params['cols']) ? $params['cols'] : 25;
		$r = isset($params['rows']) ? $params['rows'] : 3;

		return '<textarea name="' . html_special_chars($n) . '" cols="' . html_special_chars($c) . '" rows="' . html_special_chars($r) . '" ' . $a . '/>' . html_special_chars($v) . '</textarea>';
	}
	
	public function _WFFileField($params, $smarty)
	{
		if (empty($params['name'])) $smarty->trigger_error('name empty');

		$n = $params['name'];
		// $v = isset($params['value']) ? $params['value'] : '';
		$a = isset($params['attrs']) ? ' ' . $params['attrs'] : '';
		
		return '<input type="file" name="' . html_special_chars($n) . '"' . $a . ' />';
	}

	public function _WFDateField($params, $smarty)
	{
		$output = '';
		
		
		if (empty($params['name'])) $smarty->trigger_error('name empty');

		$n = $params['name'];
		$v = isset($params['value']) ? $params['value'] : NULL;
		$r = isset($params['required']) ? $params['required'] : false;
		$ed = isset($params['emptydisplay']) ? $params['emptydisplay'] : '-';
		$start_year = isset($params['start_year']) ? trim($params['start_year']) : NULL;
		$end_year = isset($params['end_year']) ? trim($params['end_year']) : NULL;
		$f = isset($params['format']) ? $params['format'] : 'Y-m-d';
		$attrs = isset($params['attrs']) ? $params['attrs'] : '';
		$postfixes = isset($params['postfixes']) ? $params['postfixes'] : '_year,_month,_day';
		
		
		$postfixes = explode(',', $postfixes);

		
		// Set 'now' values
		sscanf(date('Y-m-d'), "%04d-%02d-%02d", $now_y, $now_m, $now_d);

		
		// Set value parts

		if (! is_null($v))
		{
			sscanf($v, "%04d-%02d-%02d", $year, $month, $day);
		}
		else
		{
			if ($r)
			{
				$year = $now_y;
				$month = $now_m;
				$day = $now_d;
			}
			else
			{
				$year = '0000';
				$month = '00';
				$day = '00';
			}
		}

		
		// Years

		// Parse range
		
		if (is_null($start_year) && is_null($end_year))
		{
			// year range is not set
			$start_year = $end_year = $now_y;
		}
		else
		{
			// assume skipped years as now
			if (is_null($start_year)) $start_year = $now_y; // if start year is not set assume 'now'
			if (is_null($end_year)) $end_year = $now_y; // if end year is not set assume 'now'

			
			// calc deltas
			$start_year_delta = false; 
			if (strpos($start_year, '+') === 0 || strpos($start_year, '-') === 0) $start_year_delta = true;
			$end_year_delta = false; 
			if (strpos($end_year, '+') === 0 || strpos($end_year, '-') === 0) $end_year_delta = true;
			
			if ($start_year_delta && $end_year_delta)
			{
				// calc deltas from now
				$start_year = $now_y + $start_year;
				$end_year = $now_y + $end_year;
			}
			// calc deltas from each other
			elseif ($start_year_delta)
			{
				$start_year = $end_year + $start_year;
			}
			elseif ($end_year_delta)
			{
				$end_year = $start_year + $end_year;
			}
		}

		// Make options
		
		$year_options = array();
		
		if ($start_year <= $end_year)
		{
			// years go upwards
			for ($i = $start_year; $i <= $end_year; $i++)
			{
				$t = sprintf("%04d", $i);
				$year_options[] = array('v' => $i, 't' => $t);
			}
		}
		else
		{
			// years go downwards
			for ($i = $start_year; $i >= $end_year; $i--)
			{
				$t = sprintf("%04d", $i);
				$year_options[] = array('v' => $i, 't' => $t);
			}
		}
		
		
		// Months

		$month_options = array();

		for ($i = 1; $i <= 12; $i++)
		{
			$t = sprintf("%02d", $i);
			$month_options[] = array('v' => $i, 't' => $t);
		}


		// Days

		$day_options = array();

		for ($i = 1; $i <= 31; $i++)
		{
			$t = sprintf("%02d", $i);
			$day_options[] = array('v' => $i, 't' => $t);
		}


		// Parse format
		
		foreach (str_split($f) as $c)
		{
			switch ($c)
			{
				case 'Y':
					$output .= $this->htmlSelect($n . $postfixes[0], $year_options, array($year), false, 1, $r, '0000', $ed, 'datalist', 'v', 't', $attrs);
				break;
				
				case 'm':
					$output .= $this->htmlSelect($n . $postfixes[1], $month_options, array($month), false, 1, $r, '00', $ed, 'datalist', 'v', 't', $attrs);
				break;
				
				case 'd':
					$output .= $this->htmlSelect($n . $postfixes[2], $day_options, array($day), false, 1, $r, '00', $ed, 'datalist', 'v', 't', $attrs);
				break;
				
				default:
					$output .= $c;
			}
		}

		
		return $output;
	}


	public function _WFTimeField($params, $smarty)
	{
		$output = '';
		
		
		if (empty($params['name'])) $smarty->trigger_error('name empty');

		$n = $params['name'];
		$v = isset($params['value']) ? $params['value'] : NULL;
		$r = isset($params['required']) ? $params['required'] : false;
		$ed = isset($params['emptydisplay']) ? $params['emptydisplay'] : '-';
		$f = isset($params['format']) ? $params['format'] : 'H:i:s';
		$attrs = isset($params['attrs']) ? $params['attrs'] : '';
		$postfixes = isset($params['postfixes']) ? $params['postfixes'] : '_hour,_minute,_second';
		
		
		$postfixes = explode(',', $postfixes);

		
		// Set 'now' values
		sscanf(date('H:i:s'), "%02d:%02d:%02d", $now_h, $now_m, $now_s);

		
		// Set value parts

		if (! is_null($v))
		{
			sscanf($v, "%02d:%02d:%02d", $hour, $minute, $second);
		}
		else
		{
			if ($r)
			{
				$hour = $now_h;
				$minute = $now_m;
				$second = $now_s;
			}
			else
			{
				$hour = '99';
				$minute = '99';
				$second = '99';
			}
		}
		

		// Hours
		
		$hour_options = array();

		for ($i = 0; $i <= 23; $i++)
		{
			$t = sprintf("%02d", $i);
			$hour_options[] = array('v' => $i, 't' => $t);
		}


		// Minutes

		$minute_options = array();

		for ($i = 0; $i <= 59; $i++)
		{
			$t = sprintf("%02d", $i);
			$minute_options[] = array('v' => $i, 't' => $t);
		}


		// Seconds

		$second_options = array();

		for ($i = 0; $i <= 59; $i++)
		{
			$t = sprintf("%02d", $i);
			$second_options[] = array('v' => $i, 't' => $t);
		}


		// Parse format
		
		foreach (str_split($f) as $c)
		{
			switch ($c)
			{
				case 'H':
					$output .= $this->htmlSelect($n . $postfixes[0], $hour_options, array($hour), false, 1, $r, '99', $ed, 'datalist', 'v', 't', $attrs);
				break;
				
				case 'i':
					$output .= $this->htmlSelect($n . $postfixes[1], $minute_options, array($minute), false, 1, $r, '99', $ed, 'datalist', 'v', 't', $attrs);
				break;
				
				case 's':
					$output .= $this->htmlSelect($n . $postfixes[2], $second_options, array($second), false, 1, $r, '99', $ed, 'datalist', 'v', 't', $attrs);
				break;
				
				default:
					$output .= $c;
			}
		}

		
		return $output;
	}


	/**
	* Drop-down list
	*/
	public function _WFDDLField($params, $smarty)
	{
		if (empty($params['name'])) $smarty->trigger_error('name empty');
		if (empty($params['options'])) $smarty->trigger_error('options empty');

		$value = isset($params['value']) ? $params['value'] : array();
		$r = isset($params['required']) ? $params['required'] : false;
		$ev = isset($params['emptyvalue']) ? $params['emptyvalue'] : '';
		$ed = isset($params['emptydisplay']) ? $params['emptydisplay'] : '-';
		$format = isset($params['format']) ? $params['format'] : 'valuemap';
		$vf = isset($params['valuefield']) ? $params['valuefield'] : 'id';
		$df = isset($params['displayfield']) ? $params['displayfield'] : 'name';
		$attrs = isset($params['attrs']) ? $params['attrs'] : '';
		$title = isset($params['title']) ? $params['title'] : array();

		return $this->htmlSelect($params['name'], $params['options'], $value, false, 1, $r, $ev, $ed, $format, $vf, $df, $attrs, $title);
	}


	/**
	* List
	*/
	public function _WFListField($params, $smarty)
	{
		if (empty($params['name'])) $smarty->trigger_error('name not set for ListField');
		if (empty($params['options'])) $smarty->trigger_error("options not set for ListField '{$params['name']}'");
		
		$value = isset($params['value']) ? $params['value'] : array();
		$m = isset($params['multiselect']) ? $params['multiselect'] : false;
		$s = isset($params['size']) && $params['size'] > 1 ? $params['size'] : 3;
		$r = isset($params['required']) ? $params['required'] : false;
		$ev = isset($params['emptyvalue']) ? $params['emptyvalue'] : '';
		$ed = isset($params['emptydisplay']) ? $params['emptydisplay'] : '-';
		$format = isset($params['format']) ? $params['format'] : 'valuemap';
		$vf = isset($params['valuefield']) ? $params['valuefield'] : 'id';
		$df = isset($params['displayfield']) ? $params['displayfield'] : 'name';
		$attrs = isset($params['attrs']) ? $params['attrs'] : '';
		$title = isset($params['title']) ? $params['title'] : array();

		$name = $m ? $params['name'] . '[]' : $params['name']; // indicate an array for PHP if multiselect allowed

		return $this->htmlSelect($name, $params['options'], $value, $m, $s, $r, $ev, $ed, $format, $vf, $df, $attrs, $title);
	}


	/**
	* Group
	*/
	public function _WFGroupField($params, $smarty)
	{
		if (empty($params['name'])) $smarty->trigger_error('name not set for GroupField');
		if (! isset($params['options'])) $smarty->trigger_error("options not set for GroupField '{$params['name']}'");
		
		$value = isset($params['value']) ? $params['value'] : array();
		$m = isset($params['multiselect']) ? $params['multiselect'] : false;
		$r = isset($params['required']) ? $params['required'] : false;
		$ev = isset($params['emptyvalue']) ? $params['emptyvalue'] : '';
		$ed = isset($params['emptydisplay']) ? $params['emptydisplay'] : '-';
		$format = isset($params['format']) ? $params['format'] : 'valuemap';
		$vf = isset($params['valuefield']) ? $params['valuefield'] : 'id';
		$df = isset($params['displayfield']) ? $params['displayfield'] : 'name';
		$attrs = isset($params['attrs']) ? $params['attrs'] : '';
		$template = isset($params['template']) ? $params['template'] : '<fieldset name="{$name}"{$attrs}{$data_values}>{$items}</fieldset>';
		$item_template = isset($params['item_template']) ? $params['item_template'] : '<div class="{$disabled_class} {$selected_class}">{$input}<label for="{$id}">{$title}</label></div> ' . "\n";
		$skip_selected = isset($params['skip_selected']) ? $params['skip_selected'] : false;
		$disabled_values = isset($params['disabled_values']) ? $params['disabled_values'] : array();
		$data = isset($params['data']) ? $params['data'] : NULL; // arbitrary data for arbitrary purpose

		$name = $m ? $params['name'] . '[]' : $params['name']; // indicate an array for PHP if multiselect allowed

		// return $this->htmlSelect($name, $params['options'], $value, $m, $s, $r, $ev, $ed, $format, $vf, $df, $attrs);
		

		$options = $params['options'];
		$values = $value;
		$required = $r;
		$emptyvalue = $ev;
		$emptydisplay = $ed;
		// $format = 'valuemap',
		$valuefield = $vf;
		$displayfield = $df;
		// $attrs = ''
		$multiselect = $m;
				
		
		
		$html_options = array();
		

		if (! is_array($values))
		{
			$values = array($values);
		}

		//if ($name =='end_year') {dump($values); dump($options);}
		/*
		if (! $required)
		{
			$selected = in_array($emptyvalue, $values, true) ? ' selected="selected"' : '';
			
			$html_fieldset[] = '<option value="' . html_special_chars($emptyvalue) . '"' . $selected . '>' . html_special_chars($emptydisplay) . '</option>' . "\n";
		}
		*/

		$input_type = $multiselect ? 'checkbox' : 'radio';

		if ($format == 'datalist')
		{
			foreach ($options as $option)
			{
				$v = $option[$valuefield];
				$id = str_rand() . '_' . html_special_chars($name) . '_' . html_special_chars($v);
				
				$selected = in_array($v, $values, true);
				
				if ($skip_selected && $selected) continue;
				
				
				$disabled = in_array($v, $disabled_values, true);
				

				$_disabled = $disabled ? ' disabled="disabled"' : '';
				$_checked = $selected ? ' checked="checked"' : '';
				$_input = '<input name="' . html_special_chars($name) . '" type="' . $input_type . '" value="' . html_special_chars($v) . '"' . $_checked . $_disabled . ' id="' . $id . '" />';
				$_id = $id;
				$_title = html_special_chars($option[$displayfield]);
				$_disabled_class = $disabled ? 'disabled' : '';
				$_selected_class = $selected ? 'selected' : '';

				// IDEA: implement using return string value as a template
				// $item_template = is_callable($item_template) ? $item_template() : $item_template;
				
				$html_options[] = parseTemplate($item_template, array('input' => $_input, 'id' => $_id, 'title' => $_title, 'value' => $v, 'option' => $option, 'disabled_class' => $_disabled_class, 'selected_class' => $_selected_class, 'data' => $data));
			}
		}
		else
		{
			foreach ($options as $i => $option)
			{
				$v = $i;
				$id = str_rand() . '_' . html_special_chars($name) . '_' . html_special_chars($v);
				
				$selected = in_array($v, $values, true) ? ' checked="checked"' : '';

				if ($skip_selected && $selected) continue;


				$disabled = in_array($v, $disabled_values, true);


				$_disabled = $disabled ? ' disabled="disabled"' : '';
				$_checked = $selected ? ' checked="checked"' : '';
				$_input = '<input name="' . html_special_chars($name) . '" type="' . $input_type . '" value="' . html_special_chars($v) . '"' . $_checked . $_disabled . ' id="' . $id . '" />';
				$_id = $id;
				$_title = html_special_chars($option);
				$_disabled_class = $disabled ? 'disabled' : '';
				$_selected_class = $selected ? 'selected' : '';
				
				$html_options[] = parseTemplate($item_template, array('input' => $_input, 'id' => $_id, 'title' => $_title, 'value' => $v, 'i' => $i, 'option' => $option, 'disabled_class' => $_disabled_class, 'selected_class' => $_selected_class, 'data' => $data));
			}
		}
		

		//$multiple = $multiselect ? ' multiple="multiple"' : '';
		//$size = $size > 1 ? ' size="' . $size . '"' : '';
		$attrs = strlen($attrs) > 0 ? ' ' . $attrs : '';
		
		include_once('class.JSON.php');
		
		
		$data_values = '';
		// if (! empty($values))
		if (! empty($values) && ! $skip_selected)
		{
			$data_values = ' data-values="' . htmlspecialchars(JSON::encode($values)) . '"';
		}
			
		// return '<fieldset name="' . html_special_chars($name) . '"' . $attrs . $data_values . '>' . implode('', $html_options) . '</fieldset>';
		$_name = html_special_chars($name);
		$_attrs = $attrs;
		$_data_values = $data_values;
		$_items = implode('', $html_options);
		return parseTemplate($template, array('name' => $_name, 'attrs' => $_attrs, 'data_values' => $_data_values, 'items' => $_items, 'data' => $data));
		

		
		// return '<input type="checkbox" name="' . html_special_chars($n) . '" value="' . html_special_chars($v) . '"' . $c . $a . ' />';
	}


	private function configAttrsAddClass(&$config)
	{
		$attrs = ! empty($config['attrs']) ? $config['attrs'] : '';
		
		$attrs .= ' class = "' . $config['name'] . '"';
		
		
		$config['attrs'] = $attrs;
	}
	
	public function _WFFieldWidget($params, $smarty)
	{
		$output = '';
		
		
		$config = $params['config'];
		
		
		// Apply inline params
		
		unset($params['config']);
		
		$config = array_merge($config, $params);

		
		// Process smart params
		
		if (isset($config['add_class']) && $config['add_class'])
		{
			unset($config['add_class']);

			$this->configAttrsAddClass($config);
		}

		// Dispatch widget by type
		switch ($config['type'])
		{
			case 'date':
				$config['postfixes'] = implode(',', $config['postfixes']);
				
				$output = $this->_WFDateField($config, $smarty);			
			break;
			
			case 'time':
				$config['postfixes'] = implode(',', $config['postfixes']);
				
				$output = $this->_WFTimeField($config, $smarty);			
			break;
			
			case 'hidden':
				$output = $this->_WFHiddenField($config, $smarty);			
			break;
			
			case 'checkbox': // obsolete, use 'option' instead
			case 'option':
				$output = $this->_WFOptionField($config, $smarty);			
			break;
			
			case 'flag':
				$output = $this->_WFFlagField($config, $smarty);			
			break;

			case 'string':
				$output = $this->_WFStringField($config, $smarty);			
			break;
			
			case 'password':
				$output = $this->_WFPasswordField($config, $smarty);			
			break;

			case 'text':
				$output = $this->_WFTextField($config, $smarty);			
			break;

			case 'ddl':
				$output = $this->_WFDDLField($config, $smarty);			
			break;
			
			case 'list':
				$output = $this->_WFListField($config, $smarty);			
			break;
			
			case 'file':
				$output = $this->_WFFileField($config, $smarty);			
			break;
			
			case 'group':
				$output = $this->_WFGroupField($config, $smarty);			
			break;
		}
		
		return $output;
	}


	function _WFFieldEmpty($params, $content, &$smarty, &$repeat) 
	{ 
		$output = '';

		if (($params['config']['error_flags'] & WebFormField::EMPTY_VALUE) == WebFormField::EMPTY_VALUE)
		{
			$output = $content;
		}
		
		return $output;
	}

	function _WFFieldWrong($params, $content, &$smarty, &$repeat) 
	{ 
		$output = '';

		if (($params['config']['error_flags'] & WebFormField::WRONG_VALUE) == WebFormField::WRONG_VALUE)
		{
			$output = $content;
		}
		
		return $output;
	}


	public function getDelimiters()
	{
		return array($this->smarty->left_delimiter, $this->smarty->right_delimiter);
	}

	public function setDelimiters(Array $delimiters)
	{
		$this->smarty->left_delimiter = $delimiters[0];
		$this->smarty->right_delimiter = $delimiters[1];
	}

	public function parseString($template_contents)
	{
		$this->string_template_data['contents'] = $template_contents;
		$this->string_template_data['timestamp'] = time();
		
		/*
		$c = $this->application->getSystemConfig();
		$this->set('syscon', $c->get());
		*/
		
		return $this->smarty->fetch('str:string');
	}
	
	
	public function postfilter_minifyCSS($content, $smarty)
	{
		$content = preg_replace('|/\\*.*?\\*/|s', '', $content);             // remove comments
		
		$content = preg_replace('/^\\s*/m', '', $content);                   // remove leading space 
		$content = preg_replace('/\\s*$/m', '', $content);                   // remove trailing space
		$content = str_replace("\t", " ", $content);                         // <tabs> to <spaces>
		$content = preg_replace('/ +/', ' ', $content);                      // compress <spaces>
		
		$content = preg_replace('/\\n?\\{\\n?/', ' {', $content);             // line up '{'
		
		$content = preg_replace('/\\n?\\}\\n?/', "}\n", $content);           // line up '}'
		
		$content = preg_replace('/;\\n/', "; ", $content);                   // line up ';'
		
		$content = str_replace(": ", ":", $content);                         // remove spaces after ':'

		$content = preg_replace('/ *\\> */', ">", $content);                 // trim '>'
		
		$content = str_replace(array("\r\n", "\r", "\n"), "\n", $content);   // unify EOLs
		
		$content = preg_replace('/,\\s*\\n/', ",", $content);               // line up trailing ','
		
		// TODO
		// $content = str_replace("\n", "", $content);   // cut EOLs
		
		return $content;
	}
		
	public function qqq($matches)
	{
		$s = $matches[1];
		// dump($s);
		
		$s = preg_replace('/\t/', '[tab]', $s); // \r symbol can be before EOL when contents saved in windows mode		
		dump($s);
		
		return $matches[0];
	}
	
	public function prefilter_minifyHTML($content, $smarty)
	{
		// dump(' -> minifyHTML' . "\n");
    // $content = preg_replace('/ +/', ' ', $content);
    // $content = preg_replace('/[ \\t]*\r?\n[ \\t]*/', ' ', $content);
    // $content = preg_replace('/\\n/', ' ', $content);
    
		$content = str_replace(array("\r\n", "\r", "\n"), "\n", $content); // convert to Unix EOLs for simplicity

    $content = preg_replace('/^[ \\t]+$/m', '', $content);  // compress empty lines
		
    $content = preg_replace('/^[ \\t]+/m', '', $content);                   // remove leading space 
		$content = preg_replace('/[ \\t]+$/m', '', $content);                   // remove trailing space

    $content = preg_replace('/\\n+/', "\n", $content);      // compress EOLs
    
    
    // $content = preg_replace_callback('!<div id="content">(.*?)</div>!s', array($this, 'qqq'), $content);  // compress EOLs
		
		return $content;
	}
	
	public function parseFile($template_path)
	{
		// if (! file_exists($template_path) && ! file_exists(dirpath($this->template_dir) . $template_path))
			// throw new Exception('Template file "' . $template_path . '" not found!');

		$template_path = $this->getTemplateActualPath($template_path);
		
		
		$output = '';
		// dump($template_path . ' -> ' . baseext($template_path));
		// dump('parseFile: ' . $template_path);
		if ($this->minify_css && baseext($template_path) == 'css')
		{
			$minify_debug = $this->minify_debug_css;
			$no_recompile = $this->minify_debug_css_no_recompile;
			
			if (! $minify_debug && $this->debug)
			{
				if (! $no_recompile) $this->smarty->force_compile = true;
				$output = $this->smarty->fetch($template_path);
			}
			else
			{
				if ($minify_debug) $this->smarty->force_compile = true; // debug
	
				$this->smarty->registerFilter('post', array($this, 'postfilter_minifyCSS'));
				$output = $this->smarty->fetch($template_path);
				$this->smarty->unregisterFilter('post', 'postfilter_minifyCSS');

				if ($minify_debug) $this->smarty->force_compile = false; // debug
			}
		}
		elseif ($this->minify_tpl && baseext($template_path) == 'tpl')
		{
			$minify_debug = $this->minify_debug_tpl;
			$no_recompile = $this->minify_debug_tpl_no_recompile;
			
			if (! $minify_debug && $this->debug)
			{
				$was_forced = $this->smarty->force_compile;
				
				if (! $no_recompile && ! $was_forced) $this->smarty->force_compile = true; // set
				$output = $this->smarty->fetch($template_path);
				if (! $no_recompile && ! $was_forced) $this->smarty->force_compile = false; // reset
			}
			else
			{
				$was_forced = $this->smarty->force_compile;
				
				if ($minify_debug && ! $was_forced) $this->smarty->force_compile = true; // debug
				
				
				$fname = 'prefilter_minifyHTML';
				
				$was_registered = isset($this->smarty->_plugins['prefilter'][$fname]);
				
				if (! $was_registered) $this->smarty->registerFilter('pre', array($this, $fname));
				$output = $this->smarty->fetch($template_path);
				if (! $was_registered) $this->smarty->unregisterFilter('pre', $fname);
				

				if ($minify_debug && ! $was_forced) $this->smarty->force_compile = false; // debug
			}
		}
		else
		{
			$output = $this->smarty->fetch($template_path);
		}

		
		return $output;
	}
	
	public function readFile($template_path)
	{
		/*
		$path = '';
		
		if (file_exists($template_path))
		{
			$path = $template_path;
		}
		else if (file_exists(dirpath($this->template_dir) . $template_path))
		{
			$path = dirpath($this->template_dir) . $template_path;
		}
		else throw new Exception('Template file "' . $template_path . '" not found!');
		*/
		$path = $this->getTemplateActualPath($template_path);
		
		return file_get_contents($path);
	}

	public function set($var, $value = NULL)
	{
		if (is_array($var)) $this->smarty->assign($var); else $this->smarty->assign($var, $value);
	}

	public function get($var)
	{
		return $this->smarty->getTemplateVars($var);
	}
	
	public function clear($var)
	{
		$this->smarty->clearAssign($var);
	}

	public function clearAll()
	{
		$this->smarty->clearAllAssign();
	}
	
	
	function _smarty_html_options2($params, &$smarty)
	{
			$name = null;
			$values = null;
			$options = null;
			$selected = array();
			$output = null;
			$keyfield = null;
			$valuefield = null;
			$nonempty = false;
			
			$extra = '';
			
			foreach($params as $_key => $_val) 
			{
					switch($_key) 
					{
							case 'name':
							case 'keyfield':
							case 'valuefield':
									$$_key = (string)$_val;
									break;
							
							case 'nonempty':
									$$_key = in_array($_val, array(1, 'yes', 'true', true)) ? true : false;
									break;
									
							case 'options':
									$$_key = (array)$_val;
									break;
									
							case 'values':
							case 'output':
									$$_key = array_values((array)$_val);
									break;
	
							case 'selected':
									$$_key = array_map('strval', array_values((array)$_val));
									break;
									
							default:
									if(!is_array($_val)) 
									{
											$extra .= ' '.$_key.'="'.htmlspecialchars($_val).'"';
									}
									else 
									{
											$smarty ->trigger_error("html_options2: extra attribute '$_key' cannot be an array", E_USER_NOTICE);
									}
									break;
					}
			}
	
			if (!isset($options) && !isset($values))
					return ''; /* raise error here? */
	
			$_html_result = '';

			if (isset($options))
			{
				if (! empty($keyfield) && ! empty($valuefield))
				{
					if (! $nonempty)
					{
						// array_unshift($options, array($keyfield => '', $valuefield => '-'));
						$_html_result .= $this -> __smarty_html_options2_optoutput(NULL, '', array());
					}
					foreach ($options as $_val) $_html_result .= $this -> __smarty_html_options2_optoutput($_val[$keyfield], $_val[$valuefield], $selected);
				}
				else
				{
					if (! $nonempty)
					{
						// $options = array_merge(array('' => '-'), $options);
						$_html_result .= $this -> __smarty_html_options2_optoutput(NULL, '', array());
					}
					foreach ($options as $_key=>$_val) $_html_result .= $this -> __smarty_html_options2_optoutput($_key, $_val, $selected);
				}
			}
			else
			{
					if (! $nonempty)
					{
						// array_unshift($values, '');
						// array_unshift($output, '-');
						$_html_result .= $this -> __smarty_html_options2_optoutput(NULL, '', array());
					}

					foreach ($values as $_i=>$_key) {
							$_val = isset($output[$_i]) ? $output[$_i] : '';
							$_html_result .= $this -> __smarty_html_options2_optoutput($_key, $_val, $selected);
					}
	
			}
	
			if(!empty($name)) {
					$_html_result = '<select name="' . $name . '"' . $extra . '>' . "\n" . $_html_result . '</select>' . "\n";
			}
	
			return $_html_result;
	}
	
	function __smarty_html_options2_optoutput($key, $value, $selected)
	{
		if (! is_array($value))
		{
			if (! is_null($key))
			{
				$_html_result = '<option label="' . htmlspecialchars($value) . '" value="' . htmlspecialchars($key) . '"';
			}
			else
			{
				$_html_result = '<option';
			}
			
			if (in_array((string)$key, $selected)) $_html_result .= ' selected="selected"';
			
			$_html_result .= '>' . htmlspecialchars($value) . '</option>' . "\n";
		}
		else
		{
			$_html_result = $this -> __smarty_html_options2_optgroup($key, $value, $selected);
		}
		
		return $_html_result;
	}
	
	function __smarty_html_options2_optgroup($key, $values, $selected) {
			$optgroup_html = '<optgroup label="' . htmlspecialchars($key) . '">' . "\n";
			foreach ($values as $key => $value) {
					$optgroup_html .= $this -> __smarty_html_options2_optoutput($key, $value, $selected);
			}
			$optgroup_html .= "</optgroup>\n";
			return $optgroup_html;
	}
}

?>