<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty html modifier plugin
 *
 * @author   Roman Tsourick <tsourick at gmail dot com>
 * @param string
 * @return string
 */
// function smarty_modifier_html($string, $char_set = 'ISO-8859-1')
function smarty_modifier_html($string, $char_set = 'UTF-8')
{
	$string = htmlspecialchars($string, ENT_QUOTES, $char_set);

	$tt = array('«' => '&laquo;', '»' => '&raquo;');
	$string = str_replace(array_keys($tt), array_values($tt), $string);

	return $string;
}

?>
