<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2013
*
* Smarty plugin
* @package smarty
* @subpackage plugin
*/

require_once('class.TextTools.php');

function smarty_modifier_rubtostr($n)
{
	return TextTools::rubtostr($n);
}

?>