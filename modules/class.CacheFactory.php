<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2013
*
* @package modules
* @subpackage cache
*/

/**
* CacheInterface class
*
* @version 1.0
*/

interface CacheInterface
{
	public function __construct();

	public function connect();
	
	public function set($key, $var, $expires = 0);
	public function get($var);
	public function clear($var);
}

class CacheFactoryException extends Exception
{
}

class CacheFactory
{
	private static $loadedEngines = array();

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	private function __construct()
	{
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function createCache($engine_name)
	{
		$class_name = 'Cache' . ucfirst($engine_name);
		
		
		// Load parser engine class
		if (! array_key_exists($engine_name, self::$loadedEngines))
		{
			// Check driver
			
			$class_path = align_path(dirpath(dirname(realpath(__FILE__))) . 'caches/class.' . $class_name . '.php');

			if (! is_file($class_path) || ! is_readable($class_path)) throw new CacheFactoryException("Cache '{$engine_name}' is not a readable regular file at '{$class_path}'");
			
			include_once($class_path);

			self::$loadedEngines[$engine_name] = true;
		}
		
		
		// Create cache
		$cache = new $class_name();
		
		
		return $cache;
	}
}

?>
