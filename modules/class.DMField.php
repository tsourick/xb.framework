<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

class DMField
{
	private $dmEntity; // host DMEntity instance

	private $name; // string
	private $type; // string

	private $ormField; // ORMField
	
//	private $inputType;
//	private $inputDefault;

	
	// public function __construct(DMEntity $dmEntity, $name, $type, $ormTypeConfig, $inputType = 's', $inputDefault = NULL)
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DMEntity $dmEntity, $name, $type, $ormTypeConfig)
	{
		$this->dmEntity = $dmEntity;

		$this->name = $name;
		$this->type = $type;
		

		$ormTable = $dmEntity->ORMTable();
		$ormTypeConfig['name'] = $name;
		$ormField = ORMField::createFromConfig($ormTable, $ormTypeConfig);
		
		$this->ormField = $ormField;

		
//		$this->inputType = $inputType;
//		$this->inputDefault = $inputDefault;
		
		
		$dmEntity->addField($this);
	}
	
	/**
	*
	*
	* @return string
	*/

	public function getName()
	{
		return $this->name;
	}

	/**
	*
	*
	* @return string
	*/

	public function getPreparedName()
	{
		return $this->ormField->getPreparedName();
	}
	
	/**
	*
	*
	* @return string
	*/

	public function getFullName()
	{
		return $this->ormField->getFullName();
	}

	/**
	*
	*
	* @return string
	*/

	public function getPreparedFullName()
	{
		return $this->ormField->getPreparedFullName();
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getType()
	{
		return $this->type;
	}

	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setName($name)
	{
		if (strcmp($this->name, $name) != 0)
		{
			$this->ormField->setName($name);
			
			
			$old_name = $this->name;

			$this->name = $name;
			
			$this->dmEntity->remapField($this, $old_name); // Update field name upstream
		}
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setType($type)
	{
		if (strcmp($this->type, $type) != 0)
		{
			$dm_field_type_config = DMDataModel::$fieldTypeConfigs[$type];
			

			$orm_field_type_config = $dm_field_type_config['ormTypeConfig'];
			
			$this->ormField->setORMFieldType(ORMFieldType::createFromConfig($orm_field_type_config['type']));
			$this->ormField->setNull(isset($orm_field_type_config['null']) ? $orm_field_type_config['null'] : false);
			$this->ormField->setDefaultValue(isset($orm_field_type_config['default']) ? $orm_field_type_config['default'] : NULL);
			$this->ormField->setAutoincrement(isset($orm_field_type_config['auto_increment']) ? $orm_field_type_config['auto_increment'] : false);
			$this->ormField->setComment(isset($orm_field_type_config['comment']) ? $orm_field_type_config['comment'] : '');
			
			
			$this->type = $type;
		}
	}
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function entity()
	{
		return $this->dmEntity;
	}


	/**
	* @deprecated
	*/
	public function getORMField()
	{
		return $this->ormField;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function ORMField()
	{
		return $this->ormField;
	}
	
	/*
	public function getInputType()
	{
		return $this->inputType;
	}

	public function getInputDefault()
	{
		return $this->inputDefault;
	}

	
	public function getCGIValue()
	{
		$value = NULL;
		
		
		switch ($this->inputType)
		{
			case 'date':
				$value = get_cgi_date($this->name, $this->inputDefault);
			break;
			
			case 'time':
				$value = get_cgi_time($this->name, $this->inputDefault);
			break;
			
			case 'datetime':
				$value = get_cgi_datetime($this->name, $this->inputDefault);
			break;
			
			case 'file':
				$value = get_cgi_file($this->name);
			break;
			
			default:
				$value = get_cgi_value($this->name, $this->inputType, $this->inputDefault);
		}
		

		return $value;
	}
	*/
	
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function createFromConfig(DMEntity $dmEntity, $config)
	{
		// return new self($dmEntity, $config['name'], $config['type'], $config['ormTypeConfig'], $config['input_type'], $config['input_default']);
		return new self($dmEntity, $config['name'], $config['type'], $config['ormTypeConfig']);
	}
}


?>
