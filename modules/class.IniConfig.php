<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage ini
*/

require_once('class.IniConfigFileParser.php');


/**
* undocumented class
*/

class IniConfigException extends Exception
{
}

class IniConfig
{
	private $configFilePath;
	private $config;
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct($config_file_path)
 	{
		if (! is_readable($config_file_path)) throw new IniConfigException("Config file at '{$config_file_path}' is not readable");
		
		
		// Parse config file
		
		$data = IniConfigFileParser::parse($config_file_path);


		$this->configFilePath = $config_file_path;
		$this->config = $data;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getConfigFilePath()
	{
		return $this->configFilePath;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function get($path = NULL, $throw = true)
	{
		$value = NULL;


		if (is_null($path))
		{
			// Return all the options
			
			$value = $this->config;
		}
		else
		{
			// Return requested section or option
			
			if (strpos($path, '/') === false)
			{
				$section_name = $path;
				$option_name = '';
			}
			else
			{
				list($section_name, $option_name) = explode('/', $path);
			}
	
			if (! array_key_exists($section_name, $this->config))
			{
				if ($throw) throw new IniConfigException("Section '{$section_name}' not found in '{$this->configFilePath}'");
			}
			else
			{
				if (! empty($option_name))
				{
					if (! array_key_exists($option_name, $this->config[$section_name]))
					{
						if ($throw) throw new IniConfigException("Option '{$option_name}' not found in section '{$section_name}' in '{$this->configFilePath}'");
					}
					else
					{
						$value = $this->config[$section_name][$option_name];
					}
				}
				else $value = $this->config[$section_name];
			}
		}

		
		return $value;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function set($path, $value)
	{
		if (strpos($path, '/') === false)
		{
			$section_name = $path;
			$option_name = '';
		}
		else
		{
			list($section_name, $option_name) = explode('/', $path);
		}
		
		if (empty($section_name)) throw new IniConfigException("Section name is empty");

		if (empty($option_name))
		{
			if (! is_array($value)) throw new IniConfigException("Value passed for section is not an array");
			
			$this->config[$section_name] = $value;
		}
		else
		{
			if (! array_key_exists($section_name, $this->config))
			{
				$this->config[$section_name] = array();
			}

			$this->config[$section_name][$option_name] = $value;
		}
	}
}

?>
