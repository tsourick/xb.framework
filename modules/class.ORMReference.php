<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage orm
*/

class ORMReferenceException extends ORMException
{
}

class ORMReference
{
	private $ormTable; // Parent table reference
	
	private $name;
	
	private $type; // Name of type
	private $ormReferenceFields; // Array of table fields


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(ORMTable $ormTable, $name, $type, $ormReferenceFields = array())
	{
		$this->ormTable = $ormTable;
		
		$this->name = $name;
		
		$this->type = $type;
		$this->ormIndexFields = $ormIndexFields;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getName()
	{
		return $this->name;
	}

/**
*
	Config structure:
	array
	(
		'symbol' => '<symbol>',
		'index_name' => '<name of index>',
		'index_field_names' => array('<field name from index>', '<field name from index>', ... ),
		'table_name' => '<name of referenced table>',
		'table_field_names' => array('<field name from table>', '<field name from table>', ... ),
		'match' => '(FULL|PARTIAL|SIMPLE)',
		'ondelete' => '(RESTRICT|CASCADE|SET NULL|NO ACTION)',
		'onupdate' => '(RESTRICT|CASCADE|SET NULL|NO ACTION)'
	)
*/

}

?>
