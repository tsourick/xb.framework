<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
*/

require_once ('class.SQL.php');


/**
* Database-based session class (singleton)
*/

class SessionDB
{
	private static $instance;
	
	private static $name;
	private static $tableName = 'session';
	private $id;
	// private $custom_field_names = array('user_id', 'account_id');
	private static $custom_field_names = array();
	
	private $dbConnection = NULL;

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function getObject()
	{
		$object = NULL;
		
		if (! is_null(self::$instance))
		{
			$object = self::$instance;
		}
		
		return $object;
	}

	/**
	* Disables object cloning
	* Triggers E_USER_ERROR
	*/
	
	public function __clone()
	{
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}
	
	/**
	* Starts session.
	* Sets custom handlers, starts session, checks for session fixation attempt
	* and regenerates session id/restarts session if any.
	*/
	
	private function __construct($dbConnection)
	{
		if (! is_null(self::$instance)) throw new Exception(__CLASS__ . ' singleton already created');

		if (is_null($dbConnection)) throw new Exception("Connection not provided");
		
		$this->dbConnection = $dbConnection;

		$this->max_life_time = ini_get('session.gc_maxlifetime');

		if (empty(self::$name)) self::$name = 'sid';
		
		$this->set_handlers();
					
		session_name(self::$name);
		session_start();
		
		if (! isset($_SESSION['_']))
		{
			session_regenerate_id();
		
			$_SESSION['_'] = '';
			
			session_write_close();
			
			$this->set_handlers();
			
			session_name(self::$name);
			session_start();
		}
		
		$this->id = session_id();
		
		self::$instance = $this;
	}
	

	/**
	* Forces session data write down
	*/
	
	public function __destruct()
	{
		session_write_close();
	}
	
	
	/**
	* Returns class singleton
	*/
/*
	public static function singleton()
	{
		if (! isset(self::$instance))
		{
			$c = __CLASS__;
			self::$instance = new $c();
		}
		
		return self::$instance;
	}
*/   
	/**
	* Alias for singleton()
	*/
/*	
	public static function start()
	{
		return Session::singleton();
	}
*/
	public static function start($dbConnection)
	{
		return new SessionDB($dbConnection);
	}

	/**
	* Destroys session
	*/
	
	public function stop()
	{
		$_SESSION = array();
		
		if (isset($_COOKIE[self::$name]))
		{
				setcookie(self::$name, '', time() - 42000, '/');
		}

		session_destroy();
	}	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function setCustomFieldNames($names)
	{
		self::$custom_field_names = $names;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function setName($name)
	{
		self::$name = $name;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function getName()
	{
		return self::$name;
	}


	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function setTableName($name)
	{
		self::$tableName = $name;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function getTableName()
	{
		return self::$tableName;
	}

	/**
	* Session "open" handler
	*/
	
	public function _open($save_path, $session_name)
	{
		$db = $this->dbConnection;
		
		$lock_name = $this->getName() . $this->getId();
		$timeout = Framework::get('sessiondb/lock_timeout');

		$row = $db->getRowCustom('GET_LOCK("'.$lock_name.'", ' . $timeout . ') AS result');


		return true;
	}
	
	/**
	* Session "close" handler
	*/

	public function _close()
	{
		$db = $this->dbConnection;
		
		$lock_name = $this->getName() . $this->getId();

		$row = $db->getRowCustom('RELEASE_LOCK("'.$lock_name.'") AS result');


		return true;
	}
	
	/**
	* Session "read" handler
	*/
	
	public function _read($id)
	{
		$db = $this->dbConnection;
		
		$data = '';
		
		$_t = SQL::prepare_name(self::$tableName);
		$row = $db->query('SELECT data FROM ' . $_t . ' WHERE `id` = "' . $id . '" AND `created` >= DATE_ADD(now(), INTERVAL - ' . $this->max_life_time . ' SECOND)')->fetch();
		// dump('read from session');
		if (! empty($row)) $data = $row['data'];
		
		return $data;
	}
	
	/**
	* Session "write" handler
	*/
	
	public function _write($id, $data)
	{
		$db = $this->dbConnection;

		$db->beginTransaction();
		
		$_t = SQL::prepare_name(self::$tableName);

		list($cnt) = $db->query('SELECT COUNT(*) FROM ' . $_t . ' WHERE `id` = "' . $id . '"')->fetch();
		
		if ($cnt == 1)
		{
			$s = $db->prepare('UPDATE ' . $_t . ' SET `data` = ?, `created` = NOW() WHERE `id` = ?');
			$s->execute(array($data, $id));
		}
		else
		{
			$s = $db->prepare('INSERT INTO ' . $_t . ' (`id`, `data`) VALUES (?, ?)');
			$s->execute(array($id, $data));
		}

		$db->commit();
		
		return true;
	}
	
	/**
	* Session "destroy" handler
	*/

	public function _destroy($id)
	{
		$db = $this->dbConnection;

		$_t = SQL::prepare_name(self::$tableName);
		$s = $db->prepare('DELETE FROM ' . $_t . ' WHERE `id` = ?');
		$s->execute(array($id));
		
		return true;
	}
	
	/**
	* Session "gc" handler
	*/

	public function _gc($maxlifetime)
	{
		$db = $this->dbConnection;

		$_t = SQL::prepare_name(self::$tableName);
		$s = $db->prepare('DELETE FROM ' . $_t . ' WHERE `created` < DATE_ADD(now(), INTERVAL - ? SECOND)');
		$s->execute(array($maxlifetime));
		
		return true;
	}
	
	public function getId()
	{
		return session_id();
	}
	
	/**
	* Sets custom session handlers
	*/
	
	private function set_handlers()
	{
		session_set_save_handler
		(
			array($this, '_open'),
			array($this, '_close'),
			array($this, '_read'),
			array($this, '_write'),
			array($this, '_destroy'),
			array($this, '_gc')
		);
	}


	/**
	* Sets a value to store in a session
	*/
	
	public function set($name, $value = NULL)
	{
		$t = self::$tableName;
		
		
		if (is_array($name))
		{
			// Array of values
			
			$regular_values = array();
			$custom_field_values = array();
			
			foreach ($name as $_name => $_value)
			{
				if (in_array($_name, self::$custom_field_names))
				{
					$custom_field_values[$_name] = $_value;
				}
				else
				{
					$regular_values[$_name] = $_value;
				}
			}
			
			if (! empty($custom_field_values))
			{
				// custom fields
				
				$custom_field_values['id'] = $this->id;
				
				
				$db = $this->dbConnection;
		
				$db->beginTransaction();
				$db->saveRow($t, $custom_field_values, 'id');
				$db->commit();
			}
			
			if (! empty($regular_values))
			{
				// common storage
				
				$_SESSION = array_merge($_SESSION, $regular_values);
			}
		}
		else
		{
			// Single value
			
			if (in_array($name, self::$custom_field_names))
			{
				// custom fields
				
				$db = $this->dbConnection;
		
				$db->beginTransaction();
				$db->saveRow($t, array('id' => $this->id, $name => $value), 'id');
				$db->commit();
			}
			else
			{
				// common storage
				
				$_SESSION[$name] = $value;
			}
		}
	}

	/**
	* Clears value stored in a session
	*/
	
	public function clear($name)
	{
		if (in_array($name, self::$custom_field_names))
		{
			// custom fields
			
			$db = $this->dbConnection;
	
			$db->beginTransaction();
		
			$t = self::$tableName;
			$_t = SQL::prepare_name($t);
		
			$cnt = $db->getRowCount($t, array('id' => $this->id));
			
			if ($cnt == 1)
			{
				$s = $db->prepare('UPDATE ' . $_t . ' SET `' . $name . '` = NULL WHERE `id` = ?');
				$s->execute(array($this->id));
			}

			$db->commit();
		}
		else
		{
			unset($_SESSION[$name]);
		}
	}

	/**
	* Returns value stored in a session
	*/
	
	public function get($name)
	{
		$value = NULL;
		
		if (in_array($name, self::$custom_field_names))
		{
			// custom fields
			
			$db = $this->dbConnection;
	
			$t = self::$tableName;
			
			$row = $db->getRow($name, $t, array('id' => $this->id));

			if (! empty($row)) $value = $row[$name];
		}
		else
		{
			if (isset($_SESSION[$name])) $value = $_SESSION[$name];
		}

		return $value;
	}
}
?>
