<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
*
*
* Framework config options:
*   use_subdirs - split files on create into subdirectories made from two frist letters of the file physical name; default is false
*   use_subdirs_only - if using subdirs also check for the root directory itself when reading; default is false
*
*
*/

/**
* undocumented class
*/

class FileMassStorageException extends Exception
{
}

class FileMassStorageHelper
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function getMimeType($path)
	{
		$mt = '';
		
		// try mime_content_type()
		if (function_exists('mime_content_type'))
		{
			$mt = mime_content_type($path);
		}
		
		// try getimagesize()
		if (empty($mt))
		{
			if ($info = @getimagesize($path))
			{
				if (! empty($info['mime'])) $mt = $info['mime'];
			}
		}
		
		// if (empty($mt)) $mt = 'application/octet-stream';
		
		return $mt;
	}
	
	
	static private function makeNestedFilePath($dir_path, $file_name)
	{
		return dirpath($dir_path) . dirpath(strtolower(substr($file_name, 0, 1))) . dirpath(strtolower(substr($file_name, 1, 1))) . $file_name;
	}
	
	
	/**
	* Makes file physical path with respect to 'use_subdirs', 'use_subdirs_only' options.
	*/
	static public function createFilePhysicalPath($dir_path, $file_name)
	{
		$path = NULL;
		
		
		$dir_path = dirpath($dir_path);
		
		
		$use_subdirs = Framework::get('filemassstorage/use_subdirs', false, false);
		if ($use_subdirs)
		{
			// $path = $dir_path . dirpath(substr($file_name, 0, 1)) . dirpath(substr($file_name, 1, 1)) . $file_name;
			$path = self::makeNestedFilePath($dir_path, $file_name);
			
			fs_create_directory(dirname($path)); // create full path (i.e. two subdirs) unconditionally (if new file will fail these subdirs will be retained)
		}
		else
		{
			$path = $dir_path . $file_name;
		}
		
		
		if (! fs_create_new_file($path)) $path = NULL;
		
		
		return $path;
	}
	
	
	/**
	* Searches for a file within given directory and/or subdirs with respect to 'use_subdirs', 'use_subdirs_only' options.
	* 
	* If 'use_subdirs' is true then the names of subdirs are made of two first letters of the file name as
	* <dir path>/<first letter>/<second letter>/<file name>.
	*
	* If 'use_subdirs_only' is false then both root dir and subsubdir are checked.
	*
	* @param
	* @param
	*
	* @return
	*/

	static public function discoverFilePhysicalPath($dir_path, $file_name)
	{
		$path = NULL;
		
		
		$dir_path = dirpath($dir_path);
		
		
		$use_subdirs = Framework::get('filemassstorage/use_subdirs', false, false);
		if ($use_subdirs)
		{
			// $_path = $dir_path . dirpath(substr($file_name, 0, 1)) . dirpath(substr($file_name, 1, 1)) . $file_name;
			$_path = self::makeNestedFilePath($dir_path, $file_name);
			
			$use_subdirs_only = Framework::get('filemassstorage/use_subdirs_only', false, false);
			if ($use_subdirs_only)
			{
				if (file_exists($_path)) $path = $_path;
			}
			else
			{
				if (file_exists($_path)) $path = $_path;
				else
				{
					$_path = $dir_path . $file_name;
					
					if (file_exists($_path)) $path = $_path;
				}
			}
		}
		else
		{
			$_path = $dir_path . $file_name;
			
			if (file_exists($_path)) $path = $_path;
		}
		
		
		return $path;
	}
}


class FileMassStorageItem
{
	protected $name;
	private $size;
	private $mimeType;
	private $data;

	protected $defaultMimeType = 'application/octet-stream';

	private $dbConnection = NULL;
	private $tableName;
	private $storageDir;
	
	private $id;
	private $physicalName;
	
	private $contents = null;
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DBConnection $db_connection, $table_name, $storage_dir, $id)
	{
		$metaInfo = $db_connection->getRowAll($table_name, compact('id'));

		if (empty($metaInfo)) throw new FileMassStorageException("Record id:{$id} not found in '{$table_name}'");
		
		$this->name = $metaInfo['name'];
		$this->size = $metaInfo['size'];
		$this->mimeType = $metaInfo['mime_type'];
		$this->data = ! empty($metaInfo['data']) ? unserialize($metaInfo['data']) : array();
		
		$this->id = $id;
		$this->physicalName = $metaInfo['physical_name'];

		$this->dbConnection = $db_connection;
		$this->tableName = $table_name;
		$this->storageDir = $storage_dir;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getId()
	{
		return $this->id;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getName()
	{
		return $this->name;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setSize($size)
	{
		$this->size = $size;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getSize()
	{
		return $this->size;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPhysicalName()
	{
		return $this->physicalName;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getPhysicalPath()
	{
		$path = FileMassStorageHelper::discoverFilePhysicalPath($this->storageDir, $this->physicalName);
		
		// return $this->storageDir . $this->physicalName;
		return $path;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setMimeType($mime_type)
	{
		$this->mimeType = $mime_type;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getMimeType()
	{
		return $this->mimeType;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setDefaultMimeType($mime_type)
	{
		$this->defaultMimeType = $mime_type;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getDefaultMimeType()
	{
		return $this->defaultMimeType;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getData()
	{
		return $this->data;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setData($data)
	{
		$this->data = $data;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function save()
	{
		$id = $this->getId();
		$name = $this->getName();
		$size = $this->getSize();
		$mime_type = $this->getMimeType();
		$data = $this->getData();
		if (! empty($data)) $data = serialize($data);

		
		// $row = $this->dbConnection->getRowAll($this->tableName, compact('id'));
		
		// if (empty($row)) throw new FileMassStorageException("File record [{$id}] not found in '{$this->tableName}'");
		
		// update

		// file_put_contents($this->storageDir . $row['physical_name'], $this->getContents());
		file_put_contents($this->getPhysicalPath(), $this->getContents());
		
			
		$this->dbConnection->update($this->tableName, compact('name', 'size', 'mime_type', 'data'), compact('id'));

		return $id;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setContents($contents)
	{
		$this->setSize(strlen($contents));
		
		$this->contents = $contents;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getContents()
	{
		if (is_null($this->contents))
		{
			// $this->contents = file_get_contents($this->storageDir . $this->physicalName);
			$path = $this->getPhysicalPath();
			
			if (is_null($path)) throw new FileMassStorageException("File '{$this->physicalName}' not found within storage dir '{$this->storageDir}'");
			
			
			$this->contents = file_get_contents($path);
		}
		
		return $this->contents;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function send($disposition = 'inline', $cache = false, $exit = true)
	{
		// $content = file_get_contents($this->storageDir . $this->physicalName);
		$content = $this->getContents();

		if (! $cache) xbf_http_nocache();

		xbf_http_send_file($this->getName(), $this->getMimeType(), $content, $this->getSize(), $disposition, $exit);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	protected function getFileData($path)
	{
		return '';
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function updateFromFile($path, $name = '')
	{
		clearstatcache();
		if (! is_readable($path)) throw new FileMassStorageException("'{$path}' is not readable");
		
		$id = $this->getId();

		if (empty($name)) $name = basename($path);
		$size = filesize($path);
		$mime_type = FileMassStorageHelper::getMimeType($path);
		if (empty($mime_type)) $mime_type = $this->defaultMimeType;
		$data = $this->getFileData($path);
		if (! empty($data)) $data = serialize($data);


		// update

		//$physical_name = $this->getPhysicalName();
		
		//$target_path = $this->storageDir . $physical_name;
		
		$target_path = $this->getPhysicalPath();

		if (! @copy($path, $target_path)) throw new FileMassStorageException("File copy failed from '{$path}' to '{$target_path}'");


		$r = $this->dbConnection->update($this->tableName, compact('name', 'size', 'mime_type', 'data'), compact('id'));

		// if (! $r) throw new FileMassStorageException("File record ('{$target_path}') update failed for record [$id] in '{$this->tableName}'");
		

		return $id;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function saveToFile($target_dir_path, $name = '')
	{
		$target_dir_path = dirpath(realpath($target_dir_path));

		clearstatcache();
		if (! is_writable($target_dir_path)) throw new FileMassStorageException("'{$path}' is not writtable");
		
		// Save

		// $physical_name = $this->getPhysicalName();
		// $source_path = $this->storageDir . $physical_name;
		$source_path = $this->getPhysicalPath();

		$target_name = ! empty($name) ? $name : $this->getName();
		$target_path = $target_dir_path . $target_name;

		if (! @copy($source_path, $target_path)) throw new FileMassStorageException("File copy failed from '{$source_path}' to '{$target_path}'");
	}
}

class FileMassStorage
{
	protected $dbConnection = NULL;
	protected $tableName;

	protected $defaultMimeType = 'application/octet-stream';

	protected $storageDir;
	

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DBConnection $db_connection, $table_name, $storage_dir)
	{
		$this->dbConnection = $db_connection;
		$this->tableName = $table_name;
		
		clearstatcache();
		if (! is_writable($storage_dir)) throw new FileMassStorageException("'{$storage_dir}' is not writable");

		$this->storageDir = $storage_dir;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function setDefaultMimeType($mime_type)
	{
		$this->defaultMimeType = $mime_type;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getDefaultMimeType()
	{
		return $this->defaultMimeType;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getItem($id)
	{
		return new FileMassStorageItem($this->dbConnection, $this->tableName, $this->storageDir, $id);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	protected function getFileData($path)
	{
		return '';
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/
	/*
	public function saveItem(FileMassStorageItem $item)
	{
		$name = $item->getName();
		$size = $item->getSize();
		$mime_type = $item->getMimeType();
		$data = $item->getData();
		if (! empty($data)) $data = serialize($data);

		$row = $this->dbConnection->getRowAll($this->tableName, compact('name'));
		
		if (! empty($row))
		{
			// update

			file_put_contents($this->storageDir . $row['physical_name'], $item->getContents());
			
			$this->dbConnection->update($this->tableName, compact('name', 'size', 'mime_type', 'data'), array('id' => $row['id']));

			$id = $row['id'];
		}
		else
		{
			$physical_name = str_rand(32);

			file_put_contents($this->storageDir . $physical_name, $item->getContents());
			
			$this->dbConnection->ins($this->tableName, compact('name', 'size', 'mime_type', 'physical_name', 'data'));

			$id = $this->dbConnection->lastInsertId();
		}
		
		return new FileMassStorageItem($this->dbConnection, $this->tableName, $this->storageDir, $id);
	}
	*/
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function createItemFromFile($path, $name = '')
	{
		clearstatcache();
		if (! is_readable($path)) throw new FileMassStorageException("'{$path}' is not readable");
		
		if (empty($name)) $name = basename($path);
		$size = filesize($path);
		$mime_type = FileMassStorageHelper::getMimeType($path);
		if (empty($mime_type)) $mime_type = $this->defaultMimeType;
		$physical_name = str_rand(32);
		// $data = '';
		$data = $this->getFileData($path);
		if (! empty($data)) $data = serialize($data);
		
		// $target_path = $this->storageDir . $physical_name;
		$target_path = FileMassStorageHelper::createFilePhysicalPath($this->storageDir, $physical_name);

		if (! @copy($path, $target_path)) throw new FileMassStorageException("File copy failed from '{$path}' to '{$target_path}'");
		
		
		$this->dbConnection->ins($this->tableName, compact('name', 'size', 'mime_type', 'physical_name', 'data'));
		
		$id = $this->dbConnection->lastInsertId();
		
		return new FileMassStorageItem($this->dbConnection, $this->tableName, $this->storageDir, $id);
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function createItemFromString($string, $name)
	{
		// clearstatcache();
		// if (! is_readable($path)) throw new FileMassStorageException("'{$path}' is not readable");
		
		// if (empty($name)) $name = basename($path);
		// $size = filesize($path);
		$size = strlen($string);
		// $mime_type = FileMassStorageHelper::getMimeType($path);
		// if (empty($mime_type)) $mime_type = $this->defaultMimeType;
		$mime_type = $this->defaultMimeType;
		$physical_name = str_rand(32);
		$data = '';
		// $data = $this->getFileData($path);
		// if (! empty($data)) $data = serialize($data);
		// wechos(substr($string, 0, 100) . ' ... ' . substr($string, -100));
		// $target_path = $this->storageDir . $physical_name;
		$target_path = FileMassStorageHelper::createFilePhysicalPath($this->storageDir, $physical_name);

		// if (! @copy($path, $target_path)) throw new FileMassStorageException("File copy failed from {$path} to {$target_path}");

		if (false === @file_put_contents($target_path, $string)) throw new FileMassStorageException("File write failed to '{$target_path}'");
		
		
		$this->dbConnection->ins($this->tableName, compact('name', 'size', 'mime_type', 'physical_name', 'data'));
		
		$id = $this->dbConnection->lastInsertId();
		
		return new FileMassStorageItem($this->dbConnection, $this->tableName, $this->storageDir, $id);
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function deleteItems($ids)
	{
		if (! is_array($ids)) $ids = array($ids);
		
		
		$physical_names = $this->dbConnection->getRows('physical_name', $this->tableName, SQL::in('id', $ids), '', '', NULL, '', 'physical_name');


		$this->dbConnection->delete($this->tableName, NULL, SQL::in('id', $ids));

		
		foreach ($physical_names as $n)
		{
			// $file_path = $this->storageDir . $n;
			$file_path = FileMassStorageHelper::discoverFilePhysicalPath($this->storageDir, $n);

			if (! @unlink($file_path)) throw new FileMassStorageException("Could not delete file '{$n}' within storage dir '{$this->storageDir}' at path '{$file_path}'");
		}
	}
}



class ImageMassStorageItem extends FileMassStorageItem
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	protected function getFileData($path)
	{
		$data = '';
		
		if ($info = @getimagesize($path))
		{
			$data = array('width' => $info[0], 'height' => $info[1]);
		}
		
		return $data;
	}
}

class ImageMassStorage extends FileMassStorage
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	protected function getFileData($path)
	{
		$data = '';
		
		if ($info = @getimagesize($path))
		{
			$data = array('width' => $info[0], 'height' => $info[1]);
		}
		
		return $data;
	}
}

?>
