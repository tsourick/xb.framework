<?php


class MMLWriter
{
	protected $file_path;
	protected $source_encoding;
	protected $target_encoding;
	
	
	public function __construct($file_path, $source_encoding = 'utf-8', $target_encoding = 'utf-8')
	{
		if (! is_writable($file_path)) throw new Exception("File is not writable at '{$file_path}'");
			
		$this->file_path = $file_path;
		$this->source_encoding = $source_encoding;
		$this->target_encoding = $target_encoding;
	}
	
	
	protected function encodeString($s)
	{
		$s = trim($s);

		$from = array('"', '&', '>', '<', '\'', );
	
		$to = array('&quot;', '&amp;', '&gt;', '&lt;', '&apos;');
		
		return str_replace($from, $to, $s);
	}
	
	
	public function write($s)
	{
		if (file_put_contents($this->file_path, $s, FILE_APPEND) === false) throw new Exception("Could not write to '{$this->file_path}'");
	}
}
