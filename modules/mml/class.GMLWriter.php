<?php


Framework::useClass('MMLWriter', 'mml');


class GMLWriter extends MMLWriter implements MMLWriterInterface
{
	public function writeStart()
	{
		$s = <<<S
<?xml version="1.0" encoding="{$this->target_encoding}"?>
<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
	<channel>
S;
		
		$this->write($s);
	}
	
	public function writeShopInfo($data)
	{
		$data = array_slice_values($data, 'title, link, description');
		
		foreach ($data as $k => $v)
		{
			$v = $this->encodeString($v);
			$this->write("<$k>$v</$k>");
		}
	}

	public function writeCurrencies($list)
	{
		// interface stub
	}
	
	public function writeCategories($list)
	{
		// interface stub
	}
	
	public function writeOffersStart()
	{
		// interface stub
	}
	
	public function writeOffersEnd()
	{
		// interface stub
	}

	public function writeOffer($data, $params = array(), $extra = array())
	{
		$title = $this->encodeString($data['title']);
		$link = $this->encodeString($data['link']);
		$description = $this->encodeString($data['description']);

		$id = $data['id'];
		$condition = $data['condition'];
		$price = $data['price'];
		$availability = $data['availability'];
		$category = $this->encodeString($data['category']);
		$image_link = $this->encodeString($data['image_link']);

		$gtin = $data['gtin'];
		$brand = $this->encodeString($data['brand']);
		$mpn = $this->encodeString($data['mpn']);
		$quantity = $data['quantity'];
		
		// TODO: shipping


		$this->write('<item>');

		$this->write
		(
"<title>{$title}</title>
<link>{$link}</link>
<description>{$description}</description>
<g:id>{$id}</g:id>
<g:condition>{$condition}</g:condition>
<g:price>{$price}</g:price>
<g:availability>{$availability}</g:availability>
<g:google_product_category>{$category}</g:google_product_category>
<g:image_link>{$image_link}</g:image_link>
<g:gtin>{$gtin}</g:gtin>
<g:brand>{$brand}</g:brand>
<g:mpn>{$mpn}</g:mpn>
<g:quantity>{$quantity}</g:quantity>
");

		$this->write('</item>' . "\n\n");
	}
	
	public function writeEnd()
	{
		$s = '
	</channel>
</rss>
';
		
		$this->write($s);
	}
}

