<?php


Framework::useClass('MMLWriter', 'mml');


class YMLWriter extends MMLWriter implements MMLWriterInterface
{
	public function writeStart()
	{
		$date = date('Y-m-d H:i');
		
		$s = <<<S
<?xml version="1.0" encoding="{$this->target_encoding}"?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">

<yml_catalog date="{$date}">
	<shop>
S;
		
		$this->write($s);
	}
	
	public function writeShopInfo($data)
	{
		$data = array_slice_values($data, 'name, company, url, local_delivery_cost');
		
		foreach ($data as $k => $v)
		{
			$v = $this->encodeString($v);
			$this->write("<$k>$v</$k>");
		}
	}
	
	public function writeCurrencies($list)
	{
		$this->write("<currencies>");
		
		foreach ($list as $item)
		{
			$this->write("<currency id=\"{$item['id']}\" rate=\"{$item['rate']}\" plus=\"{$item['plus']}\" />");
		}
		
		$this->write("</currencies>");
	}
	
	public function writeCategories($list)
	{
		$this->write("<categories>");
		
		foreach ($list as $item)
		{
			$_parent_id = ! empty($item['parent_id']) ? "parentId=\"{$item['parent_id']}\"" : '';
			$this->write("<category id=\"{$item['id']}\" {$_parent_id}>{$item['name']}</category>");
		}
		
		$this->write("</categories>");
	}

	public function writeOffersStart()
	{
		$this->write('<offers>');
	}
	
	public function writeOffersEnd()
	{
		$this->write('</offers>');
	}
	
	
	private function encodeWriteIf($data, $name, $tag = null)
	{
		if (isset($data[$name]))
		{
			$tag = $tag ?: $name;
			
			$value = $this->encodeString($data[$name]);

			$s = '<'.$tag.'>'.$value.'</'.$tag.'>';
			
			$this->write($s);
		}
	}
	
	public function writeOffer($data, $params = array(), $extra = array())
	{
		$id = $data['id'];
		$available = $data['available'] ? 'true' : 'false';
		$url = $this->encodeString($data['url']);
		$price = $data['price'];
		$name = $this->encodeString($data['name']);
		$description = $this->encodeString($data['description']);
		$vendor = $this->encodeString($data['vendor']);
		$vendor_code = $this->encodeString($data['vendor_code']);
		
		$category_id = $data['categoryId'];
		$picture = isset($data['picture']) ? $this->encodeString($data['picture']) : NULL;
		$store = isset($data['store']) ? ($data['store'] ? 'true' : 'false') : NULL;
		$pickup = isset($data['pickup']) ? ($data['pickup'] ? 'true' : 'false') : NULL;
		$delivery = isset($data['delivery']) ? ($data['delivery'] ? 'true' : 'false') : NULL;
		$local_delivery_cost = isset($data['local_delivery_cost']) ? $this->encodeString($data['local_delivery_cost']) : NULL;
		$sales_notes = isset($data['sales_notes']) ? $this->encodeString($data['sales_notes']) : NULL;
		$market_category = $this->encodeString($data['market_category']);

		$this->write('<offer id="'.$id.'" available="'.$available.'">');

		$this->write
		(
"<url>{$url}</url>
<price>{$price}</price>
<currencyId>RUR</currencyId>
<categoryId>{$category_id}</categoryId>
<market_category>{$market_category}</market_category>
");

		if (! is_null($picture))
		{
			$this->write(
"<picture>{$picture}</picture>
");
		}
		
		if (! is_null($store)) $this->write("<store>{$store}</store>");
		if (! is_null($pickup)) $this->write("<pickup>{$pickup}</pickup>");
		if (! is_null($delivery)) $this->write("<delivery>{$delivery}</delivery>");
		if (! is_null($local_delivery_cost)) $this->write("<local_delivery_cost>{$local_delivery_cost}</local_delivery_cost>");
		
		$this->write
		(
"<name>{$name}</name>
<vendor>{$vendor}</vendor>
<vendorCode>{$vendor_code}</vendorCode>
<description>{$description}</description>
"
		);

		
		$this->encodeWriteIf($extra, 'typePrefix');
		$this->encodeWriteIf($extra, 'model');
		
		
		if (!is_null($sales_notes))
		{
			$this->write
			(
"<sales_notes>{$sales_notes}</sales_notes>
"		
			);
		}
		
		foreach ($params as $k => $v)
		{
			if ($v == '') continue; // skip empty strings
				
			$k = $this->encodeString($k);
			$v = $this->encodeString($v);
			$this->write("<param name=\"{$k}\">{$v}</param>");
		}

		$this->write('</offer>' . "\n\n");
	}
	
	public function writeEnd()
	{
		$s = '
			</shop>
		</yml_catalog>
		';
		
		$this->write($s);
	}
}
