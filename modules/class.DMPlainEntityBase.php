<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

class DMPlainEntityBase extends DMEntity
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DMDataModel $dm, $name, $dmFields = array())
	{
		parent::__construct($dm, $name, $dmFields);

		
		// Predefined fields
		
		// Preset PRIMARY key field

		$dmField = $this->createFieldFromConfig(array('name' => 'id', 'type' => 'pri_id'));
		
		
		$this->dmPrimaryFields[] = $dmField;
		
		
		$this->ormTable->addIndex
		(
			ORMIndex::createFromConfig
			(
				$this->ormTable,
				array
				(
					'name' => 'PRIMARY',
					'type' => 'PRIMARY',
					'fields' => array
					(
						array('name' => 'id')
					)
				)
			)
		);
	}
}

?>
