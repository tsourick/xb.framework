<?php

/**
* deprecated class
*/

class GMLExportToFile
{
	private $file_path;
	
	
	public function __construct($file_path, $source_encoding = 'utf-8', $target_encoding = 'utf-8')
	{
		if (! is_writable($file_path)) throw new Exception("File is not writable at '{$file_path}'");
			
		$this->file_path = $file_path;
		$this->source_encoding = $source_encoding;
		$this->target_encoding = $target_encoding;
	}
	
	
	private function encodeString($s)
	{
		$s = trim($s);

		$from = array('"', '&', '>', '<', '\'', );
	
		$to = array('&quot;', '&amp;', '&gt;', '&lt;', '&apos;');
		
		return str_replace($from, $to, $s);
	}
	

	public function write($s)
	{
		if (file_put_contents($this->file_path, $s, FILE_APPEND) === false) throw new Exception("Could not write to '{$this->file_path}'");
	}
	
	
	public function writeStart()
	{
		$s = <<<S
<?xml version="1.0" encoding="{$this->target_encoding}"?>
<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
	<channel>
S;
		
		$this->write($s);
	}
	
	public function writeShopInfo($data)
	{
		$data = array_slice_values($data, 'title, link, description');
		
		foreach ($data as $k => $v)
		{
			$v = $this->encodeString($v);
			$this->write("<$k>$v</$k>");
		}
	}
	/*
	public function writeCurrencies($list)
	{
		$this->write("<currencies>");
		
		foreach ($list as $item)
		{
			$this->write("<currency id=\"{$item['id']}\" rate=\"{$item['rate']}\" plus=\"{$item['plus']}\" />");
		}
		
		$this->write("</currencies>");
	}
	
	public function writeCategories($list)
	{
		$this->write("<categories>");
		
		foreach ($list as $item)
		{
			$_parent_id = ! empty($item['parent_id']) ? "parentId=\"{$item['parent_id']}\"" : '';
			$this->write("<category id=\"{$item['id']}\" {$_parent_id}>{$item['name']}</category>");
		}
		
		$this->write("</categories>");
	}
	*/
	
/*
	public function writeOffersStart()
	{
		$this->write('<offers>');
	}
	
	public function writeOffersEnd()
	{
		$this->write('</offers>');
	}
	*/
	public function writeItem($data, $params = array())
	{
		$title = $this->encodeString($data['title']);
		$link = $this->encodeString($data['link']);
		$description = $this->encodeString($data['description']);

		$id = $data['id'];
		$condition = $data['condition'];
		$price = $data['price'];
		$availability = $data['availability'];
		$category = $this->encodeString($data['category']);
		$image_link = $this->encodeString($data['image_link']);

		$gtin = $data['gtin'];
		$brand = $this->encodeString($data['brand']);
		$mpn = $this->encodeString($data['mpn']);
		$quantity = $data['quantity'];
		
		// TODO: shipping


		$this->write('<item>');

		$this->write
		(
"<title>{$title}</title>
<link>{$link}</link>
<description>{$description}</description>

<g:id>{$id}</g:id>
<g:condition>{$condition}</g:condition>
<g:price>{$price}</g:price>
<g:availability>{$availability}</g:availability>
<g:google_product_category>{$category}</g:google_product_category>
<g:image_link>{$image_link}</g:image_link>
<g:gtin>{$gtin}</g:gtin>
<g:brand>{$brand}</g:brand>
<g:mpn>{$mpn}</g:mpn>
<g:quantity>{$quantity}</g:quantity>
");

		$this->write('</item>');
	}
	
	public function writeEnd()
	{
		$s = '
	</channel>
</rss>
';
		
		$this->write($s);
	}
}

?>
