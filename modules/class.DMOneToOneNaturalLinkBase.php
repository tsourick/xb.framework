<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage datamodel
*/

class DMOneToOneNaturalLinkBase extends DMLinkThroughTable
{
	private $dmFieldPairs = array(); // from-to link field pairs
	
	// private $ormFromFields = array();
	// private $ormToFields = array();

	// private $fromFieldPairs = array(); // (0: (<from DMField>), 1: (<to link ORMField>)), ...
	// private $toFieldPairs = array(); // (0: (<from DMField>), 1: (<to link ORMField>)), ...
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function __construct(DMDataModel $dm, $name, DMEntity $from_entity, $from_required, DMEntity $to_entity, $to_required, $custom_field_map = NULL)
	// public function __construct(DMDataModel $dm, $config)
	{
		// parent::__construct($dm, $config);
		parent::__construct($dm, $name, $from_entity, $from_required, $to_entity, $to_required, $custom_field_map);
		
		
		$this->type = '1:1 natural';
		
		
		// $from_entity = $dm->entity($config['from']);
		// $to_entity = $dm->entity($config['to']);


		if (! $from_required && ! $to_required)
		{
			// Create link table
			
			$this->createLinkTable($from_entity, $to_entity);
			
			
			// Add indexes to link table
			
			$index_config = array();
			
			$index_config['name'] = $from_entity->getName();
			$index_config['type'] = 'UNIQUE';
			
			$from_fields = $from_entity->getPrimaryFields();
			foreach ($from_fields as $from_field)
			{
				$index_config['fields'][]['name'] = $from_entity->getName() . '_' . $from_field->getName();
			}
			
			$this->ormTable->createIndexFromConfig($index_config);


			$index_config = array();
			
			$index_config['name'] = $to_entity->getName();
			$index_config['type'] = 'UNIQUE';
			
			$to_fields = $to_entity->getPrimaryFields();
			foreach ($to_fields as $to_field)
			{
				$index_config['fields'][]['name'] = $to_entity->getName() . '_' . $to_field->getName();
			}
			
			$this->ormTable->createIndexFromConfig($index_config);
		}
		else
		{
			// Scan primary indexes, store link fields
			
			
			$from_fields = $from_entity->getPrimaryFields();
			$to_fields = $to_entity->getPrimaryFields();

			
			// Check field names
			
			$from_field_names = array();
			foreach ($from_fields as $from_field)
			{
				$from_field_names[] = $from_field->getName();
			}
			
			$to_field_names = array();
			foreach ($to_fields as $to_field)
			{
				$to_field_names[] = $to_field->getName();
			}
			
			sort($from_field_names);
			sort($to_field_names);
			
			if ($from_field_names != $to_field_names) throw new DMLinkException("Could not link entities " . $from_entity->getName() . " and " . $to_entity->getName() . " with OneToOneLink because primary key fields differ.");


			// Save field pairs
			foreach ($from_field_names as $field_name)
			{
				$from_field = $from_entity->field($field_name);
				$to_field = $to_entity->field($field_name);
				
				$this->dmFieldPairs[$field_name] = array($from_field, $to_field);
			}
		}

		
		// Register link in entities through endpoints

		$from_entity->addLinkEndPoint(new DMLinkEndPoint($this, true, false));
		$to_entity->addLinkEndPoint(new DMLinkEndPoint($this, false, true));
		
		
		// Finalize construction
		
		$this->fromEntity = $from_entity;
		$this->fromRequired = $from_required;
		
		$this->toEntity = $to_entity;
		$this->toRequired = $to_required;
	}
	
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function checkAndAdjustForDelete($actOn, $condition = NULL)
	{
		// wechos('checkAndAdjustForDelete: ');
		// wdebug($condition);
		if ($actOn == 'from')
		{
			$local_entity = $this->fromEntity;
			$foreign_entity = $this->toEntity;
			$foreign_entity_required = $this->toRequired;
			
			$local_entity_name = $this->fromEntity->getName();
			$foreign_entity_name = $this->toEntity->getName();
			
			if (! is_null($this->ormTable)) // ! $config['from_required'] && ! $config['to_required']
			{
				$local_field_pairs =& $this->fromFieldPairs;
				$foreign_field_pairs =& $this->toFieldPairs;
			}
			else
			{
				$field_pairs = $this->dmFieldPairs;
			}
		}
		else
		{
			$local_entity = $this->toEntity;
			$foreign_entity = $this->fromEntity;
			$foreign_entity_required = $this->fromRequired;

			$local_entity_name = $this->toEntity->getName();
			$foreign_entity_name = $this->fromEntity->getName();

			if (! is_null($this->ormTable)) // ! $config['from_required'] && ! $config['to_required']
			{
				$local_field_pairs =& $this->toFieldPairs;
				$foreign_field_pairs =& $this->fromFieldPairs;
			}
			else
			{
				$field_pairs = $this->dmFieldPairs;
			}
		}
		
		
		// Get local entity items' (deleted items') pk values
		if (! is_null($this->ormTable))
		{
			$local_entity_pk_names = array();
			
			foreach ($local_field_pairs as $field_pair)
			{
				$local_entity_pk_names[] = $field_pair[0]->getName(); // dmField
			}
			
			$local_entity_pk_values = $local_entity->getItemDataList($local_entity_pk_names, $condition);
		}
		else
		{
			$local_entity_pk_names = array();
			
			foreach ($field_pairs as $field_name => $field_pair)
			{
				$local_entity_pk_names[] = $field_name;
			}
			
			$local_entity_pk_values = $local_entity->getItemDataList($local_entity_pk_names, $condition);
		}
		
		
		if (! empty($local_entity_pk_values)) // there is something to delete
		{
			if (! is_null($this->ormTable)) // ! $config['from_required'] && ! $config['to_required'] => link table in use
			{
				// Create link condition
				
				$link_condition = array();
				
				foreach ($local_entity_pk_values as $pk_data)
				{
					$and_condition = array();
					foreach ($local_field_pairs as $field_pair)
					{
						$entity_field = $field_pair[0];
						$link_field = $field_pair[1];
						
						$and_condition[] = $link_field->getPreparedName() . ' = ' . SQL::prepare_value($pk_data[$entity_field->getName()]);
					}
					
					assert(! empty($and_condition)); // prevent delete all the table data
					
					$link_condition[] = implode(' AND ', $and_condition);
				}
				
				assert(! empty($link_condition)); // prevent delete all the table data
				
				$link_condition = '(' . implode(') OR (', $link_condition) . ')';
	
	
				// Delete links
				
				$this->ormTable->delete($link_condition);
			}
			else
			{
				if ($foreign_entity_required)
				{
					// Create foreign condition
					
					$foreign_condition = array();
					
					foreach ($local_entity_pk_values as $pk_data)
					{
						$and_condition = array();
						foreach ($field_pairs as $field_name => $field_pair)
						{
							$from_field = $field_pair[0];
							$to_field = $field_pair[1];
							
							$and_condition[] = $to_field->ORMField()->getPreparedName() . ' = ' . SQL::prepare_value($pk_data[$field_name]);
						}
						
						assert(! empty($and_condition)); // prevent delete all the table data
						
						$foreign_condition[] = implode(' AND ', $and_condition);
					}
					
					assert(! empty($foreign_condition)); // prevent delete all the table data
					
					$foreign_condition = '(' . implode(') OR (', $foreign_condition) . ')';
					

					// Delete bound items
					$foreign_entity->_deleteItemData($foreign_condition);
				}
			}
		}
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function checkAndAdjustForInsert($actOn, $data)
	{
		if ($actOn == 'from')
		{
			$local_entity = $this->fromEntity;
			$foreign_entity = $this->toEntity;

			$local_entity_required = $this->fromRequired;
			$foreign_entity_required = $this->toRequired;
			
			$local_entity_name = $this->fromEntity->getName();
			$foreign_entity_name = $this->toEntity->getName();
			
			if (! is_null($this->ormTable)) // ! $config['from_required'] && ! $config['to_required']
			{
				$local_field_pairs =& $this->fromFieldPairs;
				$foreign_field_pairs =& $this->toFieldPairs;
			}
			else
			{
				$field_pairs = $this->dmFieldPairs;
			}
		}
		else
		{
			$local_entity = $this->toEntity;
			$foreign_entity = $this->fromEntity;

			$local_entity_required = $this->toRequired;
			$foreign_entity_required = $this->fromRequired;

			$local_entity_name = $this->toEntity->getName();
			$foreign_entity_name = $this->fromEntity->getName();

			if (! is_null($this->ormTable)) // ! $config['from_required'] && ! $config['to_required']
			{
				$local_field_pairs =& $this->toFieldPairs;
				$foreign_field_pairs =& $this->fromFieldPairs;
			}
			else
			{
				$field_pairs = $this->dmFieldPairs;
			}
		}
		
		
		if (! is_null($this->ormTable)) // ! $config['from_required'] && ! $config['to_required'] => link table in use
		{
			// Check dependency if any
			
			$foreign_key_values = array();
				
			if (isset($data[$local_entity_name]['depends']) && isset($data[$local_entity_name]['depends'][$foreign_entity_name]))
			{
				// Check foreign row
	
				
				// Create conditions
				
				$link_foreign_key_values = array();
				
				foreach ($foreign_field_pairs as $field_pair)
				{
					$foreign_field_name = $field_pair[0]->getName();
					$link_foreign_field_name = $field_pair[1]->getName();
					
					if (! isset($data[$local_entity_name]['depends'][$foreign_entity_name][$foreign_field_name]))
						throw new DMLinkException("'$foreign_entity_name' entity item referred to, but '$foreign_field_name' key field value not provided on insert into entity '$local_entity_name'.");
					
					$value = $data[$local_entity_name]['depends'][$foreign_entity_name][$foreign_field_name];
					
					$foreign_key_values[$foreign_field_name] = $value;
					$link_foreign_key_values[$link_foreign_field_name] = $value;
				}
				
				
				// Check row count
				
				$c = $foreign_entity->ORMTable()->getRowCount($foreign_key_values);
	
				// if ($c == 0) throw new DMLinkException("'$foreign_entity_name' entity item with key values (" . implode(', ', array_map(function($a1value, $a2value){return "$a1value: $a2value";}, array_keys($foreign_key_values), $foreign_key_values)) . ") and referred to as foreign was not found on insert into entity '$local_entity_name'.");
				if ($c == 0) throw new DMLinkException("'$foreign_entity_name' entity item with key values (" . implode_params($foreign_key_values, array(', ', ': ')) . ") and referred to as foreign was not found on insert into entity '$local_entity_name'.");
								
				
				// Check foreign row for free
				
				$c = $this->ormTable->getRowCount($link_foreign_key_values);
	
				if ($c > 0) throw new DMLinkException("'$foreign_entity_name' entity item with key values (" . implode_params($foreign_key_values, array(', ', ': ')) . ") and referred to as foreign can not be linked more than one time (already linked) on insert into entity '$local_entity_name'.");
			}
			
		
			// Add link if any
			
			if (! empty($foreign_key_values))
			{
				$link_values = array();
				
				foreach ($local_field_pairs as $field_pair)
				{
					$entity_field = $field_pair[0];
					$link_field = $field_pair[1];
					
					$link_values[$link_field->getName()] = $data[$local_entity_name]['values'][$entity_field->getName()];
				}
				
				foreach ($foreign_field_pairs as $field_pair)
				{
					$entity_field = $field_pair[0];
					$link_field = $field_pair[1];
					
					$link_values[$link_field->getName()] = $foreign_key_values[$entity_field->getName()];
				}
	
	
				$this->ormTable->ins($link_values);
			}
		}
		else
		{
			// Check upon required

			if ($local_entity_required && ! $foreign_entity_required)
			{
				// Check dependency if any
				
				$foreign_key_values = array();
					
				if (isset($data[$local_entity_name]['depends']) && isset($data[$local_entity_name]['depends'][$foreign_entity_name]))
				{
					// Check foreign row
		
					
					// Create condition
					
					$local_key_values = array(); // for future development; now equals to $foreign_key_values
					
					foreach ($field_pairs as $field_name => $field_pair)
					{
						if (! isset($data[$local_entity_name]['depends'][$foreign_entity_name][$field_name]))
							throw new DMLinkException("'$foreign_entity_name' entity item referred to, but '$field_name' key field value not provided on insert into entity '$local_entity_name'.");
						
						$value = $data[$local_entity_name]['depends'][$foreign_entity_name][$field_name];
						
						$foreign_key_values[$field_name] = $value;
						$local_key_values[$field_name] = $value;
					}
					
					
					// Check row count
					
					$c = $foreign_entity->ORMTable()->getRowCount($foreign_key_values);
		
					if ($c == 0) throw new DMLinkException("'$foreign_entity_name' entity item with key values (" . implode_params($foreign_key_values, array(', ', ': ')) . ") and referred to as foreign was not found on insert into entity '$local_entity_name'.");


					// Check foreign row for free
					
					$c = $local_entity->ORMTable()->getRowCount($local_key_values);
		
					if ($c > 0) throw new DMLinkException("'$foreign_entity_name' entity item with key values (" . implode_params($foreign_key_values, array(', ', ': ')) . ") and referred to as foreign can not be linked more than one time (already linked) on insert into entity '$local_entity_name'.");
				}
				
				
				// Double insert if required
				if (empty($foreign_key_values))
				{
					// Double insert required (new foreign item)
					
					if (! isset($data[$foreign_entity_name]))
						throw new DMLinkException("Double insert required on insert into entity '$local_entity_name', but no data entry provided for entity '$foreign_entity_name'.");
					
					if (! isset($data[$foreign_entity_name]['values']))
						throw new DMLinkException("Double insert required on insert into entity '$local_entity_name', but no values provided for entity '$foreign_entity_name'.");
	
					
					// Insert data for dependent item
					$foreign_key_values = $foreign_entity->_insertItemData($data);
				}
				
				
				// Overwrite local primary key with foreign one
				foreach ($foreign_key_values as $field_name => $field_value)
				{
					$data[$local_entity_name]['values'][$field_name] = $field_value;
				}
			}
			elseif (! $local_entity_required && $foreign_entity_required)
			{
				// Just insert values in DMEntity, nothing to do here
			}
			elseif ($local_entity_required && $foreign_entity_required)
			{
				// Double insert required (new foreign)
				
				if (! isset($data[$foreign_entity_name]))
					throw new DMLinkException("Double insert required on insert into entity '$local_entity_name', but no data entry provided for entity '$foreign_entity_name'.");
				
				if (! isset($data[$foreign_entity_name]['values']))
					throw new DMLinkException("Double insert required on insert into entity '$local_entity_name', but no values provided for entity '$foreign_entity_name'.");

				
				// Insert data for dependent item
				$foreign_key_values = $foreign_entity->_insertItemData($data);
				
				
				// Overwrite local primary key with foreign one
				foreach ($foreign_key_values as $field_name => $field_value)
				{
					$data[$local_entity_name]['values'][$field_name] = $field_value;
				}
			}
		}
		
		
		return $data;
	}
	/*
	static public function createFromConfig(DMDataModel $dm, $config)
	{
		$custom_field_map = isset($config['custom_field_map']) ? $config['custom_field_map'] : NULL;
		
		$from_entity = $dm->entity($config['from']);
		$to_entity = $dm->entity($config['to']);
		
		
		$dmLink = new self($dm, $config['name'], $from_entity, $config['from_required'], $to_entity, $config['to_required'], $custom_field_map);
		
		
		return $dmLink;
	}
	*/
}

?>
