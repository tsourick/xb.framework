<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage mml
*/


Framework::useClass('ClassLoaderFactory');

/**
* MMLExporterInterface class
*
* @version 1.0
*/

interface MMLWriterInterface
{
	public function writeStart();
	
	public function writeShopInfo($data);
	public function writeCurrencies($list);
	public function writeCategories($list);
	
	public function writeOffersStart();
	public function writeOffer($data, $params = array(), $extra = array());
	public function writeOffersEnd();
	
	public function writeEnd();
}

class MMLWriterFactoryException extends Exception
{
}

class MMLWriterFactory extends ClassLoaderFactory
{
	protected static $loadedClasses = array();

	/**
	* Private constructor
	*/

	private function __construct()
	{
	}
	
	/**
	*
	*
	* @param string $engine_name ('Y'|'G') for yandex or google
	*
	* @return
	*/

	public static function create($engine_name, $file_path, $source_encoding = 'utf-8', $target_encoding = 'utf-8')
	{
		$class_name = ucfirst(substr($engine_name, 0, 1)) . 'MLWriter';
		
		
		static::loadClass($class_name, 'mml');
		
		
		return new $class_name($file_path, $source_encoding, $target_encoding);
	}
}
