<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
*/

/**
* SIMPLE_FORM class
*
* @version 1.0
*/


require_once('class.Form.php');

class SimpleForm extends Form
{
	var $cgi_trigger_name;
	var $form_handler;
	var $redirect_url;
	
	
	/**
	*
	*
	* & $cms, $field_groups, $cgi_trigger_name, $form_handler, $redirect_url
	*/

	function __construct($field_groups, $form_handler = NULL, $redirect_url = NULL, $cgi_trigger_name = 'check_trigger')
	{
		// Set trigger field into the form
		
		$trigger_group = array
		(
			'fields' => array
			(
				$cgi_trigger_name => array('type' => 'hidden', 'value' => 1, 'required' => true)
			)
		);
		
		array_unshift($field_groups, $trigger_group);

		
		parent :: __construct($field_groups);
		
		
		$this -> cgi_trigger_name = $cgi_trigger_name;
		$this -> form_handler = $form_handler;
		$this -> redirect_url = $redirect_url;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function check()
	{
		$r = false;


		// Get process trigger field value
		$cgi_trigger_value = get_cgi_value($this -> cgi_trigger_name, 'i', 0);
		

		if ($cgi_trigger_value == 1)
		{
			// Capture form
			
			if ($this -> capture())
			{
				$r = true;
			}
		}


		return $r;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function handle()
	{
		$r = true;
		
		if ($this->form_handler !== NULL)
		{
			$r = call_user_func($this -> form_handler, $this -> get_data());
		}

		if ($r && $this -> redirect_url !== NULL) xbf_http_redirect($this -> redirect_url);
		
		return $r;
	}
	
	
	/**
	*
	*
	* 
	*/

	public function process()
	{
		$r = true;


		// Get process trigger field value
		$cgi_trigger_value = get_cgi_value($this -> cgi_trigger_name, 'i', 0);
		

		$frm = array();
		
		if ($cgi_trigger_value == 1)
		{
			// Capture form
			
			if ($this -> capture())
			{
				// Process and redirect if successful
				
				if ($this->form_handler !== NULL)
				{
					$r = call_user_func($this -> form_handler, $this -> get_data());
				}
				
				if ($r && $this -> redirect_url !== NULL) xbf_http_redirect($this -> redirect_url);
			}
			else $frm = $this -> get_form();
		}
		else $frm = $this -> get_form();

		
		if ($r) $r = $frm;
		
		
		return $r;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function getRawData()
	{
		return parent::getData();
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function get_raw_data()
	{
		return $this->getRawData();
	}
	
	
	/**
	* Overrides derived get_data() method. Use get_raw_data() instead 
	*/
	
	public function getData()
	{
		$data = parent :: getData();

		unset($data[$this->cgi_trigger_name]);
		
		return $data;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function get_data()
	{
		return $this->getData();
	}
}

?>
