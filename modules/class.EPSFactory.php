<?php

/**
* XB.Framework PHP Framework
*
* Electronic payment system factory
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage tparser
*/


class EPSFactoryException extends Exception
{
}

class EPSFactory
{
	private static $loadedEngines = array();

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	private function __construct()
	{
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function createEPS($engine_name)
	{
		$class_name = 'EPS' . ucfirst($engine_name);
		
		
		// Load parser engine class
		if (! array_key_exists($engine_name, self::$loadedEngines))
		{
			// Check driver
			
			$engine_path = align_path(dirpath(dirname(realpath(__FILE__))) . 'eps/class.' . $class_name . '.php');

			if (! is_file($engine_path) || ! is_readable($engine_path)) throw new EPSException("EPS '{$engine_name}' is not a readable regular file at '{$engine_path}'");
			
			include_once($engine_path);

			self::$loadedEngines[$engine_name] = true;
		}
		
		
		// Create EPS

		$config = Framework::get($engine_name);

		$eps = new $class_name($config);
		
		
		return $eps;
	}
}

?>
