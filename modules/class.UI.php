<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
* @subpackage ui
*/

/**
* UI class
*
* @version 1.0
*/


class UI
{
	/**
	* Creates pager and sorting data for a list of data rows.
	*/
	
	/**
	* Returns structured data for list view.
	*
	* If second callback is NULL, first callback is called twice and the mode ('data' or 'count') is passed as a first parameter.
	*
	* Row count callback params:
	*   $params_in
	* 
	* Callback params:
	*   $limits = NULL, $sorting_field = '', $sorting_order = '', $params_in = array(), $params_out_tricky = array()
	*   OR
	*   $mode, $limits = NULL, $sorting_field = '', $sorting_order = '', $params_in = array(), $params_out_tricky = array()
	*
	* Callback can return arbitrary values through $params_out_ref (contains 'params_out' key to reference). The values will be stored under 'params' key
	* in the result. $params_out_ref is passed to all callbacks. This is responsibility of those callbacks to decide whether merge resulting values or not.
	* Reference is not passed directly for backward compatibility, because call_user_func() can not accept references.
	*
	* @param callback $get_rows_callback
  *	@param callback $get_row_count_callback
  *	@param callback $postprocess_callback
  *	@param array $cgi_vars names of cgi variables; defaults are array('page' => 'page', 'sfield' => 'sfield', 'sorder' => 'sorder')
  *	@param int $row_per_page number of rows per page
  *	@param array $params_in arbitrary parameters to pass into callbacks
	*/

	static public function list_view($callback, $get_row_count_callback = NULL, $postprocess_callback = NULL, $cgi_vars = array('page' => 'page', 'sfield' => 'sfield', 'sorder' => 'sorder'), $rows_per_page = 10, $params_in = array())
	{
		$R = array('items' => array(), 'pager' => array(), 'params' => array()); // params are out params
		
		
		$page = get_cgi_value($cgi_vars['page'], 'i');
		$sorting_field = get_cgi_value($cgi_vars['sfield']);
		$sorting_order = get_cgi_value($cgi_vars['sorder']);
		$sorting_order = get_set_value($sorting_order, array('ASC', 'DESC')); 


		$params_out = array();
		$params_out_ref = array('params_out' => &$params_out); // call_user_func() can not accept references; made for backward compatibility
		
		
		// Get row count
		
		if (! is_null($get_row_count_callback))
		{
			$row_count = call_user_func($get_row_count_callback, $params_in, $params_out_ref);
		}
		else
		{
			$row_count = call_user_func($callback, 'count', NULL, '', '', $params_in, $params_out_ref);
		}
		
		$R['params'] = $params_out;

		
		// Get row data		
		if ($row_count > 0)
		{
			$page_count = ceil($row_count / $rows_per_page);
			$page = (! empty($page) && $page > 0 && $page <= $page_count ) ? $page : 1;
			$limits = array($rows_per_page * ($page - 1), $rows_per_page);
			
			
			/*
			$first_page_row_count = 20;
			
			$page_count = 0;
			if ($row_count > 0)
			{
				$rest_row_count = $row_count - $first_page_row_count;
				$rest_row_count = $rest_row_count > 0 ? $rest_row_count : 0;
				
				$page_count = 1 + ceil($rest_row_count / $rows_per_page);
			}
		
			$page = (! empty($page) && $page > 0 && $page <= $page_count ) ? $page : 1;
			
			if ($page == 1)
			{
				$limits = array(0, $first_page_row_count);
			}
			else // assume $page > 1
			{
				$limits = array($first_page_row_count + $rows_per_page * ($page - 2), $rows_per_page);
			}
			*/
			

			if (! is_null($get_row_count_callback))
			{
				$rows = call_user_func($callback, $limits, $sorting_field, $sorting_order, $params_in, $params_out_ref);
			}
			else
			{
				$rows = call_user_func($callback, 'data', $limits, $sorting_field, $sorting_order, $params_in, $params_out_ref);
			}


			if (is_callable($postprocess_callback))
			{
				$rows = call_user_func($postprocess_callback, $rows, $params_in, $params_out_ref);
			}
			
			
			$pager_data = pager($page_count, $page, 3, 1, 3);

			$pager_data['on_page'] = count($rows);
			$pager_data['page_count'] = $page_count;
			$pager_data['row_count'] = $row_count;
			$pager_data['per_page'] = $rows_per_page;

			$pager_query = cgi_array_to_query(get_cgi_values_except(array($cgi_vars['page'])));
			$pager_data['query'] = ! empty($pager_query) ? $pager_query . '&amp;' : '';
			
			$pager_data['page'] = $page;


			$sorting_data = array();
			$sorting_data['field'] = $sorting_field;
			$sorting_data['order'] = $sorting_order;
			$sorting_data['order_toggle'] = ($sorting_order == 'DESC' ? 'ASC' : 'DESC');
			$sorting_data['query'] = cgi_array_to_query(get_cgi_values_except(array($cgi_vars['sfield'], $cgi_vars['sorder'], $cgi_vars['page'])));


			$R = array('items' => $rows, 'pager' => $pager_data, 'sorting' => $sorting_data, 'params' => $params_out);
		}
		

		return $R;
	}
}
?>
