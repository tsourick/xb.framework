<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
*/

/**
* undocumented class
*/

class FWDBTable
{
	private $connection;
	
	private $name;
	private $fields;
	private $primary_key;
	private $external_keys;
	
	function __construct($connection, $name, $fields)
	{
		$this->name = $name;
		
		$primary_key = array();
		$external_keys = array();
		
		foreach ($fields as $i => $field)
		{
			if (! empty($field['pri']))
			{
				$this->primary_key[] = $i;
			}
			
			if (! empty($field['ext']))
			{
				$this->external_keys[$field['ext']][] = $i;
			}
		}
		
		$this->fields = $fields;
	}
	
	public function getRows($fields, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->connection->getRows($fields, $this->name, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
	
	public function join($type, $table, $this_key, $those_key, $fields)
	{
	}
}

class FWDBVirtualView
{
	private $table;
	private $fields;
	private $condition;
	
	function __construct($table, $fields, $condition)
	{
		$this->table = $table;
	}

	public function getRows($fields, $condition = NULL, $group = '', $order = '', $limits = NULL, $row_key_field = '', $row_value_field = '')
	{
		return $this->connection->getRows($fields, $this->name, $condition, $group, $order, $limits, $row_key_field, $row_value_field);
	}
}

class FWDBTreeTable
{
}

$t_menu = new FWDBTable
(
	'am_menu_item',
	array
	(
		array('name' => 'id', 'type_def' => 'int(10) unsigned not null auto_increment', 'pri' => true),
		array('name' => 'title', 'type_def' => 'varchar(255) not null'),
		array('name' => 'site_page_id', 'type_def' => 'int(10) unsigned not null'),
		array('name' => 'href', 'type_def' => 'varchar(255) not null'),
		array('name' => 'item_id', 'type_def' => 'int(10) unsigned not null', 'ext' => 'parent_item'),
		array('name' => 'node_left_index', 'type_def' => 'int(10) unsigned not null'),
		array('name' => 'node_right_index', 'type_def' => 'int(10) unsigned not null'),
		array('name' => 'node_level', 'type_def' => 'int(10) unsigned not null'),
		array('name' => 'menu_id', 'type_def' => 'int(10) unsigned not null', 'ext' => 'parent_menu')
	)
);

$t_pages = new FWDBTable
(
	'site_page',
	array
	(
		array('name' => 'id', 'pri' => true),
		array('name' => 'title'),
		array('name' => 'alias'),
		array('name' => 'uri'),
		array('name' => 'site_page_template_id', 'ext' => 'template'),
		array('name' => 'site_page_id', 'ext' => 'parent_page'),
		array('name' => 'node_left_index'),
		array('name' => 'node_right_index'),
		array('name' => 'node_level')
	)
);

$t_menu_pages = $t_menu->join('LEFT OUTER', $t_pages, 'site_page_id', 'id', array('1.title, 1.href, '));

$vv = new FWDBVirtualView($t, array('id', 'title', ));
?>
