<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
*/

/**
* FORM class
*
* The class operates on a set of field groups which is organized as an associative array.
* To setup the form class instance you should make the field configuration array and pass
* it to set_fields() method. The unified field data structure is used both to setup fields
* and to return such information like errors occured upon checking of the user input.
* The field group structure looks like this:
* 
* array //group set
*	(
*		<group name> => array
*		(
*			'title' => <group title>,		// [in] for diplay purposes
*			'required' => [true|false],	// [in]
*			'error' => ['empty'],				// [out] set upon error
*			'fields' => <field set>			// [in]
*		),
*		
*		...
*	);
*
* array //field set
* (
*		<field name> => array
*		(
*			'title' => <field type>,		// [in] for display purposes
*			'type' => ['string','options','hidden'],
*																	// [in]
*			['options' => <options set>],
*																	// [in] for options type
*			'required' => [true|false],
*																	// [in]
*			'regexp' => <perl-compatible regular expressions without delimiters; | symbol is used as a delimiter>,
*																	// [in]
*			'value' => <cgi input value>,
*																	// [out]
*			'error' => ['empty'|'wrong'],
*																	// [out] set upon error
*			'hint' => <field hint>			// [in] for display purposes
*		),
*
*		...
*	);
*
*
* Parameters intended for display puprposes are optional. It is allowed to add arbitraty parameters
* to retain any useful data, especially for display puproses. The only restriction is to keep its
* names different from the predefined ones.
*
*	@version	2.0
*/


class Form
{
	var $field_groups = array();
	

	/**
	*
	*
	* @param array $field_groups grouped field set settings
	*/

	function __construct($field_groups = NULL)
	{
		if (! empty($field_groups)) $this -> set_fields($field_groups);
	}
	
	/**
	*
	*
	* @param array $field_groups grouped field set settings
	*/

	function set_fields($field_groups)
	{
		$this -> field_groups = $field_groups;
		
		
		// Init parameters
		reset($this -> field_groups);
		while (list($group_name, $group) = each($this -> field_groups))
		{
			reset($group['fields']);
			while (list($field_name) = each($group['fields']))
			{
				$f =& $this -> field_groups[$group_name]['fields'][$field_name];
				
				$f['name'] = $field_name;
				if (! isset($f['value'])) $f['value'] = NULL;
				if (! isset($f['default'])) $f['default'] = NULL;
				if (! isset($f['regexp'])) $f['regexp'] = '';
				if (! isset($f['required'])) $f['required'] = false;
				if (! isset($f['error'])) $f['error'] = '';
			}
			
			$g =& $this -> field_groups[$group_name];
			
			if (! isset($g['title'])) $g['title'] = '';
			if (! isset($g['required'])) $g['required'] = false;
			if (! isset($g['error'])) $g['error'] = '';
		}
	}
	
	/**
	*
	*
	* 
	*/

	function capture()
	{
		$r = true;
		
		
		reset($this -> field_groups);
		while (list($group_name, $group) = each($this -> field_groups))
		{
			$group_empty = true;
			
			
			reset($group['fields']);
			while (list($field_name, $field) = each($group['fields']))
			{
				$field_error = '';
				
				
				// Capture field value from cgi
				
				switch ($field['type'])
				{
					case 'info':
						continue 2;

					case 'optionssingle':
					case 'optionsmulti':
						$value = get_cgi_value($field_name, 'a');
					break;
					
					case 'date':
						if (isset($field['postfixes'])) $value = get_cgi_date($field_name, $field['default'], $field['postfixes']);
						else $value = get_cgi_date($field_name, $field['default']);
					break;

					case 'time':
						if (isset($field['postfixes'])) $value = get_cgi_time($field_name, $field['default'], $field['postfixes']);
						else $value = get_cgi_time($field_name, $field['default']);
					break;

					case 'datetime':
						if (isset($field['postfixes'])) $value = get_cgi_datetime($field_name, $field['default'], $field['postfixes']);
						else $value = get_cgi_datetime($field_name, $field['default']);
					break;
					
					case 'checkbox':
						$value = get_cgi_value($field_name, 'i', 0);
					break;
					
					case 'integer':
						if ($field['default'] !== NULL) $value = get_cgi_value($field_name, 'i', $field['default']);
						else $value = get_cgi_value($field_name, 'i');
					break;
					
					case 'file':
					case 'image':
						$value = NULL;

						if (isset($_FILES[$field_name]))
						{
							$file =& $_FILES[$field_name];
							
							if ($file['size'] > 0 && $file['error'] == UPLOAD_ERR_OK && is_uploaded_file($file['tmp_name']))
							{
								switch ($field['return'])
								{
									case 'content':
										$value = file_get_contents($file['tmp_name']);
									break;

									case 'path':
									default:
										$value = $file['tmp_name'];
								}
							}
						}
					break;
					
					default:
						if ($field['default'] !== NULL) $value = get_cgi_value($field_name, 's', $field['default']);
						else $value = get_cgi_value($field_name, 's');
				}

				
				// Check field for empty
				
				$field_empty = false;
				
				if ($field['type'] == 'string')
				{
					if (strlen($value) == 0) $field_empty = true;
				}
				else
				{
					if (empty($value)) $field_empty = true;
				}
				
/*
				// Compare password with re-typed value 
				
				$pwd_mismatch = false;
				
				if ($field['type'] == 'password')
				{
					$_value = get_cgi_value('_' . $field_name, 's');
					
					if (strcmp($value, $_value) != 0) $pwd_mismatch = true;
				}
*/				
				// Check field value with regexp
				
				$field_wrong = false;

				if ($field['type'] != 'optionsmulti')
				{
					if (! $field_empty && ! empty($field['regexp']))
					{
						if (! preg_match('|' . $field['regexp'] . '|', $value)) $field_wrong = true;
					}
				}
				
				
				// Set field errors if exist
				if ($field['required'] && $field_empty)
				{
					$field_error = 'empty';
					
					
					$r = false;
				}
				elseif ($field_wrong)
				{
					$field_error = 'wrong';


					$r = false;
				}
				/*
				elseif ($pwd_mismatch)
				{
					$field_error = 'mismatch';


					$r = false;
				}
				*/
				
				// Set field data & error
				$f =& $this -> field_groups[$group_name]['fields'][$field_name];
				
				$f['value'] = $value;
				$f['error'] = $field_error;


				// Reset group_empty if any field is not empty
				if ($group_empty && ! $field_empty) $group_empty = false;
			}
			
			
			if ($group['required'] && $group_empty)
			{
				$this -> field_groups[$group_name]['error'] = 'empty';					
				

				$r = false;
			}
		}


		return $r;
	}
	
	/**
	* Get data of particuar fields
	*
	* @return array field=>value pairs
	*/

	public function getData()
	{
		$data = array();
		
		
		reset($this -> field_groups);
		while (list(, $group) = each($this -> field_groups))
		{
			reset($group['fields']);
			while (list($field_name, $field) = each($group['fields']))
			{
				if ($field['type'] != 'info') $data[$field_name] = $field['value'];
			}
		}


		return $data;
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function get_data()
	{
		return $this->getData();
	}

	/**
	* Set data for particuar fields
	*
	* @param array $data field=>value pairs
	*/

	public function setData($data)
	{
		// Set value parameter for every field
		reset($this -> field_groups);
		while (list($group_name, $group) = each($this -> field_groups))
		{
			reset($group['fields']);
			while (list($field_name) = each($group['fields']))
			{
				if (isset($data[$field_name])) $this -> field_groups[$group_name]['fields'][$field_name]['value'] = $data[$field_name];
			}
		}
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function set_data($data)
	{
		$this->setData($data);
	}
	
	/**
	*
	*
	* @param string $name name of field to retrieve
	* @return mixed value of a field (after capture of explicit set) or NULL
	*/

	public function getValue($name)
	{
		$value = NULL;
		
		
		reset($this -> field_groups);
		while (list(, $group) = each($this -> field_groups))
		{
			reset($group['fields']);
			while (list($field_name, $field) = each($group['fields']))
			{
				if ($field_name == $name)
				{
					$value = $field['value'];
					break 2;
				}
			}
		}

		
		return $value;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function get_value($name)
	{
		return $this->getValue($name);
	}

	/**
	*
	*
	* @return array grouped field set with all settings, data, statuses etc. set, if captured, or just settings otherwise
	*/

	public function getForm()
	{
		return $this->field_groups;
	}

	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function get_form()
	{
		return $this->getForm();
	}
}

?>
