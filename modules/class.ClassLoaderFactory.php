<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
*/


class ClassLoaderFactoryException extends Exception
{
}

class ClassLoaderFactory
{
	protected static $loadedClasses = array(); // MUST BE REDEFINED IN DERIVED CLASSES


	/**
	* Private constructor
	*/

	private function __construct()
	{
	}
	
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public static function loadClass($class_name, $subdir = '')
	{
		// TODO: can be substituted with Framework::useClass
		
		// Load parser engine class
		if (! array_key_exists($class_name, static::$loadedClasses))
		{
			// Check driver
			
			$modules_dir = dirpath(dirname(realpath(__FILE__)));
			
			$class_path = align_path($modules_dir . ($subdir ? dirpath($subdir) : '') . 'class.' . $class_name . '.php');

			if (! is_file($class_path) || ! is_readable($class_path)) throw new ClassLoaderFactoryException("Class '{$class_name}' is not a readable regular file at '{$class_path}'");
			
			include_once($class_path);

			static::$loadedClasses[$class_name] = true;
		}
	}
}
