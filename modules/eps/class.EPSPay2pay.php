<?php

/**
* XB.CMS Content Management System
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2014
*/


Framework::useClass('EPS', 'eps');


/**
*
* Config params:
* - merchant_id: required
* - secret_key: required
* - default_currency: optional
* - default_language: optional
*/

class EPSPay2pay extends EPS
{
	private $lastRequestData; // unparsed request
	private $lastPaymentData; // parsed normalized request
	private $lastError; // last error message


	/**
	*
	* @param array $params
	* - currency: optional, default is 'default_curency' config value
	*/
	
	public function makePaymentUrl($order_id, $sum, $description, $params = array())
	{
		$url = 'https://merchant.pay2pay.com/?page=init';
		

		$c = $this->config;

		
		$currency = array_get_value($params, 'currency', array_get_value($c, 'default_currency'));
		$language = array_get_value($params, 'language', array_get_value($c, 'default_language'));

		
		$_merchant_id = $c['merchant_id'];
		$secret_key = $c['secret_key'];
		$_language = $language;
		$_order_id = $order_id;
		$_amount = $sum;
		$_currency = ($currency == 'RUR' ? 'RUB' : $currency);
		$_description = $description;
		$_test_mode = $c['test'];
		

		$xml = "
		<?xml version=\"1.0\" encoding=\"UTF-8\"?>
			<request>
				<version>1.2</version>
				<merchant_id>{$_merchant_id}</merchant_id>
				<language>{$_language}</language>
				<order_id>{$_order_id}</order_id>
				<amount>{$_amount}</amount>
				<currency>{$_currency}</currency>
				<description>{$_description}</description>
				<test_mode>{$_test_mode}</test_mode>
			</request>
		";
		// dump($xml);
		$xml = trim($xml);

		// Calc hash
		$sign = md5($secret_key . $xml . $secret_key);

		// Encode (base64)
		$xml = base64_encode($xml);
		$sign = base64_encode($sign);


		$params = array
		(
			'xml' => $xml,
			'sign' => $sign
		);
		
		$url .= '&' . cgi_array_to_query($params, '&');
		

		return $url;
	}

	
	public function validateRequest()
	{
		$valid = true;
		

		try
		{
			$c = $this->config;
			
			
			$xml = get_cgi_value('xml'); 
			$sign = get_cgi_value('sign'); 
	
			
			$this->lastRequestData = compact('xml', 'sign');
			
			
			if (is_null($xml) || is_null($sign)) throw new EPSException("no CGI values given");
			
			
			$xml = str_replace(' ', '+', $xml);
			$xml = base64_decode($xml);
	
			$sign = str_replace(' ', '+', $sign);
			$sign = base64_decode($sign);
	
			$hash = md5($c['hidden_key'] . $xml . $c['hidden_key']);
			

			if ($sign != $hash) throw new EPSException("sign mismatch");
			dump('sign:' . $sign);
			dump('xml:' . $xml);
			// $this->lastPaymentData = simplexml_load_string($xml);
			$xml = simplexml_load_string($xml);
			$json = json_encode($xml);
			$this->lastPaymentData = json_decode($json, true);

		}
		catch (EPSException $e)
		{
			$valid = false;
			
			$msg = $e->getMessage();
			
			$this->setLastError($msg);
			
			$this->sendResponse(false, $msg);
		}
		
		
		return $valid;
/*
		
		
		
		
		
		if (isset($_REQUEST['xml']) and isset($_REQUEST['sign']))
		// Если не получили параметры xml и sign, то нечего проверять
		{
			// Инициализация переменной для хранения сообщения об ошибке
			$error = '';
			// Декодируем входные параметры
			$xml = base64_decode(str_replace(' ', '+', $_REQUEST['xml']));
			$sign = base64_decode(str_replace(' ', '+', $_REQUEST['sign']));
			// преобразуем входной xml в удобный для использования формат
			$vars = simplexml_load_string($xml);
			
			if ($vars->order_id)
			// Если поле order_id не заполнено, продолжать нет смысла.
			// Т.к. информацию о заказе не получили и не сможем проверить
			// корректность остальных параметров
			// А также если тип сообщения не result
			{
				// Получаем информацию о заказе из БД
				$order = new Order($vars->order_id);
				if ($order)
				// Если не нашли заказ с указанным номером, то возвращаем ошибку
				{
					// Загружаем настройки для работы с Pay2Pay
					$pmconfigs = GetConfigs_Pay2Pay();
					if ($order->order_status == $pmconfigs['trans_status_pending'])
					// Если по заказу ожидается оплата, то продолжаем проверку
					{
						$s = md5($pmconfigs['hidden_key'] . $xml . $pmconfigs['hidden_key']);
						
						if ($sign == $s)
						// Если подпись не совпадает, возвращаем ошибку
						{
						
							$currency = $order->currency_code_iso;
						
							// Код валюты заказа преобразуем к формату принятом в Pay2Pay
							if ($currency == 'RUR') $currency = 'RUB';
							
							if (strtoupper($currency) == $vars->currency)
							if (strtoupper($currency) == $vars->currency)
							// Нельзя принимать платеж, если валюта платежа и заказа не совпали
							{
								if ($order->order_total <= $vars->amount)
								// Сумма платежа может превышать сумму заказа.
								// Например, в случае оплаты через системы денежных переводов
								{
									if (($vars->status == 'success') or ($vars->status == 'fail'))
									// Если статус платежа окончательный, то нужно обновить заказ
									{
										// Выбираем какой статус установить для заказа
										$status = $pmconfigs['trans_status_failed'];
										if ($vars->status == 'success')
										$status = $pmconfigs['trans_status_success'];
										// Устанавливаем статус заказа
										$order->status = $status;
										$order->save();
									}
								}
								else 	
								$error = 'Amount check failed';
							}
							else
							$error = 'Currency check failed';
						}
						else
						$error = 'Security check failed';
					}
				}
				else
				$error = 'Unknown order_id';
			}
			else
			$error = 'Incorrect order_id';
			
			// Отвечаем серверу Pay2Pay
			if ($error == '')
			$ret = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
			<result>
			<status>yes</status>
			<err_msg></err_msg>
			</result>";
			else
			$ret = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
			<result>
			<status>no</status>
			<err_msg>$error</err_msg>
			</result>";
			die($ret);		
		}
		
		*/
	}
	
	public function sendResponse($status, $error_message)
	{
		$values = array
		(
			'status' => ($status ? 'yes' : 'no'),
			'error_message' => $error_message
		);
		
		$template = '<?xml version="1.0" encoding="UTF-8"?>
		<result>
			<status>{$status}</status>
			<err_msg>{$error_message}</err_msg>
		</result>';
			
		$contents = parseTemplate($template, $values);
		dump('response: ' . $contents);
		echo $contents;
	}
	/*
	public function getSuccessResponse()
	{
		$inv_id = $_REQUEST["InvId"];
		
		return "OK$inv_id\n";
	}
	
	public function getFailureResponse()
	{
		return "bad sign\n";
	}
	*/

	public function getRequestData()
	{
		return $this->lastRequestData;
	}	

	public function getPaymentData()
	{
		return $this->lastPaymentData;
	}
	

	public function getPaymentStatus()
	{
		$status = NULL;
		
		if (! empty($this->lastPaymentData) && isset($this->lastPaymentData['status']))
		{
			$status = $this->lastPaymentData['status'];
		}
		
		return $status;
	}
	
	
	public function orderPaid()
	{
		return ($this->getPaymentStatus() == 'success');
	}
	
	public function orderCancelled()
	{
		return ($this->getPaymentStatus() == 'fail');
	}
	
	
	private function setLastError($msg)
	{
		$this->lastError = $msg;
	}
	
	public function getLastError()
	{
		return $this->lastError;
	}
}

?>