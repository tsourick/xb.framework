<?php

/**
* XB.CMS Content Management System
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*/


class EPSException extends Exception
{
}


/**
* Electronic payment system base class
*/

abstract class EPS
{
	protected $config;

	
	public function __construct($config)
	{
		$this->config = $config;
	}
	
	abstract public function makePaymentUrl($order_id, $sum, $description, $params = array());
	abstract public function validateRequest();
	// abstract public function getFailureData();
	// abstract public function getPaymentData();
}

?>
