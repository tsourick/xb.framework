<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* @package modules
*/

class XBZipArchive extends ZipArchive
{
	/**
	*
	*
	* @param
	* @param
	*
	* @return
	*/

	public function addDir($base_dir_path, $base_local_dir_path = '')
	{
		$base_dir_path = dirpath(realpath($base_dir_path));
		
		
		if (! empty($base_local_dir_path))
		{
			$base_local_dir_path = dirpath($base_local_dir_path, '/');
		}
		else
		{
			if ($base_dir_path != DIRECTORY_SEPARATOR)
			{
				$p = explode(DIRECTORY_SEPARATOR, $base_dir_path);
				$base_local_dir_path = dirpath($p[count($p) - 2], '/');
			}
			else
			{
				$base_local_dir_path = '/';
			}
		}

		
		if ($base_local_dir_path != '/')
		{
			$this->addEmptyDir($base_local_dir_path);
		}
		
		
		if ($d = opendir($base_dir_path))
		{
			while (false !== ($name = readdir($d)))
			{
				if ($name != '.' && $name != '..')
				{
					$path = $base_dir_path . $name;
					if (is_dir($path))
					{
						$dir_path = dirpath($path);
						$dir_local_path = ($base_local_dir_path != '/' ? $base_local_dir_path : '') . $name;
						
						$this->addDir($dir_path, $dir_local_path);
					}
					else
					{
						$file_path = $path;
						$file_local_path = $base_local_dir_path . $name;

						$this->addFile($file_path, $file_local_path);
					}
				}
			}
			closedir($d);
		}
	}
}

?>
