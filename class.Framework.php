<?php

/**
* XB.Framework PHP Framework
*
* @author Roman Tsourick <tsourick@gmail.com>
* @copyright Roman Tsourick 2011
*
* Framework initialization file, the only file to include to use the framework
*
* {@internal Framework::init() is called at the end of file}}
*
* @package framework
*/


/**
* Helper exception (try ... catch ... finally ...)
*/

class FinallyException extends Exception
{
}


/**
* Class exception
*/

class FrameworkException extends Exception
{
}


/**
* Framework initializer and config holder class
*
* Includes base libraries and sets up paths to modules for inclusion without path.
* Holds framework config and lets set/get its value.
* Static class.
*/

class Framework
{
	// private static $instance;
	
	/**
	* @internal
	* @var array config array holder
	*/
	
	private static $config;

	
	/**
	* @internal
	* @var string current working dir cached on init()
	*/
	
	private static $cwd;

	
	/**
	* @internal Hidden constructor, object of this class may not be instantiated
	*/

	private function __construct()
	{
	}


	/**
	* Returns dir of the framework
	*/
	
	public static function getCwd()
	{
		return self::$cwd;
	}


	/**
	* Includes core libraries and modifies include path so that require/include may be used without path
	*/

	public static function init()
	{
		ini_set('include_path', '.'); // Prevent alien scripts from including while searching for files to include
		
		
		$cwd = dirname(realpath(__FILE__)) . DIRECTORY_SEPARATOR;
		
		$cd = $cwd . 'core/';
		require_once($cd . 'lib.logger.php');
		require_once($cd . 'lib.debug.php');
		require_once($cd . 'lib.cli.php');
		require_once($cd . 'lib.tool.array.php');
		require_once($cd . 'lib.tool.general.php');
		require_once($cd . 'lib.tool.weboutput.php');
		require_once($cd . 'lib.tool.widget.php');
		require_once($cd . 'lib.tool.http.php');
		require_once($cd . 'lib.tool.date.php');
		require_once($cd . 'lib.tool.filesystem.php');

		// require_once($cd . 'legacy.lib.tool.http.php');
		

		append_include_path($cwd . 'modules');
		
		
		self::$cwd = $cwd;
	}		
	
	/**
	* Sets internal config to array provided
	*
	* framework/install_dir is set up to dir path of this file
	*
	* @param array $config associative array of config values
	*/

	public static function setConfig($config)
	{
		$config['framework']['install_dir'] = dirpath(dirname(realpath(__FILE__)));
		
		
		self::$config = $config;
	}
	
	/**
	* Returns config value, config section or the whole config as array
	*
	* @param string|NULL $path path to config secton or value or NULL to get the hole config array
	* @param bool $required if true (default) an exception is raised if no value found within the config
	* @param mixed $default_value default value to return if not found within config
	*
	* @return mixed
	*/

	public static function get($path = NULL, $required = true, $default_value = NULL)
	{
		$value = $default_value;
		
		
		if (is_null($path))
		{
			// Return all the options
			
			$value = self::$config;
		}
		else
		{
			// Return requested section or option
			
			if (strpos($path, '/') === false)
			{
				$section_name = $path;
				$option_name = '';
			}
			else
			{
				list($section_name, $option_name) = explode('/', $path);
			}
	
			if (! array_key_exists($section_name, self::$config))
			{
				if ($required) throw new FrameworkException("Section '{$section_name}' not found");
			}
			else
			{
				if (! empty($option_name))
				{
					if (! array_key_exists($option_name, self::$config[$section_name]))
					{
						if ($required) throw new FrameworkException("Option '{$option_name}' not found in section '{$section_name}'");
					}
					else
					{
						$value = self::$config[$section_name][$option_name];
					}
				}
				else $value = self::$config[$section_name];
			}
		}
		
		
		return $value;
	}
	
	
	/**
	* Sets config value or config section values as a whole
	*
	* @param string $path path to value or section
	* @param string|array $value config value or array of section values
	*/

	public static function set($path, $value)
	{
		if (strpos($path, '/') === false)
		{
			$section_name = $path;
			$option_name = '';
		}
		else
		{
			list($section_name, $option_name) = explode('/', $path);
		}
		
		if (empty($section_name)) throw new FrameworkException("Section name is empty");

		if (empty($option_name))
		{
			if (! is_array($value)) throw new FrameworkException("Value passed for section is not an array");
			
			self::$config[$section_name] = $value;
		}
		else
		{
			if (! array_key_exists($section_name, self::$config))
			{
				self::$config[$section_name] = array();
			}

			self::$config[$section_name][$option_name] = $value;
		}
	}
	
	
	/**
	* Includes the class source for further use
	*
	* Uses require_once() and absolute path to include the file from 'modules' directory.
	* Assumes that the file name is 'class.<class name>.php' and substitutes <class name> with the string given.
	*
	* @param string $class_name middle part of the file name
	* @param string $dir_rel_path path to the file dir relative to the framework modules dir
	*/
	
	public static function useClass($class_name, $dir_rel_path = '')
	{
		if (! empty($dir_rel_path))
		{
			$dir_rel_path = dirpath($dir_rel_path);
		}
		
		require_once self::$cwd . 'modules' . DIRECTORY_SEPARATOR . $dir_rel_path . 'class.' . $class_name . '.php';
	}
}

Framework::init();
?>
